//
//  ShareViewController.h
//  engageShareExt
//
//  Created by Maksym Savisko on 11/29/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ShareViewController : SLComposeServiceViewController

@end
