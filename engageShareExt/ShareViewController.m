//
//  ShareViewController.m
//  engageShareExt
//
//  Created by Maksym Savisko on 11/29/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ShareViewController.h"

#import "RequestManager.h"
#import "KeychainManager.h"
#import "UserDefaultsManager.h"

@import MobileCoreServices;

@interface ShareViewController ()

/**
 Return YES if user Successfully authorized in Request Manager. Default NO
 */
@property BOOL isAuthorize;

/**
 Return YES if user trying to share correct URL link
 */
@property BOOL isUrlSharing;

/**
 First Object That Was Share
 */
@property (nonatomic, strong) NSExtensionItem *inputItem;

/**
 Absolute URL String, if we trying share link.
 */
@property (nonatomic, strong) NSString *urlString;

/**
 URL Description if it Exist.
 */
@property (nonatomic, strong) NSString *urlDescription;

@end


@implementation ShareViewController

#pragma mark - Life Cycle

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.urlDescription = self.contentText;
    self.textView.text = @"";
    
    self.inputItem = self.extensionContext.inputItems.firstObject;
    
    NSItemProvider *urlItemProvider = [[self.inputItem.userInfo valueForKey:NSExtensionItemAttachmentsKey] objectAtIndex:0];
    
    if ([urlItemProvider hasItemConformingToTypeIdentifier:(__bridge NSString *)kUTTypeURL]) {
        self.isUrlSharing = YES;
        [urlItemProvider loadItemForTypeIdentifier:(__bridge NSString *)kUTTypeURL options:nil completionHandler:^(NSURL *url, NSError *error) {
            self.urlString = url.absoluteString;
        }];
    }
}

#pragma mark - ShareViewController Methods

- (void)presentationAnimationDidFinish {
    
    if (!self.isUrlSharing) {
        [self showAlertWithText:NSLocalizedString(@"For this moment you can share only with a links from the web", @"v1.0")];
        return;
    }
    
    [self reloadConfigurationItems];
    
    self.placeholder = NSLocalizedString(@"Add description...", @"v1.0");
    
    
    NSString *token = [KeychainManager apiToken];
    NSString *email = [KeychainManager userEmail];
    BOOL isTokenExist = [UserDefaultsManager isApiTokenExist];
    
    //Check that app do not deleted, because if it delete, token will exist in keychain store.
    //But user default app group will be deleted, when all app in group will be deleted.
    if (!isTokenExist) {
        [self showAlertWithText:NSLocalizedString(@"Please, login to the app", @"v1.0")];
        return;
    }
    
    //Check for valid token in Keychain.
    //If token or email exist - Show alert for Login and close posting view.
    if (token == nil || email == nil || token.length == 0 || email.length == 0) {
        [self showAlertWithText:NSLocalizedString(@"Please, login to the app", @"v1.0")];
        return;
    }
    
    NSString *companyPlane = [UserDefaultsManager companyPlane];
    
    if (![companyPlane isEqualToString:@"premium"]) {
        [self showAlertWithText:NSLocalizedString(@"This functionality is not available in your plan", @"v1.0")];
        return;
    }
    
    [[RequestManager sharedManager] setApiToken:token email:email];
    self.isAuthorize = YES;
}

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    
    if (self.urlString.length == 0 && self.isViewLoaded) {
        return NO;
    }
    
    return YES;
}

- (void)didSelectPost {
    
    // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    if (self.isUrlSharing && self.isAuthorize) {
        [[RequestManager sharedManager] createLearningWithInfo:[self serializeForSend] success:^(NSDictionary *result) {
            [self dismiss];
        } failure:^(NSString *message) {
            [self dismiss];
        }];
    }
    else {
        [self dismiss];
    }
    
    // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
    //[self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
}

- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    
    SLComposeSheetConfigurationItem *descriptionItem = [[SLComposeSheetConfigurationItem alloc] init];
    
    if (self.urlDescription) {
        descriptionItem.title = self.urlDescription;
        descriptionItem.valuePending = NO;
    }
    else {
        descriptionItem.title = NSLocalizedString(@"Loading description..", @"v1.0");
        descriptionItem.valuePending = YES;
    }
    
    SLComposeSheetConfigurationItem *urlItem = [[SLComposeSheetConfigurationItem alloc] init];
    
    if (self.urlString) {
        urlItem.title = self.urlString;
        urlItem.valuePending = NO;
    }
    else {
        urlItem.title = NSLocalizedString(@"Loading URL..", @"v1.0");
        urlItem.valuePending = YES;
    }
    
    return @[descriptionItem, urlItem];
}

#pragma mark - Action

-(void) dismiss {
    [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
}

#pragma mark - Helpers Methods

- (NSDictionary *) serializeForSend {
    
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    [result setValue:self.urlString forKey:@"site_url"];
    
    if (self.textView.text.length > 0) {
        [result setValue:self.textView.text forKey:@"comment"];
        [result setValue:@"real_life" forKey:@"ltype"];
    }
    else {
        [result setValue:@"reading" forKey:@"ltype"];
    }
    
    return result;
}

- (void) showAlertWithText:(NSString *) text {
    
    if (text.length == 0) {
        return;
    }
    
    UIAlertController * alert= [UIAlertController
                                alertControllerWithTitle:@"Share"
                                message:text
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
    {
        [self.extensionContext completeRequestReturningItems:nil completionHandler:nil];
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
