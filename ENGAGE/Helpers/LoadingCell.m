//
//  LoadingCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 15.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "LoadingCell.h"

static NSInteger kLoadingCellTag = 2223;



@implementation UITableViewCell (LoadingCell)

-(BOOL) isLoadingCell {
    return (self.tag == kLoadingCellTag);
}

@end






@implementation LoadingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(LoadingCell *) cell {
    LoadingCell *cell = [[LoadingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    CGRect rect = cell.bounds;
    rect.origin.y = 20;
    rect.size.height = 20;
    UIView *content = [[UIView alloc] initWithFrame:rect];
    content.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    content.backgroundColor = [UIColor clearColor];
    [cell addSubview:content];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = content.center;
    activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    activityIndicator.color = [UIColor appTextColor];
    [content addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    cell.tag = kLoadingCellTag;
    
    return cell;
}

@end
