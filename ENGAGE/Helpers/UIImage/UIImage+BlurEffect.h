//
//  UIImage+BlurEffect.h
//  Evinci
//
//  Created by Alex Kravchenko on 01.06.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (BlurEffect)

- (UIImage *)blurredImageWithRadius:(CGFloat)radius iterations:(NSUInteger)iterations tintColor:(UIColor *)tintColor;

@end
