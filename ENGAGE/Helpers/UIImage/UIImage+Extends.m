//
//  UIImage+Extends.m
//  Evinci
//
//  Created by Alex Kravchenko on 02.06.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "UIImage+Extends.h"

@implementation UIImage (Extends)

#pragma mark Color

- (UIImage *)imageWithColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextClipToMask(context, rect, self.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark Size

+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeight: (CGFloat) newHeight
{
    CGFloat oldHeight = sourceImage.size.height;
    CGFloat scaleFactor = newHeight / oldHeight;
    
    CGFloat newWidth = sourceImage.size.width * scaleFactor;
    
    return [self imageWithImage:sourceImage scaledToSize:CGSizeMake(newWidth, newHeight)];
}

+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (CGFloat) newWidth
{
    CGFloat oldWidth = sourceImage.size.width;
    CGFloat scaleFactor = newWidth / oldWidth;
    
    CGFloat newHeight = sourceImage.size.height * scaleFactor;
    
    return [self imageWithImage:sourceImage scaledToSize:CGSizeMake(newWidth, newHeight)];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (NSData *)dataWithImage:(UIImage *)image {
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    return imageData;
}

@end
