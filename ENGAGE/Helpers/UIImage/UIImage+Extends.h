//
//  UIImage+Extends.h
//  Evinci
//
//  Created by Alex Kravchenko on 02.06.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extends)

- (UIImage *)imageWithColor:(UIColor *)color;


+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeight: (CGFloat) newHeight;
+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (CGFloat) newWidth;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

+ (NSData *)dataWithImage:(UIImage *)image;

@end
