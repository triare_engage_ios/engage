//
//  NSDate+TimeAgo.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 09.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "NSDate+TimeAgo.h"

#define SECOND  1
#define MINUTE  (SECOND * 60)
#define HOUR    (MINUTE * 60)
#define DAY     (HOUR   * 24)
#define WEEK    (DAY    * 7)
#define MONTH   (DAY    * 31)
#define YEAR    (DAY    * 365.24)



@implementation NSDate (TimeAgo)


- (NSString *)formattedAsTimeAgo
{

    NSDate *now = [NSDate date];
    NSTimeInterval secondsSince = -(int)[self timeIntervalSinceDate:now];
    

    if(secondsSince < 0)
        return NSLocalizedString(@"future", @"v1.0");
    
    
    if(secondsSince < MINUTE)
        return @"now";
    

    if(secondsSince < HOUR) {
        int minutesSince = (int)secondsSince / MINUTE;
        return [NSString stringWithFormat:@"%d min", minutesSince];
    }
    
    if(secondsSince < DAY) {
        int hoursSince = (int)secondsSince / HOUR;
        return [NSString stringWithFormat:@"%d h", hoursSince];
    }
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setTimeStyle:NO];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    return [dateFormatter stringFromDate:self];
  
}





@end
