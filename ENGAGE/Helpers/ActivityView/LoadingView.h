//
//  LoadingView.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActivityView.h"

@interface LoadingView : UIView

@property (nonatomic, strong) ActivityView *activity;

@end
