//
//  UIViewController+LoadingView.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "UIViewController+LoadingView.h"

#define LOADING_VIEW_TAG 94713475

@implementation UIViewController (LoadingView)


-(LoadingView *) loadingView {
    return [self.view viewWithTag:LOADING_VIEW_TAG];
}

-(void) initLoadinView:(LoadingViewStyle) style {
    
    if ([self loadingView] == nil) {
        LoadingView *view = [[LoadingView alloc] initWithFrame:self.view.bounds];
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        view.tag = LOADING_VIEW_TAG;
        [self.view addSubview:view];
        
        if (style == LoadingViewStyleCoverContent) {
            view.backgroundColor = [UIColor colorWithWhite:1 alpha:.8];
            view.activity.iconsColor = [UIColor appGrayColor];
        }
    }
}

-(void) showLoadingViewBellowView:(UIView *) topView style:(LoadingViewStyle) style {
    [self initLoadinView:style];
    
    if (topView) {
        LoadingView *view = [self loadingView];
        [view removeFromSuperview];
        [self.view insertSubview:view belowSubview:topView];
    }
    
    
    if (![[self loadingView].activity isAnimating]) {
        [[self loadingView].activity start];
    }
}

-(void) showLoadingViewBellowView:(UIView *) topView {
    [self showLoadingViewBellowView:topView style:LoadingViewStyleLight];
}

-(void) hideLoadingView {
    [[self loadingView] removeFromSuperview];
}

-(void) resumeLoadingView {
    [[self loadingView].activity resume];
}


@end
