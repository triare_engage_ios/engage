//
//  LoadingView.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self customInit];
}


- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        [self customInit];
    }
    return self;
}

-(void) customInit {

    self.activity = [[ActivityView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
    self.activity.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    self.activity.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    [self addSubview:self.activity];
}

@end
