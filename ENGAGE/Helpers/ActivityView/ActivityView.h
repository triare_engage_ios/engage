//
//  ActivityView.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityView : UIView

@property (nonatomic, strong) UIColor *iconsColor;

-(BOOL) isAnimating;

-(void) start;
-(void) resume;
-(void) stop;

@end
