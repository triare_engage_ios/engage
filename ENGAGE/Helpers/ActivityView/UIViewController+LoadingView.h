//
//  UIViewController+LoadingView.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingView.h"

typedef NS_ENUM(NSInteger, LoadingViewStyle) {
    LoadingViewStyleLight = 0,
    LoadingViewStyleCoverContent
};

@interface UIViewController (LoadingView)

-(void) showLoadingViewBellowView:(UIView *) topView style:(LoadingViewStyle) style;
-(void) showLoadingViewBellowView:(UIView *) topView;
-(void) hideLoadingView;

-(void) resumeLoadingView;

@end
