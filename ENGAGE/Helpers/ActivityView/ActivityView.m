//
//  ActivityView.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ActivityView.h"
#import "UIImage+Extends.h"

@interface ActivityView ()

@property (nonatomic, strong) UIImageView *imageView1;
@property (nonatomic, strong) UIImageView *imageView2;

@end

@implementation ActivityView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self customInit];
}


- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        [self customInit];
    }
    return self;
}

-(void) customInit {
    
    self.imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
    self.imageView1.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    self.imageView1.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    self.imageView1.image = [UIImage imageNamed:@"activityIcon1"];
    [self addSubview:self.imageView1];
    self.imageView1.hidden = YES;
    
    self.imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 90, 90)];
    self.imageView2.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    self.imageView2.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    self.imageView2.image = [UIImage imageNamed:@"activityIcon2"];
    [self addSubview:self.imageView2];
    self.imageView2.hidden = YES;
}

-(void) start {
    self.imageView1.hidden = NO;
    self.imageView2.hidden = NO;
    
    CABasicAnimation * rotation2 = [self ratatingAnimation];
    rotation2.duration = .15;
    [self.imageView2.layer addAnimation:rotation2 forKey:@"rotateImage"];
}

-(void) resume {
    if ([self isAnimating]) {
        [self start];
    }
}

-(void) stop {
    self.imageView1.hidden = YES;
    self.imageView2.hidden = YES;
    
    [self.imageView1.layer removeAllAnimations];
    [self.imageView2.layer removeAllAnimations];
}

-(CABasicAnimation *) ratatingAnimation {
    CABasicAnimation * rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotation.duration = .2;
    rotation.toValue = @(M_PI * 2.0 * rotation.duration);
    rotation.cumulative = YES;
    rotation.repeatCount = MAXFLOAT;
    
    return rotation;
}

-(BOOL) isAnimating {
    return !self.imageView2.hidden && [self.imageView2.layer animationForKey:@"rotateImage"];
}

-(void)setIconsColor:(UIColor *)iconsColor {
    _iconsColor = iconsColor;
    
    self.imageView1.image = [[UIImage imageNamed:@"activityIcon1"] imageWithColor:self.iconsColor];
    self.imageView2.image = [[UIImage imageNamed:@"activityIcon2"] imageWithColor:self.iconsColor];
}

@end
