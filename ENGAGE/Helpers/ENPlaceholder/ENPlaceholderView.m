//
//  ENPlaceholderView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/27/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENPlaceholderView.h"

@interface ENPlaceholderView ()

@property (nonatomic, strong) UILabel *placeholderTitle;


@end

@implementation ENPlaceholderView

+ (ENPlaceholderView *) createPlaceholderWithText:(NSString *) text andFont:(UIFont *) textFont {
    
    CGFloat width = SCREEN_WIDTH - 30;
    CGFloat height = [ENPlaceholderView heightOfPlaceholder:text andFont:textFont];
    
    ENPlaceholderView *placeholder = [[ENPlaceholderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, height + 30)];
    placeholder.backgroundColor = [UIColor clearColor];
    
    placeholder.placeholderTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, width, height)];
    placeholder.placeholderTitle.text = text;
    placeholder.placeholderTitle.textAlignment = NSTextAlignmentLeft;
    placeholder.placeholderTitle.numberOfLines = 0;
    placeholder.placeholderTitle.font = textFont;
    placeholder.placeholderTitle.textColor = [UIColor appTextColor];
    [placeholder addSubview:placeholder.placeholderTitle];
    
    return placeholder;
}

+ (ENPlaceholderView *) createCenterPlaceholderWithText:(NSString *) text andFond:(UIFont *) textFont {
    
    CGFloat height = [ENPlaceholderView heightOfPlaceholder:text andFont:textFont andWidth:SCREEN_WIDTH - 20];
    CGFloat width = SCREEN_WIDTH - 20;
    
    ENPlaceholderView *placeholder = [[ENPlaceholderView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - EN_NAVIGATION_BAR_HEIGHT - EN_NAVIGATION_TAB_BAR_HEIGHT)];
    placeholder.backgroundColor = [UIColor clearColor];
    
    placeholder.placeholderTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, placeholder.frame.size.height/2 - height/2, width, height)];
    placeholder.placeholderTitle.text = text;
    placeholder.placeholderTitle.textAlignment = NSTextAlignmentCenter;
    placeholder.placeholderTitle.numberOfLines = 0;
    placeholder.placeholderTitle.font = textFont;
    placeholder.placeholderTitle.textColor = [UIColor appTextColor];
    [placeholder addSubview:placeholder.placeholderTitle];
    
    return placeholder;
}

+ (CGFloat) heightOfPlaceholder:(NSString *)text andFont:(UIFont *) textFont {
 
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = textFont;
    l.text = text;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(SCREEN_WIDTH - 40, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    return height;
}

+ (CGFloat) heightOfPlaceholder:(NSString *)text andFont:(UIFont *) textFont andWidth:(CGFloat) width {
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = textFont;
    l.text = text;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    return height;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
