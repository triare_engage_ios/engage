//
//  ENPlaceholderView.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/27/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENPlaceholderView : UIView

+ (ENPlaceholderView *) createPlaceholderWithText:(NSString *) text andFont:(UIFont *) textFont;
+ (ENPlaceholderView *) createCenterPlaceholderWithText:(NSString *) text andFond:(UIFont *) textFont;

@end
