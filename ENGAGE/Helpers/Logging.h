//
//  Logging.h
//
//  Created by Alex Kravchenko on 18.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"


#define MLOG_ENABLED 1




#define MLOG_CMD [NSString stringWithFormat:@"%s [Line %d]", __PRETTY_FUNCTION__, __LINE__]
#define MLOG_CLASS NSStringFromClass([self class])

#define MLog(format, ...)  AKLog(MLOG_CMD, MLOG_CLASS, format, ##__VA_ARGS__)
#define MLogI(integer)  MLog(@"%d", integer)
#define MLogF(float)  MLog(@"%f", float)
#define MLogO(object)  MLog(@"%@", object)

#define MLogAlert(format, ...)  AKLogAlert(MLOG_CLASS, format, ##__VA_ARGS__)


static NSString *loggingTime() {
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
    
    return [dateFormat stringFromDate:today];
}

static inline void AKLog(NSString *cmd, NSString *className, NSString *format, ...) {
    
    //className can be used for filter log
    
    if (MLOG_ENABLED == 0) return;
    
    printf("%s ", [loggingTime() UTF8String]);
    printf("%s: ", [cmd UTF8String]);

    va_list args;
    va_start(args, format);
    NSString *formattedString = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    printf("%s ", [formattedString UTF8String]);
    printf("\n");
}


static inline void AKLogAlert(NSString *className, NSString *format, ...) {
    
    //className can be used for filter log
    
    if (MLOG_ENABLED == 0) return;
    
    va_list args;
    va_start(args, format);
    NSString *formattedString = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    NSString *text = [NSString stringWithFormat:@"%@:\n%@",className, formattedString];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"INFO" message:text preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
#if SHAREEXT
#else
    UIViewController *rootController = [[(AppDelegate *)[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    [rootController presentViewController:alertController animated:YES completion:nil];
#endif
}
