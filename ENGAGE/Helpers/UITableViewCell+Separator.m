//
//  TableViewCell+Separator.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 17.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "UITableViewCell+Separator.h"

#define SEPARATOR_TAG 998799

@implementation UITableViewCell (Separator)

- (void)showSeparatorLine {
    [self showSeparatorLine:YES];
}

- (void)showSeparatorLine:(BOOL)showLine {
    
    UIView *separator = [self.contentView viewWithTag:SEPARATOR_TAG];
    
    if (separator == nil)
    {
        separator = [[UIView alloc] initWithFrame:CGRectMake(15, self.contentView.frame.size.height - 1, self.contentView.frame.size.width - 30, 1)];
        separator.backgroundColor = [UIColor colorWithWhite:1 alpha:.5];
        separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        separator.tag = SEPARATOR_TAG;
        [self.contentView addSubview:separator];
    }
    
    separator.hidden = !showLine;
}

@end
