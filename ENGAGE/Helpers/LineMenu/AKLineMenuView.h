//
//  AKLineMenuView.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 19.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AKLineMenuView;
@protocol AKLineMenuViewDelegate <NSObject>
@optional
- (void)lineMenu:(AKLineMenuView *) menu didSelectItem:(NSInteger)itemIndex;
@end

@interface AKLineMenuView : UIView

@property (nonatomic, weak) id <AKLineMenuViewDelegate>  delegate;
@property NSInteger selectedIndex;

-(void) addMenuItems:(NSArray *) items;
-(void) selectItemAtIndex:(NSInteger) index animate:(BOOL) animate;

@end
