//
//  AKLineMenuView.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 19.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "AKLineMenuView.h"


#define BUTTONS_TAG 100
#define ANIMATION_DURATION .2

@interface AKLineMenuView ()

@property (nonatomic, strong) UIView *buttonsView;
@property (nonatomic, strong) UIView *lineView;


@end



@implementation AKLineMenuView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.lineView.backgroundColor = [UIColor appOrangeColor];
}

-(void) commonInit {
    //TODO: Find way how to fix it for self.bounds
    CGRect lineMenu = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    self.frame = lineMenu;
    
    self.buttonsView = [[UIView alloc] initWithFrame:lineMenu];
    self.buttonsView.backgroundColor = [UIColor clearColor];
    self.buttonsView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:self.buttonsView];
    
    self.lineView = [[UIView alloc] initWithFrame:CGRectMake(0, lineMenu.size.height - 3, 100, 3)];
    self.lineView.backgroundColor = [UIColor blackColor];
    [self addSubview:self.lineView];
    
    self.selectedIndex = 0;
}

-(instancetype)init {
    if (self = [super init]) {
        [self commonInit];
    }
    
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    
    return self;
}


-(void)layoutSubviews {
    [super layoutSubviews];
    
    //TODO: Find way how to fix it for self.buttonsView.frame.size.width
    //CGFloat buttonWidth = self.buttonsView.frame.size.width / self.buttonsView.subviews.count;
    CGRect buttonViewRect = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    CGFloat buttonWidth = buttonViewRect.size.width / self.buttonsView.subviews.count;
    
    for (NSInteger i = 0; i < self.buttonsView.subviews.count; i++) {
        UIButton *button = [self.buttonsView viewWithTag:BUTTONS_TAG + i];
        button.frame = CGRectMake(buttonWidth * i, 0, buttonWidth, buttonViewRect.size.height);
    }
    
    [self moveLineToIndex:self.selectedIndex animated:NO];
}







-(void) addMenuItems:(NSArray *) items {
    
    self.selectedIndex = 0;
    
    for (UIView *v in self.buttonsView.subviews) {
        [v removeFromSuperview];
    }
    
    for (NSInteger i = 0; i < items.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, 100, 30);
        button.tag = BUTTONS_TAG + i;
        [button addTarget:self action:@selector(didSelect:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[items objectAtIndex:i] forState:UIControlStateNormal];
        
        [button setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont appFont:14];
        
        [self.buttonsView addSubview:button];
    }
    
    [self selectItemAtIndex:0 animate:NO];
}

-(void) didSelect:(UIButton *) button {
    
    NSInteger index = button.tag - BUTTONS_TAG;
    
    if (index == self.selectedIndex) return;
    
    [self selectItemAtIndex:index animate:YES];
    
    if ([self.delegate respondsToSelector:@selector(lineMenu:didSelectItem:)]) {
        [self.delegate lineMenu:self didSelectItem:self.selectedIndex];
    }
}

-(void) moveLineToIndex:(NSInteger) index animated:(BOOL) animated {
    
    //TODO: Find way to fix this
    //CGFloat buttonWidth = self.buttonsView.frame.size.width / self.buttonsView.subviews.count;
    CGRect buttonViewRect = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    CGFloat buttonWidth = buttonViewRect.size.width / self.buttonsView.subviews.count;
    CGRect lineFrame = CGRectMake(0, buttonViewRect.size.height - 3, 100, 3);
    
    //CGRect lFrame = self.lineView.frame;
    CGRect lFrame = lineFrame;
    lFrame.size.width = buttonWidth;
    lFrame.origin.x = buttonViewRect.origin.x + (buttonWidth * index);
    
    if (animated) {
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.lineView.frame = lFrame;
        }];
    } else {
        self.lineView.frame = lFrame;
    }
}

-(void) selectItemAtIndex:(NSInteger) index animate:(BOOL) animate {
    
    if (index >= self.buttonsView.subviews.count) {
        return;
    }
    
    [self changeButtonColor:index animate:animate];
    self.selectedIndex = index;
    [self moveLineToIndex:index animated:animate];
}

-(void) changeButtonColor:(NSInteger) index animate:(BOOL) animate {
    
    UIButton *selectedButton = [self.buttonsView viewWithTag:self.selectedIndex + BUTTONS_TAG];
    UIButton *buttonForSelect = [self.buttonsView viewWithTag:index + BUTTONS_TAG];
    
    if (animate) {
        [UIView transitionWithView:selectedButton duration:ANIMATION_DURATION options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [selectedButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
        } completion:nil];
        
        [UIView transitionWithView:buttonForSelect duration:ANIMATION_DURATION options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [buttonForSelect setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
        } completion:nil];
        
    } else {
        [selectedButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
        [buttonForSelect setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    }
    
    
}

@end
