//
//  ContentViewController.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ContentViewController.h"

@implementation ContentViewController

-(void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void) addBackgroundImage {
    if (self.backgroundImageView == nil) {
        self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        self.backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:self.backgroundImageView];
        [self.view sendSubviewToBack:self.backgroundImageView];
    }
}

-(void) refreshBackgroundImage {
    self.backgroundImageView.image = [[DataManager userCompany] background];
}

//Update background image from URL
-(void) refreshBackgroundImageWithURL:(NSURL *) url {
    [self.backgroundImageView sd_setImageWithURL:url placeholderImage:[[DataManager user].company background]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addBackgroundImage];
    [self refreshBackgroundImage];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.registerKeyboardNotifications) [self addKeyboardNotification];
    
    [self resumeLoadingView];
}


-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.registerKeyboardNotifications) [self removeKeyboardNotification];
}

#pragma mark Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {

}

- (void)keyboardWillHide:(NSNotification *)notification {
    
}

@end
