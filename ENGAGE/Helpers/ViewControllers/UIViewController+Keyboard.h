//
//  UIViewController+Keyboard.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Keyboard)

-(void) addKeyboardNotification;
-(void) removeKeyboardNotification;

- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardWillHide:(NSNotification *)notification;
- (void)keyboardDidShow:(NSNotification *)notification;

@end
