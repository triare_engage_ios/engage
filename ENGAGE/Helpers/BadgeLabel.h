//
//  BadgeLabel.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 26.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BadgeLabel : UILabel

@property (nonatomic, strong) NSNumber *isNeedResizeBadge;
@property CGFloat badgeHeight;

-(void) resizeWidth;

@end
