//
//  AKApplication.m
//
//
//  Created by Alex Kravchenko on 16.06.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "AKApplication.h"

@implementation AKApplication

+(NSString *) bundleId {
    return [[NSBundle mainBundle] bundleIdentifier];
}

+(NSString *) appVersion {
    return [NSString stringWithFormat:@"%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
}

@end
