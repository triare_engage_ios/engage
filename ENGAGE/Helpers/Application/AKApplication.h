//
//  AKApplication.h
//  
//
//  Created by Alex Kravchenko on 16.06.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AKApplication : NSObject

+(NSString *) bundleId;
+(NSString *) appVersion;

@end
