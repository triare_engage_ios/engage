//
//  SFSafariManager.m
//  ENGAGE
//
//  Created by Maksym Savisko on 1/6/17.
//  Copyright © 2017 Maksym Savisko. All rights reserved.
//

#import "SFSafariManager.h"

@implementation SFSafariManager

+ (void)openURLWithSafariController:(NSString*)stringUrl forController:(UIViewController <SFSafariViewControllerDelegate> *) vc
{
    NSURL *URL = [NSURL URLWithString:stringUrl];
    

        if ([SFSafariViewController class] != nil && [vc conformsToProtocol:@protocol(SFSafariViewControllerDelegate)])
        {
            SFSafariViewController *sfvc = [[SFSafariViewController alloc] initWithURL:URL];
            sfvc.delegate = (UIViewController <SFSafariViewControllerDelegate> *) vc;
            [vc presentViewController:sfvc animated:YES completion:nil];
        }
        else
        {
            if (![[UIApplication sharedApplication] openURL:URL])
            {
                MLog(@"%@%@",@"Failed to open url:", [URL description]);
            }
        }

}

@end
