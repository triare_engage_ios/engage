//
//  NSDictionary+SecureValue.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 09.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "NSDictionary+SecureValue.h"

@implementation NSDictionary (SecureValue)

-(NSString *) secureStringForKey:(NSString *) keyValue andDefault:(NSString *) defaultValue {
    id value = [self valueForKeyPath:keyValue];
    if ([value isKindOfClass:[NSString class]]) {
        return value;
    }
    
    return defaultValue;
}

-(NSString *) secureStringForKey:(NSString *) keyValue {
    return [self secureStringForKey:keyValue andDefault:nil];
}

-(NSNumber *) secureNumberForKey:(NSString *) keyValue andDefault:(NSNumber *) defaultValue {
    id value = [self valueForKeyPath:keyValue];
    if ([value isKindOfClass:[NSNumber class]]) {
        return value;
    }
    
    return defaultValue;
}

-(NSNumber *) secureNumberForKey:(NSString *) keyValue {
    return [self secureNumberForKey:keyValue andDefault:nil];
}

+ (BOOL) secureSetIn:(NSDictionary*)dico withValue:(id)object forKey:(NSString*)key {
    if (object == nil)
        return NO;
    [dico setValue:object forKey:key];
    return YES;
}

- (BOOL) isValueNotNullandNotNilForKey:(NSString*)key  {
    
    BOOL isNullorNil = NO;
    
    if ([[self valueForKey:key] isEqual:[NSNull null]]) {
        isNullorNil = YES;
    }
    
    if ([self valueForKey:key] == nil) {
        isNullorNil = YES;
    }
    
    return isNullorNil;
}

@end
