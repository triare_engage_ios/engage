//
//  NSDictionary+SecureValue.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 09.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (SecureValue)

-(NSString *) secureStringForKey:(NSString *) keyValue;
-(NSString *) secureStringForKey:(NSString *) keyValue andDefault:(NSString *) defaultValue;


-(NSNumber *) secureNumberForKey:(NSString *) keyValue;
-(NSNumber *) secureNumberForKey:(NSString *) keyValue andDefault:(NSNumber *) defaultValue;

+ (BOOL) secureSetIn:(NSDictionary*)dico withValue:(id)object forKey:(NSString*)key;

- (BOOL) isValueNotNullandNotNilForKey:(NSString*)key;

@end
