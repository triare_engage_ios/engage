//
//  ContentTableView.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 08.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ContentTableView.h"

@implementation ContentTableView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    UIEdgeInsets insets = self.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT;
    insets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.contentInset = insets;
    
    self.backgroundColor = [UIColor clearColor];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void) scrollTableToTop:(BOOL) animated {
    [self setContentOffset:CGPointMake(0, self.contentInset.top * -1) animated:animated];
}

@end
