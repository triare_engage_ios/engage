//
//  ContentScrollView.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentScrollView : UIScrollView

-(void) scrollTableToTop:(BOOL) animated;

@end
