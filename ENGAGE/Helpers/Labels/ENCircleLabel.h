//
//  ENCircleLabel.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/29/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENCircleLabel : UILabel

- (instancetype) initWithFrame:(CGRect) frame;

@end
