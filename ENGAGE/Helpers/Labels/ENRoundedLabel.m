//
//  ENRoundedLabel.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/1/16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ENRoundedLabel.h"

#define kDefaultCornerRadius self.frame.size.width / 2.0

@implementation ENRoundedLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        [self applyStyle];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyStyle];
}

-(void) applyStyle {
    self.font = [UIFont appFontBold:17];
    self.textColor = [UIColor blackColor];
}


- (void) layoutSubviews {
    [super layoutSubviews];
    
    if (self.cornerRadius) {
        self.layer.cornerRadius = self.cornerRadius;
    }
    else {
        self.layer.cornerRadius = kDefaultCornerRadius;
    }
    
    if (self.borderWidth) {
        self.layer.borderWidth = self.borderWidth;
    }
    
    if (self.borderColor) {
        self.layer.borderColor = self.borderColor.CGColor;
    }
    
    self.clipsToBounds = YES;
    
}



@end
