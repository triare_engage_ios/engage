//
//  ENRoundedLabel.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/1/16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENRoundedLabel : UILabel
@property (nonatomic) CGFloat cornerRadius; //default half of weight;
@property (nonatomic) CGFloat borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

- (instancetype) initWithFrame:(CGRect) frame;

@end
