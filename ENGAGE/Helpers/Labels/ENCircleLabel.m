//
//  ENCircleLabel.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/29/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENCircleLabel.h"

@implementation ENCircleLabel

- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        [self applyStyle];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyStyle];
}

-(void) applyStyle {
    self.font = [UIFont appFont:17];
    self.textColor = [UIColor appTextColor];
    self.textAlignment = NSTextAlignmentCenter;
    self.numberOfLines = 3;
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.layer.borderWidth = 3.0;
    self.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5].CGColor;
    self.clipsToBounds = YES;
}

@end
