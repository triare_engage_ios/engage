//
//  NSString+URLValidation.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "NSString+URLValidation.h"

@implementation NSString (URLValidation)

- (BOOL)isValidURL
{
    NSUInteger length = [self length];
    // Empty strings should return NO
    if (length > 0) {
        NSError *error = nil;
        NSDataDetector *dataDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
        if (dataDetector && !error) {
            NSRange range = NSMakeRange(0, length);
            NSRange notFoundRange = (NSRange){NSNotFound, 0};
            NSRange linkRange = [dataDetector rangeOfFirstMatchInString:self options:0 range:range];
            if (!NSEqualRanges(notFoundRange, linkRange) && NSEqualRanges(range, linkRange)) {
                return YES;
            }
        }
        else {
            MLog(@"Could not create link data detector: %@ %@", [error localizedDescription], [error userInfo]);
        }
    }
    
    return NO;
}

@end
