//
//  NSString+URLValidation.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLValidation)

- (BOOL)isValidURL;

@end
