//
//  AKFileManager.m
//
//  Created by Alex Kravchenko on 20.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "AKFileManager.h"

@implementation AKFileManager

+ (NSString *) documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = paths.firstObject;
    return basePath;
}

+ (NSString *) cachesDirectory
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString* cachePath = paths.firstObject;

    return cachePath;
}

+ (NSString *) temporaryDirectory
{
    return NSTemporaryDirectory();
}

+(NSString *) applicationFile:(NSString *) filename
{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
}


+(NSError *) createDirectoryIfNeed:(NSString *) path {
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:nil]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return error;
}


+(NSString *) companyDirectory {
    return [[AKFileManager documentsDirectory] stringByAppendingPathComponent:@"company"];
}



@end
