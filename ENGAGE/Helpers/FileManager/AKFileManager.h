//
//  AKFileManager.h
//
//  Created by Alex Kravchenko on 20.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AKFileManager : NSObject

+ (NSString *) documentsDirectory;
+ (NSString *) cachesDirectory;
//data will delete automatically by iOS
+ (NSString *) temporaryDirectory;

//return file from boundle
+(NSString *) applicationFile:(NSString *) filename;

+(NSError *) createDirectoryIfNeed:(NSString *) path;


+(NSString *) companyDirectory;

@end
