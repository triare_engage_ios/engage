//
//  LoadingCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 15.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UITableViewCell (LoadingCell)

-(BOOL) isLoadingCell;

@end


@interface LoadingCell : UITableViewCell

+(LoadingCell *) cell;

@end
