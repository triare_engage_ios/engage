//
//  BadgeLabel.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 26.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "BadgeLabel.h"

#define STANDART_LABEL_HEIGHT 24

@implementation BadgeLabel

-(void)awakeFromNib {
    [super awakeFromNib];
    
    if (self.isNeedResizeBadge == nil) {
        self.isNeedResizeBadge = @YES;
    }
    
    if (self.badgeHeight == 0) {
        self.badgeHeight = STANDART_LABEL_HEIGHT;
    }
    
    self.backgroundColor = [UIColor appOrangeColor];
    self.textColor = [UIColor appTextColor];
    self.textAlignment = NSTextAlignmentCenter;
}

-(void)setText:(NSString *)text {
    [super setText:text];
    
    if (self.isNeedResizeBadge.boolValue) {
        [self resizeBadge];
    }
}

-(CGFloat) badgeWidth {
    CGSize size = [self sizeThatFits:CGSizeZero];
    if (size.width < self.badgeHeight) {
        size.width = self.badgeHeight;
    }
    
    return size.width;
}

-(void) resizeBadge {
    CGFloat width = [self badgeWidth];
    
    self.layer.cornerRadius = self.badgeHeight/2;
    
    if (self.constraints.count) {
        NSLayoutConstraint *heightConstraint = [self constraintForAttribute:NSLayoutAttributeHeight];
        heightConstraint.constant = self.badgeHeight;
        
        NSLayoutConstraint *widthConstraint = [self constraintForAttribute:NSLayoutAttributeWidth];
        widthConstraint.constant = width;
    }
    
    if (self.superview.constraints.count) {
        NSLayoutConstraint *topConstraint = [self constraintInSuperviewForAttribute:NSLayoutAttributeTop];
        topConstraint.constant = self.badgeHeight/2 * -1;
        
        NSLayoutConstraint *trailingConstraint = [self constraintInSuperviewForAttribute:NSLayoutAttributeTrailing];
        trailingConstraint.constant = width/2;
    }
}

-(NSLayoutConstraint *) constraintForAttribute:(NSLayoutAttribute) layot {
    NSLayoutConstraint *neededConstraint;
    for (NSLayoutConstraint *constraint in self.constraints) {
        if (constraint.firstAttribute == layot) {
            neededConstraint = constraint;
            break;
        }
    }
    
    return neededConstraint;
}

-(NSLayoutConstraint *) constraintInSuperviewForAttribute:(NSLayoutAttribute) layot {
    NSLayoutConstraint *neededConstraint;
    for (NSLayoutConstraint *constraint in self.superview.constraints) {
        if (constraint.firstItem == self && constraint.firstAttribute == layot) {
            neededConstraint = constraint;
            break;
        }
    }
    
    return neededConstraint;
}

-(void) resizeWidth {

    if (self.constraints.count) {
        NSLayoutConstraint *widthConstraint = [self constraintForAttribute:NSLayoutAttributeWidth];
        widthConstraint.constant = [self badgeWidth];
    }
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.height/2;
    self.clipsToBounds = YES;
}

@end
