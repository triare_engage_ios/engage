//
//  ContentViewController.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Keyboard.h"
#import "UIViewController+LoadingView.h"
#import "ContentTableView.h"

@interface ContentViewController : UIViewController

@property BOOL registerKeyboardNotifications;

@property (nonatomic, strong) UIImageView *backgroundImageView;

@property BOOL isDataLoaded;



-(void) refreshBackgroundImage;

/*
 Update background image from URL
 Used in:
 - ChallengeInfoVC
 */
-(void) refreshBackgroundImageWithURL:(NSURL *) url;

@end
