//
//  UIDevice+Hardware.h
//  Evinci
//
//  Created by Alex Kravchenko on 26.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface UIDevice (Hardware)

+ (NSString *) platform;
+ (NSString *) platformString;

@end
