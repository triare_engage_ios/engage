//
//  ContentScrollView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentScrollView.h"

@implementation ContentScrollView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    UIEdgeInsets insets = self.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT;
    insets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.contentInset = insets;
    
    self.backgroundColor = [UIColor clearColor];
}

-(void) scrollTableToTop:(BOOL) animated {
    [self setContentOffset:CGPointMake(0, self.contentInset.top * -1) animated:animated];
}


@end
