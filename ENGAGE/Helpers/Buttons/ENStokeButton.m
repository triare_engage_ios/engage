//
//  EVStokeButton.m
//  Evinci
//
//  Created by Alex Kravchenko on 26.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ENStokeButton.h"

@implementation ENStokeButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyStyle];
}


- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        [self applyStyle];
    }
    return self;
}

-(void) applyStyle {
    
    self.layer.cornerRadius = 7;
    
    [self setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont appFontBold:12];
    
    self.backgroundColor = [UIColor clearColor];
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor appOrangeColor].CGColor;
    
    self.adjustsImageWhenHighlighted = NO;
}

-(void)setEnabled:(BOOL)enabled {
    
    if (enabled) {
        self.alpha = 1;
    } else {
        self.alpha = .6;
    }
    
    [super setEnabled:enabled];
}

-(void) refreshUI {
    [self applyStyle];
}

-(void)setBorderColor:(UIColor *)borderColor {
    _borderColor = borderColor;
    
    self.layer.borderColor = self.borderColor.CGColor;
}

-(void) setBorderColor:(UIColor *)borderColor width:(CGFloat) width {
    self.borderColor = borderColor;
    
    self.layer.borderWidth = width;
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.backgroundColor = [UIColor appOrangeColor];
    } else {
        [self setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
        self.backgroundColor = [UIColor whiteColor];
    }
}

@end
