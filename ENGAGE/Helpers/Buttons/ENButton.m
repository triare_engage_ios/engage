//
//  ENButton.m
//
//
//  Created by Alex Kravchenko on 25.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ENButton.h"

#define LEFT_PADDING 6

@interface ENButton ()
@property BOOL isNeedHideImage;

@end

@implementation ENButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyStyle];
}


- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        [self applyStyle];
    }
    return self;
}

-(void) applyStyle {
    
    self.layer.cornerRadius = 7;
    
    self.style = ENButtonStyleOrange;
    
    [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.titleLabel.font = [UIFont appFontBold:17];
}

-(void) setStyle:(ENButtonStyle)style {
    _style = style;
    
    switch (self.style) {
        case ENButtonStyleOrange:
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            self.backgroundColor = [UIColor appOrangeColor];
            break;
            
        case ENButtonStyleWhite:
            [self setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];

            self.backgroundColor = [UIColor whiteColor];
            break;
            
        default:
            break;
    }
}


-(void) layoutSubviews {
    [super layoutSubviews];
    
    if (self.isNeedHideImage) {
        return;
    }
    
    if (self.imageView.image) {
        //made image on left and center text
        CGRect iFrame = self.imageView.frame;
        iFrame.origin.x = LEFT_PADDING + self.imageEdgeInsets.left;
        self.imageView.frame = iFrame;
        
        CGRect tFrame = self.titleLabel.frame;
        tFrame.origin.x = LEFT_PADDING + iFrame.origin.x + iFrame.size.width + self.imageEdgeInsets.right + self.titleEdgeInsets.left;
        tFrame.size.width = self.frame.size.width - tFrame.origin.x - self.titleEdgeInsets.right - LEFT_PADDING;
        self.titleLabel.frame = tFrame;
    }
}

-(void)setEnabled:(BOOL)enabled {
    
    if (enabled) {
        self.alpha = 1;
    } else {
        self.alpha = .3;
    }
    
    [super setEnabled:enabled];
}

- (void) needHideImage:(BOOL)hide {
    _isNeedHideImage = hide;
    [self setImage:nil forState:UIControlStateNormal];
}

@end
