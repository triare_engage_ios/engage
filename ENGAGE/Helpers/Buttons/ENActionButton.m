//
//  ENActionButton.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/6/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENActionButton.h"

#define LEFT_PADDING 6

@implementation ENActionButton

- (void)awakeFromNib {
    [super awakeFromNib];
    [self applyStyle];
}

- (instancetype) initWithFrame:(CGRect) frame {
    if (self = [super initWithFrame:frame]) {
        [self applyStyle];
    }
    return self;
}

- (void) applyStyle {
    [self setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont appFontBold:17];
    
    self.layer.cornerRadius = 7;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor appTextColor].CGColor;

}

-(void) layoutSubviews {
    [super layoutSubviews];
    
    
    if (self.imageView.image) {
        //made image on left and center text
        CGRect iFrame = self.imageView.frame;
        iFrame.origin.x = LEFT_PADDING + self.imageEdgeInsets.left;
        self.imageView.frame = iFrame;
        
        CGRect tFrame = self.titleLabel.frame;
        tFrame.origin.x = LEFT_PADDING + iFrame.origin.x + iFrame.size.width + self.imageEdgeInsets.right + self.titleEdgeInsets.left;
        tFrame.size.width = self.frame.size.width - tFrame.origin.x - self.titleEdgeInsets.right - LEFT_PADDING;
        self.titleLabel.frame = tFrame;
    }
}

-(void)setEnabled:(BOOL)enabled {
    
    if (enabled) {
        self.alpha = 1;
    } else {
        self.alpha = .3;
    }
    
    [super setEnabled:enabled];
}

@end
