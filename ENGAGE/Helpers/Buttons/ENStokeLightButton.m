//
//  ENStokeLightButton.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 23.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ENStokeLightButton.h"

@implementation ENStokeLightButton

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setBorderColor:[UIColor appTextColor] width:1];
    [self setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    self.backgroundColor = [UIColor clearColor];
    
    if (self.selected) {
        [self setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    } else {
        [self setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    }
}

-(void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        [self setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
        self.layer.borderColor = [UIColor appOrangeColor].CGColor;
    } else {
        [self setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
        self.layer.borderColor = [UIColor appTextColor].CGColor;
    }
}

@end
