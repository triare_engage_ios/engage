//
//  ENButton.h
//
//
//  Created by Alex Kravchenko on 25.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ENButtonStyle) {
    ENButtonStyleOrange = 0,
    ENButtonStyleWhite = 1
};

@interface ENButton : UIButton

@property (nonatomic) ENButtonStyle style;

- (void) needHideImage:(BOOL)hide;

@end
