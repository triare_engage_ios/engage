//
//  ENStokeButton.h
//
//
//  Created by Alex Kravchenko on 26.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENStokeButton : UIButton

@property (nonatomic, strong) UIColor *borderColor;

-(void) setBorderColor:(UIColor *)borderColor width:(CGFloat) width;

-(void) refreshUI;

@end
