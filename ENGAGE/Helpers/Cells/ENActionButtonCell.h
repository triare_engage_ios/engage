//
//  ENActionButtonCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENButton.h"

@interface ENActionButtonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENButton *actionButton;
@property (nonatomic, copy) void (^onAction)(ENActionButtonCell *cell);

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

-(void)setActionButtonTitle:(NSString *)text;

@end
