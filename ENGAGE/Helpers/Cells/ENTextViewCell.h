//
//  ENTextViewCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPTextView.h"

@interface ENTextViewCell : UITableViewCell

/**
 Custom text view with placeholder and placeholder color
 */
@property (weak, nonatomic) IBOutlet MPTextView *textView;

/**
 Invoke when text should changed
 @return text, return text, that exist in textView
 @return cell, return link for current cell, used for understand IndexPath of cell
 */
@property (nonatomic, copy) void (^onChangeText)(NSString *text, ENTextViewCell *cell);

/**
 Invoke when text in text view did begin editing
 @return cell, link for current cell, used for understand IndexPath of cell
 */
@property (nonatomic, copy) void (^onDidBeginChangingText)(ENTextViewCell *cell);

/**
 IF YES - text view will not accept space as first symbol
 Default - NO.
 */
@property (nonatomic) BOOL isNeedValidateFirstSpace;

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

-(void) setTextViewPlaceholderText:(NSString *) text;
-(void) setTextViewPlaceholderColor:(UIColor *) color;

@end
