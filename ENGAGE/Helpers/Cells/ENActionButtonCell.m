//
//  ENActionButtonCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENActionButtonCell.h"

#define kDefaultCellHeight 70
#define kDefaultCellIdentifier @"enActionButtonCell"

@interface ENActionButtonCell ()


@end

@implementation ENActionButtonCell

#pragma mark - Public Methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

-(void)setActionButtonTitle:(NSString *)text {
    [self.actionButton setTitle:text forState:UIControlStateNormal];
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.onAction = nil;
    [self.actionButton setTitle:nil forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Action

- (IBAction)actionButtonPressed:(id)sender {
    
   __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onAction) {
        weakSelf.onAction (weakSelf);
    }
}


@end
