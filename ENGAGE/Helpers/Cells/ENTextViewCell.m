//
//  ENTextViewCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENTextViewCell.h"
#import "MPTextView.h"

#define kDefaultCellHeight 70
#define kDefaultCellIdentifier @"enTextViewCell"

@interface ENTextViewCell () <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *textViewBackground;

@end

@implementation ENTextViewCell

#pragma mark - Public Methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

-(void) setTextViewPlaceholderText:(NSString *) text {
    self.textView.placeholderText = text;
}

-(void) setTextViewPlaceholderColor:(UIColor *) color {
    self.textView.placeholderColor = color;
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.textView.font = [UIFont appFontThin:17];
    self.textView.textColor = [UIColor blackColor];
    
    self.textViewBackground.image = [[UIImage imageNamed:@"message-textBackground.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    self.textView.delegate = self;
    
    self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.textView.dataDetectorTypes = UIDataDetectorTypeNone;
    self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textView.spellCheckingType = UITextSpellCheckingTypeDefault;
    self.textView.keyboardType = UIKeyboardTypeDefault;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.onChangeText = nil;
    self.onDidBeginChangingText = nil;
    self.textView.text = nil;
    self.textView.placeholderText = nil;
    self.textView.placeholderColor = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - TextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    __weak typeof(self) weakSelf = self;
    
    NSString * newString = [[textView text] stringByReplacingCharactersInRange:range withString:text];
    
    //Validation for first symbol space
    if (self.isNeedValidateFirstSpace) {
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        if (textView.text.length == 0 && [newString stringByTrimmingCharactersInSet:set].length == 0) {
            return NO;
        }
    }
    
    if (weakSelf.onChangeText) {
        weakSelf.onChangeText (newString, weakSelf);
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onDidBeginChangingText) {
        weakSelf.onDidBeginChangingText (weakSelf);
    }
}


@end
