//
//  ENDropMenuCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENDropMenuCell.h"

#define kDefaultCellIdentifier @"dropMenuCell"
#define kDefaultCellHeight 50.0

@interface ENDropMenuCell ()
@property (weak, nonatomic) IBOutlet UILabel *itemTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end

@implementation ENDropMenuCell

#pragma mark - Public methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

- (void) addItem:(ENDropMenuItem *) item {
    self.itemTitleLabel.text = item.title;
}

- (void) needHideSeparatorLine:(BOOL) needHide {
    self.separatorView.hidden = needHide;
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.itemTitleLabel.font = [UIFont appFont:17];
    self.itemTitleLabel.textColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor clearColor];
    self.separatorView.backgroundColor = [UIColor appGrayColor];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    self.itemTitleLabel.text = nil;
    self.separatorView.hidden = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
