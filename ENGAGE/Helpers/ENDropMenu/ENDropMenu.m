//
//  ENDropMenu.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENDropMenu.h"
#import "ENDropMenuCell.h"

#define kDefaultCornerRadius 5.0

@interface ENDropMenu () <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) UITableView *tableView;

@end


@implementation ENDropMenu

#pragma mark - Initialization

- (instancetype) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //self.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor colorWithRed:0/255. green:0/255. blue:0/255. alpha:0.4];
        
        
        self.layer.shadowOffset = CGSizeMake(0, 0);
        self.layer.shadowRadius = 2;
        self.layer.shadowOpacity = 0.1;
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.tableView registerNib:[UINib nibWithNibName:[ENDropMenuCell cellNimbName] bundle:nil] forCellReuseIdentifier:[ENDropMenuCell cellReuseIdentifier]];
        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.scrollEnabled = NO;
        self.tableView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.tableView];
    }
    
    return self;
}

#pragma mark - Life Cycle

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = kDefaultCornerRadius;
    self.clipsToBounds = YES;
}

#pragma mark - Action

-(void) closeAction {
    if (self.onClose) {
        self.onClose();
        self.onClose = nil;
    }
}

#pragma mark - Public Methods

+(CGFloat) menuHeightWithItems:(NSArray *) items {
    return items.count * [ENDropMenuCell cellHeight]; //bottom height
}

#pragma mark - Properties

-(void) setItems:(NSArray *)items {
    _items = items;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate & Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [ENDropMenuCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ENDropMenuItem *item = [self.items objectAtIndex:indexPath.row];
    
    ENDropMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:[ENDropMenuCell cellReuseIdentifier]];
    
    [cell addItem:item];
    
    //Hide last separator line
    if (self.items.count == indexPath.row + 1) {
        [cell needHideSeparatorLine:YES];
    }
     
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ENDropMenuItem *item = [self.items objectAtIndex:indexPath.row];
    
    [self closeAction];
    
    if (item.onSelect) {
        item.onSelect(indexPath.row);
    }
}

@end
