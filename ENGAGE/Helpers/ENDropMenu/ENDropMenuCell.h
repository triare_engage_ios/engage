//
//  ENDropMenuCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENDropMenuItem.h"

@interface ENDropMenuCell : UITableViewCell

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;
- (void) needHideSeparatorLine:(BOOL) needHide;

- (void) addItem:(ENDropMenuItem *) item;

@end
