//
//  ENDropMenuItem.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    DropMenuItemTypeGiveATap,
    DropMenuItemTypeNewLearning,
    DropMenuItemTypeSuggestionToAdmin,
    DropMenuItemTypeEvaluate,
    DropMenuItemTypeActivityFeed,
    DropMenuItemTypeLearningList,
    DropMenuItemTypePropositionsList,
    DropMenuItemTypeNewQuestion,
    DropMenuItemTypeNewTeamMessage
} DropMenuItemType;

@interface ENDropMenuItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic) DropMenuItemType itemType;
@property (nonatomic, copy) void (^onSelect)(NSInteger index);

@end
