//
//  ENDropMenuHelper.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENDropMenuItem.h"
#import "ENDropMenu.h"

typedef enum : NSUInteger {
    DropMenuStyleBottom,
    DropMenuStyleTop
} DropMenuStyle;

typedef enum : NSUInteger {
    DropMenuScreenActivityFeed,
    DropMenuScreenLearningList,
    DropMenuScreenPropositionsList,
    DropMenuScreenProfile
} DropMenuScreen;

@interface ENDropMenuHelper : NSObject

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Items
+(NSArray *) itemsTypeForUser:(User *) user;
+(NSArray *) itemsTypeForScreen:(DropMenuScreen) screen;
+(NSString *) titleForItemType:(DropMenuItemType) itemType;
+(void) showViewControllerForItemType:(DropMenuItemType) itemType;

#pragma mark - Sizes
+(CGRect) startMenuFrameForView:(UIView *) parentView withHeight:(CGFloat) menuHeight style:(DropMenuStyle) style;
+(CGRect) endMenuFrameForView:(UIView *) parentView withHeight:(CGFloat) menuHeight style:(DropMenuStyle) style;

#pragma mark - Views
// ======================== Show view ========================= //
+(UIView *) backViewForView:(UIView *) parentView style:(DropMenuStyle) style;
+(UIVisualEffectView *) blurViewForView:(UIView *) parentView style:(DropMenuStyle) style;
+(UIButton *) backButtonView:(UIView *) parentView withTarget:(nullable id) target action:(nullable SEL)action style:(DropMenuStyle) style;
+(ENDropMenu *) dropMenuWithFrame:(CGRect) frameRect style:(DropMenuStyle) style;

// ========================= Hide view ========================= //
+(UIView *) backViewForHide:(UIView *) parentView style:(DropMenuStyle) style;
+(UIVisualEffectView *) blurViewForHide:(UIView *) parentView style:(DropMenuStyle) style;
+(ENDropMenu *) dropMenuViewForHide:(UIView *) parentView style:(DropMenuStyle) style;

#pragma mark - Notification

+(void) postNotificationForStyle:(DropMenuStyle) style;

NS_ASSUME_NONNULL_END

@end
