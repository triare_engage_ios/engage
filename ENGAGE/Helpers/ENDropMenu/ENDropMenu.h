//
//  ENDropMenu.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENDropMenuItem.h"

@interface ENDropMenu : UIView

@property (nonatomic, copy) void (^onClose)();
@property (nonatomic, strong) NSArray *items;

+(CGFloat) menuHeightWithItems:(NSArray *) items;

@end
