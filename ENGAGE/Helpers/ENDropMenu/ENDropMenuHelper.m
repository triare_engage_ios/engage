//
//  ENDropMenuHelper.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENDropMenuHelper.h"
#import "AppDelegate.h"
#import "AnalyticsManager.h"

#define kDefaultMenuWidth 200.0
#define kBackViewTag 9999
#define kBlurViewTag 9998
#define kDropMenuTag 9997

@implementation ENDropMenuHelper

#pragma mark - Items

+(NSArray *) itemsTypeForUser:(User *) user {
    
    
    if ([user.company.plan isEqualToString:@"premium"]) {
        
        if ([user isAdmin] || [user isManager]) {
            return @[@(DropMenuItemTypeNewTeamMessage), @(DropMenuItemTypeNewLearning), @(DropMenuItemTypeEvaluate), @(DropMenuItemTypeNewQuestion), @(DropMenuItemTypeGiveATap)];
        }
        else {
            return @[@(DropMenuItemTypeNewLearning), @(DropMenuItemTypeSuggestionToAdmin), @(DropMenuItemTypeEvaluate), @(DropMenuItemTypeGiveATap)];
        }
        
    }
    
    if ([user.company.plan isEqualToString:@"standart"]) {
        
        if ([user isAdmin] || [user isManager]) {
            return @[@(DropMenuItemTypeNewTeamMessage), @(DropMenuItemTypeEvaluate), @(DropMenuItemTypeGiveATap)];
        }
        else {
            return @[@(DropMenuItemTypeSuggestionToAdmin), @(DropMenuItemTypeEvaluate), @(DropMenuItemTypeGiveATap)];
        }
        
    }
    
    else {
        if ([user isAdmin] || [user isManager]) {
            return @[@(DropMenuItemTypeNewTeamMessage), @(DropMenuItemTypeGiveATap)];
        }
        else {
            return @[@(DropMenuItemTypeGiveATap)];
        }
    }

}

+(NSArray *) itemsTypeForScreen:(DropMenuScreen) screen {
    switch (screen) {
        case DropMenuScreenActivityFeed:
            return @[@(DropMenuItemTypeLearningList)];
            break;
        
        case DropMenuScreenLearningList:
            return @[@(DropMenuItemTypeActivityFeed)];
            break;
            
        case DropMenuScreenPropositionsList:
            return @[@(DropMenuItemTypeActivityFeed), @(DropMenuItemTypeLearningList)];
            break;
            
        case DropMenuScreenProfile:
            return @[@(DropMenuItemTypeNewLearning)];
    }
}

+(NSString *) titleForItemType:(DropMenuItemType) itemType {
    
    switch (itemType) {
        case DropMenuItemTypeGiveATap:
            return [NSLocalizedString(@"give a tap", @"v1.0") uppercaseString];
            break;
            
        case DropMenuItemTypeEvaluate:
            return [NSLocalizedString(@"evaluate", @"v1.0") uppercaseString];
            break;
            
        case DropMenuItemTypeNewLearning:
            return [NSLocalizedString(@"new learning", @"v1.0") uppercaseString];
            break;
            
        case DropMenuItemTypeSuggestionToAdmin:
            return [NSLocalizedString(@"suggestion", @"v1.0") uppercaseString];
            break;
        
        case DropMenuItemTypeActivityFeed:
            return [NSLocalizedString(@"activity feed", @"v1.0") uppercaseString];
            break;
            
        case DropMenuItemTypeLearningList:
            return [NSLocalizedString(@"learnings", @"v1.0") uppercaseString];
            break;
            
        case DropMenuItemTypePropositionsList:
            return [NSLocalizedString(@"propositions", @"v1.0") uppercaseString];
            break;
            
        case DropMenuItemTypeNewQuestion:
            return [NSLocalizedString(@"new question", @"v1.0") uppercaseString];
            break;
            
        case DropMenuItemTypeNewTeamMessage:
            return [NSLocalizedString(@"new team message", @"v1.0") uppercaseString];
            break;
    }
}

+(void) showViewControllerForItemType:(DropMenuItemType) itemType {
    
    if (itemType == DropMenuItemTypeGiveATap) {
        [[AppDelegate theApp].mainViewController showGiveTap];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionGiveATap label:ENAnalyticLabelOpen];
        return;
    }
    
    if (itemType == DropMenuItemTypeEvaluate) {
        [[AppDelegate theApp].mainViewController showEvaluate];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionEvaluate label:ENAnalyticLabelOpen];
        return;
    }
    
    if (itemType == DropMenuItemTypeNewLearning) {
        [[AppDelegate theApp].mainViewController showAddLearning];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionNewLearning label:ENAnalyticLabelOpen];
        return;
    }
    
    if (itemType == DropMenuItemTypeSuggestionToAdmin) {
        [[AppDelegate theApp].mainViewController presentGiveSuggestionToAdmin];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionSuggestionToAdmin label:ENAnalyticLabelOpen];
        return;
    }
    
    if (itemType == DropMenuItemTypeActivityFeed) {
        [[AppDelegate theApp].mainViewController showActivityFeed];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionActivityFeed label:ENAnalyticLabelOpen];
        return;
    }
    
    if (itemType == DropMenuItemTypeLearningList) {
        [[AppDelegate theApp].mainViewController showLearningListInActivityFeed];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionLearnings label:ENAnalyticLabelOpen];
        return;
    }
    
    if (itemType == DropMenuItemTypePropositionsList) {
        return;
    }
    
    if (itemType == DropMenuItemTypeNewQuestion) {
        [[AppDelegate theApp].mainViewController presentNewQuestion];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionNewQuestion label:ENAnalyticLabelOpen];
        return;
    }
    
    if (itemType == DropMenuItemTypeNewTeamMessage) {
        [[AppDelegate theApp].mainViewController presentNewTeamMessage];
        [AnalyticsManager trackEvent:ENAnalyticCategoryActionMenu action:ENAnalyticActionNewTeamMessage label:ENAnalyticLabelOpen];
        return;
    }
    
}

#pragma mark - Size

+(CGRect) startMenuFrameForView:(UIView *) parentView withHeight:(CGFloat) menuHeight style:(DropMenuStyle) style {
    
    switch (style) {
        case DropMenuStyleBottom:
            return CGRectMake(parentView.bounds.size.width / 2 - kDefaultMenuWidth / 2, parentView.bounds.size.height, kDefaultMenuWidth, menuHeight);
            break;
            
        case DropMenuStyleTop:
            return CGRectMake(parentView.bounds.size.width / 2 - kDefaultMenuWidth / 2, 0 - menuHeight - 5, kDefaultMenuWidth, menuHeight);
            break;
    }
}

+(CGRect) endMenuFrameForView:(UIView *) parentView withHeight:(CGFloat) menuHeight style:(DropMenuStyle) style {
    switch (style) {
        case DropMenuStyleBottom:
            return CGRectMake(parentView.bounds.size.width / 2 - kDefaultMenuWidth / 2, SCREEN_HEIGHT - EN_NAVIGATION_TAB_BAR_HEIGHT - 5 - menuHeight, kDefaultMenuWidth, menuHeight);
            break;
            
        case DropMenuStyleTop:
            return CGRectMake(parentView.bounds.size.width / 2 - kDefaultMenuWidth / 2, EN_NAVIGATION_BAR_HEIGHT + 5, kDefaultMenuWidth, menuHeight);
            break;
    }
}

#pragma mark - Views

+(UIView *) backViewForView:(UIView *) parentView style:(DropMenuStyle) style {
    //In current realization in all styles the same realization
    UIView *backView = [[UIView alloc] initWithFrame:parentView.bounds];
    backView.backgroundColor = [UIColor clearColor];
    backView.tag = kBackViewTag;
    return backView;
}

+(UIVisualEffectView *) blurViewForView:(UIView *) parentView style:(DropMenuStyle) style {
    return [self blurViewForView:parentView withEffectStyle:UIBlurEffectStyleLight style:style];
}

+(UIVisualEffectView *) blurViewForView:(UIView *) parentView withEffectStyle:(UIBlurEffectStyle) effectStyle style:(DropMenuStyle) style {
    
    CGRect rectFrame = CGRectZero;
    
    switch (style) {
        case DropMenuStyleBottom:
            rectFrame = CGRectMake(parentView.bounds.origin.x, parentView.bounds.origin.y, parentView.bounds.size.width, parentView.bounds.size.height - EN_NAVIGATION_TAB_BAR_HEIGHT);
            break;
            
        case DropMenuStyleTop:
            rectFrame = CGRectMake(parentView.bounds.origin.x, parentView.bounds.origin.y + EN_NAVIGATION_BAR_HEIGHT, parentView.bounds.size.width, parentView.bounds.size.height - EN_NAVIGATION_BAR_HEIGHT);
            break;
    }
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:effectStyle]];
    blurEffectView.frame = rectFrame;
            blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    blurEffectView.alpha = 0.0;
    blurEffectView.tag = kBlurViewTag;
    
    return blurEffectView;
}

+(UIButton *) backButtonView:(UIView *) parentView withTarget:(nullable id) target action:(nullable SEL)action style:(DropMenuStyle) style {
    
    UIButton *backBt = [UIButton buttonWithType:UIButtonTypeCustom];
    backBt.frame = parentView.bounds;
    [backBt addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    backBt.backgroundColor = [UIColor clearColor];
    backBt.alpha = 0;
    return backBt;
}

+(ENDropMenu *) dropMenuWithFrame:(CGRect) frameRect style:(DropMenuStyle) style {
    
    ENDropMenu *menu = [[ENDropMenu alloc] initWithFrame:frameRect];
    menu.alpha = 0.0;
    menu.tag = kDropMenuTag;
    return menu;
}

+(UIView *) backViewForHide:(UIView *) parentView style:(DropMenuStyle) style {
    return [parentView viewWithTag:kBackViewTag];
}

+(UIVisualEffectView *) blurViewForHide:(UIView *) parentView style:(DropMenuStyle) style {
    return [parentView viewWithTag:kBlurViewTag];
}

+(ENDropMenu *) dropMenuViewForHide:(UIView *) parentView style:(DropMenuStyle) style {
    return [parentView viewWithTag:kDropMenuTag];
}

#pragma mark - Notification

+(void) postNotificationForStyle:(DropMenuStyle) style {
    switch (style) {
        case DropMenuStyleBottom:
            [[NSNotificationCenter defaultCenter] postNotificationName:EN_NOTIFICATION_NAVIGATION_TAB_BAR_DROP_MENU_CLOSED object:nil];
            break;
            
        case DropMenuStyleTop:
            [[NSNotificationCenter defaultCenter] postNotificationName:EN_NOTIFICATION_NAVIGATION_BAR_DROP_MENU_CLOSED object:nil];
            
    }
}

@end
