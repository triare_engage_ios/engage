//
//  SFSafariManager.h
//  ENGAGE
//
//  Created by Maksym Savisko on 1/6/17.
//  Copyright © 2017 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SafariServices/SafariServices.h>

@interface SFSafariManager : NSObject
+ (void)openURLWithSafariController:(NSString*)stringUrl forController:(UIViewController <SFSafariViewControllerDelegate> *) vc;

@end
