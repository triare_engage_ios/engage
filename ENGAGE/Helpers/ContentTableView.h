//
//  ContentTableView.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 08.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentTableView : UITableView

-(void) scrollTableToTop:(BOOL) animated;

@end
