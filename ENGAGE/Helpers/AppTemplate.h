//
//  AppTemplate.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (Template)

+(UIColor *) appTextColor;
+(UIColor *) blackTextColor;
+(UIColor *) appTextFieldColor;


+(UIColor *) appGrayColor;
+(UIColor *) appLightGrayColor;
+(UIColor *) appGrayColorForPlaceHolder;
+(UIColor *) appOrangeColor;
+(UIColor *) appYellowColor;


+(UIColor *) appNavigationBarColor;
+(UIColor *) appNavigationTextColor;

+(UIColor *) appClockColorForTeamMessage;

+(UIColor *) appPieChartBlueColor;
+(UIColor *) appPieChartGreenColor;
+(UIColor *) appPieChartOrangeColor;
+(UIColor *) appPieChartDarkOrangeColor;
+(UIColor *) appPieChartDarkRedColor;

+(UIColor *) avatarBorderColor;


+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end


@interface UIFont (Template)

+(UIFont *) appFont:(CGFloat) fontSize;
+(UIFont *) appFontThin:(CGFloat) fontSize;
+(UIFont *) appFontBold:(CGFloat) fontSize;

@end



@interface AppTemplate : NSObject

+(void) addBorder:(UIView *) view;

+(NSURL *) smallIconURL:(NSString *) link;
+(NSURL *) mediumIconURL:(NSString *) link;
+(NSURL *) bigIconURL:(NSString *) link;
+(NSURL *) originalIconURL:(NSString *) link;

@end



@interface UIStoryboard (Template)

+(UIStoryboard *) mainStoryboard;
+(UIStoryboard *) activityFeedStoryboard;
+(UIStoryboard *) userProfileStoryboard;

@end




