//
//  AppTemplate.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "AppTemplate.h"



@implementation UIColor (Template)

+(UIColor *) appTextColor {
    return [UIColor whiteColor];
}

+(UIColor *) blackTextColor {
    return [UIColor colorWithRed:20/255. green:20/255. blue:20/255. alpha:1];
}

+(UIColor *) appTextFieldColor {
    return [UIColor grayColor];
}

+(UIColor *) appPieChartBlueColor {
    return [UIColor colorWithRed:90/255. green:200/255. blue:250/255. alpha:1];
}

+(UIColor *) appPieChartGreenColor {
    return [UIColor colorWithRed:76/255. green:217/255. blue:100/255. alpha:1];
}

+(UIColor *) appPieChartOrangeColor {
    return [UIColor colorWithRed:255/255. green:204/255. blue:0/255. alpha:1];
}

+(UIColor *) appPieChartDarkOrangeColor {
    return [UIColor colorWithRed:255/255. green:149/255. blue:0/255. alpha:1];
}

+(UIColor *) appPieChartDarkRedColor {
    return [UIColor colorWithRed:255/255. green:59/255. blue:48/255. alpha:1];
}



+(UIColor *) appGrayColor {
    return [UIColor colorWithRed:125/255. green:125/255. blue:125/255. alpha:1];
}

+(UIColor *) appGrayColorForPlaceHolder {
    return [UIColor colorWithRed:171/255. green:181/255. blue:188/255. alpha:1];
}

+(UIColor *) appLightGrayColor {
    return [UIColor colorWithRed:205/255. green:205/255. blue:205/255. alpha:1];
}

+(UIColor *) appOrangeColor {
    return [UIColor colorWithRed:254/255. green:120/255. blue:49/255. alpha:1];
}

+(UIColor *) appYellowColor {
    return [UIColor colorWithRed:254/255. green:168/255. blue:62/255. alpha:1];
}


+(UIColor *) appNavigationBarColor {
    return [UIColor colorWithWhite:0 alpha:.5];
}

+(UIColor *) appNavigationTextColor {
    return [UIColor whiteColor];
}

+(UIColor *) appClockColorForTeamMessage {
    return [UIColor colorWithRed:87/255. green:87/255. blue:87/255. alpha:1];
}


+(UIColor *) avatarBorderColor {
    return [UIColor colorFromHexString:@"#F8CD3B"];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    if (hexString == nil) {
        return nil;
    }
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    if ([hexString rangeOfString:@"#"].location == 0) {
        [scanner setScanLocation:1]; // bypass '#' character
    }
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end



@implementation UIFont (Template)

+(UIFont *) appFont:(CGFloat) fontSize {
    return [UIFont fontWithName:@"CirceRounded-Regular" size:fontSize];
}

+(UIFont *) appFontThin:(CGFloat) fontSize {
    return [UIFont fontWithName:@"CirceRounded-Regular5" size:fontSize];
}

+(UIFont *) appFontBold:(CGFloat) fontSize {
    return [UIFont fontWithName:@"CirceRounded-Regular4" size:fontSize];
}


@end



@implementation AppTemplate

+(void) addBorder:(UIView *) view {
    view.layer.borderWidth = 1;
    view.layer.borderColor = [UIColor appLightGrayColor].CGColor;
}

+(NSURL *) iconURL:(NSString *) link forSize:(NSString *) size {
    NSURL *url = [NSURL URLWithString:link];
    
    NSMutableArray *pathItems = [NSMutableArray arrayWithArray:[url.path componentsSeparatedByString:@"/"]];
    NSInteger parameterIndex = pathItems.count - 2;
    
    if (parameterIndex < 0) return url;
    
    NSString *parameter = [pathItems objectAtIndex:parameterIndex];
    if ([parameter isEqualToString:@"original"]) {
        [pathItems replaceObjectAtIndex:parameterIndex withObject:size];
    }
    
    
    for (NSInteger i = 0; i < pathItems.count; i++) {
        NSString *p = [pathItems objectAtIndex:i];
        [pathItems replaceObjectAtIndex:i withObject:[p stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    }
    
    NSString *path = [pathItems componentsJoinedByString:@"/"];
    NSString *newLink = [NSString stringWithFormat:@"%@://%@%@?%@", [url scheme], [url host], path, [url query]];

    return [NSURL URLWithString:newLink];
}

+(NSURL *) smallIconURL:(NSString *) link {
    return [self iconURL:link forSize:@"small"];
}

+(NSURL *) mediumIconURL:(NSString *) link {
    return [self iconURL:link forSize:@"medium"];
}

+(NSURL *) bigIconURL:(NSString *) link {
    return [self iconURL:link forSize:@"big"];
}

+(NSURL *) originalIconURL:(NSString *) link {
    return [self iconURL:link forSize:@"original"];
}

@end




@implementation UIStoryboard (Template)

+(UIStoryboard *) mainStoryboard {
    return [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}

+(UIStoryboard *) activityFeedStoryboard {
    return [UIStoryboard storyboardWithName:@"ActivityFeed" bundle:nil];
}

+(UIStoryboard *) userProfileStoryboard {
    return [UIStoryboard storyboardWithName:@"UserProfile" bundle:nil];
}

@end
