//
//  UIView+Extends.h
//
//  Created by Alex Kravchenko on 24.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extends)

+(void) makeRoundedView:(UIView *)view;
+(void) makeRoundedView:(UIView *)view diameter:(CGFloat)diameter;

+(void) makeAppRoundedView:(UIView *)view;

@end
