//
//  UIView+Extends.m
//
//
//  Created by Alex Kravchenko on 24.05.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "UIView+Extends.h"

@implementation UIView (Extends)

+(void) makeRoundedView:(UIView *)view {
    [self makeRoundedView:view diameter:view.frame.size.width];
}
+(void) makeRoundedView:(UIView *)view diameter:(CGFloat)diameter;
{
    view.layer.cornerRadius = diameter / 2.0;
    view.clipsToBounds = YES;
}

+(void) makeAppRoundedView:(UIView *)view {
    view.layer.cornerRadius = 10;
    view.clipsToBounds = YES;
}

@end
