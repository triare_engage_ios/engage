//
//  ENCircleView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/29/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENCircleView.h"

@implementation ENCircleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.width / 2.0;
    self.clipsToBounds = YES;
    
}

@end
