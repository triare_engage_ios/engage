//
//  ENRoundedView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/3/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENRoundedView.h"

#define kDefaultCornerRadius 10.0

@implementation ENRoundedView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews {
    [super layoutSubviews];
    
    if (self.cornerRadius) {
        self.layer.cornerRadius = self.cornerRadius;
    } else {
        self.layer.cornerRadius = kDefaultCornerRadius;
    }
    
    self.clipsToBounds = YES;
}

- (CGFloat) halfOfWidth {
    return self.frame.size.width / 2.0;
}

- (void) makeCircle {
    self.cornerRadius = [self halfOfWidth];
}

@end
