//
//  ENRoundedView.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/3/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENRoundedView : UIView
@property (nonatomic) CGFloat cornerRadius;

- (void) makeCircle;

@end
