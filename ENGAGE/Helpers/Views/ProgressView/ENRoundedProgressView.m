//
//  ENRoundedProgressView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/3/16.
//  Copyright © 2016 Triare. All rights reserved.
//

#import "ENRoundedProgressView.h"

#define kDefaultCornderRadius 3.0
#define kDefaultScaleHeight 2.5

@interface ENRoundedProgressView ()

@end

@implementation ENRoundedProgressView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews {
    [super layoutSubviews];
    
    if (self.cornerRadius) {
        self.layer.cornerRadius = self.cornerRadius;
    }
    else {
        self.layer.cornerRadius = kDefaultCornderRadius;
    }
    
    self.clipsToBounds = YES;
    
    if (self.scaleHeight) {
        [self setTransform:CGAffineTransformMakeScale(1.0, self.scaleHeight)];
    }
    else {
        [self setTransform:CGAffineTransformMakeScale(1.0, kDefaultScaleHeight)];
    }
}

@end
