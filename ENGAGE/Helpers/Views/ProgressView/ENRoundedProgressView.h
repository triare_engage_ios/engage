//
//  ENRoundedProgressView.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/3/16.
//  Copyright © 2016 Triare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENRoundedProgressView : UIProgressView
@property (nonatomic) CGFloat cornerRadius; //default 3.0;
@property (nonatomic) CGFloat scaleHeight; //default 2.5;

@end
