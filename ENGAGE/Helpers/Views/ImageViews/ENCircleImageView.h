//
//  ENCircleImageView.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENCircleImageView : UIImageView

@end
