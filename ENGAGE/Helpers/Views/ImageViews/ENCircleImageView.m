//
//  ENCircleImageView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ENCircleImageView.h"

@implementation ENCircleImageView

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews {
    [super layoutSubviews];
    
    //    self.layer.borderWidth = 1.0;
    //self.layer.borderColor = [UIColor appLightGrayColor].CGColor;
    
    self.layer.cornerRadius = self.frame.size.width / 2.0;
    self.clipsToBounds = YES;
}


@end
