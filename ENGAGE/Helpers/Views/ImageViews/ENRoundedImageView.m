//
//  ENRoundedImageView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/2/16.
//  Copyright © 2016 Triare. All rights reserved.
//

#import "ENRoundedImageView.h"

#define kDefaultImageCornerRadius 10.0
#define kDefaultImageBorderColor [UIColor blackColor].CGColor


@interface ENRoundedImageView ()

@end

@implementation ENRoundedImageView

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    if (self.imageCornerRadius) {
        self.layer.cornerRadius = self.imageCornerRadius;
    }
    else {
        self.layer.cornerRadius = kDefaultImageCornerRadius;
    }

    if (self.imageBorderWidth) {
        self.layer.borderWidth = self.imageBorderWidth;
    }
    
    if (self.imageBorderColor) {
        self.layer.borderColor = self.imageBorderColor.CGColor;
    } else {
        self.layer.borderColor = kDefaultImageBorderColor;
    }
    
    
    self.clipsToBounds = YES;
}

- (void) setImageCornerRadius:(CGFloat)imageCornerRadius {
    _imageCornerRadius = imageCornerRadius;
}

- (CGFloat) halfOfWidth {
    return self.frame.size.width / 2.0;
}

- (void) makeCircle {
    self.imageCornerRadius = [self halfOfWidth];
}


@end
