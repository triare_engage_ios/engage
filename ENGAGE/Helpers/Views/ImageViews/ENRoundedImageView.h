//
//  ENRoundedImageView.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/2/16.
//  Copyright © 2016 Triare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ENRoundedImageView : UIImageView
@property (nonatomic) CGFloat imageCornerRadius; //default 10.0
@property (nonatomic) CGFloat imageBorderWidth; //default 0.0
@property (nonatomic, strong) UIColor *imageBorderColor; //default black color;

- (CGFloat) halfOfWidth;
- (void) makeCircle;

@end
