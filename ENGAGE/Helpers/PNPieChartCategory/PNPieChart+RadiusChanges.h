//
//  PNPieChart+RadiusChanges.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <PNChart/PNChart.h>

@interface PNPieChart (RadiusChanges)

- (void)recompute;

@end
