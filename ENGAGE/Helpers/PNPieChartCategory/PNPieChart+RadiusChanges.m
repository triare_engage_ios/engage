//
//  PNPieChart+RadiusChanges.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "PNPieChart+RadiusChanges.h"

#define kPieChartSize 290.0

@implementation PNPieChart (RadiusChanges)


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

- (void)recompute {
    
    CGRect rectSize = CGRectMake(0, 0, kPieChartSize, kPieChartSize);
    
    self.outerCircleRadius = CGRectGetWidth(rectSize) / 2;
    self.innerCircleRadius = CGRectGetWidth(rectSize) / 3;
}

#pragma clang diagnostic pop

@end
