//
//  TakePictureVC.h
//  Note
//
//  Created by Alex Kravchenko on 18.06.15.
//  Copyright (c) 2015 Moving Player. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TakePictureVC : UIViewController

@property (nonatomic, copy) void (^onSelectPicture)(UIImage *image);
@property BOOL editAfterTakePicture;

@property CGFloat imageWidth;


+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToHeight:(CGFloat)newHeight;
+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToWidth:(CGFloat)newWidth;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end