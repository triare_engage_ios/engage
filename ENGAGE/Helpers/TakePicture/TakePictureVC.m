//
//  TakePictureVC.m
//  Note
//
//  Created by Alex Kravchenko on 18.06.15.
//  Copyright (c) 2015 Moving Player. All rights reserved.
//

#import "TakePictureVC.h"
#include <AssetsLibrary/AssetsLibrary.h>
#import "CropImageEditor.h"

typedef NS_ENUM (unsigned int, SourceMode)
{
    SourceModeCamera,
    SourceModePhotoLibrary
};

#define kOrangeColor [UIColor colorWithRed:255 / 255. green:204 / 255. blue:0 alpha:1]

@interface TakePictureVC () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property SourceMode sourceMode;

//overlayView
@property (weak, nonatomic) IBOutlet UIView *cameraOverlay;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *photorollButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraSwitchButton;

@property (weak, nonatomic) IBOutlet UIView *flashView;
@property (weak, nonatomic) IBOutlet UIButton *flashAutoButton;
@property (weak, nonatomic) IBOutlet UIButton *flashOnButton;
@property (weak, nonatomic) IBOutlet UIButton *flashOffButton;
@property (weak, nonatomic) IBOutlet UIButton *changeFlashButton;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;

@property (nonatomic, weak) UIButton *selectedFlashButton;


@end

@implementation TakePictureVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", @"v1.0") forState:UIControlStateNormal];
    [self.photorollButton setTitle:NSLocalizedString(@"Photoroll", @"v1.0") forState:UIControlStateNormal];
    [self.flashAutoButton setTitle:NSLocalizedString(@"Auto", @"v1.0") forState:UIControlStateNormal];
    [self.flashOnButton setTitle:NSLocalizedString(@"On", @"v1.0") forState:UIControlStateNormal];
    [self.flashOffButton setTitle:NSLocalizedString(@"Off", @"v1.0") forState:UIControlStateNormal];
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    
    [self addChildViewController:self.imagePicker];
    
    self.imagePicker.view.frame = self.view.bounds;
    [self.view addSubview:self.imagePicker.view];
    
    [self.imagePicker didMoveToParentViewController:self];
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [self showCamera];
    }
    else
    {
        [self showPhotoLibrary];
    }
    
    if ([self hasSmallDisplay])
    {
        self.buttonsView.frame = CGRectMake(self.buttonsView.frame.origin.x, [UIScreen mainScreen].bounds.size.height - self.buttonsView.frame.size.height, self.buttonsView.frame.size.width, self.buttonsView.frame.size.height);
    }
}

- (UIImage *)resizeImage:(UIImage *)image
{
    if (image.size.width > self.imageWidth)
    {
        return [[self class] imageWithImage:image scaledToWidth:self.imageWidth];
    }
    return image;
}

- (void)closeViewController
{
    //[self.imagePicker popToRootViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showCamera
{
    self.sourceMode = SourceModeCamera;
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePicker.showsCameraControls = NO;
    self.imagePicker.cameraOverlayView = self.cameraOverlay;
    
    [self changeFlashMode:nil];
    
    CGAffineTransform translate = CGAffineTransformMakeTranslation(0, 50);
    self.imagePicker.cameraViewTransform = translate;
}

- (void)showPhotoLibrary
{
    self.sourceMode = SourceModePhotoLibrary;
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
}

- (void)didTakenPicture:(UIImage *)originalImage preview:(UIImage *)preview
{
    if (self.editAfterTakePicture)
    {
        [self editPicture:originalImage preview:preview];
    }
    else
    {
        if (self.onSelectPicture)
        {
            if (self.imageWidth > 0)
            {
                originalImage = [self resizeImage:originalImage];
            }
            
            self.onSelectPicture(originalImage);
        }
        [self closeViewController];
    }
}

- (void)editPicture:(UIImage *)originalImage preview:(UIImage *)preview
{
    CropImageEditor *imageEditor = [[CropImageEditor alloc] initWithNibName:@"CropImageEditor" bundle:nil];
    [imageEditor isGalleryPicture:YES];
    imageEditor.checkBounds = YES;
    imageEditor.rotateEnabled = YES;
    
    __weak typeof(self) weakSelf = self;
    
    imageEditor.doneCallback = ^(UIImage *editedImage, int status) {
        if (status == 0)  //Go with pix
            
        {
            if (weakSelf.onSelectPicture)
            {
                if (weakSelf.imageWidth > 0)
                {
                    editedImage = [weakSelf resizeImage:editedImage];
                }
                weakSelf.onSelectPicture(editedImage);
            }
            
            
            //save image to Device Photo Library
            /*
             ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
             [library writeImageToSavedPhotosAlbum:[editedImage CGImage]
             
             orientation:(ALAssetOrientation)editedImage.imageOrientation
             completionBlock:^(NSURL *assetURL, NSError *error){
             if (error) {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Saving"
             message:[error localizedDescription]
             delegate:nil
             cancelButtonTitle:@"Ok"
             otherButtonTitles: nil];
             [alert show];
             }
             else
             {
             if (weakSelf.onSelectPicture) {
             
             if (weakSelf.imageWidth > 0) {
             editedImage = [weakSelf resizeImage:editedImage];
             }
             
             weakSelf.onSelectPicture([TakePictureVC imageWithImage:editedImage scaledToHeight:150]);
             }
             }
             [weakSelf closeViewController];
             }];
             */
        }
        else if (status == 1)//take another one
        {
            [weakSelf.imagePicker popViewControllerAnimated:YES];
        }
    };
    
    
    imageEditor.sourceImage = originalImage;
    imageEditor.previewImage = preview;
    
    if (preview == nil)
    {
        imageEditor.previewImage = originalImage;
    }
    
    [self.imagePicker pushViewController:imageEditor animated:YES];
    [self.imagePicker setNavigationBarHidden:YES animated:NO];
    [imageEditor setSquareAction:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.imagePicker beginAppearanceTransition:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.imagePicker endAppearanceTransition];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.imagePicker popToRootViewControllerAnimated:NO];
    [self.imagePicker beginAppearanceTransition:NO animated:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.imagePicker endAppearanceTransition];
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (self.sourceMode == SourceModePhotoLibrary && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [self showCamera];
    }
    else
    {
        [self closeViewController];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image =  info[UIImagePickerControllerOriginalImage];
    NSURL *assetURL = info[UIImagePickerControllerReferenceURL];

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library assetForURL:assetURL resultBlock: ^(ALAsset *asset) {
        UIImage *preview = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
        
#pragma clang diagnostic pop
        [self didTakenPicture:image preview:preview];
    }
            failureBlock: ^(NSError *error) {
                [self didTakenPicture:image preview:nil];
            }];
}

#pragma mark CameraOverlay

- (IBAction)cancelAction:(id)sender
{
    [self closeViewController];
}

- (IBAction)photorollAction:(id)sender
{
    [self showPhotoLibrary];
}

- (IBAction)takePicture:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [self.imagePicker takePicture];
    }
}

- (IBAction)switchCamera:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        BOOL isFrontCamera = NO;
        if (self.imagePicker.cameraDevice == UIImagePickerControllerCameraDeviceFront)
        {
            isFrontCamera = YES;
        }
        
        [UIView transitionWithView:self.imagePicker.view duration:1.0 options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionTransitionFlipFromLeft animations: ^{
            if (isFrontCamera)
            {
                self.imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                self.flashView.hidden = NO;
            }
            else
            {
                self.imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
                self.flashView.hidden = YES;
            }
        }
                        completion:NULL];
    }
}

- (IBAction)selectFlash:(id)sender
{
    self.changeFlashButton.hidden = YES;
    
    self.selectedFlashButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.selectedFlashButton setTitleColor:kOrangeColor forState:UIControlStateNormal];
    
    CGRect autoFrame = self.flashAutoButton.frame;
    autoFrame.origin.x = 20;
    
    
    CGRect onFrame = self.flashOnButton.frame;
    onFrame.origin.x = 80;
    
    
    CGRect offFrame = self.flashOffButton.frame;
    offFrame.origin.x = 140;
    
    
    [UIView animateWithDuration:0.2 animations: ^{
        self.flashAutoButton.hidden = NO;
        self.flashOnButton.hidden = NO;
        self.flashOffButton.hidden = NO;
        
        self.flashAutoButton.frame = autoFrame;
        self.flashOnButton.frame = onFrame;
        self.flashOffButton.frame = offFrame;
    }];
}

- (IBAction)changeFlashMode:(id)sender
{
    if (sender == nil)
    {
        sender = self.flashAutoButton;
    }
    
    if (sender == self.flashOnButton)
    {
        self.imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
    }
    else if (sender == self.flashOffButton)
    {
        self.imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    }
    else
    {
        self.imagePicker.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    }
    
    
    [self.selectedFlashButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.changeFlashButton.hidden = NO;
    
    self.selectedFlashButton = sender;
    self.selectedFlashButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    
    CGRect frame = self.selectedFlashButton.frame;
    frame.origin.x = 20;
    
    [UIView animateWithDuration:0.2 animations: ^{
        self.flashAutoButton.hidden = YES;
        self.flashOnButton.hidden = YES;
        self.flashOffButton.hidden = YES;
        
        self.selectedFlashButton.frame = frame;
        self.selectedFlashButton.hidden = NO;
    }];
}

#pragma mark Helpers

+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToHeight:(CGFloat)newHeight
{
    CGFloat oldHeight = sourceImage.size.height;
    CGFloat scaleFactor = newHeight / oldHeight;
    
    CGFloat newWidth = sourceImage.size.width * scaleFactor;
    
    return [self imageWithImage:sourceImage scaledToSize:CGSizeMake(newWidth, newHeight)];
}

+ (UIImage *)imageWithImage:(UIImage *)sourceImage scaledToWidth:(CGFloat)newWidth
{
    CGFloat oldWidth = sourceImage.size.width;
    CGFloat scaleFactor = newWidth / oldWidth;
    
    CGFloat newHeight = sourceImage.size.height * scaleFactor;
    
    return [self imageWithImage:sourceImage scaledToSize:CGSizeMake(newWidth, newHeight)];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (BOOL) hasSmallDisplay{
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height < 568.0);
}

@end
