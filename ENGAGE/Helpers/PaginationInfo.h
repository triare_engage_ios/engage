//
//  PaginationInfo.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 09.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaginationInfo : NSObject

@property NSInteger currentPage;
@property NSInteger totalPages;
@property NSInteger elementsPerPage;

@property BOOL isLoading;

@end
