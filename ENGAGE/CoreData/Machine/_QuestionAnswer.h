// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to QuestionAnswer.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Question;

@interface QuestionAnswerID : NSManagedObjectID {}
@end

@interface _QuestionAnswer : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) QuestionAnswerID *objectID;

@property (nonatomic, strong, nullable) NSNumber* answersCount;

@property (atomic) int64_t answersCountValue;
- (int64_t)answersCountValue;
- (void)setAnswersCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* questionId;

@property (atomic) int64_t questionIdValue;
- (int64_t)questionIdValue;
- (void)setQuestionIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) Question *question;

@end

@interface _QuestionAnswer (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveAnswersCount;
- (void)setPrimitiveAnswersCount:(nullable NSNumber*)value;

- (int64_t)primitiveAnswersCountValue;
- (void)setPrimitiveAnswersCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveQuestionId;
- (void)setPrimitiveQuestionId:(nullable NSNumber*)value;

- (int64_t)primitiveQuestionIdValue;
- (void)setPrimitiveQuestionIdValue:(int64_t)value_;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (Question*)primitiveQuestion;
- (void)setPrimitiveQuestion:(Question*)value;

@end

@interface QuestionAnswerAttributes: NSObject 
+ (NSString *)answersCount;
+ (NSString *)questionId;
+ (NSString *)title;
@end

@interface QuestionAnswerRelationships: NSObject
+ (NSString *)question;
@end

NS_ASSUME_NONNULL_END
