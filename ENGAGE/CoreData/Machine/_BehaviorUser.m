// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BehaviorUser.m instead.

#import "_BehaviorUser.h"

@implementation BehaviorUserID
@end

@implementation _BehaviorUser

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BehaviorUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BehaviorUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BehaviorUser" inManagedObjectContext:moc_];
}

- (BehaviorUserID*)objectID {
	return (BehaviorUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"behaviorIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"behaviorId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"givenCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"givenCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isPointWinnerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isPointWinner"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"receivedCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"receivedCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic avatarUrl;

@dynamic behaviorId;

- (int64_t)behaviorIdValue {
	NSNumber *result = [self behaviorId];
	return [result longLongValue];
}

- (void)setBehaviorIdValue:(int64_t)value_ {
	[self setBehaviorId:@(value_)];
}

- (int64_t)primitiveBehaviorIdValue {
	NSNumber *result = [self primitiveBehaviorId];
	return [result longLongValue];
}

- (void)setPrimitiveBehaviorIdValue:(int64_t)value_ {
	[self setPrimitiveBehaviorId:@(value_)];
}

@dynamic fullName;

@dynamic givenCount;

- (int64_t)givenCountValue {
	NSNumber *result = [self givenCount];
	return [result longLongValue];
}

- (void)setGivenCountValue:(int64_t)value_ {
	[self setGivenCount:@(value_)];
}

- (int64_t)primitiveGivenCountValue {
	NSNumber *result = [self primitiveGivenCount];
	return [result longLongValue];
}

- (void)setPrimitiveGivenCountValue:(int64_t)value_ {
	[self setPrimitiveGivenCount:@(value_)];
}

@dynamic isPointWinner;

- (BOOL)isPointWinnerValue {
	NSNumber *result = [self isPointWinner];
	return [result boolValue];
}

- (void)setIsPointWinnerValue:(BOOL)value_ {
	[self setIsPointWinner:@(value_)];
}

- (BOOL)primitiveIsPointWinnerValue {
	NSNumber *result = [self primitiveIsPointWinner];
	return [result boolValue];
}

- (void)setPrimitiveIsPointWinnerValue:(BOOL)value_ {
	[self setPrimitiveIsPointWinner:@(value_)];
}

@dynamic jobTitle;

@dynamic receivedCount;

- (int64_t)receivedCountValue {
	NSNumber *result = [self receivedCount];
	return [result longLongValue];
}

- (void)setReceivedCountValue:(int64_t)value_ {
	[self setReceivedCount:@(value_)];
}

- (int64_t)primitiveReceivedCountValue {
	NSNumber *result = [self primitiveReceivedCount];
	return [result longLongValue];
}

- (void)setPrimitiveReceivedCountValue:(int64_t)value_ {
	[self setPrimitiveReceivedCount:@(value_)];
}

@dynamic userId;

- (int64_t)userIdValue {
	NSNumber *result = [self userId];
	return [result longLongValue];
}

- (void)setUserIdValue:(int64_t)value_ {
	[self setUserId:@(value_)];
}

- (int64_t)primitiveUserIdValue {
	NSNumber *result = [self primitiveUserId];
	return [result longLongValue];
}

- (void)setPrimitiveUserIdValue:(int64_t)value_ {
	[self setPrimitiveUserId:@(value_)];
}

@end

@implementation BehaviorUserAttributes 
+ (NSString *)avatarUrl {
	return @"avatarUrl";
}
+ (NSString *)behaviorId {
	return @"behaviorId";
}
+ (NSString *)fullName {
	return @"fullName";
}
+ (NSString *)givenCount {
	return @"givenCount";
}
+ (NSString *)isPointWinner {
	return @"isPointWinner";
}
+ (NSString *)jobTitle {
	return @"jobTitle";
}
+ (NSString *)receivedCount {
	return @"receivedCount";
}
+ (NSString *)userId {
	return @"userId";
}
@end

