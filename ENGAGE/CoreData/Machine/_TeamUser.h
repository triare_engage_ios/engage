// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TeamUser.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class UserBehavior;
@class EvaluateBehavior;

@interface TeamUserID : NSManagedObjectID {}
@end

@interface _TeamUser : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TeamUserID *objectID;

@property (nonatomic, strong, nullable) NSString* avatarUrl;

@property (nonatomic, strong, nullable) NSString* companyPlan;

@property (nonatomic, strong, nullable) NSString* email;

@property (nonatomic, strong, nullable) NSString* firstName;

@property (nonatomic, strong, nullable) NSString* fullName;

@property (nonatomic, strong, nullable) NSNumber* haveChallengeBadges;

@property (atomic) BOOL haveChallengeBadgesValue;
- (BOOL)haveChallengeBadgesValue;
- (void)setHaveChallengeBadgesValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isInvationAccepted;

@property (atomic) BOOL isInvationAcceptedValue;
- (BOOL)isInvationAcceptedValue;
- (void)setIsInvationAcceptedValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isPointWinner;

@property (atomic) BOOL isPointWinnerValue;
- (BOOL)isPointWinnerValue;
- (void)setIsPointWinnerValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isQuestionPresent;

@property (atomic) BOOL isQuestionPresentValue;
- (BOOL)isQuestionPresentValue;
- (void)setIsQuestionPresentValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isTeamMessagesPresent;

@property (atomic) BOOL isTeamMessagesPresentValue;
- (BOOL)isTeamMessagesPresentValue;
- (void)setIsTeamMessagesPresentValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* jobTitle;

@property (nonatomic, strong, nullable) NSString* lastName;

@property (nonatomic, strong, nullable) NSNumber* receivedBadEvaluationsCount;

@property (atomic) int64_t receivedBadEvaluationsCountValue;
- (int64_t)receivedBadEvaluationsCountValue;
- (void)setReceivedBadEvaluationsCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* receivedGoodEvaluationsCount;

@property (atomic) int64_t receivedGoodEvaluationsCountValue;
- (int64_t)receivedGoodEvaluationsCountValue;
- (void)setReceivedGoodEvaluationsCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* role;

@property (nonatomic, strong, nullable) NSString* unconfirmedEmail;

@property (nonatomic, strong, nullable) NSNumber* unreadSuggestionsCount;

@property (atomic) int16_t unreadSuggestionsCountValue;
- (int16_t)unreadSuggestionsCountValue;
- (void)setUnreadSuggestionsCountValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSNumber* userId;

@property (atomic) int64_t userIdValue;
- (int64_t)userIdValue;
- (void)setUserIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSSet<UserBehavior*> *behaviors;
- (nullable NSMutableSet<UserBehavior*>*)behaviorsSet;

@property (nonatomic, strong, nullable) NSSet<EvaluateBehavior*> *evaluations;
- (nullable NSMutableSet<EvaluateBehavior*>*)evaluationsSet;

@end

@interface _TeamUser (BehaviorsCoreDataGeneratedAccessors)
- (void)addBehaviors:(NSSet<UserBehavior*>*)value_;
- (void)removeBehaviors:(NSSet<UserBehavior*>*)value_;
- (void)addBehaviorsObject:(UserBehavior*)value_;
- (void)removeBehaviorsObject:(UserBehavior*)value_;

@end

@interface _TeamUser (EvaluationsCoreDataGeneratedAccessors)
- (void)addEvaluations:(NSSet<EvaluateBehavior*>*)value_;
- (void)removeEvaluations:(NSSet<EvaluateBehavior*>*)value_;
- (void)addEvaluationsObject:(EvaluateBehavior*)value_;
- (void)removeEvaluationsObject:(EvaluateBehavior*)value_;

@end

@interface _TeamUser (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAvatarUrl;
- (void)setPrimitiveAvatarUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveCompanyPlan;
- (void)setPrimitiveCompanyPlan:(nullable NSString*)value;

- (nullable NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(nullable NSString*)value;

- (nullable NSString*)primitiveFirstName;
- (void)setPrimitiveFirstName:(nullable NSString*)value;

- (nullable NSString*)primitiveFullName;
- (void)setPrimitiveFullName:(nullable NSString*)value;

- (nullable NSNumber*)primitiveHaveChallengeBadges;
- (void)setPrimitiveHaveChallengeBadges:(nullable NSNumber*)value;

- (BOOL)primitiveHaveChallengeBadgesValue;
- (void)setPrimitiveHaveChallengeBadgesValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsInvationAccepted;
- (void)setPrimitiveIsInvationAccepted:(nullable NSNumber*)value;

- (BOOL)primitiveIsInvationAcceptedValue;
- (void)setPrimitiveIsInvationAcceptedValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsPointWinner;
- (void)setPrimitiveIsPointWinner:(nullable NSNumber*)value;

- (BOOL)primitiveIsPointWinnerValue;
- (void)setPrimitiveIsPointWinnerValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsQuestionPresent;
- (void)setPrimitiveIsQuestionPresent:(nullable NSNumber*)value;

- (BOOL)primitiveIsQuestionPresentValue;
- (void)setPrimitiveIsQuestionPresentValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsTeamMessagesPresent;
- (void)setPrimitiveIsTeamMessagesPresent:(nullable NSNumber*)value;

- (BOOL)primitiveIsTeamMessagesPresentValue;
- (void)setPrimitiveIsTeamMessagesPresentValue:(BOOL)value_;

- (nullable NSString*)primitiveJobTitle;
- (void)setPrimitiveJobTitle:(nullable NSString*)value;

- (nullable NSString*)primitiveLastName;
- (void)setPrimitiveLastName:(nullable NSString*)value;

- (nullable NSNumber*)primitiveReceivedBadEvaluationsCount;
- (void)setPrimitiveReceivedBadEvaluationsCount:(nullable NSNumber*)value;

- (int64_t)primitiveReceivedBadEvaluationsCountValue;
- (void)setPrimitiveReceivedBadEvaluationsCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveReceivedGoodEvaluationsCount;
- (void)setPrimitiveReceivedGoodEvaluationsCount:(nullable NSNumber*)value;

- (int64_t)primitiveReceivedGoodEvaluationsCountValue;
- (void)setPrimitiveReceivedGoodEvaluationsCountValue:(int64_t)value_;

- (nullable NSString*)primitiveRole;
- (void)setPrimitiveRole:(nullable NSString*)value;

- (nullable NSString*)primitiveUnconfirmedEmail;
- (void)setPrimitiveUnconfirmedEmail:(nullable NSString*)value;

- (nullable NSNumber*)primitiveUnreadSuggestionsCount;
- (void)setPrimitiveUnreadSuggestionsCount:(nullable NSNumber*)value;

- (int16_t)primitiveUnreadSuggestionsCountValue;
- (void)setPrimitiveUnreadSuggestionsCountValue:(int16_t)value_;

- (nullable NSNumber*)primitiveUserId;
- (void)setPrimitiveUserId:(nullable NSNumber*)value;

- (int64_t)primitiveUserIdValue;
- (void)setPrimitiveUserIdValue:(int64_t)value_;

- (NSMutableSet<UserBehavior*>*)primitiveBehaviors;
- (void)setPrimitiveBehaviors:(NSMutableSet<UserBehavior*>*)value;

- (NSMutableSet<EvaluateBehavior*>*)primitiveEvaluations;
- (void)setPrimitiveEvaluations:(NSMutableSet<EvaluateBehavior*>*)value;

@end

@interface TeamUserAttributes: NSObject 
+ (NSString *)avatarUrl;
+ (NSString *)companyPlan;
+ (NSString *)email;
+ (NSString *)firstName;
+ (NSString *)fullName;
+ (NSString *)haveChallengeBadges;
+ (NSString *)isInvationAccepted;
+ (NSString *)isPointWinner;
+ (NSString *)isQuestionPresent;
+ (NSString *)isTeamMessagesPresent;
+ (NSString *)jobTitle;
+ (NSString *)lastName;
+ (NSString *)receivedBadEvaluationsCount;
+ (NSString *)receivedGoodEvaluationsCount;
+ (NSString *)role;
+ (NSString *)unconfirmedEmail;
+ (NSString *)unreadSuggestionsCount;
+ (NSString *)userId;
@end

@interface TeamUserRelationships: NSObject
+ (NSString *)behaviors;
+ (NSString *)evaluations;
@end

NS_ASSUME_NONNULL_END
