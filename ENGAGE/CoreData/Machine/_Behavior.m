// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Behavior.m instead.

#import "_Behavior.h"

@implementation BehaviorID
@end

@implementation _Behavior

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Behavior" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Behavior";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Behavior" inManagedObjectContext:moc_];
}

- (BehaviorID*)objectID {
	return (BehaviorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"behaviorIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"behaviorId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isActiveValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isActive"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"valueIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"valueId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic behaviorDescription;

@dynamic behaviorId;

- (int64_t)behaviorIdValue {
	NSNumber *result = [self behaviorId];
	return [result longLongValue];
}

- (void)setBehaviorIdValue:(int64_t)value_ {
	[self setBehaviorId:@(value_)];
}

- (int64_t)primitiveBehaviorIdValue {
	NSNumber *result = [self primitiveBehaviorId];
	return [result longLongValue];
}

- (void)setPrimitiveBehaviorIdValue:(int64_t)value_ {
	[self setPrimitiveBehaviorId:@(value_)];
}

@dynamic iconUrl;

@dynamic isActive;

- (BOOL)isActiveValue {
	NSNumber *result = [self isActive];
	return [result boolValue];
}

- (void)setIsActiveValue:(BOOL)value_ {
	[self setIsActive:@(value_)];
}

- (BOOL)primitiveIsActiveValue {
	NSNumber *result = [self primitiveIsActive];
	return [result boolValue];
}

- (void)setPrimitiveIsActiveValue:(BOOL)value_ {
	[self setPrimitiveIsActive:@(value_)];
}

@dynamic presentatioFileUrl;

@dynamic title;

@dynamic valueId;

- (int64_t)valueIdValue {
	NSNumber *result = [self valueId];
	return [result longLongValue];
}

- (void)setValueIdValue:(int64_t)value_ {
	[self setValueId:@(value_)];
}

- (int64_t)primitiveValueIdValue {
	NSNumber *result = [self primitiveValueId];
	return [result longLongValue];
}

- (void)setPrimitiveValueIdValue:(int64_t)value_ {
	[self setPrimitiveValueId:@(value_)];
}

@dynamic value;

@end

@implementation BehaviorAttributes 
+ (NSString *)behaviorDescription {
	return @"behaviorDescription";
}
+ (NSString *)behaviorId {
	return @"behaviorId";
}
+ (NSString *)iconUrl {
	return @"iconUrl";
}
+ (NSString *)isActive {
	return @"isActive";
}
+ (NSString *)presentatioFileUrl {
	return @"presentatioFileUrl";
}
+ (NSString *)title {
	return @"title";
}
+ (NSString *)valueId {
	return @"valueId";
}
@end

@implementation BehaviorRelationships 
+ (NSString *)value {
	return @"value";
}
@end

