// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LeaderUser.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

#import "TeamUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface LeaderUserID : TeamUserID {}
@end

@interface _LeaderUser : TeamUser
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LeaderUserID *objectID;

@property (nonatomic, strong, nullable) NSNumber* givenTapsCount;

@property (atomic) int16_t givenTapsCountValue;
- (int16_t)givenTapsCountValue;
- (void)setGivenTapsCountValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSNumber* receivedTapsCount;

@property (atomic) int16_t receivedTapsCountValue;
- (int16_t)receivedTapsCountValue;
- (void)setReceivedTapsCountValue:(int16_t)value_;

@end

@interface _LeaderUser (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveGivenTapsCount;
- (void)setPrimitiveGivenTapsCount:(nullable NSNumber*)value;

- (int16_t)primitiveGivenTapsCountValue;
- (void)setPrimitiveGivenTapsCountValue:(int16_t)value_;

- (nullable NSNumber*)primitiveReceivedTapsCount;
- (void)setPrimitiveReceivedTapsCount:(nullable NSNumber*)value;

- (int16_t)primitiveReceivedTapsCountValue;
- (void)setPrimitiveReceivedTapsCountValue:(int16_t)value_;

@end

@interface LeaderUserAttributes: NSObject 
+ (NSString *)givenTapsCount;
+ (NSString *)receivedTapsCount;
@end

NS_ASSUME_NONNULL_END
