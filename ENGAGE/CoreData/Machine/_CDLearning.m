// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CDLearning.m instead.

#import "_CDLearning.h"

@implementation CDLearningID
@end

@implementation _CDLearning

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CDLearning" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CDLearning";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CDLearning" inManagedObjectContext:moc_];
}

- (CDLearningID*)objectID {
	return (CDLearningID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"feedIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"feedId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isFromLearningListValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isFromLearningList"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isSenderTopPointWinnerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSenderTopPointWinner"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ratingValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"rating"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"senderIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"senderId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userRatingValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userRating"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic createDate;

@dynamic feedId;

- (int64_t)feedIdValue {
	NSNumber *result = [self feedId];
	return [result longLongValue];
}

- (void)setFeedIdValue:(int64_t)value_ {
	[self setFeedId:@(value_)];
}

- (int64_t)primitiveFeedIdValue {
	NSNumber *result = [self primitiveFeedId];
	return [result longLongValue];
}

- (void)setPrimitiveFeedIdValue:(int64_t)value_ {
	[self setPrimitiveFeedId:@(value_)];
}

@dynamic isFromLearningList;

- (BOOL)isFromLearningListValue {
	NSNumber *result = [self isFromLearningList];
	return [result boolValue];
}

- (void)setIsFromLearningListValue:(BOOL)value_ {
	[self setIsFromLearningList:@(value_)];
}

- (BOOL)primitiveIsFromLearningListValue {
	NSNumber *result = [self primitiveIsFromLearningList];
	return [result boolValue];
}

- (void)setPrimitiveIsFromLearningListValue:(BOOL)value_ {
	[self setPrimitiveIsFromLearningList:@(value_)];
}

@dynamic isSenderTopPointWinner;

- (BOOL)isSenderTopPointWinnerValue {
	NSNumber *result = [self isSenderTopPointWinner];
	return [result boolValue];
}

- (void)setIsSenderTopPointWinnerValue:(BOOL)value_ {
	[self setIsSenderTopPointWinner:@(value_)];
}

- (BOOL)primitiveIsSenderTopPointWinnerValue {
	NSNumber *result = [self primitiveIsSenderTopPointWinner];
	return [result boolValue];
}

- (void)setPrimitiveIsSenderTopPointWinnerValue:(BOOL)value_ {
	[self setPrimitiveIsSenderTopPointWinner:@(value_)];
}

@dynamic learningType;

@dynamic rating;

- (int64_t)ratingValue {
	NSNumber *result = [self rating];
	return [result longLongValue];
}

- (void)setRatingValue:(int64_t)value_ {
	[self setRating:@(value_)];
}

- (int64_t)primitiveRatingValue {
	NSNumber *result = [self primitiveRating];
	return [result longLongValue];
}

- (void)setPrimitiveRatingValue:(int64_t)value_ {
	[self setPrimitiveRating:@(value_)];
}

@dynamic senderAvatarUrl;

@dynamic senderFullName;

@dynamic senderId;

- (int64_t)senderIdValue {
	NSNumber *result = [self senderId];
	return [result longLongValue];
}

- (void)setSenderIdValue:(int64_t)value_ {
	[self setSenderId:@(value_)];
}

- (int64_t)primitiveSenderIdValue {
	NSNumber *result = [self primitiveSenderId];
	return [result longLongValue];
}

- (void)setPrimitiveSenderIdValue:(int64_t)value_ {
	[self setPrimitiveSenderId:@(value_)];
}

@dynamic senderJobTitle;

@dynamic siteDescription;

@dynamic siteImageUrl;

@dynamic siteTitle;

@dynamic siteUrl;

@dynamic userComment;

@dynamic userRating;

- (int64_t)userRatingValue {
	NSNumber *result = [self userRating];
	return [result longLongValue];
}

- (void)setUserRatingValue:(int64_t)value_ {
	[self setUserRating:@(value_)];
}

- (int64_t)primitiveUserRatingValue {
	NSNumber *result = [self primitiveUserRating];
	return [result longLongValue];
}

- (void)setPrimitiveUserRatingValue:(int64_t)value_ {
	[self setPrimitiveUserRating:@(value_)];
}

@dynamic activityFeed;

@end

@implementation CDLearningAttributes 
+ (NSString *)createDate {
	return @"createDate";
}
+ (NSString *)feedId {
	return @"feedId";
}
+ (NSString *)isFromLearningList {
	return @"isFromLearningList";
}
+ (NSString *)isSenderTopPointWinner {
	return @"isSenderTopPointWinner";
}
+ (NSString *)learningType {
	return @"learningType";
}
+ (NSString *)rating {
	return @"rating";
}
+ (NSString *)senderAvatarUrl {
	return @"senderAvatarUrl";
}
+ (NSString *)senderFullName {
	return @"senderFullName";
}
+ (NSString *)senderId {
	return @"senderId";
}
+ (NSString *)senderJobTitle {
	return @"senderJobTitle";
}
+ (NSString *)siteDescription {
	return @"siteDescription";
}
+ (NSString *)siteImageUrl {
	return @"siteImageUrl";
}
+ (NSString *)siteTitle {
	return @"siteTitle";
}
+ (NSString *)siteUrl {
	return @"siteUrl";
}
+ (NSString *)userComment {
	return @"userComment";
}
+ (NSString *)userRating {
	return @"userRating";
}
@end

@implementation CDLearningRelationships 
+ (NSString *)activityFeed {
	return @"activityFeed";
}
@end

