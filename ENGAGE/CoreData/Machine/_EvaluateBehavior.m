// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to EvaluateBehavior.m instead.

#import "_EvaluateBehavior.h"

@implementation EvaluateBehaviorID
@end

@implementation _EvaluateBehavior

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"EvaluateBehavior" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"EvaluateBehavior";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"EvaluateBehavior" inManagedObjectContext:moc_];
}

- (EvaluateBehaviorID*)objectID {
	return (EvaluateBehaviorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"evaluateStateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"evaluateState"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic evaluateState;

- (int16_t)evaluateStateValue {
	NSNumber *result = [self evaluateState];
	return [result shortValue];
}

- (void)setEvaluateStateValue:(int16_t)value_ {
	[self setEvaluateState:@(value_)];
}

- (int16_t)primitiveEvaluateStateValue {
	NSNumber *result = [self primitiveEvaluateState];
	return [result shortValue];
}

- (void)setPrimitiveEvaluateStateValue:(int16_t)value_ {
	[self setPrimitiveEvaluateState:@(value_)];
}

@dynamic teamUser;

@end

@implementation EvaluateBehaviorAttributes 
+ (NSString *)evaluateState {
	return @"evaluateState";
}
@end

@implementation EvaluateBehaviorRelationships 
+ (NSString *)teamUser {
	return @"teamUser";
}
@end

