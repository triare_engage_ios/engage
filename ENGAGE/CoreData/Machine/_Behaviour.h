// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Behaviour.h instead.

#import <CoreData/CoreData.h>

extern const struct BehaviourAttributes {
	__unsafe_unretained NSString *behaviorDescription;
	__unsafe_unretained NSString *behaviorId;
	__unsafe_unretained NSString *iconUrl;
	__unsafe_unretained NSString *presentatioFileUrl;
	__unsafe_unretained NSString *title;
} BehaviourAttributes;

@interface BehaviourID : NSManagedObjectID {}
@end

@interface _Behaviour : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BehaviourID* objectID;

@property (nonatomic, strong) NSString* behaviorDescription;

//- (BOOL)validateBehaviorDescription:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* behaviorId;

@property (atomic) int64_t behaviorIdValue;
- (int64_t)behaviorIdValue;
- (void)setBehaviorIdValue:(int64_t)value_;

//- (BOOL)validateBehaviorId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* iconUrl;

//- (BOOL)validateIconUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* presentatioFileUrl;

//- (BOOL)validatePresentatioFileUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@end

@interface _Behaviour (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveBehaviorDescription;
- (void)setPrimitiveBehaviorDescription:(NSString*)value;

- (NSNumber*)primitiveBehaviorId;
- (void)setPrimitiveBehaviorId:(NSNumber*)value;

- (int64_t)primitiveBehaviorIdValue;
- (void)setPrimitiveBehaviorIdValue:(int64_t)value_;

- (NSString*)primitiveIconUrl;
- (void)setPrimitiveIconUrl:(NSString*)value;

- (NSString*)primitivePresentatioFileUrl;
- (void)setPrimitivePresentatioFileUrl:(NSString*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

@end
