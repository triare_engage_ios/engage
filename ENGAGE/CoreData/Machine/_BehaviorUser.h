// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BehaviorUser.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface BehaviorUserID : NSManagedObjectID {}
@end

@interface _BehaviorUser : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BehaviorUserID *objectID;

@property (nonatomic, strong, nullable) NSString* avatarUrl;

@property (nonatomic, strong, nullable) NSNumber* behaviorId;

@property (atomic) int64_t behaviorIdValue;
- (int64_t)behaviorIdValue;
- (void)setBehaviorIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* fullName;

@property (nonatomic, strong, nullable) NSNumber* givenCount;

@property (atomic) int64_t givenCountValue;
- (int64_t)givenCountValue;
- (void)setGivenCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* isPointWinner;

@property (atomic) BOOL isPointWinnerValue;
- (BOOL)isPointWinnerValue;
- (void)setIsPointWinnerValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* jobTitle;

@property (nonatomic, strong, nullable) NSNumber* receivedCount;

@property (atomic) int64_t receivedCountValue;
- (int64_t)receivedCountValue;
- (void)setReceivedCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* userId;

@property (atomic) int64_t userIdValue;
- (int64_t)userIdValue;
- (void)setUserIdValue:(int64_t)value_;

@end

@interface _BehaviorUser (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAvatarUrl;
- (void)setPrimitiveAvatarUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveBehaviorId;
- (void)setPrimitiveBehaviorId:(nullable NSNumber*)value;

- (int64_t)primitiveBehaviorIdValue;
- (void)setPrimitiveBehaviorIdValue:(int64_t)value_;

- (nullable NSString*)primitiveFullName;
- (void)setPrimitiveFullName:(nullable NSString*)value;

- (nullable NSNumber*)primitiveGivenCount;
- (void)setPrimitiveGivenCount:(nullable NSNumber*)value;

- (int64_t)primitiveGivenCountValue;
- (void)setPrimitiveGivenCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveIsPointWinner;
- (void)setPrimitiveIsPointWinner:(nullable NSNumber*)value;

- (BOOL)primitiveIsPointWinnerValue;
- (void)setPrimitiveIsPointWinnerValue:(BOOL)value_;

- (nullable NSString*)primitiveJobTitle;
- (void)setPrimitiveJobTitle:(nullable NSString*)value;

- (nullable NSNumber*)primitiveReceivedCount;
- (void)setPrimitiveReceivedCount:(nullable NSNumber*)value;

- (int64_t)primitiveReceivedCountValue;
- (void)setPrimitiveReceivedCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveUserId;
- (void)setPrimitiveUserId:(nullable NSNumber*)value;

- (int64_t)primitiveUserIdValue;
- (void)setPrimitiveUserIdValue:(int64_t)value_;

@end

@interface BehaviorUserAttributes: NSObject 
+ (NSString *)avatarUrl;
+ (NSString *)behaviorId;
+ (NSString *)fullName;
+ (NSString *)givenCount;
+ (NSString *)isPointWinner;
+ (NSString *)jobTitle;
+ (NSString *)receivedCount;
+ (NSString *)userId;
@end

NS_ASSUME_NONNULL_END
