// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to QuestionCustomAnswer.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Question;

@interface QuestionCustomAnswerID : NSManagedObjectID {}
@end

@interface _QuestionCustomAnswer : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) QuestionCustomAnswerID *objectID;

@property (nonatomic, strong, nullable) NSNumber* questionId;

@property (atomic) int64_t questionIdValue;
- (int64_t)questionIdValue;
- (void)setQuestionIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) Question *question;

@end

@interface _QuestionCustomAnswer (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveQuestionId;
- (void)setPrimitiveQuestionId:(nullable NSNumber*)value;

- (int64_t)primitiveQuestionIdValue;
- (void)setPrimitiveQuestionIdValue:(int64_t)value_;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (Question*)primitiveQuestion;
- (void)setPrimitiveQuestion:(Question*)value;

@end

@interface QuestionCustomAnswerAttributes: NSObject 
+ (NSString *)questionId;
+ (NSString *)title;
@end

@interface QuestionCustomAnswerRelationships: NSObject
+ (NSString *)question;
@end

NS_ASSUME_NONNULL_END
