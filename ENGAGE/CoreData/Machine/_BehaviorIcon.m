// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BehaviorIcon.m instead.

#import "_BehaviorIcon.h"

@implementation BehaviorIconID
@end

@implementation _BehaviorIcon

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BehaviorIcon" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BehaviorIcon";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BehaviorIcon" inManagedObjectContext:moc_];
}

- (BehaviorIconID*)objectID {
	return (BehaviorIconID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"iconIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"iconId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic category;

@dynamic iconId;

- (int64_t)iconIdValue {
	NSNumber *result = [self iconId];
	return [result longLongValue];
}

- (void)setIconIdValue:(int64_t)value_ {
	[self setIconId:@(value_)];
}

- (int64_t)primitiveIconIdValue {
	NSNumber *result = [self primitiveIconId];
	return [result longLongValue];
}

- (void)setPrimitiveIconIdValue:(int64_t)value_ {
	[self setPrimitiveIconId:@(value_)];
}

@dynamic iconImageUrl;

@end

@implementation BehaviorIconAttributes 
+ (NSString *)category {
	return @"category";
}
+ (NSString *)iconId {
	return @"iconId";
}
+ (NSString *)iconImageUrl {
	return @"iconImageUrl";
}
@end

