// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Company.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class User;

@interface CompanyID : NSManagedObjectID {}
@end

@interface _Company : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CompanyID *objectID;

@property (nonatomic, strong, nullable) NSString* backgroundUrl;

@property (nonatomic, strong, nullable) NSNumber* companyId;

@property (atomic) int64_t companyIdValue;
- (int64_t)companyIdValue;
- (void)setCompanyIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSDate* createdAt;

@property (nonatomic, strong, nullable) NSString* logoUrl;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* plan;

@property (nonatomic, strong, nullable) User *user;

@end

@interface _Company (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBackgroundUrl;
- (void)setPrimitiveBackgroundUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveCompanyId;
- (void)setPrimitiveCompanyId:(nullable NSNumber*)value;

- (int64_t)primitiveCompanyIdValue;
- (void)setPrimitiveCompanyIdValue:(int64_t)value_;

- (nullable NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(nullable NSDate*)value;

- (nullable NSString*)primitiveLogoUrl;
- (void)setPrimitiveLogoUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSString*)primitivePlan;
- (void)setPrimitivePlan:(nullable NSString*)value;

- (User*)primitiveUser;
- (void)setPrimitiveUser:(User*)value;

@end

@interface CompanyAttributes: NSObject 
+ (NSString *)backgroundUrl;
+ (NSString *)companyId;
+ (NSString *)createdAt;
+ (NSString *)logoUrl;
+ (NSString *)name;
+ (NSString *)plan;
@end

@interface CompanyRelationships: NSObject
+ (NSString *)user;
@end

NS_ASSUME_NONNULL_END
