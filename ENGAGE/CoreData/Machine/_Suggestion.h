// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Suggestion.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface SuggestionID : NSManagedObjectID {}
@end

@interface _Suggestion : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SuggestionID *objectID;

@property (nonatomic, strong, nullable) NSString* body;

@property (nonatomic, strong, nullable) NSDate* createDate;

@property (nonatomic, strong, nullable) NSNumber* isRead;

@property (atomic) BOOL isReadValue;
- (BOOL)isReadValue;
- (void)setIsReadValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* senderAvatarUrl;

@property (nonatomic, strong, nullable) NSNumber* senderId;

@property (atomic) int64_t senderIdValue;
- (int64_t)senderIdValue;
- (void)setSenderIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* senderName;

@property (nonatomic, strong, nullable) NSNumber* suggestionId;

@property (atomic) int64_t suggestionIdValue;
- (int64_t)suggestionIdValue;
- (void)setSuggestionIdValue:(int64_t)value_;

@end

@interface _Suggestion (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBody;
- (void)setPrimitiveBody:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreateDate;
- (void)setPrimitiveCreateDate:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveIsRead;
- (void)setPrimitiveIsRead:(nullable NSNumber*)value;

- (BOOL)primitiveIsReadValue;
- (void)setPrimitiveIsReadValue:(BOOL)value_;

- (nullable NSString*)primitiveSenderAvatarUrl;
- (void)setPrimitiveSenderAvatarUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveSenderId;
- (void)setPrimitiveSenderId:(nullable NSNumber*)value;

- (int64_t)primitiveSenderIdValue;
- (void)setPrimitiveSenderIdValue:(int64_t)value_;

- (nullable NSString*)primitiveSenderName;
- (void)setPrimitiveSenderName:(nullable NSString*)value;

- (nullable NSNumber*)primitiveSuggestionId;
- (void)setPrimitiveSuggestionId:(nullable NSNumber*)value;

- (int64_t)primitiveSuggestionIdValue;
- (void)setPrimitiveSuggestionIdValue:(int64_t)value_;

@end

@interface SuggestionAttributes: NSObject 
+ (NSString *)body;
+ (NSString *)createDate;
+ (NSString *)isRead;
+ (NSString *)senderAvatarUrl;
+ (NSString *)senderId;
+ (NSString *)senderName;
+ (NSString *)suggestionId;
@end

NS_ASSUME_NONNULL_END
