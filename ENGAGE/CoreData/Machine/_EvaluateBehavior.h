// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to EvaluateBehavior.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

#import "Behavior.h"

NS_ASSUME_NONNULL_BEGIN

@class TeamUser;

@interface EvaluateBehaviorID : BehaviorID {}
@end

@interface _EvaluateBehavior : Behavior
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) EvaluateBehaviorID *objectID;

@property (nonatomic, strong, nullable) NSNumber* evaluateState;

@property (atomic) int16_t evaluateStateValue;
- (int16_t)evaluateStateValue;
- (void)setEvaluateStateValue:(int16_t)value_;

@property (nonatomic, strong, nullable) TeamUser *teamUser;

@end

@interface _EvaluateBehavior (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveEvaluateState;
- (void)setPrimitiveEvaluateState:(nullable NSNumber*)value;

- (int16_t)primitiveEvaluateStateValue;
- (void)setPrimitiveEvaluateStateValue:(int16_t)value_;

- (TeamUser*)primitiveTeamUser;
- (void)setPrimitiveTeamUser:(TeamUser*)value;

@end

@interface EvaluateBehaviorAttributes: NSObject 
+ (NSString *)evaluateState;
@end

@interface EvaluateBehaviorRelationships: NSObject
+ (NSString *)teamUser;
@end

NS_ASSUME_NONNULL_END
