// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityFeed.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Challenge;
@class Comment;
@class CDLearning;
@class TopPoint;

@interface ActivityFeedID : NSManagedObjectID {}
@end

@interface _ActivityFeed : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityFeedID *objectID;

@property (nonatomic, strong, nullable) NSString* behaviourIconUrl;

@property (nonatomic, strong, nullable) NSNumber* behaviourId;

@property (atomic) int64_t behaviourIdValue;
- (int64_t)behaviourIdValue;
- (void)setBehaviourIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* behaviourTitle;

@property (nonatomic, strong, nullable) NSString* body;

@property (nonatomic, strong, nullable) NSNumber* commentsCount;

@property (atomic) int16_t commentsCountValue;
- (int16_t)commentsCountValue;
- (void)setCommentsCountValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSDate* createDate;

@property (nonatomic, strong, nullable) NSNumber* feedId;

@property (atomic) int64_t feedIdValue;
- (int64_t)feedIdValue;
- (void)setFeedIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* isReceiverPointWinner;

@property (atomic) double isReceiverPointWinnerValue;
- (double)isReceiverPointWinnerValue;
- (void)setIsReceiverPointWinnerValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* isRetaped;

@property (atomic) BOOL isRetapedValue;
- (BOOL)isRetapedValue;
- (void)setIsRetapedValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isTeamMessage;

@property (atomic) BOOL isTeamMessageValue;
- (BOOL)isTeamMessageValue;
- (void)setIsTeamMessageValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isTopPoint;

@property (atomic) BOOL isTopPointValue;
- (BOOL)isTopPointValue;
- (void)setIsTopPointValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* itemType;

@property (nonatomic, strong, nullable) NSString* receiverAvatarUrl;

@property (nonatomic, strong, nullable) NSNumber* receiverId;

@property (atomic) int64_t receiverIdValue;
- (int64_t)receiverIdValue;
- (void)setReceiverIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* receiverJob;

@property (nonatomic, strong, nullable) NSString* receiverName;

@property (nonatomic, strong, nullable) NSNumber* retapsCount;

@property (atomic) int16_t retapsCountValue;
- (int16_t)retapsCountValue;
- (void)setRetapsCountValue:(int16_t)value_;

@property (nonatomic, strong, nullable) NSString* senderAvatarUrl;

@property (nonatomic, strong, nullable) NSNumber* senderId;

@property (atomic) int64_t senderIdValue;
- (int64_t)senderIdValue;
- (void)setSenderIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* senderName;

@property (nonatomic, strong, nullable) Challenge *challenge;

@property (nonatomic, strong, nullable) NSSet<Comment*> *comments;
- (nullable NSMutableSet<Comment*>*)commentsSet;

@property (nonatomic, strong, nullable) CDLearning *learning;

@property (nonatomic, strong, nullable) TopPoint *topPoint;

@end

@interface _ActivityFeed (CommentsCoreDataGeneratedAccessors)
- (void)addComments:(NSSet<Comment*>*)value_;
- (void)removeComments:(NSSet<Comment*>*)value_;
- (void)addCommentsObject:(Comment*)value_;
- (void)removeCommentsObject:(Comment*)value_;

@end

@interface _ActivityFeed (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBehaviourIconUrl;
- (void)setPrimitiveBehaviourIconUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveBehaviourId;
- (void)setPrimitiveBehaviourId:(nullable NSNumber*)value;

- (int64_t)primitiveBehaviourIdValue;
- (void)setPrimitiveBehaviourIdValue:(int64_t)value_;

- (nullable NSString*)primitiveBehaviourTitle;
- (void)setPrimitiveBehaviourTitle:(nullable NSString*)value;

- (nullable NSString*)primitiveBody;
- (void)setPrimitiveBody:(nullable NSString*)value;

- (nullable NSNumber*)primitiveCommentsCount;
- (void)setPrimitiveCommentsCount:(nullable NSNumber*)value;

- (int16_t)primitiveCommentsCountValue;
- (void)setPrimitiveCommentsCountValue:(int16_t)value_;

- (nullable NSDate*)primitiveCreateDate;
- (void)setPrimitiveCreateDate:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveFeedId;
- (void)setPrimitiveFeedId:(nullable NSNumber*)value;

- (int64_t)primitiveFeedIdValue;
- (void)setPrimitiveFeedIdValue:(int64_t)value_;

- (nullable NSNumber*)primitiveIsReceiverPointWinner;
- (void)setPrimitiveIsReceiverPointWinner:(nullable NSNumber*)value;

- (double)primitiveIsReceiverPointWinnerValue;
- (void)setPrimitiveIsReceiverPointWinnerValue:(double)value_;

- (nullable NSNumber*)primitiveIsRetaped;
- (void)setPrimitiveIsRetaped:(nullable NSNumber*)value;

- (BOOL)primitiveIsRetapedValue;
- (void)setPrimitiveIsRetapedValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsTeamMessage;
- (void)setPrimitiveIsTeamMessage:(nullable NSNumber*)value;

- (BOOL)primitiveIsTeamMessageValue;
- (void)setPrimitiveIsTeamMessageValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsTopPoint;
- (void)setPrimitiveIsTopPoint:(nullable NSNumber*)value;

- (BOOL)primitiveIsTopPointValue;
- (void)setPrimitiveIsTopPointValue:(BOOL)value_;

- (nullable NSString*)primitiveItemType;
- (void)setPrimitiveItemType:(nullable NSString*)value;

- (nullable NSString*)primitiveReceiverAvatarUrl;
- (void)setPrimitiveReceiverAvatarUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveReceiverId;
- (void)setPrimitiveReceiverId:(nullable NSNumber*)value;

- (int64_t)primitiveReceiverIdValue;
- (void)setPrimitiveReceiverIdValue:(int64_t)value_;

- (nullable NSString*)primitiveReceiverJob;
- (void)setPrimitiveReceiverJob:(nullable NSString*)value;

- (nullable NSString*)primitiveReceiverName;
- (void)setPrimitiveReceiverName:(nullable NSString*)value;

- (nullable NSNumber*)primitiveRetapsCount;
- (void)setPrimitiveRetapsCount:(nullable NSNumber*)value;

- (int16_t)primitiveRetapsCountValue;
- (void)setPrimitiveRetapsCountValue:(int16_t)value_;

- (nullable NSString*)primitiveSenderAvatarUrl;
- (void)setPrimitiveSenderAvatarUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveSenderId;
- (void)setPrimitiveSenderId:(nullable NSNumber*)value;

- (int64_t)primitiveSenderIdValue;
- (void)setPrimitiveSenderIdValue:(int64_t)value_;

- (nullable NSString*)primitiveSenderName;
- (void)setPrimitiveSenderName:(nullable NSString*)value;

- (Challenge*)primitiveChallenge;
- (void)setPrimitiveChallenge:(Challenge*)value;

- (NSMutableSet<Comment*>*)primitiveComments;
- (void)setPrimitiveComments:(NSMutableSet<Comment*>*)value;

- (CDLearning*)primitiveLearning;
- (void)setPrimitiveLearning:(CDLearning*)value;

- (TopPoint*)primitiveTopPoint;
- (void)setPrimitiveTopPoint:(TopPoint*)value;

@end

@interface ActivityFeedAttributes: NSObject 
+ (NSString *)behaviourIconUrl;
+ (NSString *)behaviourId;
+ (NSString *)behaviourTitle;
+ (NSString *)body;
+ (NSString *)commentsCount;
+ (NSString *)createDate;
+ (NSString *)feedId;
+ (NSString *)isReceiverPointWinner;
+ (NSString *)isRetaped;
+ (NSString *)isTeamMessage;
+ (NSString *)isTopPoint;
+ (NSString *)itemType;
+ (NSString *)receiverAvatarUrl;
+ (NSString *)receiverId;
+ (NSString *)receiverJob;
+ (NSString *)receiverName;
+ (NSString *)retapsCount;
+ (NSString *)senderAvatarUrl;
+ (NSString *)senderId;
+ (NSString *)senderName;
@end

@interface ActivityFeedRelationships: NSObject
+ (NSString *)challenge;
+ (NSString *)comments;
+ (NSString *)learning;
+ (NSString *)topPoint;
@end

NS_ASSUME_NONNULL_END
