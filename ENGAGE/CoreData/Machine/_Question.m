// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Question.m instead.

#import "_Question.h"

@implementation QuestionID
@end

@implementation _Question

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Question" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Question";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Question" inManagedObjectContext:moc_];
}

- (QuestionID*)objectID {
	return (QuestionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"answeredUsersCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"answeredUsersCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"answersCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"answersCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"customAnswersCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"customAnswersCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"haveCustomAnswersValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"haveCustomAnswers"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isMultipleAnswerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isMultipleAnswer"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"questionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"questionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"usersCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"usersCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic answeredUsersCount;

- (int64_t)answeredUsersCountValue {
	NSNumber *result = [self answeredUsersCount];
	return [result longLongValue];
}

- (void)setAnsweredUsersCountValue:(int64_t)value_ {
	[self setAnsweredUsersCount:@(value_)];
}

- (int64_t)primitiveAnsweredUsersCountValue {
	NSNumber *result = [self primitiveAnsweredUsersCount];
	return [result longLongValue];
}

- (void)setPrimitiveAnsweredUsersCountValue:(int64_t)value_ {
	[self setPrimitiveAnsweredUsersCount:@(value_)];
}

@dynamic answersCount;

- (int64_t)answersCountValue {
	NSNumber *result = [self answersCount];
	return [result longLongValue];
}

- (void)setAnswersCountValue:(int64_t)value_ {
	[self setAnswersCount:@(value_)];
}

- (int64_t)primitiveAnswersCountValue {
	NSNumber *result = [self primitiveAnswersCount];
	return [result longLongValue];
}

- (void)setPrimitiveAnswersCountValue:(int64_t)value_ {
	[self setPrimitiveAnswersCount:@(value_)];
}

@dynamic body;

@dynamic createdAt;

@dynamic customAnswersCount;

- (int64_t)customAnswersCountValue {
	NSNumber *result = [self customAnswersCount];
	return [result longLongValue];
}

- (void)setCustomAnswersCountValue:(int64_t)value_ {
	[self setCustomAnswersCount:@(value_)];
}

- (int64_t)primitiveCustomAnswersCountValue {
	NSNumber *result = [self primitiveCustomAnswersCount];
	return [result longLongValue];
}

- (void)setPrimitiveCustomAnswersCountValue:(int64_t)value_ {
	[self setPrimitiveCustomAnswersCount:@(value_)];
}

@dynamic haveCustomAnswers;

- (BOOL)haveCustomAnswersValue {
	NSNumber *result = [self haveCustomAnswers];
	return [result boolValue];
}

- (void)setHaveCustomAnswersValue:(BOOL)value_ {
	[self setHaveCustomAnswers:@(value_)];
}

- (BOOL)primitiveHaveCustomAnswersValue {
	NSNumber *result = [self primitiveHaveCustomAnswers];
	return [result boolValue];
}

- (void)setPrimitiveHaveCustomAnswersValue:(BOOL)value_ {
	[self setPrimitiveHaveCustomAnswers:@(value_)];
}

@dynamic isMultipleAnswer;

- (BOOL)isMultipleAnswerValue {
	NSNumber *result = [self isMultipleAnswer];
	return [result boolValue];
}

- (void)setIsMultipleAnswerValue:(BOOL)value_ {
	[self setIsMultipleAnswer:@(value_)];
}

- (BOOL)primitiveIsMultipleAnswerValue {
	NSNumber *result = [self primitiveIsMultipleAnswer];
	return [result boolValue];
}

- (void)setPrimitiveIsMultipleAnswerValue:(BOOL)value_ {
	[self setPrimitiveIsMultipleAnswer:@(value_)];
}

@dynamic questionId;

- (int64_t)questionIdValue {
	NSNumber *result = [self questionId];
	return [result longLongValue];
}

- (void)setQuestionIdValue:(int64_t)value_ {
	[self setQuestionId:@(value_)];
}

- (int64_t)primitiveQuestionIdValue {
	NSNumber *result = [self primitiveQuestionId];
	return [result longLongValue];
}

- (void)setPrimitiveQuestionIdValue:(int64_t)value_ {
	[self setPrimitiveQuestionId:@(value_)];
}

@dynamic startsAt;

@dynamic usersCount;

- (int64_t)usersCountValue {
	NSNumber *result = [self usersCount];
	return [result longLongValue];
}

- (void)setUsersCountValue:(int64_t)value_ {
	[self setUsersCount:@(value_)];
}

- (int64_t)primitiveUsersCountValue {
	NSNumber *result = [self primitiveUsersCount];
	return [result longLongValue];
}

- (void)setPrimitiveUsersCountValue:(int64_t)value_ {
	[self setPrimitiveUsersCount:@(value_)];
}

@dynamic answers;

- (NSMutableOrderedSet<QuestionAnswer*>*)answersSet {
	[self willAccessValueForKey:@"answers"];

	NSMutableOrderedSet<QuestionAnswer*> *result = (NSMutableOrderedSet<QuestionAnswer*>*)[self mutableOrderedSetValueForKey:@"answers"];

	[self didAccessValueForKey:@"answers"];
	return result;
}

@dynamic customAnswers;

- (NSMutableOrderedSet<QuestionCustomAnswer*>*)customAnswersSet {
	[self willAccessValueForKey:@"customAnswers"];

	NSMutableOrderedSet<QuestionCustomAnswer*> *result = (NSMutableOrderedSet<QuestionCustomAnswer*>*)[self mutableOrderedSetValueForKey:@"customAnswers"];

	[self didAccessValueForKey:@"customAnswers"];
	return result;
}

@end

@implementation _Question (AnswersCoreDataGeneratedAccessors)
- (void)addAnswers:(NSOrderedSet<QuestionAnswer*>*)value_ {
	[self.answersSet unionOrderedSet:value_];
}
- (void)removeAnswers:(NSOrderedSet<QuestionAnswer*>*)value_ {
	[self.answersSet minusOrderedSet:value_];
}
- (void)addAnswersObject:(QuestionAnswer*)value_ {
	[self.answersSet addObject:value_];
}
- (void)removeAnswersObject:(QuestionAnswer*)value_ {
	[self.answersSet removeObject:value_];
}
- (void)insertObject:(QuestionAnswer*)value inAnswersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answers"];
}
- (void)removeObjectFromAnswersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answers"];
}
- (void)insertAnswers:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answers"];
}
- (void)removeAnswersAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answers"];
}
- (void)replaceObjectInAnswersAtIndex:(NSUInteger)idx withObject:(QuestionAnswer*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answers"];
}
- (void)replaceAnswersAtIndexes:(NSIndexSet *)indexes withAnswers:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answers"];
}
@end

@implementation _Question (CustomAnswersCoreDataGeneratedAccessors)
- (void)addCustomAnswers:(NSOrderedSet<QuestionCustomAnswer*>*)value_ {
	[self.customAnswersSet unionOrderedSet:value_];
}
- (void)removeCustomAnswers:(NSOrderedSet<QuestionCustomAnswer*>*)value_ {
	[self.customAnswersSet minusOrderedSet:value_];
}
- (void)addCustomAnswersObject:(QuestionCustomAnswer*)value_ {
	[self.customAnswersSet addObject:value_];
}
- (void)removeCustomAnswersObject:(QuestionCustomAnswer*)value_ {
	[self.customAnswersSet removeObject:value_];
}
- (void)insertObject:(QuestionCustomAnswer*)value inCustomAnswersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"customAnswers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self customAnswers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"customAnswers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"customAnswers"];
}
- (void)removeObjectFromCustomAnswersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"customAnswers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self customAnswers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"customAnswers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"customAnswers"];
}
- (void)insertCustomAnswers:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"customAnswers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self customAnswers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"customAnswers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"customAnswers"];
}
- (void)removeCustomAnswersAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"customAnswers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self customAnswers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"customAnswers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"customAnswers"];
}
- (void)replaceObjectInCustomAnswersAtIndex:(NSUInteger)idx withObject:(QuestionCustomAnswer*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"customAnswers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self customAnswers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"customAnswers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"customAnswers"];
}
- (void)replaceCustomAnswersAtIndexes:(NSIndexSet *)indexes withCustomAnswers:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"customAnswers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self customAnswers] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"customAnswers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"customAnswers"];
}
@end

@implementation QuestionAttributes 
+ (NSString *)answeredUsersCount {
	return @"answeredUsersCount";
}
+ (NSString *)answersCount {
	return @"answersCount";
}
+ (NSString *)body {
	return @"body";
}
+ (NSString *)createdAt {
	return @"createdAt";
}
+ (NSString *)customAnswersCount {
	return @"customAnswersCount";
}
+ (NSString *)haveCustomAnswers {
	return @"haveCustomAnswers";
}
+ (NSString *)isMultipleAnswer {
	return @"isMultipleAnswer";
}
+ (NSString *)questionId {
	return @"questionId";
}
+ (NSString *)startsAt {
	return @"startsAt";
}
+ (NSString *)usersCount {
	return @"usersCount";
}
@end

@implementation QuestionRelationships 
+ (NSString *)answers {
	return @"answers";
}
+ (NSString *)customAnswers {
	return @"customAnswers";
}
@end

