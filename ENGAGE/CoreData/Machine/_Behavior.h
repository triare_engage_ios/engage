// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Behavior.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Value;

@interface BehaviorID : NSManagedObjectID {}
@end

@interface _Behavior : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BehaviorID *objectID;

@property (nonatomic, strong, nullable) NSString* behaviorDescription;

@property (nonatomic, strong, nullable) NSNumber* behaviorId;

@property (atomic) int64_t behaviorIdValue;
- (int64_t)behaviorIdValue;
- (void)setBehaviorIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* iconUrl;

@property (nonatomic, strong, nullable) NSNumber* isActive;

@property (atomic) BOOL isActiveValue;
- (BOOL)isActiveValue;
- (void)setIsActiveValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* presentatioFileUrl;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) NSNumber* valueId;

@property (atomic) int64_t valueIdValue;
- (int64_t)valueIdValue;
- (void)setValueIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) Value *value;

@end

@interface _Behavior (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBehaviorDescription;
- (void)setPrimitiveBehaviorDescription:(nullable NSString*)value;

- (nullable NSNumber*)primitiveBehaviorId;
- (void)setPrimitiveBehaviorId:(nullable NSNumber*)value;

- (int64_t)primitiveBehaviorIdValue;
- (void)setPrimitiveBehaviorIdValue:(int64_t)value_;

- (nullable NSString*)primitiveIconUrl;
- (void)setPrimitiveIconUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveIsActive;
- (void)setPrimitiveIsActive:(nullable NSNumber*)value;

- (BOOL)primitiveIsActiveValue;
- (void)setPrimitiveIsActiveValue:(BOOL)value_;

- (nullable NSString*)primitivePresentatioFileUrl;
- (void)setPrimitivePresentatioFileUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (nullable NSNumber*)primitiveValueId;
- (void)setPrimitiveValueId:(nullable NSNumber*)value;

- (int64_t)primitiveValueIdValue;
- (void)setPrimitiveValueIdValue:(int64_t)value_;

- (Value*)primitiveValue;
- (void)setPrimitiveValue:(Value*)value;

@end

@interface BehaviorAttributes: NSObject 
+ (NSString *)behaviorDescription;
+ (NSString *)behaviorId;
+ (NSString *)iconUrl;
+ (NSString *)isActive;
+ (NSString *)presentatioFileUrl;
+ (NSString *)title;
+ (NSString *)valueId;
@end

@interface BehaviorRelationships: NSObject
+ (NSString *)value;
@end

NS_ASSUME_NONNULL_END
