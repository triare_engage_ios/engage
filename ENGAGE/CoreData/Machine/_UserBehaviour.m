// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserBehaviour.m instead.

#import "_UserBehaviour.h"

const struct UserBehaviourAttributes UserBehaviourAttributes = {
	.behaviourId = @"behaviourId",
	.count = @"count",
	.iconUrl = @"iconUrl",
	.title = @"title",
};

const struct UserBehaviourRelationships UserBehaviourRelationships = {
	.teamUser = @"teamUser",
};

@implementation UserBehaviourID
@end

@implementation _UserBehaviour

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserBehaviour" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserBehaviour";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserBehaviour" inManagedObjectContext:moc_];
}

- (UserBehaviourID*)objectID {
	return (UserBehaviourID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"behaviourIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"behaviourId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic behaviourId;

- (int64_t)behaviourIdValue {
	NSNumber *result = [self behaviourId];
	return [result longLongValue];
}

- (void)setBehaviourIdValue:(int64_t)value_ {
	[self setBehaviourId:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBehaviourIdValue {
	NSNumber *result = [self primitiveBehaviourId];
	return [result longLongValue];
}

- (void)setPrimitiveBehaviourIdValue:(int64_t)value_ {
	[self setPrimitiveBehaviourId:[NSNumber numberWithLongLong:value_]];
}

@dynamic count;

- (int16_t)countValue {
	NSNumber *result = [self count];
	return [result shortValue];
}

- (void)setCountValue:(int16_t)value_ {
	[self setCount:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCountValue {
	NSNumber *result = [self primitiveCount];
	return [result shortValue];
}

- (void)setPrimitiveCountValue:(int16_t)value_ {
	[self setPrimitiveCount:[NSNumber numberWithShort:value_]];
}

@dynamic iconUrl;

@dynamic title;

@dynamic teamUser;

@end

