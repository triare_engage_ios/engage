// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TopPoint.m instead.

#import "_TopPoint.h"

@implementation TopPointID
@end

@implementation _TopPoint

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TopPoint" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TopPoint";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TopPoint" inManagedObjectContext:moc_];
}

- (TopPointID*)objectID {
	return (TopPointID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"feedIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"feedId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic count;

- (int64_t)countValue {
	NSNumber *result = [self count];
	return [result longLongValue];
}

- (void)setCountValue:(int64_t)value_ {
	[self setCount:@(value_)];
}

- (int64_t)primitiveCountValue {
	NSNumber *result = [self primitiveCount];
	return [result longLongValue];
}

- (void)setPrimitiveCountValue:(int64_t)value_ {
	[self setPrimitiveCount:@(value_)];
}

@dynamic feedId;

- (int64_t)feedIdValue {
	NSNumber *result = [self feedId];
	return [result longLongValue];
}

- (void)setFeedIdValue:(int64_t)value_ {
	[self setFeedId:@(value_)];
}

- (int64_t)primitiveFeedIdValue {
	NSNumber *result = [self primitiveFeedId];
	return [result longLongValue];
}

- (void)setPrimitiveFeedIdValue:(int64_t)value_ {
	[self setPrimitiveFeedId:@(value_)];
}

@dynamic imageUrl;

@dynamic feed;

@end

@implementation TopPointAttributes 
+ (NSString *)count {
	return @"count";
}
+ (NSString *)feedId {
	return @"feedId";
}
+ (NSString *)imageUrl {
	return @"imageUrl";
}
@end

@implementation TopPointRelationships 
+ (NSString *)feed {
	return @"feed";
}
@end

