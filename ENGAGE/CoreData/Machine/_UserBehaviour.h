// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserBehaviour.h instead.

#import <CoreData/CoreData.h>

extern const struct UserBehaviourAttributes {
	__unsafe_unretained NSString *behaviourId;
	__unsafe_unretained NSString *count;
	__unsafe_unretained NSString *iconUrl;
	__unsafe_unretained NSString *title;
} UserBehaviourAttributes;

extern const struct UserBehaviourRelationships {
	__unsafe_unretained NSString *teamUser;
} UserBehaviourRelationships;

@class TeamUser;

@interface UserBehaviourID : NSManagedObjectID {}
@end

@interface _UserBehaviour : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserBehaviourID* objectID;

@property (nonatomic, strong) NSNumber* behaviourId;

@property (atomic) int64_t behaviourIdValue;
- (int64_t)behaviourIdValue;
- (void)setBehaviourIdValue:(int64_t)value_;

//- (BOOL)validateBehaviourId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* count;

@property (atomic) int16_t countValue;
- (int16_t)countValue;
- (void)setCountValue:(int16_t)value_;

//- (BOOL)validateCount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* iconUrl;

//- (BOOL)validateIconUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) TeamUser *teamUser;

//- (BOOL)validateTeamUser:(id*)value_ error:(NSError**)error_;

@end

@interface _UserBehaviour (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveBehaviourId;
- (void)setPrimitiveBehaviourId:(NSNumber*)value;

- (int64_t)primitiveBehaviourIdValue;
- (void)setPrimitiveBehaviourIdValue:(int64_t)value_;

- (NSNumber*)primitiveCount;
- (void)setPrimitiveCount:(NSNumber*)value;

- (int16_t)primitiveCountValue;
- (void)setPrimitiveCountValue:(int16_t)value_;

- (NSString*)primitiveIconUrl;
- (void)setPrimitiveIconUrl:(NSString*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (TeamUser*)primitiveTeamUser;
- (void)setPrimitiveTeamUser:(TeamUser*)value;

@end
