// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TeamMessage.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface TeamMessageID : NSManagedObjectID {}
@end

@interface _TeamMessage : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TeamMessageID *objectID;

@property (nonatomic, strong, nullable) NSString* body;

@property (nonatomic, strong, nullable) NSDate* createDate;

@property (nonatomic, strong, nullable) NSNumber* messageId;

@property (atomic) int64_t messageIdValue;
- (int64_t)messageIdValue;
- (void)setMessageIdValue:(int64_t)value_;

@end

@interface _TeamMessage (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBody;
- (void)setPrimitiveBody:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreateDate;
- (void)setPrimitiveCreateDate:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveMessageId;
- (void)setPrimitiveMessageId:(nullable NSNumber*)value;

- (int64_t)primitiveMessageIdValue;
- (void)setPrimitiveMessageIdValue:(int64_t)value_;

@end

@interface TeamMessageAttributes: NSObject 
+ (NSString *)body;
+ (NSString *)createDate;
+ (NSString *)messageId;
@end

NS_ASSUME_NONNULL_END
