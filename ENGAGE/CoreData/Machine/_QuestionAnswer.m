// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to QuestionAnswer.m instead.

#import "_QuestionAnswer.h"

@implementation QuestionAnswerID
@end

@implementation _QuestionAnswer

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"QuestionAnswer" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"QuestionAnswer";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"QuestionAnswer" inManagedObjectContext:moc_];
}

- (QuestionAnswerID*)objectID {
	return (QuestionAnswerID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"answersCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"answersCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"questionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"questionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic answersCount;

- (int64_t)answersCountValue {
	NSNumber *result = [self answersCount];
	return [result longLongValue];
}

- (void)setAnswersCountValue:(int64_t)value_ {
	[self setAnswersCount:@(value_)];
}

- (int64_t)primitiveAnswersCountValue {
	NSNumber *result = [self primitiveAnswersCount];
	return [result longLongValue];
}

- (void)setPrimitiveAnswersCountValue:(int64_t)value_ {
	[self setPrimitiveAnswersCount:@(value_)];
}

@dynamic questionId;

- (int64_t)questionIdValue {
	NSNumber *result = [self questionId];
	return [result longLongValue];
}

- (void)setQuestionIdValue:(int64_t)value_ {
	[self setQuestionId:@(value_)];
}

- (int64_t)primitiveQuestionIdValue {
	NSNumber *result = [self primitiveQuestionId];
	return [result longLongValue];
}

- (void)setPrimitiveQuestionIdValue:(int64_t)value_ {
	[self setPrimitiveQuestionId:@(value_)];
}

@dynamic title;

@dynamic question;

@end

@implementation QuestionAnswerAttributes 
+ (NSString *)answersCount {
	return @"answersCount";
}
+ (NSString *)questionId {
	return @"questionId";
}
+ (NSString *)title {
	return @"title";
}
@end

@implementation QuestionAnswerRelationships 
+ (NSString *)question {
	return @"question";
}
@end

