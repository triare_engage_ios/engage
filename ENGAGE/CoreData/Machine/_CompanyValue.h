// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CompanyValue.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface CompanyValueID : NSManagedObjectID {}
@end

@interface _CompanyValue : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CompanyValueID *objectID;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) NSNumber* valueId;

@property (atomic) int64_t valueIdValue;
- (int64_t)valueIdValue;
- (void)setValueIdValue:(int64_t)value_;

@end

@interface _CompanyValue (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (nullable NSNumber*)primitiveValueId;
- (void)setPrimitiveValueId:(nullable NSNumber*)value;

- (int64_t)primitiveValueIdValue;
- (void)setPrimitiveValueIdValue:(int64_t)value_;

@end

@interface CompanyValueAttributes: NSObject 
+ (NSString *)title;
+ (NSString *)valueId;
@end

NS_ASSUME_NONNULL_END
