// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.m instead.

#import "_User.h"

@implementation UserID
@end

@implementation _User

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"User";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"User" inManagedObjectContext:moc_];
}

- (UserID*)objectID {
	return (UserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"userIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic avatarUrl;

@dynamic email;

@dynamic firstName;

@dynamic fullName;

@dynamic jobTitle;

@dynamic lastName;

@dynamic pushDeviceToken;

@dynamic role;

@dynamic userId;

- (int64_t)userIdValue {
	NSNumber *result = [self userId];
	return [result longLongValue];
}

- (void)setUserIdValue:(int64_t)value_ {
	[self setUserId:@(value_)];
}

- (int64_t)primitiveUserIdValue {
	NSNumber *result = [self primitiveUserId];
	return [result longLongValue];
}

- (void)setPrimitiveUserIdValue:(int64_t)value_ {
	[self setPrimitiveUserId:@(value_)];
}

@dynamic company;

@end

@implementation UserAttributes 
+ (NSString *)avatarUrl {
	return @"avatarUrl";
}
+ (NSString *)email {
	return @"email";
}
+ (NSString *)firstName {
	return @"firstName";
}
+ (NSString *)fullName {
	return @"fullName";
}
+ (NSString *)jobTitle {
	return @"jobTitle";
}
+ (NSString *)lastName {
	return @"lastName";
}
+ (NSString *)pushDeviceToken {
	return @"pushDeviceToken";
}
+ (NSString *)role {
	return @"role";
}
+ (NSString *)userId {
	return @"userId";
}
@end

@implementation UserRelationships 
+ (NSString *)company {
	return @"company";
}
@end

