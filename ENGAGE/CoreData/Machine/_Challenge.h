// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Challenge.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class ActivityFeed;

@interface ChallengeID : NSManagedObjectID {}
@end

@interface _Challenge : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ChallengeID *objectID;

@property (nonatomic, strong, nullable) NSString* backgroundImageUrl;

@property (nonatomic, strong, nullable) NSString* borderColorHex;

@property (nonatomic, strong, nullable) NSString* challengeDescription;

@property (nonatomic, strong, nullable) NSDate* createDate;

@property (nonatomic, strong, nullable) NSNumber* feedId;

@property (atomic) int64_t feedIdValue;
- (int64_t)feedIdValue;
- (void)setFeedIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* imageIconUrl;

@property (nonatomic, strong, nullable) NSNumber* isActivityFeedChallenge;

@property (atomic) BOOL isActivityFeedChallengeValue;
- (BOOL)isActivityFeedChallengeValue;
- (void)setIsActivityFeedChallengeValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isCurrentChallenge;

@property (atomic) BOOL isCurrentChallengeValue;
- (BOOL)isCurrentChallengeValue;
- (void)setIsCurrentChallengeValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* receiverAvatarUrl;

@property (nonatomic, strong, nullable) NSNumber* receiverId;

@property (atomic) int64_t receiverIdValue;
- (int64_t)receiverIdValue;
- (void)setReceiverIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* receiverJob;

@property (nonatomic, strong, nullable) NSString* receiverName;

@property (nonatomic, strong, nullable) NSString* state;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) NSDate* untilDate;

@property (nonatomic, strong, nullable) ActivityFeed *feed;

@end

@interface _Challenge (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBackgroundImageUrl;
- (void)setPrimitiveBackgroundImageUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveBorderColorHex;
- (void)setPrimitiveBorderColorHex:(nullable NSString*)value;

- (nullable NSString*)primitiveChallengeDescription;
- (void)setPrimitiveChallengeDescription:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreateDate;
- (void)setPrimitiveCreateDate:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveFeedId;
- (void)setPrimitiveFeedId:(nullable NSNumber*)value;

- (int64_t)primitiveFeedIdValue;
- (void)setPrimitiveFeedIdValue:(int64_t)value_;

- (nullable NSString*)primitiveImageIconUrl;
- (void)setPrimitiveImageIconUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveIsActivityFeedChallenge;
- (void)setPrimitiveIsActivityFeedChallenge:(nullable NSNumber*)value;

- (BOOL)primitiveIsActivityFeedChallengeValue;
- (void)setPrimitiveIsActivityFeedChallengeValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsCurrentChallenge;
- (void)setPrimitiveIsCurrentChallenge:(nullable NSNumber*)value;

- (BOOL)primitiveIsCurrentChallengeValue;
- (void)setPrimitiveIsCurrentChallengeValue:(BOOL)value_;

- (nullable NSString*)primitiveReceiverAvatarUrl;
- (void)setPrimitiveReceiverAvatarUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveReceiverId;
- (void)setPrimitiveReceiverId:(nullable NSNumber*)value;

- (int64_t)primitiveReceiverIdValue;
- (void)setPrimitiveReceiverIdValue:(int64_t)value_;

- (nullable NSString*)primitiveReceiverJob;
- (void)setPrimitiveReceiverJob:(nullable NSString*)value;

- (nullable NSString*)primitiveReceiverName;
- (void)setPrimitiveReceiverName:(nullable NSString*)value;

- (nullable NSString*)primitiveState;
- (void)setPrimitiveState:(nullable NSString*)value;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (nullable NSDate*)primitiveUntilDate;
- (void)setPrimitiveUntilDate:(nullable NSDate*)value;

- (ActivityFeed*)primitiveFeed;
- (void)setPrimitiveFeed:(ActivityFeed*)value;

@end

@interface ChallengeAttributes: NSObject 
+ (NSString *)backgroundImageUrl;
+ (NSString *)borderColorHex;
+ (NSString *)challengeDescription;
+ (NSString *)createDate;
+ (NSString *)feedId;
+ (NSString *)imageIconUrl;
+ (NSString *)isActivityFeedChallenge;
+ (NSString *)isCurrentChallenge;
+ (NSString *)receiverAvatarUrl;
+ (NSString *)receiverId;
+ (NSString *)receiverJob;
+ (NSString *)receiverName;
+ (NSString *)state;
+ (NSString *)title;
+ (NSString *)untilDate;
@end

@interface ChallengeRelationships: NSObject
+ (NSString *)feed;
@end

NS_ASSUME_NONNULL_END
