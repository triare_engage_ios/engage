// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BehaviorTeamUserEvaluations.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

#import "Behavior.h"

NS_ASSUME_NONNULL_BEGIN

@interface BehaviorTeamUserEvaluationsID : BehaviorID {}
@end

@interface _BehaviorTeamUserEvaluations : Behavior
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BehaviorTeamUserEvaluationsID *objectID;

@property (nonatomic, strong, nullable) NSNumber* badEvaluationsCount;

@property (atomic) int64_t badEvaluationsCountValue;
- (int64_t)badEvaluationsCountValue;
- (void)setBadEvaluationsCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* goodEvaluationsCount;

@property (atomic) int64_t goodEvaluationsCountValue;
- (int64_t)goodEvaluationsCountValue;
- (void)setGoodEvaluationsCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* progress;

@property (atomic) float progressValue;
- (float)progressValue;
- (void)setProgressValue:(float)value_;

@property (nonatomic, strong, nullable) NSNumber* totalEvaluationsCount;

@property (atomic) int64_t totalEvaluationsCountValue;
- (int64_t)totalEvaluationsCountValue;
- (void)setTotalEvaluationsCountValue:(int64_t)value_;

@end

@interface _BehaviorTeamUserEvaluations (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveBadEvaluationsCount;
- (void)setPrimitiveBadEvaluationsCount:(nullable NSNumber*)value;

- (int64_t)primitiveBadEvaluationsCountValue;
- (void)setPrimitiveBadEvaluationsCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveGoodEvaluationsCount;
- (void)setPrimitiveGoodEvaluationsCount:(nullable NSNumber*)value;

- (int64_t)primitiveGoodEvaluationsCountValue;
- (void)setPrimitiveGoodEvaluationsCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveProgress;
- (void)setPrimitiveProgress:(nullable NSNumber*)value;

- (float)primitiveProgressValue;
- (void)setPrimitiveProgressValue:(float)value_;

- (nullable NSNumber*)primitiveTotalEvaluationsCount;
- (void)setPrimitiveTotalEvaluationsCount:(nullable NSNumber*)value;

- (int64_t)primitiveTotalEvaluationsCountValue;
- (void)setPrimitiveTotalEvaluationsCountValue:(int64_t)value_;

@end

@interface BehaviorTeamUserEvaluationsAttributes: NSObject 
+ (NSString *)badEvaluationsCount;
+ (NSString *)goodEvaluationsCount;
+ (NSString *)progress;
+ (NSString *)totalEvaluationsCount;
@end

NS_ASSUME_NONNULL_END
