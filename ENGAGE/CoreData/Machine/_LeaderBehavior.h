// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LeaderBehavior.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

#import "Behavior.h"

NS_ASSUME_NONNULL_BEGIN

@interface LeaderBehaviorID : BehaviorID {}
@end

@interface _LeaderBehavior : Behavior
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LeaderBehaviorID *objectID;

@property (nonatomic, strong, nullable) NSNumber* tapsCount;

@property (atomic) int16_t tapsCountValue;
- (int16_t)tapsCountValue;
- (void)setTapsCountValue:(int16_t)value_;

@end

@interface _LeaderBehavior (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveTapsCount;
- (void)setPrimitiveTapsCount:(nullable NSNumber*)value;

- (int16_t)primitiveTapsCountValue;
- (void)setPrimitiveTapsCountValue:(int16_t)value_;

@end

@interface LeaderBehaviorAttributes: NSObject 
+ (NSString *)tapsCount;
@end

NS_ASSUME_NONNULL_END
