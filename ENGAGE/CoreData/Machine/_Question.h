// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Question.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class QuestionAnswer;
@class QuestionCustomAnswer;

@interface QuestionID : NSManagedObjectID {}
@end

@interface _Question : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) QuestionID *objectID;

@property (nonatomic, strong, nullable) NSNumber* answeredUsersCount;

@property (atomic) int64_t answeredUsersCountValue;
- (int64_t)answeredUsersCountValue;
- (void)setAnsweredUsersCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* answersCount;

@property (atomic) int64_t answersCountValue;
- (int64_t)answersCountValue;
- (void)setAnswersCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* body;

@property (nonatomic, strong, nullable) NSDate* createdAt;

@property (nonatomic, strong, nullable) NSNumber* customAnswersCount;

@property (atomic) int64_t customAnswersCountValue;
- (int64_t)customAnswersCountValue;
- (void)setCustomAnswersCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* haveCustomAnswers;

@property (atomic) BOOL haveCustomAnswersValue;
- (BOOL)haveCustomAnswersValue;
- (void)setHaveCustomAnswersValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isMultipleAnswer;

@property (atomic) BOOL isMultipleAnswerValue;
- (BOOL)isMultipleAnswerValue;
- (void)setIsMultipleAnswerValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* questionId;

@property (atomic) int64_t questionIdValue;
- (int64_t)questionIdValue;
- (void)setQuestionIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSDate* startsAt;

@property (nonatomic, strong, nullable) NSNumber* usersCount;

@property (atomic) int64_t usersCountValue;
- (int64_t)usersCountValue;
- (void)setUsersCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSOrderedSet<QuestionAnswer*> *answers;
- (nullable NSMutableOrderedSet<QuestionAnswer*>*)answersSet;

@property (nonatomic, strong, nullable) NSOrderedSet<QuestionCustomAnswer*> *customAnswers;
- (nullable NSMutableOrderedSet<QuestionCustomAnswer*>*)customAnswersSet;

@end

@interface _Question (AnswersCoreDataGeneratedAccessors)
- (void)addAnswers:(NSOrderedSet<QuestionAnswer*>*)value_;
- (void)removeAnswers:(NSOrderedSet<QuestionAnswer*>*)value_;
- (void)addAnswersObject:(QuestionAnswer*)value_;
- (void)removeAnswersObject:(QuestionAnswer*)value_;

- (void)insertObject:(QuestionAnswer*)value inAnswersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAnswersAtIndex:(NSUInteger)idx;
- (void)insertAnswers:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAnswersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAnswersAtIndex:(NSUInteger)idx withObject:(QuestionAnswer*)value;
- (void)replaceAnswersAtIndexes:(NSIndexSet *)indexes withAnswers:(NSArray *)values;

@end

@interface _Question (CustomAnswersCoreDataGeneratedAccessors)
- (void)addCustomAnswers:(NSOrderedSet<QuestionCustomAnswer*>*)value_;
- (void)removeCustomAnswers:(NSOrderedSet<QuestionCustomAnswer*>*)value_;
- (void)addCustomAnswersObject:(QuestionCustomAnswer*)value_;
- (void)removeCustomAnswersObject:(QuestionCustomAnswer*)value_;

- (void)insertObject:(QuestionCustomAnswer*)value inCustomAnswersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromCustomAnswersAtIndex:(NSUInteger)idx;
- (void)insertCustomAnswers:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeCustomAnswersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInCustomAnswersAtIndex:(NSUInteger)idx withObject:(QuestionCustomAnswer*)value;
- (void)replaceCustomAnswersAtIndexes:(NSIndexSet *)indexes withCustomAnswers:(NSArray *)values;

@end

@interface _Question (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveAnsweredUsersCount;
- (void)setPrimitiveAnsweredUsersCount:(nullable NSNumber*)value;

- (int64_t)primitiveAnsweredUsersCountValue;
- (void)setPrimitiveAnsweredUsersCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveAnswersCount;
- (void)setPrimitiveAnswersCount:(nullable NSNumber*)value;

- (int64_t)primitiveAnswersCountValue;
- (void)setPrimitiveAnswersCountValue:(int64_t)value_;

- (nullable NSString*)primitiveBody;
- (void)setPrimitiveBody:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreatedAt;
- (void)setPrimitiveCreatedAt:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveCustomAnswersCount;
- (void)setPrimitiveCustomAnswersCount:(nullable NSNumber*)value;

- (int64_t)primitiveCustomAnswersCountValue;
- (void)setPrimitiveCustomAnswersCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveHaveCustomAnswers;
- (void)setPrimitiveHaveCustomAnswers:(nullable NSNumber*)value;

- (BOOL)primitiveHaveCustomAnswersValue;
- (void)setPrimitiveHaveCustomAnswersValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsMultipleAnswer;
- (void)setPrimitiveIsMultipleAnswer:(nullable NSNumber*)value;

- (BOOL)primitiveIsMultipleAnswerValue;
- (void)setPrimitiveIsMultipleAnswerValue:(BOOL)value_;

- (nullable NSNumber*)primitiveQuestionId;
- (void)setPrimitiveQuestionId:(nullable NSNumber*)value;

- (int64_t)primitiveQuestionIdValue;
- (void)setPrimitiveQuestionIdValue:(int64_t)value_;

- (nullable NSDate*)primitiveStartsAt;
- (void)setPrimitiveStartsAt:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveUsersCount;
- (void)setPrimitiveUsersCount:(nullable NSNumber*)value;

- (int64_t)primitiveUsersCountValue;
- (void)setPrimitiveUsersCountValue:(int64_t)value_;

- (NSMutableOrderedSet<QuestionAnswer*>*)primitiveAnswers;
- (void)setPrimitiveAnswers:(NSMutableOrderedSet<QuestionAnswer*>*)value;

- (NSMutableOrderedSet<QuestionCustomAnswer*>*)primitiveCustomAnswers;
- (void)setPrimitiveCustomAnswers:(NSMutableOrderedSet<QuestionCustomAnswer*>*)value;

@end

@interface QuestionAttributes: NSObject 
+ (NSString *)answeredUsersCount;
+ (NSString *)answersCount;
+ (NSString *)body;
+ (NSString *)createdAt;
+ (NSString *)customAnswersCount;
+ (NSString *)haveCustomAnswers;
+ (NSString *)isMultipleAnswer;
+ (NSString *)questionId;
+ (NSString *)startsAt;
+ (NSString *)usersCount;
@end

@interface QuestionRelationships: NSObject
+ (NSString *)answers;
+ (NSString *)customAnswers;
@end

NS_ASSUME_NONNULL_END
