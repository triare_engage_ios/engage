// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Comment.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class ActivityFeed;

@interface CommentID : NSManagedObjectID {}
@end

@interface _Comment : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CommentID *objectID;

@property (nonatomic, strong, nullable) NSString* body;

@property (nonatomic, strong, nullable) NSNumber* commentId;

@property (atomic) int64_t commentIdValue;
- (int64_t)commentIdValue;
- (void)setCommentIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSDate* createDate;

@property (nonatomic, strong, nullable) NSString* senderAvatarUrl;

@property (nonatomic, strong, nullable) NSNumber* senderId;

@property (atomic) int64_t senderIdValue;
- (int64_t)senderIdValue;
- (void)setSenderIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* senderName;

@property (nonatomic, strong, nullable) ActivityFeed *activityFeed;

@end

@interface _Comment (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBody;
- (void)setPrimitiveBody:(nullable NSString*)value;

- (nullable NSNumber*)primitiveCommentId;
- (void)setPrimitiveCommentId:(nullable NSNumber*)value;

- (int64_t)primitiveCommentIdValue;
- (void)setPrimitiveCommentIdValue:(int64_t)value_;

- (nullable NSDate*)primitiveCreateDate;
- (void)setPrimitiveCreateDate:(nullable NSDate*)value;

- (nullable NSString*)primitiveSenderAvatarUrl;
- (void)setPrimitiveSenderAvatarUrl:(nullable NSString*)value;

- (nullable NSNumber*)primitiveSenderId;
- (void)setPrimitiveSenderId:(nullable NSNumber*)value;

- (int64_t)primitiveSenderIdValue;
- (void)setPrimitiveSenderIdValue:(int64_t)value_;

- (nullable NSString*)primitiveSenderName;
- (void)setPrimitiveSenderName:(nullable NSString*)value;

- (ActivityFeed*)primitiveActivityFeed;
- (void)setPrimitiveActivityFeed:(ActivityFeed*)value;

@end

@interface CommentAttributes: NSObject 
+ (NSString *)body;
+ (NSString *)commentId;
+ (NSString *)createDate;
+ (NSString *)senderAvatarUrl;
+ (NSString *)senderId;
+ (NSString *)senderName;
@end

@interface CommentRelationships: NSObject
+ (NSString *)activityFeed;
@end

NS_ASSUME_NONNULL_END
