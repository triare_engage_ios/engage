// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Company.m instead.

#import "_Company.h"

@implementation CompanyID
@end

@implementation _Company

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Company" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Company";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Company" inManagedObjectContext:moc_];
}

- (CompanyID*)objectID {
	return (CompanyID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"companyIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"companyId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic backgroundUrl;

@dynamic companyId;

- (int64_t)companyIdValue {
	NSNumber *result = [self companyId];
	return [result longLongValue];
}

- (void)setCompanyIdValue:(int64_t)value_ {
	[self setCompanyId:@(value_)];
}

- (int64_t)primitiveCompanyIdValue {
	NSNumber *result = [self primitiveCompanyId];
	return [result longLongValue];
}

- (void)setPrimitiveCompanyIdValue:(int64_t)value_ {
	[self setPrimitiveCompanyId:@(value_)];
}

@dynamic createdAt;

@dynamic logoUrl;

@dynamic name;

@dynamic plan;

@dynamic user;

@end

@implementation CompanyAttributes 
+ (NSString *)backgroundUrl {
	return @"backgroundUrl";
}
+ (NSString *)companyId {
	return @"companyId";
}
+ (NSString *)createdAt {
	return @"createdAt";
}
+ (NSString *)logoUrl {
	return @"logoUrl";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)plan {
	return @"plan";
}
@end

@implementation CompanyRelationships 
+ (NSString *)user {
	return @"user";
}
@end

