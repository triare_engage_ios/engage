// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserBehavior.m instead.

#import "_UserBehavior.h"

@implementation UserBehaviorID
@end

@implementation _UserBehavior

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UserBehavior" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UserBehavior";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UserBehavior" inManagedObjectContext:moc_];
}

- (UserBehaviorID*)objectID {
	return (UserBehaviorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic count;

- (int16_t)countValue {
	NSNumber *result = [self count];
	return [result shortValue];
}

- (void)setCountValue:(int16_t)value_ {
	[self setCount:@(value_)];
}

- (int16_t)primitiveCountValue {
	NSNumber *result = [self primitiveCount];
	return [result shortValue];
}

- (void)setPrimitiveCountValue:(int16_t)value_ {
	[self setPrimitiveCount:@(value_)];
}

@dynamic teamUser;

@end

@implementation UserBehaviorAttributes 
+ (NSString *)count {
	return @"count";
}
@end

@implementation UserBehaviorRelationships 
+ (NSString *)teamUser {
	return @"teamUser";
}
@end

