// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TeamUser.m instead.

#import "_TeamUser.h"

@implementation TeamUserID
@end

@implementation _TeamUser

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TeamUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TeamUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TeamUser" inManagedObjectContext:moc_];
}

- (TeamUserID*)objectID {
	return (TeamUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"haveChallengeBadgesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"haveChallengeBadges"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isInvationAcceptedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isInvationAccepted"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isPointWinnerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isPointWinner"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isQuestionPresentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isQuestionPresent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isTeamMessagesPresentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isTeamMessagesPresent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"receivedBadEvaluationsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"receivedBadEvaluationsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"receivedGoodEvaluationsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"receivedGoodEvaluationsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"unreadSuggestionsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"unreadSuggestionsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"userIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"userId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic avatarUrl;

@dynamic companyPlan;

@dynamic email;

@dynamic firstName;

@dynamic fullName;

@dynamic haveChallengeBadges;

- (BOOL)haveChallengeBadgesValue {
	NSNumber *result = [self haveChallengeBadges];
	return [result boolValue];
}

- (void)setHaveChallengeBadgesValue:(BOOL)value_ {
	[self setHaveChallengeBadges:@(value_)];
}

- (BOOL)primitiveHaveChallengeBadgesValue {
	NSNumber *result = [self primitiveHaveChallengeBadges];
	return [result boolValue];
}

- (void)setPrimitiveHaveChallengeBadgesValue:(BOOL)value_ {
	[self setPrimitiveHaveChallengeBadges:@(value_)];
}

@dynamic isInvationAccepted;

- (BOOL)isInvationAcceptedValue {
	NSNumber *result = [self isInvationAccepted];
	return [result boolValue];
}

- (void)setIsInvationAcceptedValue:(BOOL)value_ {
	[self setIsInvationAccepted:@(value_)];
}

- (BOOL)primitiveIsInvationAcceptedValue {
	NSNumber *result = [self primitiveIsInvationAccepted];
	return [result boolValue];
}

- (void)setPrimitiveIsInvationAcceptedValue:(BOOL)value_ {
	[self setPrimitiveIsInvationAccepted:@(value_)];
}

@dynamic isPointWinner;

- (BOOL)isPointWinnerValue {
	NSNumber *result = [self isPointWinner];
	return [result boolValue];
}

- (void)setIsPointWinnerValue:(BOOL)value_ {
	[self setIsPointWinner:@(value_)];
}

- (BOOL)primitiveIsPointWinnerValue {
	NSNumber *result = [self primitiveIsPointWinner];
	return [result boolValue];
}

- (void)setPrimitiveIsPointWinnerValue:(BOOL)value_ {
	[self setPrimitiveIsPointWinner:@(value_)];
}

@dynamic isQuestionPresent;

- (BOOL)isQuestionPresentValue {
	NSNumber *result = [self isQuestionPresent];
	return [result boolValue];
}

- (void)setIsQuestionPresentValue:(BOOL)value_ {
	[self setIsQuestionPresent:@(value_)];
}

- (BOOL)primitiveIsQuestionPresentValue {
	NSNumber *result = [self primitiveIsQuestionPresent];
	return [result boolValue];
}

- (void)setPrimitiveIsQuestionPresentValue:(BOOL)value_ {
	[self setPrimitiveIsQuestionPresent:@(value_)];
}

@dynamic isTeamMessagesPresent;

- (BOOL)isTeamMessagesPresentValue {
	NSNumber *result = [self isTeamMessagesPresent];
	return [result boolValue];
}

- (void)setIsTeamMessagesPresentValue:(BOOL)value_ {
	[self setIsTeamMessagesPresent:@(value_)];
}

- (BOOL)primitiveIsTeamMessagesPresentValue {
	NSNumber *result = [self primitiveIsTeamMessagesPresent];
	return [result boolValue];
}

- (void)setPrimitiveIsTeamMessagesPresentValue:(BOOL)value_ {
	[self setPrimitiveIsTeamMessagesPresent:@(value_)];
}

@dynamic jobTitle;

@dynamic lastName;

@dynamic receivedBadEvaluationsCount;

- (int64_t)receivedBadEvaluationsCountValue {
	NSNumber *result = [self receivedBadEvaluationsCount];
	return [result longLongValue];
}

- (void)setReceivedBadEvaluationsCountValue:(int64_t)value_ {
	[self setReceivedBadEvaluationsCount:@(value_)];
}

- (int64_t)primitiveReceivedBadEvaluationsCountValue {
	NSNumber *result = [self primitiveReceivedBadEvaluationsCount];
	return [result longLongValue];
}

- (void)setPrimitiveReceivedBadEvaluationsCountValue:(int64_t)value_ {
	[self setPrimitiveReceivedBadEvaluationsCount:@(value_)];
}

@dynamic receivedGoodEvaluationsCount;

- (int64_t)receivedGoodEvaluationsCountValue {
	NSNumber *result = [self receivedGoodEvaluationsCount];
	return [result longLongValue];
}

- (void)setReceivedGoodEvaluationsCountValue:(int64_t)value_ {
	[self setReceivedGoodEvaluationsCount:@(value_)];
}

- (int64_t)primitiveReceivedGoodEvaluationsCountValue {
	NSNumber *result = [self primitiveReceivedGoodEvaluationsCount];
	return [result longLongValue];
}

- (void)setPrimitiveReceivedGoodEvaluationsCountValue:(int64_t)value_ {
	[self setPrimitiveReceivedGoodEvaluationsCount:@(value_)];
}

@dynamic role;

@dynamic unconfirmedEmail;

@dynamic unreadSuggestionsCount;

- (int16_t)unreadSuggestionsCountValue {
	NSNumber *result = [self unreadSuggestionsCount];
	return [result shortValue];
}

- (void)setUnreadSuggestionsCountValue:(int16_t)value_ {
	[self setUnreadSuggestionsCount:@(value_)];
}

- (int16_t)primitiveUnreadSuggestionsCountValue {
	NSNumber *result = [self primitiveUnreadSuggestionsCount];
	return [result shortValue];
}

- (void)setPrimitiveUnreadSuggestionsCountValue:(int16_t)value_ {
	[self setPrimitiveUnreadSuggestionsCount:@(value_)];
}

@dynamic userId;

- (int64_t)userIdValue {
	NSNumber *result = [self userId];
	return [result longLongValue];
}

- (void)setUserIdValue:(int64_t)value_ {
	[self setUserId:@(value_)];
}

- (int64_t)primitiveUserIdValue {
	NSNumber *result = [self primitiveUserId];
	return [result longLongValue];
}

- (void)setPrimitiveUserIdValue:(int64_t)value_ {
	[self setPrimitiveUserId:@(value_)];
}

@dynamic behaviors;

- (NSMutableSet<UserBehavior*>*)behaviorsSet {
	[self willAccessValueForKey:@"behaviors"];

	NSMutableSet<UserBehavior*> *result = (NSMutableSet<UserBehavior*>*)[self mutableSetValueForKey:@"behaviors"];

	[self didAccessValueForKey:@"behaviors"];
	return result;
}

@dynamic evaluations;

- (NSMutableSet<EvaluateBehavior*>*)evaluationsSet {
	[self willAccessValueForKey:@"evaluations"];

	NSMutableSet<EvaluateBehavior*> *result = (NSMutableSet<EvaluateBehavior*>*)[self mutableSetValueForKey:@"evaluations"];

	[self didAccessValueForKey:@"evaluations"];
	return result;
}

@end

@implementation TeamUserAttributes 
+ (NSString *)avatarUrl {
	return @"avatarUrl";
}
+ (NSString *)companyPlan {
	return @"companyPlan";
}
+ (NSString *)email {
	return @"email";
}
+ (NSString *)firstName {
	return @"firstName";
}
+ (NSString *)fullName {
	return @"fullName";
}
+ (NSString *)haveChallengeBadges {
	return @"haveChallengeBadges";
}
+ (NSString *)isInvationAccepted {
	return @"isInvationAccepted";
}
+ (NSString *)isPointWinner {
	return @"isPointWinner";
}
+ (NSString *)isQuestionPresent {
	return @"isQuestionPresent";
}
+ (NSString *)isTeamMessagesPresent {
	return @"isTeamMessagesPresent";
}
+ (NSString *)jobTitle {
	return @"jobTitle";
}
+ (NSString *)lastName {
	return @"lastName";
}
+ (NSString *)receivedBadEvaluationsCount {
	return @"receivedBadEvaluationsCount";
}
+ (NSString *)receivedGoodEvaluationsCount {
	return @"receivedGoodEvaluationsCount";
}
+ (NSString *)role {
	return @"role";
}
+ (NSString *)unconfirmedEmail {
	return @"unconfirmedEmail";
}
+ (NSString *)unreadSuggestionsCount {
	return @"unreadSuggestionsCount";
}
+ (NSString *)userId {
	return @"userId";
}
@end

@implementation TeamUserRelationships 
+ (NSString *)behaviors {
	return @"behaviors";
}
+ (NSString *)evaluations {
	return @"evaluations";
}
@end

