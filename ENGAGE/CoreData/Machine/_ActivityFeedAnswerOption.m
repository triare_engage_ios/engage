// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityFeedAnswerOption.m instead.

#import "_ActivityFeedAnswerOption.h"

@implementation ActivityFeedAnswerOptionID
@end

@implementation _ActivityFeedAnswerOption

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ActivityFeedAnswerOption" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ActivityFeedAnswerOption";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ActivityFeedAnswerOption" inManagedObjectContext:moc_];
}

- (ActivityFeedAnswerOptionID*)objectID {
	return (ActivityFeedAnswerOptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"answerOptionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"answerOptionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isCustomValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isCustom"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"questionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"questionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic answerOptionId;

- (int64_t)answerOptionIdValue {
	NSNumber *result = [self answerOptionId];
	return [result longLongValue];
}

- (void)setAnswerOptionIdValue:(int64_t)value_ {
	[self setAnswerOptionId:@(value_)];
}

- (int64_t)primitiveAnswerOptionIdValue {
	NSNumber *result = [self primitiveAnswerOptionId];
	return [result longLongValue];
}

- (void)setPrimitiveAnswerOptionIdValue:(int64_t)value_ {
	[self setPrimitiveAnswerOptionId:@(value_)];
}

@dynamic isCustom;

- (BOOL)isCustomValue {
	NSNumber *result = [self isCustom];
	return [result boolValue];
}

- (void)setIsCustomValue:(BOOL)value_ {
	[self setIsCustom:@(value_)];
}

- (BOOL)primitiveIsCustomValue {
	NSNumber *result = [self primitiveIsCustom];
	return [result boolValue];
}

- (void)setPrimitiveIsCustomValue:(BOOL)value_ {
	[self setPrimitiveIsCustom:@(value_)];
}

@dynamic questionId;

- (int64_t)questionIdValue {
	NSNumber *result = [self questionId];
	return [result longLongValue];
}

- (void)setQuestionIdValue:(int64_t)value_ {
	[self setQuestionId:@(value_)];
}

- (int64_t)primitiveQuestionIdValue {
	NSNumber *result = [self primitiveQuestionId];
	return [result longLongValue];
}

- (void)setPrimitiveQuestionIdValue:(int64_t)value_ {
	[self setPrimitiveQuestionId:@(value_)];
}

@dynamic title;

@dynamic question;

@end

@implementation ActivityFeedAnswerOptionAttributes 
+ (NSString *)answerOptionId {
	return @"answerOptionId";
}
+ (NSString *)isCustom {
	return @"isCustom";
}
+ (NSString *)questionId {
	return @"questionId";
}
+ (NSString *)title {
	return @"title";
}
@end

@implementation ActivityFeedAnswerOptionRelationships 
+ (NSString *)question {
	return @"question";
}
@end

