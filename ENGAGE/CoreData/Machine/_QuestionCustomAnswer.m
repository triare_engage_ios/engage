// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to QuestionCustomAnswer.m instead.

#import "_QuestionCustomAnswer.h"

@implementation QuestionCustomAnswerID
@end

@implementation _QuestionCustomAnswer

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"QuestionCustomAnswer" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"QuestionCustomAnswer";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"QuestionCustomAnswer" inManagedObjectContext:moc_];
}

- (QuestionCustomAnswerID*)objectID {
	return (QuestionCustomAnswerID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"questionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"questionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic questionId;

- (int64_t)questionIdValue {
	NSNumber *result = [self questionId];
	return [result longLongValue];
}

- (void)setQuestionIdValue:(int64_t)value_ {
	[self setQuestionId:@(value_)];
}

- (int64_t)primitiveQuestionIdValue {
	NSNumber *result = [self primitiveQuestionId];
	return [result longLongValue];
}

- (void)setPrimitiveQuestionIdValue:(int64_t)value_ {
	[self setPrimitiveQuestionId:@(value_)];
}

@dynamic title;

@dynamic question;

@end

@implementation QuestionCustomAnswerAttributes 
+ (NSString *)questionId {
	return @"questionId";
}
+ (NSString *)title {
	return @"title";
}
@end

@implementation QuestionCustomAnswerRelationships 
+ (NSString *)question {
	return @"question";
}
@end

