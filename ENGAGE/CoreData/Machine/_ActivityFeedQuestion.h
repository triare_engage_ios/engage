// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityFeedQuestion.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class ActivityFeedAnswerOption;

@interface ActivityFeedQuestionID : NSManagedObjectID {}
@end

@interface _ActivityFeedQuestion : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityFeedQuestionID *objectID;

@property (nonatomic, strong, nullable) NSString* body;

@property (nonatomic, strong, nullable) NSNumber* haveCustomAnswerOption;

@property (atomic) BOOL haveCustomAnswerOptionValue;
- (BOOL)haveCustomAnswerOptionValue;
- (void)setHaveCustomAnswerOptionValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isMultiple;

@property (atomic) BOOL isMultipleValue;
- (BOOL)isMultipleValue;
- (void)setIsMultipleValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* questionId;

@property (atomic) int64_t questionIdValue;
- (int64_t)questionIdValue;
- (void)setQuestionIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSOrderedSet<ActivityFeedAnswerOption*> *answerOptions;
- (nullable NSMutableOrderedSet<ActivityFeedAnswerOption*>*)answerOptionsSet;

@end

@interface _ActivityFeedQuestion (AnswerOptionsCoreDataGeneratedAccessors)
- (void)addAnswerOptions:(NSOrderedSet<ActivityFeedAnswerOption*>*)value_;
- (void)removeAnswerOptions:(NSOrderedSet<ActivityFeedAnswerOption*>*)value_;
- (void)addAnswerOptionsObject:(ActivityFeedAnswerOption*)value_;
- (void)removeAnswerOptionsObject:(ActivityFeedAnswerOption*)value_;

- (void)insertObject:(ActivityFeedAnswerOption*)value inAnswerOptionsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAnswerOptionsAtIndex:(NSUInteger)idx;
- (void)insertAnswerOptions:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAnswerOptionsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAnswerOptionsAtIndex:(NSUInteger)idx withObject:(ActivityFeedAnswerOption*)value;
- (void)replaceAnswerOptionsAtIndexes:(NSIndexSet *)indexes withAnswerOptions:(NSArray *)values;

@end

@interface _ActivityFeedQuestion (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBody;
- (void)setPrimitiveBody:(nullable NSString*)value;

- (nullable NSNumber*)primitiveHaveCustomAnswerOption;
- (void)setPrimitiveHaveCustomAnswerOption:(nullable NSNumber*)value;

- (BOOL)primitiveHaveCustomAnswerOptionValue;
- (void)setPrimitiveHaveCustomAnswerOptionValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsMultiple;
- (void)setPrimitiveIsMultiple:(nullable NSNumber*)value;

- (BOOL)primitiveIsMultipleValue;
- (void)setPrimitiveIsMultipleValue:(BOOL)value_;

- (nullable NSNumber*)primitiveQuestionId;
- (void)setPrimitiveQuestionId:(nullable NSNumber*)value;

- (int64_t)primitiveQuestionIdValue;
- (void)setPrimitiveQuestionIdValue:(int64_t)value_;

- (NSMutableOrderedSet<ActivityFeedAnswerOption*>*)primitiveAnswerOptions;
- (void)setPrimitiveAnswerOptions:(NSMutableOrderedSet<ActivityFeedAnswerOption*>*)value;

@end

@interface ActivityFeedQuestionAttributes: NSObject 
+ (NSString *)body;
+ (NSString *)haveCustomAnswerOption;
+ (NSString *)isMultiple;
+ (NSString *)questionId;
@end

@interface ActivityFeedQuestionRelationships: NSObject
+ (NSString *)answerOptions;
@end

NS_ASSUME_NONNULL_END
