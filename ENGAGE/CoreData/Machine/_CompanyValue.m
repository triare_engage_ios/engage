// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CompanyValue.m instead.

#import "_CompanyValue.h"

@implementation CompanyValueID
@end

@implementation _CompanyValue

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CompanyValue" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CompanyValue";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CompanyValue" inManagedObjectContext:moc_];
}

- (CompanyValueID*)objectID {
	return (CompanyValueID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"valueIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"valueId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic title;

@dynamic valueId;

- (int64_t)valueIdValue {
	NSNumber *result = [self valueId];
	return [result longLongValue];
}

- (void)setValueIdValue:(int64_t)value_ {
	[self setValueId:@(value_)];
}

- (int64_t)primitiveValueIdValue {
	NSNumber *result = [self primitiveValueId];
	return [result longLongValue];
}

- (void)setPrimitiveValueIdValue:(int64_t)value_ {
	[self setPrimitiveValueId:@(value_)];
}

@end

@implementation CompanyValueAttributes 
+ (NSString *)title {
	return @"title";
}
+ (NSString *)valueId {
	return @"valueId";
}
@end

