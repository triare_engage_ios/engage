// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CDLearning.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class ActivityFeed;

@interface CDLearningID : NSManagedObjectID {}
@end

@interface _CDLearning : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CDLearningID *objectID;

@property (nonatomic, strong, nullable) NSDate* createDate;

@property (nonatomic, strong, nullable) NSNumber* feedId;

@property (atomic) int64_t feedIdValue;
- (int64_t)feedIdValue;
- (void)setFeedIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* isFromLearningList;

@property (atomic) BOOL isFromLearningListValue;
- (BOOL)isFromLearningListValue;
- (void)setIsFromLearningListValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* isSenderTopPointWinner;

@property (atomic) BOOL isSenderTopPointWinnerValue;
- (BOOL)isSenderTopPointWinnerValue;
- (void)setIsSenderTopPointWinnerValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* learningType;

@property (nonatomic, strong, nullable) NSNumber* rating;

@property (atomic) int64_t ratingValue;
- (int64_t)ratingValue;
- (void)setRatingValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* senderAvatarUrl;

@property (nonatomic, strong, nullable) NSString* senderFullName;

@property (nonatomic, strong, nullable) NSNumber* senderId;

@property (atomic) int64_t senderIdValue;
- (int64_t)senderIdValue;
- (void)setSenderIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* senderJobTitle;

@property (nonatomic, strong, nullable) NSString* siteDescription;

@property (nonatomic, strong, nullable) NSString* siteImageUrl;

@property (nonatomic, strong, nullable) NSString* siteTitle;

@property (nonatomic, strong, nullable) NSString* siteUrl;

@property (nonatomic, strong, nullable) NSString* userComment;

@property (nonatomic, strong, nullable) NSNumber* userRating;

@property (atomic) int64_t userRatingValue;
- (int64_t)userRatingValue;
- (void)setUserRatingValue:(int64_t)value_;

@property (nonatomic, strong, nullable) ActivityFeed *activityFeed;

@end

@interface _CDLearning (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSDate*)primitiveCreateDate;
- (void)setPrimitiveCreateDate:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveFeedId;
- (void)setPrimitiveFeedId:(nullable NSNumber*)value;

- (int64_t)primitiveFeedIdValue;
- (void)setPrimitiveFeedIdValue:(int64_t)value_;

- (nullable NSNumber*)primitiveIsFromLearningList;
- (void)setPrimitiveIsFromLearningList:(nullable NSNumber*)value;

- (BOOL)primitiveIsFromLearningListValue;
- (void)setPrimitiveIsFromLearningListValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIsSenderTopPointWinner;
- (void)setPrimitiveIsSenderTopPointWinner:(nullable NSNumber*)value;

- (BOOL)primitiveIsSenderTopPointWinnerValue;
- (void)setPrimitiveIsSenderTopPointWinnerValue:(BOOL)value_;

- (nullable NSString*)primitiveLearningType;
- (void)setPrimitiveLearningType:(nullable NSString*)value;

- (nullable NSNumber*)primitiveRating;
- (void)setPrimitiveRating:(nullable NSNumber*)value;

- (int64_t)primitiveRatingValue;
- (void)setPrimitiveRatingValue:(int64_t)value_;

- (nullable NSString*)primitiveSenderAvatarUrl;
- (void)setPrimitiveSenderAvatarUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveSenderFullName;
- (void)setPrimitiveSenderFullName:(nullable NSString*)value;

- (nullable NSNumber*)primitiveSenderId;
- (void)setPrimitiveSenderId:(nullable NSNumber*)value;

- (int64_t)primitiveSenderIdValue;
- (void)setPrimitiveSenderIdValue:(int64_t)value_;

- (nullable NSString*)primitiveSenderJobTitle;
- (void)setPrimitiveSenderJobTitle:(nullable NSString*)value;

- (nullable NSString*)primitiveSiteDescription;
- (void)setPrimitiveSiteDescription:(nullable NSString*)value;

- (nullable NSString*)primitiveSiteImageUrl;
- (void)setPrimitiveSiteImageUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveSiteTitle;
- (void)setPrimitiveSiteTitle:(nullable NSString*)value;

- (nullable NSString*)primitiveSiteUrl;
- (void)setPrimitiveSiteUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveUserComment;
- (void)setPrimitiveUserComment:(nullable NSString*)value;

- (nullable NSNumber*)primitiveUserRating;
- (void)setPrimitiveUserRating:(nullable NSNumber*)value;

- (int64_t)primitiveUserRatingValue;
- (void)setPrimitiveUserRatingValue:(int64_t)value_;

- (ActivityFeed*)primitiveActivityFeed;
- (void)setPrimitiveActivityFeed:(ActivityFeed*)value;

@end

@interface CDLearningAttributes: NSObject 
+ (NSString *)createDate;
+ (NSString *)feedId;
+ (NSString *)isFromLearningList;
+ (NSString *)isSenderTopPointWinner;
+ (NSString *)learningType;
+ (NSString *)rating;
+ (NSString *)senderAvatarUrl;
+ (NSString *)senderFullName;
+ (NSString *)senderId;
+ (NSString *)senderJobTitle;
+ (NSString *)siteDescription;
+ (NSString *)siteImageUrl;
+ (NSString *)siteTitle;
+ (NSString *)siteUrl;
+ (NSString *)userComment;
+ (NSString *)userRating;
@end

@interface CDLearningRelationships: NSObject
+ (NSString *)activityFeed;
@end

NS_ASSUME_NONNULL_END
