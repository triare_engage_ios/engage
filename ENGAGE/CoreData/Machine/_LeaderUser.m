// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LeaderUser.m instead.

#import "_LeaderUser.h"

@implementation LeaderUserID
@end

@implementation _LeaderUser

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LeaderUser" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LeaderUser";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LeaderUser" inManagedObjectContext:moc_];
}

- (LeaderUserID*)objectID {
	return (LeaderUserID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"givenTapsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"givenTapsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"receivedTapsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"receivedTapsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic givenTapsCount;

- (int16_t)givenTapsCountValue {
	NSNumber *result = [self givenTapsCount];
	return [result shortValue];
}

- (void)setGivenTapsCountValue:(int16_t)value_ {
	[self setGivenTapsCount:@(value_)];
}

- (int16_t)primitiveGivenTapsCountValue {
	NSNumber *result = [self primitiveGivenTapsCount];
	return [result shortValue];
}

- (void)setPrimitiveGivenTapsCountValue:(int16_t)value_ {
	[self setPrimitiveGivenTapsCount:@(value_)];
}

@dynamic receivedTapsCount;

- (int16_t)receivedTapsCountValue {
	NSNumber *result = [self receivedTapsCount];
	return [result shortValue];
}

- (void)setReceivedTapsCountValue:(int16_t)value_ {
	[self setReceivedTapsCount:@(value_)];
}

- (int16_t)primitiveReceivedTapsCountValue {
	NSNumber *result = [self primitiveReceivedTapsCount];
	return [result shortValue];
}

- (void)setPrimitiveReceivedTapsCountValue:(int16_t)value_ {
	[self setPrimitiveReceivedTapsCount:@(value_)];
}

@end

@implementation LeaderUserAttributes 
+ (NSString *)givenTapsCount {
	return @"givenTapsCount";
}
+ (NSString *)receivedTapsCount {
	return @"receivedTapsCount";
}
@end

