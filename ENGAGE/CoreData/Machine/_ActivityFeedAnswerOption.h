// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityFeedAnswerOption.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class ActivityFeedQuestion;

@interface ActivityFeedAnswerOptionID : NSManagedObjectID {}
@end

@interface _ActivityFeedAnswerOption : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ActivityFeedAnswerOptionID *objectID;

@property (nonatomic, strong, nullable) NSNumber* answerOptionId;

@property (atomic) int64_t answerOptionIdValue;
- (int64_t)answerOptionIdValue;
- (void)setAnswerOptionIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* isCustom;

@property (atomic) BOOL isCustomValue;
- (BOOL)isCustomValue;
- (void)setIsCustomValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* questionId;

@property (atomic) int64_t questionIdValue;
- (int64_t)questionIdValue;
- (void)setQuestionIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) ActivityFeedQuestion *question;

@end

@interface _ActivityFeedAnswerOption (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveAnswerOptionId;
- (void)setPrimitiveAnswerOptionId:(nullable NSNumber*)value;

- (int64_t)primitiveAnswerOptionIdValue;
- (void)setPrimitiveAnswerOptionIdValue:(int64_t)value_;

- (nullable NSNumber*)primitiveIsCustom;
- (void)setPrimitiveIsCustom:(nullable NSNumber*)value;

- (BOOL)primitiveIsCustomValue;
- (void)setPrimitiveIsCustomValue:(BOOL)value_;

- (nullable NSNumber*)primitiveQuestionId;
- (void)setPrimitiveQuestionId:(nullable NSNumber*)value;

- (int64_t)primitiveQuestionIdValue;
- (void)setPrimitiveQuestionIdValue:(int64_t)value_;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (ActivityFeedQuestion*)primitiveQuestion;
- (void)setPrimitiveQuestion:(ActivityFeedQuestion*)value;

@end

@interface ActivityFeedAnswerOptionAttributes: NSObject 
+ (NSString *)answerOptionId;
+ (NSString *)isCustom;
+ (NSString *)questionId;
+ (NSString *)title;
@end

@interface ActivityFeedAnswerOptionRelationships: NSObject
+ (NSString *)question;
@end

NS_ASSUME_NONNULL_END
