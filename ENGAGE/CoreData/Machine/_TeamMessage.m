// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TeamMessage.m instead.

#import "_TeamMessage.h"

@implementation TeamMessageID
@end

@implementation _TeamMessage

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TeamMessage" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TeamMessage";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TeamMessage" inManagedObjectContext:moc_];
}

- (TeamMessageID*)objectID {
	return (TeamMessageID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"messageIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"messageId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic body;

@dynamic createDate;

@dynamic messageId;

- (int64_t)messageIdValue {
	NSNumber *result = [self messageId];
	return [result longLongValue];
}

- (void)setMessageIdValue:(int64_t)value_ {
	[self setMessageId:@(value_)];
}

- (int64_t)primitiveMessageIdValue {
	NSNumber *result = [self primitiveMessageId];
	return [result longLongValue];
}

- (void)setPrimitiveMessageIdValue:(int64_t)value_ {
	[self setPrimitiveMessageId:@(value_)];
}

@end

@implementation TeamMessageAttributes 
+ (NSString *)body {
	return @"body";
}
+ (NSString *)createDate {
	return @"createDate";
}
+ (NSString *)messageId {
	return @"messageId";
}
@end

