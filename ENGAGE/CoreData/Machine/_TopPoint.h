// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TopPoint.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class ActivityFeed;

@interface TopPointID : NSManagedObjectID {}
@end

@interface _TopPoint : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TopPointID *objectID;

@property (nonatomic, strong, nullable) NSNumber* count;

@property (atomic) int64_t countValue;
- (int64_t)countValue;
- (void)setCountValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSNumber* feedId;

@property (atomic) int64_t feedIdValue;
- (int64_t)feedIdValue;
- (void)setFeedIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* imageUrl;

@property (nonatomic, strong, nullable) ActivityFeed *feed;

@end

@interface _TopPoint (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveCount;
- (void)setPrimitiveCount:(nullable NSNumber*)value;

- (int64_t)primitiveCountValue;
- (void)setPrimitiveCountValue:(int64_t)value_;

- (nullable NSNumber*)primitiveFeedId;
- (void)setPrimitiveFeedId:(nullable NSNumber*)value;

- (int64_t)primitiveFeedIdValue;
- (void)setPrimitiveFeedIdValue:(int64_t)value_;

- (nullable NSString*)primitiveImageUrl;
- (void)setPrimitiveImageUrl:(nullable NSString*)value;

- (ActivityFeed*)primitiveFeed;
- (void)setPrimitiveFeed:(ActivityFeed*)value;

@end

@interface TopPointAttributes: NSObject 
+ (NSString *)count;
+ (NSString *)feedId;
+ (NSString *)imageUrl;
@end

@interface TopPointRelationships: NSObject
+ (NSString *)feed;
@end

NS_ASSUME_NONNULL_END
