// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Company;

@interface UserID : NSManagedObjectID {}
@end

@interface _User : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserID *objectID;

@property (nonatomic, strong, nullable) NSString* avatarUrl;

@property (nonatomic, strong, nullable) NSString* email;

@property (nonatomic, strong, nullable) NSString* firstName;

@property (nonatomic, strong, nullable) NSString* fullName;

@property (nonatomic, strong, nullable) NSString* jobTitle;

@property (nonatomic, strong, nullable) NSString* lastName;

@property (nonatomic, strong, nullable) NSString* pushDeviceToken;

@property (nonatomic, strong, nullable) NSString* role;

@property (nonatomic, strong, nullable) NSNumber* userId;

@property (atomic) int64_t userIdValue;
- (int64_t)userIdValue;
- (void)setUserIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) Company *company;

@end

@interface _User (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAvatarUrl;
- (void)setPrimitiveAvatarUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(nullable NSString*)value;

- (nullable NSString*)primitiveFirstName;
- (void)setPrimitiveFirstName:(nullable NSString*)value;

- (nullable NSString*)primitiveFullName;
- (void)setPrimitiveFullName:(nullable NSString*)value;

- (nullable NSString*)primitiveJobTitle;
- (void)setPrimitiveJobTitle:(nullable NSString*)value;

- (nullable NSString*)primitiveLastName;
- (void)setPrimitiveLastName:(nullable NSString*)value;

- (nullable NSString*)primitivePushDeviceToken;
- (void)setPrimitivePushDeviceToken:(nullable NSString*)value;

- (nullable NSString*)primitiveRole;
- (void)setPrimitiveRole:(nullable NSString*)value;

- (nullable NSNumber*)primitiveUserId;
- (void)setPrimitiveUserId:(nullable NSNumber*)value;

- (int64_t)primitiveUserIdValue;
- (void)setPrimitiveUserIdValue:(int64_t)value_;

- (Company*)primitiveCompany;
- (void)setPrimitiveCompany:(Company*)value;

@end

@interface UserAttributes: NSObject 
+ (NSString *)avatarUrl;
+ (NSString *)email;
+ (NSString *)firstName;
+ (NSString *)fullName;
+ (NSString *)jobTitle;
+ (NSString *)lastName;
+ (NSString *)pushDeviceToken;
+ (NSString *)role;
+ (NSString *)userId;
@end

@interface UserRelationships: NSObject
+ (NSString *)company;
@end

NS_ASSUME_NONNULL_END
