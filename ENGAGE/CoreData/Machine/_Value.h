// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Value.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Behavior;

@interface ValueID : NSManagedObjectID {}
@end

@interface _Value : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ValueID *objectID;

@property (nonatomic, strong, nullable) NSString* iconUrl;

@property (nonatomic, strong, nullable) NSString* title;

@property (nonatomic, strong, nullable) NSString* valueDescription;

@property (nonatomic, strong, nullable) NSNumber* valueId;

@property (atomic) int64_t valueIdValue;
- (int64_t)valueIdValue;
- (void)setValueIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSOrderedSet<Behavior*> *behaviors;
- (nullable NSMutableOrderedSet<Behavior*>*)behaviorsSet;

@end

@interface _Value (BehaviorsCoreDataGeneratedAccessors)
- (void)addBehaviors:(NSOrderedSet<Behavior*>*)value_;
- (void)removeBehaviors:(NSOrderedSet<Behavior*>*)value_;
- (void)addBehaviorsObject:(Behavior*)value_;
- (void)removeBehaviorsObject:(Behavior*)value_;

- (void)insertObject:(Behavior*)value inBehaviorsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromBehaviorsAtIndex:(NSUInteger)idx;
- (void)insertBehaviors:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeBehaviorsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInBehaviorsAtIndex:(NSUInteger)idx withObject:(Behavior*)value;
- (void)replaceBehaviorsAtIndexes:(NSIndexSet *)indexes withBehaviors:(NSArray *)values;

@end

@interface _Value (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveIconUrl;
- (void)setPrimitiveIconUrl:(nullable NSString*)value;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

- (nullable NSString*)primitiveValueDescription;
- (void)setPrimitiveValueDescription:(nullable NSString*)value;

- (nullable NSNumber*)primitiveValueId;
- (void)setPrimitiveValueId:(nullable NSNumber*)value;

- (int64_t)primitiveValueIdValue;
- (void)setPrimitiveValueIdValue:(int64_t)value_;

- (NSMutableOrderedSet<Behavior*>*)primitiveBehaviors;
- (void)setPrimitiveBehaviors:(NSMutableOrderedSet<Behavior*>*)value;

@end

@interface ValueAttributes: NSObject 
+ (NSString *)iconUrl;
+ (NSString *)title;
+ (NSString *)valueDescription;
+ (NSString *)valueId;
@end

@interface ValueRelationships: NSObject
+ (NSString *)behaviors;
@end

NS_ASSUME_NONNULL_END
