// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Challenge.m instead.

#import "_Challenge.h"

@implementation ChallengeID
@end

@implementation _Challenge

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Challenge" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Challenge";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Challenge" inManagedObjectContext:moc_];
}

- (ChallengeID*)objectID {
	return (ChallengeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"feedIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"feedId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isActivityFeedChallengeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isActivityFeedChallenge"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isCurrentChallengeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isCurrentChallenge"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"receiverIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"receiverId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic backgroundImageUrl;

@dynamic borderColorHex;

@dynamic challengeDescription;

@dynamic createDate;

@dynamic feedId;

- (int64_t)feedIdValue {
	NSNumber *result = [self feedId];
	return [result longLongValue];
}

- (void)setFeedIdValue:(int64_t)value_ {
	[self setFeedId:@(value_)];
}

- (int64_t)primitiveFeedIdValue {
	NSNumber *result = [self primitiveFeedId];
	return [result longLongValue];
}

- (void)setPrimitiveFeedIdValue:(int64_t)value_ {
	[self setPrimitiveFeedId:@(value_)];
}

@dynamic imageIconUrl;

@dynamic isActivityFeedChallenge;

- (BOOL)isActivityFeedChallengeValue {
	NSNumber *result = [self isActivityFeedChallenge];
	return [result boolValue];
}

- (void)setIsActivityFeedChallengeValue:(BOOL)value_ {
	[self setIsActivityFeedChallenge:@(value_)];
}

- (BOOL)primitiveIsActivityFeedChallengeValue {
	NSNumber *result = [self primitiveIsActivityFeedChallenge];
	return [result boolValue];
}

- (void)setPrimitiveIsActivityFeedChallengeValue:(BOOL)value_ {
	[self setPrimitiveIsActivityFeedChallenge:@(value_)];
}

@dynamic isCurrentChallenge;

- (BOOL)isCurrentChallengeValue {
	NSNumber *result = [self isCurrentChallenge];
	return [result boolValue];
}

- (void)setIsCurrentChallengeValue:(BOOL)value_ {
	[self setIsCurrentChallenge:@(value_)];
}

- (BOOL)primitiveIsCurrentChallengeValue {
	NSNumber *result = [self primitiveIsCurrentChallenge];
	return [result boolValue];
}

- (void)setPrimitiveIsCurrentChallengeValue:(BOOL)value_ {
	[self setPrimitiveIsCurrentChallenge:@(value_)];
}

@dynamic receiverAvatarUrl;

@dynamic receiverId;

- (int64_t)receiverIdValue {
	NSNumber *result = [self receiverId];
	return [result longLongValue];
}

- (void)setReceiverIdValue:(int64_t)value_ {
	[self setReceiverId:@(value_)];
}

- (int64_t)primitiveReceiverIdValue {
	NSNumber *result = [self primitiveReceiverId];
	return [result longLongValue];
}

- (void)setPrimitiveReceiverIdValue:(int64_t)value_ {
	[self setPrimitiveReceiverId:@(value_)];
}

@dynamic receiverJob;

@dynamic receiverName;

@dynamic state;

@dynamic title;

@dynamic untilDate;

@dynamic feed;

@end

@implementation ChallengeAttributes 
+ (NSString *)backgroundImageUrl {
	return @"backgroundImageUrl";
}
+ (NSString *)borderColorHex {
	return @"borderColorHex";
}
+ (NSString *)challengeDescription {
	return @"challengeDescription";
}
+ (NSString *)createDate {
	return @"createDate";
}
+ (NSString *)feedId {
	return @"feedId";
}
+ (NSString *)imageIconUrl {
	return @"imageIconUrl";
}
+ (NSString *)isActivityFeedChallenge {
	return @"isActivityFeedChallenge";
}
+ (NSString *)isCurrentChallenge {
	return @"isCurrentChallenge";
}
+ (NSString *)receiverAvatarUrl {
	return @"receiverAvatarUrl";
}
+ (NSString *)receiverId {
	return @"receiverId";
}
+ (NSString *)receiverJob {
	return @"receiverJob";
}
+ (NSString *)receiverName {
	return @"receiverName";
}
+ (NSString *)state {
	return @"state";
}
+ (NSString *)title {
	return @"title";
}
+ (NSString *)untilDate {
	return @"untilDate";
}
@end

@implementation ChallengeRelationships 
+ (NSString *)feed {
	return @"feed";
}
@end

