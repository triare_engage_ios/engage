// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Comment.m instead.

#import "_Comment.h"

@implementation CommentID
@end

@implementation _Comment

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Comment" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Comment";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Comment" inManagedObjectContext:moc_];
}

- (CommentID*)objectID {
	return (CommentID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"commentIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"commentId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"senderIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"senderId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic body;

@dynamic commentId;

- (int64_t)commentIdValue {
	NSNumber *result = [self commentId];
	return [result longLongValue];
}

- (void)setCommentIdValue:(int64_t)value_ {
	[self setCommentId:@(value_)];
}

- (int64_t)primitiveCommentIdValue {
	NSNumber *result = [self primitiveCommentId];
	return [result longLongValue];
}

- (void)setPrimitiveCommentIdValue:(int64_t)value_ {
	[self setPrimitiveCommentId:@(value_)];
}

@dynamic createDate;

@dynamic senderAvatarUrl;

@dynamic senderId;

- (int64_t)senderIdValue {
	NSNumber *result = [self senderId];
	return [result longLongValue];
}

- (void)setSenderIdValue:(int64_t)value_ {
	[self setSenderId:@(value_)];
}

- (int64_t)primitiveSenderIdValue {
	NSNumber *result = [self primitiveSenderId];
	return [result longLongValue];
}

- (void)setPrimitiveSenderIdValue:(int64_t)value_ {
	[self setPrimitiveSenderId:@(value_)];
}

@dynamic senderName;

@dynamic activityFeed;

@end

@implementation CommentAttributes 
+ (NSString *)body {
	return @"body";
}
+ (NSString *)commentId {
	return @"commentId";
}
+ (NSString *)createDate {
	return @"createDate";
}
+ (NSString *)senderAvatarUrl {
	return @"senderAvatarUrl";
}
+ (NSString *)senderId {
	return @"senderId";
}
+ (NSString *)senderName {
	return @"senderName";
}
@end

@implementation CommentRelationships 
+ (NSString *)activityFeed {
	return @"activityFeed";
}
@end

