// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityFeedQuestion.m instead.

#import "_ActivityFeedQuestion.h"

@implementation ActivityFeedQuestionID
@end

@implementation _ActivityFeedQuestion

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ActivityFeedQuestion" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ActivityFeedQuestion";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ActivityFeedQuestion" inManagedObjectContext:moc_];
}

- (ActivityFeedQuestionID*)objectID {
	return (ActivityFeedQuestionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"haveCustomAnswerOptionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"haveCustomAnswerOption"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isMultipleValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isMultiple"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"questionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"questionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic body;

@dynamic haveCustomAnswerOption;

- (BOOL)haveCustomAnswerOptionValue {
	NSNumber *result = [self haveCustomAnswerOption];
	return [result boolValue];
}

- (void)setHaveCustomAnswerOptionValue:(BOOL)value_ {
	[self setHaveCustomAnswerOption:@(value_)];
}

- (BOOL)primitiveHaveCustomAnswerOptionValue {
	NSNumber *result = [self primitiveHaveCustomAnswerOption];
	return [result boolValue];
}

- (void)setPrimitiveHaveCustomAnswerOptionValue:(BOOL)value_ {
	[self setPrimitiveHaveCustomAnswerOption:@(value_)];
}

@dynamic isMultiple;

- (BOOL)isMultipleValue {
	NSNumber *result = [self isMultiple];
	return [result boolValue];
}

- (void)setIsMultipleValue:(BOOL)value_ {
	[self setIsMultiple:@(value_)];
}

- (BOOL)primitiveIsMultipleValue {
	NSNumber *result = [self primitiveIsMultiple];
	return [result boolValue];
}

- (void)setPrimitiveIsMultipleValue:(BOOL)value_ {
	[self setPrimitiveIsMultiple:@(value_)];
}

@dynamic questionId;

- (int64_t)questionIdValue {
	NSNumber *result = [self questionId];
	return [result longLongValue];
}

- (void)setQuestionIdValue:(int64_t)value_ {
	[self setQuestionId:@(value_)];
}

- (int64_t)primitiveQuestionIdValue {
	NSNumber *result = [self primitiveQuestionId];
	return [result longLongValue];
}

- (void)setPrimitiveQuestionIdValue:(int64_t)value_ {
	[self setPrimitiveQuestionId:@(value_)];
}

@dynamic answerOptions;

- (NSMutableOrderedSet<ActivityFeedAnswerOption*>*)answerOptionsSet {
	[self willAccessValueForKey:@"answerOptions"];

	NSMutableOrderedSet<ActivityFeedAnswerOption*> *result = (NSMutableOrderedSet<ActivityFeedAnswerOption*>*)[self mutableOrderedSetValueForKey:@"answerOptions"];

	[self didAccessValueForKey:@"answerOptions"];
	return result;
}

@end

@implementation _ActivityFeedQuestion (AnswerOptionsCoreDataGeneratedAccessors)
- (void)addAnswerOptions:(NSOrderedSet<ActivityFeedAnswerOption*>*)value_ {
	[self.answerOptionsSet unionOrderedSet:value_];
}
- (void)removeAnswerOptions:(NSOrderedSet<ActivityFeedAnswerOption*>*)value_ {
	[self.answerOptionsSet minusOrderedSet:value_];
}
- (void)addAnswerOptionsObject:(ActivityFeedAnswerOption*)value_ {
	[self.answerOptionsSet addObject:value_];
}
- (void)removeAnswerOptionsObject:(ActivityFeedAnswerOption*)value_ {
	[self.answerOptionsSet removeObject:value_];
}
- (void)insertObject:(ActivityFeedAnswerOption*)value inAnswerOptionsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answerOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answerOptions] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answerOptions"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answerOptions"];
}
- (void)removeObjectFromAnswerOptionsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answerOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answerOptions] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answerOptions"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answerOptions"];
}
- (void)insertAnswerOptions:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answerOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answerOptions] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answerOptions"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"answerOptions"];
}
- (void)removeAnswerOptionsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answerOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answerOptions] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answerOptions"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"answerOptions"];
}
- (void)replaceObjectInAnswerOptionsAtIndex:(NSUInteger)idx withObject:(ActivityFeedAnswerOption*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answerOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answerOptions] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answerOptions"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answerOptions"];
}
- (void)replaceAnswerOptionsAtIndexes:(NSIndexSet *)indexes withAnswerOptions:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answerOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self answerOptions] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"answerOptions"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"answerOptions"];
}
@end

@implementation ActivityFeedQuestionAttributes 
+ (NSString *)body {
	return @"body";
}
+ (NSString *)haveCustomAnswerOption {
	return @"haveCustomAnswerOption";
}
+ (NSString *)isMultiple {
	return @"isMultiple";
}
+ (NSString *)questionId {
	return @"questionId";
}
@end

@implementation ActivityFeedQuestionRelationships 
+ (NSString *)answerOptions {
	return @"answerOptions";
}
@end

