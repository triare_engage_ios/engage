// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UserBehavior.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

#import "Behavior.h"

NS_ASSUME_NONNULL_BEGIN

@class TeamUser;

@interface UserBehaviorID : BehaviorID {}
@end

@interface _UserBehavior : Behavior
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UserBehaviorID *objectID;

@property (nonatomic, strong, nullable) NSNumber* count;

@property (atomic) int16_t countValue;
- (int16_t)countValue;
- (void)setCountValue:(int16_t)value_;

@property (nonatomic, strong, nullable) TeamUser *teamUser;

@end

@interface _UserBehavior (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSNumber*)primitiveCount;
- (void)setPrimitiveCount:(nullable NSNumber*)value;

- (int16_t)primitiveCountValue;
- (void)setPrimitiveCountValue:(int16_t)value_;

- (TeamUser*)primitiveTeamUser;
- (void)setPrimitiveTeamUser:(TeamUser*)value;

@end

@interface UserBehaviorAttributes: NSObject 
+ (NSString *)count;
@end

@interface UserBehaviorRelationships: NSObject
+ (NSString *)teamUser;
@end

NS_ASSUME_NONNULL_END
