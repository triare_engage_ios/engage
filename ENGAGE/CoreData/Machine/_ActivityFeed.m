// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ActivityFeed.m instead.

#import "_ActivityFeed.h"

@implementation ActivityFeedID
@end

@implementation _ActivityFeed

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ActivityFeed" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ActivityFeed";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ActivityFeed" inManagedObjectContext:moc_];
}

- (ActivityFeedID*)objectID {
	return (ActivityFeedID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"behaviourIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"behaviourId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"commentsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"commentsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"feedIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"feedId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isReceiverPointWinnerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isReceiverPointWinner"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isRetapedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRetaped"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isTeamMessageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isTeamMessage"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isTopPointValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isTopPoint"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"receiverIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"receiverId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"retapsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"retapsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"senderIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"senderId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic behaviourIconUrl;

@dynamic behaviourId;

- (int64_t)behaviourIdValue {
	NSNumber *result = [self behaviourId];
	return [result longLongValue];
}

- (void)setBehaviourIdValue:(int64_t)value_ {
	[self setBehaviourId:@(value_)];
}

- (int64_t)primitiveBehaviourIdValue {
	NSNumber *result = [self primitiveBehaviourId];
	return [result longLongValue];
}

- (void)setPrimitiveBehaviourIdValue:(int64_t)value_ {
	[self setPrimitiveBehaviourId:@(value_)];
}

@dynamic behaviourTitle;

@dynamic body;

@dynamic commentsCount;

- (int16_t)commentsCountValue {
	NSNumber *result = [self commentsCount];
	return [result shortValue];
}

- (void)setCommentsCountValue:(int16_t)value_ {
	[self setCommentsCount:@(value_)];
}

- (int16_t)primitiveCommentsCountValue {
	NSNumber *result = [self primitiveCommentsCount];
	return [result shortValue];
}

- (void)setPrimitiveCommentsCountValue:(int16_t)value_ {
	[self setPrimitiveCommentsCount:@(value_)];
}

@dynamic createDate;

@dynamic feedId;

- (int64_t)feedIdValue {
	NSNumber *result = [self feedId];
	return [result longLongValue];
}

- (void)setFeedIdValue:(int64_t)value_ {
	[self setFeedId:@(value_)];
}

- (int64_t)primitiveFeedIdValue {
	NSNumber *result = [self primitiveFeedId];
	return [result longLongValue];
}

- (void)setPrimitiveFeedIdValue:(int64_t)value_ {
	[self setPrimitiveFeedId:@(value_)];
}

@dynamic isReceiverPointWinner;

- (double)isReceiverPointWinnerValue {
	NSNumber *result = [self isReceiverPointWinner];
	return [result doubleValue];
}

- (void)setIsReceiverPointWinnerValue:(double)value_ {
	[self setIsReceiverPointWinner:@(value_)];
}

- (double)primitiveIsReceiverPointWinnerValue {
	NSNumber *result = [self primitiveIsReceiverPointWinner];
	return [result doubleValue];
}

- (void)setPrimitiveIsReceiverPointWinnerValue:(double)value_ {
	[self setPrimitiveIsReceiverPointWinner:@(value_)];
}

@dynamic isRetaped;

- (BOOL)isRetapedValue {
	NSNumber *result = [self isRetaped];
	return [result boolValue];
}

- (void)setIsRetapedValue:(BOOL)value_ {
	[self setIsRetaped:@(value_)];
}

- (BOOL)primitiveIsRetapedValue {
	NSNumber *result = [self primitiveIsRetaped];
	return [result boolValue];
}

- (void)setPrimitiveIsRetapedValue:(BOOL)value_ {
	[self setPrimitiveIsRetaped:@(value_)];
}

@dynamic isTeamMessage;

- (BOOL)isTeamMessageValue {
	NSNumber *result = [self isTeamMessage];
	return [result boolValue];
}

- (void)setIsTeamMessageValue:(BOOL)value_ {
	[self setIsTeamMessage:@(value_)];
}

- (BOOL)primitiveIsTeamMessageValue {
	NSNumber *result = [self primitiveIsTeamMessage];
	return [result boolValue];
}

- (void)setPrimitiveIsTeamMessageValue:(BOOL)value_ {
	[self setPrimitiveIsTeamMessage:@(value_)];
}

@dynamic isTopPoint;

- (BOOL)isTopPointValue {
	NSNumber *result = [self isTopPoint];
	return [result boolValue];
}

- (void)setIsTopPointValue:(BOOL)value_ {
	[self setIsTopPoint:@(value_)];
}

- (BOOL)primitiveIsTopPointValue {
	NSNumber *result = [self primitiveIsTopPoint];
	return [result boolValue];
}

- (void)setPrimitiveIsTopPointValue:(BOOL)value_ {
	[self setPrimitiveIsTopPoint:@(value_)];
}

@dynamic itemType;

@dynamic receiverAvatarUrl;

@dynamic receiverId;

- (int64_t)receiverIdValue {
	NSNumber *result = [self receiverId];
	return [result longLongValue];
}

- (void)setReceiverIdValue:(int64_t)value_ {
	[self setReceiverId:@(value_)];
}

- (int64_t)primitiveReceiverIdValue {
	NSNumber *result = [self primitiveReceiverId];
	return [result longLongValue];
}

- (void)setPrimitiveReceiverIdValue:(int64_t)value_ {
	[self setPrimitiveReceiverId:@(value_)];
}

@dynamic receiverJob;

@dynamic receiverName;

@dynamic retapsCount;

- (int16_t)retapsCountValue {
	NSNumber *result = [self retapsCount];
	return [result shortValue];
}

- (void)setRetapsCountValue:(int16_t)value_ {
	[self setRetapsCount:@(value_)];
}

- (int16_t)primitiveRetapsCountValue {
	NSNumber *result = [self primitiveRetapsCount];
	return [result shortValue];
}

- (void)setPrimitiveRetapsCountValue:(int16_t)value_ {
	[self setPrimitiveRetapsCount:@(value_)];
}

@dynamic senderAvatarUrl;

@dynamic senderId;

- (int64_t)senderIdValue {
	NSNumber *result = [self senderId];
	return [result longLongValue];
}

- (void)setSenderIdValue:(int64_t)value_ {
	[self setSenderId:@(value_)];
}

- (int64_t)primitiveSenderIdValue {
	NSNumber *result = [self primitiveSenderId];
	return [result longLongValue];
}

- (void)setPrimitiveSenderIdValue:(int64_t)value_ {
	[self setPrimitiveSenderId:@(value_)];
}

@dynamic senderName;

@dynamic challenge;

@dynamic comments;

- (NSMutableSet<Comment*>*)commentsSet {
	[self willAccessValueForKey:@"comments"];

	NSMutableSet<Comment*> *result = (NSMutableSet<Comment*>*)[self mutableSetValueForKey:@"comments"];

	[self didAccessValueForKey:@"comments"];
	return result;
}

@dynamic learning;

@dynamic topPoint;

@end

@implementation ActivityFeedAttributes 
+ (NSString *)behaviourIconUrl {
	return @"behaviourIconUrl";
}
+ (NSString *)behaviourId {
	return @"behaviourId";
}
+ (NSString *)behaviourTitle {
	return @"behaviourTitle";
}
+ (NSString *)body {
	return @"body";
}
+ (NSString *)commentsCount {
	return @"commentsCount";
}
+ (NSString *)createDate {
	return @"createDate";
}
+ (NSString *)feedId {
	return @"feedId";
}
+ (NSString *)isReceiverPointWinner {
	return @"isReceiverPointWinner";
}
+ (NSString *)isRetaped {
	return @"isRetaped";
}
+ (NSString *)isTeamMessage {
	return @"isTeamMessage";
}
+ (NSString *)isTopPoint {
	return @"isTopPoint";
}
+ (NSString *)itemType {
	return @"itemType";
}
+ (NSString *)receiverAvatarUrl {
	return @"receiverAvatarUrl";
}
+ (NSString *)receiverId {
	return @"receiverId";
}
+ (NSString *)receiverJob {
	return @"receiverJob";
}
+ (NSString *)receiverName {
	return @"receiverName";
}
+ (NSString *)retapsCount {
	return @"retapsCount";
}
+ (NSString *)senderAvatarUrl {
	return @"senderAvatarUrl";
}
+ (NSString *)senderId {
	return @"senderId";
}
+ (NSString *)senderName {
	return @"senderName";
}
@end

@implementation ActivityFeedRelationships 
+ (NSString *)challenge {
	return @"challenge";
}
+ (NSString *)comments {
	return @"comments";
}
+ (NSString *)learning {
	return @"learning";
}
+ (NSString *)topPoint {
	return @"topPoint";
}
@end

