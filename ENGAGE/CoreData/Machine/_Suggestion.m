// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Suggestion.m instead.

#import "_Suggestion.h"

@implementation SuggestionID
@end

@implementation _Suggestion

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Suggestion" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Suggestion";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Suggestion" inManagedObjectContext:moc_];
}

- (SuggestionID*)objectID {
	return (SuggestionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isReadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isRead"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"senderIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"senderId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"suggestionIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"suggestionId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic body;

@dynamic createDate;

@dynamic isRead;

- (BOOL)isReadValue {
	NSNumber *result = [self isRead];
	return [result boolValue];
}

- (void)setIsReadValue:(BOOL)value_ {
	[self setIsRead:@(value_)];
}

- (BOOL)primitiveIsReadValue {
	NSNumber *result = [self primitiveIsRead];
	return [result boolValue];
}

- (void)setPrimitiveIsReadValue:(BOOL)value_ {
	[self setPrimitiveIsRead:@(value_)];
}

@dynamic senderAvatarUrl;

@dynamic senderId;

- (int64_t)senderIdValue {
	NSNumber *result = [self senderId];
	return [result longLongValue];
}

- (void)setSenderIdValue:(int64_t)value_ {
	[self setSenderId:@(value_)];
}

- (int64_t)primitiveSenderIdValue {
	NSNumber *result = [self primitiveSenderId];
	return [result longLongValue];
}

- (void)setPrimitiveSenderIdValue:(int64_t)value_ {
	[self setPrimitiveSenderId:@(value_)];
}

@dynamic senderName;

@dynamic suggestionId;

- (int64_t)suggestionIdValue {
	NSNumber *result = [self suggestionId];
	return [result longLongValue];
}

- (void)setSuggestionIdValue:(int64_t)value_ {
	[self setSuggestionId:@(value_)];
}

- (int64_t)primitiveSuggestionIdValue {
	NSNumber *result = [self primitiveSuggestionId];
	return [result longLongValue];
}

- (void)setPrimitiveSuggestionIdValue:(int64_t)value_ {
	[self setPrimitiveSuggestionId:@(value_)];
}

@end

@implementation SuggestionAttributes 
+ (NSString *)body {
	return @"body";
}
+ (NSString *)createDate {
	return @"createDate";
}
+ (NSString *)isRead {
	return @"isRead";
}
+ (NSString *)senderAvatarUrl {
	return @"senderAvatarUrl";
}
+ (NSString *)senderId {
	return @"senderId";
}
+ (NSString *)senderName {
	return @"senderName";
}
+ (NSString *)suggestionId {
	return @"suggestionId";
}
@end

