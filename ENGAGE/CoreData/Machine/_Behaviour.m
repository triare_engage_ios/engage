// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Behaviour.m instead.

#import "_Behaviour.h"

const struct BehaviourAttributes BehaviourAttributes = {
	.behaviorDescription = @"behaviorDescription",
	.behaviorId = @"behaviorId",
	.iconUrl = @"iconUrl",
	.presentatioFileUrl = @"presentatioFileUrl",
	.title = @"title",
};

@implementation BehaviourID
@end

@implementation _Behaviour

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Behavior" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Behavior";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Behavior" inManagedObjectContext:moc_];
}

- (BehaviourID*)objectID {
	return (BehaviourID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"behaviorIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"behaviorId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic behaviorDescription;

@dynamic behaviorId;

- (int64_t)behaviorIdValue {
	NSNumber *result = [self behaviorId];
	return [result longLongValue];
}

- (void)setBehaviorIdValue:(int64_t)value_ {
	[self setBehaviorId:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveBehaviorIdValue {
	NSNumber *result = [self primitiveBehaviorId];
	return [result longLongValue];
}

- (void)setPrimitiveBehaviorIdValue:(int64_t)value_ {
	[self setPrimitiveBehaviorId:[NSNumber numberWithLongLong:value_]];
}

@dynamic iconUrl;

@dynamic presentatioFileUrl;

@dynamic title;

@end

