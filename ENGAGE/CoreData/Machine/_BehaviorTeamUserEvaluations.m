// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BehaviorTeamUserEvaluations.m instead.

#import "_BehaviorTeamUserEvaluations.h"

@implementation BehaviorTeamUserEvaluationsID
@end

@implementation _BehaviorTeamUserEvaluations

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BehaviorTeamUserEvaluations" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BehaviorTeamUserEvaluations";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BehaviorTeamUserEvaluations" inManagedObjectContext:moc_];
}

- (BehaviorTeamUserEvaluationsID*)objectID {
	return (BehaviorTeamUserEvaluationsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"badEvaluationsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"badEvaluationsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"goodEvaluationsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"goodEvaluationsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"progressValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"progress"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalEvaluationsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalEvaluationsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic badEvaluationsCount;

- (int64_t)badEvaluationsCountValue {
	NSNumber *result = [self badEvaluationsCount];
	return [result longLongValue];
}

- (void)setBadEvaluationsCountValue:(int64_t)value_ {
	[self setBadEvaluationsCount:@(value_)];
}

- (int64_t)primitiveBadEvaluationsCountValue {
	NSNumber *result = [self primitiveBadEvaluationsCount];
	return [result longLongValue];
}

- (void)setPrimitiveBadEvaluationsCountValue:(int64_t)value_ {
	[self setPrimitiveBadEvaluationsCount:@(value_)];
}

@dynamic goodEvaluationsCount;

- (int64_t)goodEvaluationsCountValue {
	NSNumber *result = [self goodEvaluationsCount];
	return [result longLongValue];
}

- (void)setGoodEvaluationsCountValue:(int64_t)value_ {
	[self setGoodEvaluationsCount:@(value_)];
}

- (int64_t)primitiveGoodEvaluationsCountValue {
	NSNumber *result = [self primitiveGoodEvaluationsCount];
	return [result longLongValue];
}

- (void)setPrimitiveGoodEvaluationsCountValue:(int64_t)value_ {
	[self setPrimitiveGoodEvaluationsCount:@(value_)];
}

@dynamic progress;

- (float)progressValue {
	NSNumber *result = [self progress];
	return [result floatValue];
}

- (void)setProgressValue:(float)value_ {
	[self setProgress:@(value_)];
}

- (float)primitiveProgressValue {
	NSNumber *result = [self primitiveProgress];
	return [result floatValue];
}

- (void)setPrimitiveProgressValue:(float)value_ {
	[self setPrimitiveProgress:@(value_)];
}

@dynamic totalEvaluationsCount;

- (int64_t)totalEvaluationsCountValue {
	NSNumber *result = [self totalEvaluationsCount];
	return [result longLongValue];
}

- (void)setTotalEvaluationsCountValue:(int64_t)value_ {
	[self setTotalEvaluationsCount:@(value_)];
}

- (int64_t)primitiveTotalEvaluationsCountValue {
	NSNumber *result = [self primitiveTotalEvaluationsCount];
	return [result longLongValue];
}

- (void)setPrimitiveTotalEvaluationsCountValue:(int64_t)value_ {
	[self setPrimitiveTotalEvaluationsCount:@(value_)];
}

@end

@implementation BehaviorTeamUserEvaluationsAttributes 
+ (NSString *)badEvaluationsCount {
	return @"badEvaluationsCount";
}
+ (NSString *)goodEvaluationsCount {
	return @"goodEvaluationsCount";
}
+ (NSString *)progress {
	return @"progress";
}
+ (NSString *)totalEvaluationsCount {
	return @"totalEvaluationsCount";
}
@end

