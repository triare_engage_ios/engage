// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LeaderBehavior.m instead.

#import "_LeaderBehavior.h"

@implementation LeaderBehaviorID
@end

@implementation _LeaderBehavior

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LeaderBehavior" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LeaderBehavior";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LeaderBehavior" inManagedObjectContext:moc_];
}

- (LeaderBehaviorID*)objectID {
	return (LeaderBehaviorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"tapsCountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"tapsCount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic tapsCount;

- (int16_t)tapsCountValue {
	NSNumber *result = [self tapsCount];
	return [result shortValue];
}

- (void)setTapsCountValue:(int16_t)value_ {
	[self setTapsCount:@(value_)];
}

- (int16_t)primitiveTapsCountValue {
	NSNumber *result = [self primitiveTapsCount];
	return [result shortValue];
}

- (void)setPrimitiveTapsCountValue:(int16_t)value_ {
	[self setPrimitiveTapsCount:@(value_)];
}

@end

@implementation LeaderBehaviorAttributes 
+ (NSString *)tapsCount {
	return @"tapsCount";
}
@end

