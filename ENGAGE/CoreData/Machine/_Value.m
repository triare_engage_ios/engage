// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Value.m instead.

#import "_Value.h"

@implementation ValueID
@end

@implementation _Value

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Value" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Value";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Value" inManagedObjectContext:moc_];
}

- (ValueID*)objectID {
	return (ValueID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"valueIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"valueId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic iconUrl;

@dynamic title;

@dynamic valueDescription;

@dynamic valueId;

- (int64_t)valueIdValue {
	NSNumber *result = [self valueId];
	return [result longLongValue];
}

- (void)setValueIdValue:(int64_t)value_ {
	[self setValueId:@(value_)];
}

- (int64_t)primitiveValueIdValue {
	NSNumber *result = [self primitiveValueId];
	return [result longLongValue];
}

- (void)setPrimitiveValueIdValue:(int64_t)value_ {
	[self setPrimitiveValueId:@(value_)];
}

@dynamic behaviors;

- (NSMutableOrderedSet<Behavior*>*)behaviorsSet {
	[self willAccessValueForKey:@"behaviors"];

	NSMutableOrderedSet<Behavior*> *result = (NSMutableOrderedSet<Behavior*>*)[self mutableOrderedSetValueForKey:@"behaviors"];

	[self didAccessValueForKey:@"behaviors"];
	return result;
}

@end

@implementation _Value (BehaviorsCoreDataGeneratedAccessors)
- (void)addBehaviors:(NSOrderedSet<Behavior*>*)value_ {
	[self.behaviorsSet unionOrderedSet:value_];
}
- (void)removeBehaviors:(NSOrderedSet<Behavior*>*)value_ {
	[self.behaviorsSet minusOrderedSet:value_];
}
- (void)addBehaviorsObject:(Behavior*)value_ {
	[self.behaviorsSet addObject:value_];
}
- (void)removeBehaviorsObject:(Behavior*)value_ {
	[self.behaviorsSet removeObject:value_];
}
- (void)insertObject:(Behavior*)value inBehaviorsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"behaviors"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self behaviors] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"behaviors"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"behaviors"];
}
- (void)removeObjectFromBehaviorsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"behaviors"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self behaviors] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"behaviors"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"behaviors"];
}
- (void)insertBehaviors:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"behaviors"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self behaviors] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"behaviors"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"behaviors"];
}
- (void)removeBehaviorsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"behaviors"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self behaviors] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"behaviors"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"behaviors"];
}
- (void)replaceObjectInBehaviorsAtIndex:(NSUInteger)idx withObject:(Behavior*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"behaviors"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self behaviors] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"behaviors"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"behaviors"];
}
- (void)replaceBehaviorsAtIndexes:(NSIndexSet *)indexes withBehaviors:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"behaviors"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self behaviors] ?: [NSOrderedSet orderedSet]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"behaviors"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"behaviors"];
}
@end

@implementation ValueAttributes 
+ (NSString *)iconUrl {
	return @"iconUrl";
}
+ (NSString *)title {
	return @"title";
}
+ (NSString *)valueDescription {
	return @"valueDescription";
}
+ (NSString *)valueId {
	return @"valueId";
}
@end

@implementation ValueRelationships 
+ (NSString *)behaviors {
	return @"behaviors";
}
@end

