// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BehaviorIcon.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface BehaviorIconID : NSManagedObjectID {}
@end

@interface _BehaviorIcon : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BehaviorIconID *objectID;

@property (nonatomic, strong, nullable) NSString* category;

@property (nonatomic, strong, nullable) NSNumber* iconId;

@property (atomic) int64_t iconIdValue;
- (int64_t)iconIdValue;
- (void)setIconIdValue:(int64_t)value_;

@property (nonatomic, strong, nullable) NSString* iconImageUrl;

@end

@interface _BehaviorIcon (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveCategory;
- (void)setPrimitiveCategory:(nullable NSString*)value;

- (nullable NSNumber*)primitiveIconId;
- (void)setPrimitiveIconId:(nullable NSNumber*)value;

- (int64_t)primitiveIconIdValue;
- (void)setPrimitiveIconIdValue:(int64_t)value_;

- (nullable NSString*)primitiveIconImageUrl;
- (void)setPrimitiveIconImageUrl:(nullable NSString*)value;

@end

@interface BehaviorIconAttributes: NSObject 
+ (NSString *)category;
+ (NSString *)iconId;
+ (NSString *)iconImageUrl;
@end

NS_ASSUME_NONNULL_END
