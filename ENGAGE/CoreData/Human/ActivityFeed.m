#import "ActivityFeed.h"

#import "NSDictionary+SecureValue.h"
#import "NSDate+TimeAgo.h"
#import <MagicalRecord/MagicalRecord.h>

@interface ActivityFeed ()

// Private interface goes here.

@end

@implementation ActivityFeed

#pragma mark - Public Methods

+(NSPredicate *) predicateForType:(ActivityFeedType) feedType andId:(NSNumber *) feedId {
    
    NSString *itemType = [self itemTypeFromFeedType:feedType];
    
    NSMutableArray *predicates = [NSMutableArray array];
    NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"itemType MATCHES[cd] %@ ", itemType];
    NSPredicate *idPredicate = [NSPredicate predicateWithFormat:@"feedId == %@", feedId];
    [predicates addObject:typePredicate];
    [predicates addObject:idPredicate];
    
    return [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
}

-(void) createWithInfo:(NSDictionary *)info inContext:(NSManagedObjectContext *)context {
    
    self.feedId = [info secureNumberForKey:@"id"];
    self.itemType = [info secureStringForKey:@"type" andDefault:@"unknown"];
    NSNumber *timestamp = [info secureNumberForKey:@"created_at"];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
    
    ActivityFeedType activityFeedType = [self feedType];
    
    //Used for sorting top points in Activity Feed
    if (activityFeedType == ActivityFeedTypeTopPoint) {
        self.isTopPoint = [NSNumber numberWithBool:YES];
    }
    else {
        self.isTopPoint = [NSNumber numberWithBool:NO];
    }
    
    switch (activityFeedType) {
        case ActivityFeedTypeUnknown:
            break;
            
        case ActivityFeedTypeTap:
            [self createTapWithInfo:info];
            break;
            
        case ActivityFeedTypeTeamMessage:
            [self createTeamMessageWithInfo:info];
            break;
            
        case ActivityFeedTypeTopPoint:
            [self createTopPointWithInfo:info inContext:context];
            break;
        
        case ActivityFeedTypeChallenge:
            [self createChallengeWithInfo:info inContext:context];
            break;
        
        case ActivityFeedTypeLearning:
            [self createLearningWithInfo:info inContext:context];
            break;
    }
}

-(NSString *) formattedCreateDate {
    return [self.createDate formattedAsTimeAgo];
}

-(ActivityFeedType) feedType {
    return [self feedTypeFromItemType:self.itemType];
}

#pragma mark - Private methods

- (ActivityFeedType) feedTypeFromItemType:(NSString *) itemType {
    if ([itemType isEqualToString:@"unknown"]) {
        return ActivityFeedTypeUnknown;
    }
    
    if ([itemType isEqualToString:@"tap"]) {
        return ActivityFeedTypeTap;
    }
    
    if ([itemType isEqualToString:@"team_message"]) {
        return ActivityFeedTypeTeamMessage;
    }
    
    if ([itemType isEqualToString:@"top_point"]) {
        return ActivityFeedTypeTopPoint;
    }
    
    if ([itemType isEqualToString:@"challenge"] || [itemType isEqualToString:@"challange"]) {
        return ActivityFeedTypeChallenge;
    }
    
    if ([itemType isEqualToString:@"learning"]) {
        return ActivityFeedTypeLearning;
    }
    
    return ActivityFeedTypeUnknown;
}

+ (NSString *) itemTypeFromFeedType:(ActivityFeedType) feedType {
    
    switch (feedType) {
        case ActivityFeedTypeTap:
            return @"tap";
            break;
            
        case ActivityFeedTypeTopPoint:
            return @"top_point";
            break;
            
        case ActivityFeedTypeUnknown:
            return @"unknown";
            break;
            
        case ActivityFeedTypeChallenge:
            return @"challange";
            break;
            
        case ActivityFeedTypeTeamMessage:
            return @"team_message";
            break;
            
        case ActivityFeedTypeLearning:
            return @"learning";
            
        default:
            break;
    }
}

- (void) createTapWithInfo:(NSDictionary *) info {
    self.body = [info secureStringForKey:@"body"];
    self.commentsCount = [info secureNumberForKey:@"comments_count" andDefault:@0];
    
    self.behaviourId = [info secureNumberForKey:@"behaviour.id"];
    self.behaviourTitle = [info secureStringForKey:@"behaviour.title"];
    self.behaviourIconUrl = [info secureStringForKey:@"behaviour.icon_url"];
    
    self.receiverId = [info secureNumberForKey:@"receiver.id"];
    self.receiverJob = [info secureStringForKey:@"receiver.job_title"];
    self.receiverName = [info secureStringForKey:@"receiver.full_name"];
    self.receiverAvatarUrl = [info secureStringForKey:@"receiver.avatar_url"];
    
    self.senderId = [info secureNumberForKey:@"sender.id"];
    self.senderAvatarUrl = [info secureStringForKey:@"sender.avatar_url"];
    self.senderName = [info secureStringForKey:@"sender.full_name"];
    
    self.retapsCount = [info secureNumberForKey:@"retaps_count"];
    self.isRetaped = [info secureNumberForKey:@"rettaped"];
    
    self.isReceiverPointWinner = [info secureNumberForKey:@"receiver.top_point_winner"];
    
    //Deprecated. Use itemType
    self.isTeamMessage = [info secureNumberForKey:@"team_message"];
}

- (void) createTeamMessageWithInfo:(NSDictionary *) info {
    self.body = [info secureStringForKey:@"body"];
    self.senderId = [info secureNumberForKey:@"sender.id"];
    self.senderAvatarUrl = [info secureStringForKey:@"sender.avatar_url"];
    self.senderName = [info secureStringForKey:@"sender.full_name"];
    
    self.isReceiverPointWinner = [info secureNumberForKey:@"sender.top_point_winner"];
    
    //Deprecated. Use itemType
    self.isTeamMessage = [info secureNumberForKey:@"team_message"];
}

- (void) createTopPointWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    self.topPoint = [TopPoint MR_createEntityInContext:context];
    [self.topPoint createWithInfo:info];
    
    self.receiverId = [info secureNumberForKey:@"receiver.id"];
    self.receiverJob = [info secureStringForKey:@"receiver.job_title"];
    self.receiverName = [info secureStringForKey:@"receiver.full_name"];
    self.receiverAvatarUrl = [info secureStringForKey:@"receiver.avatar_url"];
    
    self.isReceiverPointWinner = [NSNumber numberWithBool:YES];
    
    //Deprecated. Use itemType
    self.isTeamMessage = [info secureNumberForKey:@"team_message"];
}

- (void) createChallengeWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    self.receiverId = [info secureNumberForKey:@"receiver.id"];
    self.receiverJob = [info secureStringForKey:@"receiver.job_title"];
    self.receiverName = [info secureStringForKey:@"receiver.full_name"];
    self.receiverAvatarUrl = [info secureStringForKey:@"receiver.avatar_url"];
    self.isReceiverPointWinner = [info secureNumberForKey:@"receiver.top_point_winner"];
    
    self.challenge = [Challenge MR_createEntityInContext:context];
    [self.challenge createWithInfo:info];
}

- (void) createLearningWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *) context {
    
    self.learning = [CDLearning MR_createEntityInContext:context];
    [self.learning createWithInfo:info];
}

#pragma mark - Deprecated

//Deprecated. Use: createWithInfo:(NSDictionary *)info inContext
-(void) createWithInfo:(NSDictionary *) info {
    
    self.feedId = [info secureNumberForKey:@"id"];
    self.itemType = [info secureStringForKey:@"type" andDefault:@"unknown"];
    NSNumber *timestamp = [info secureNumberForKey:@"created_at"];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
    
    //Type list:
//    1. top_point
//    2. tap
//    3. team_message
    
    ActivityFeedType activityFeedType = [self feedType];
    
    switch (activityFeedType) {
        case ActivityFeedTypeUnknown:
            break;
            
        case ActivityFeedTypeTopPoint:
            self.topPoint.imageUrl = [info secureStringForKey:@"image_url"];
            self.topPoint.count = [info secureNumberForKey:@"point_count"];
            self.topPoint.feedId = [info secureNumberForKey:@"id"];
            
            self.receiverId = [info secureNumberForKey:@"receiver.id"];
            self.receiverJob = [info secureStringForKey:@"receiver.job_title"];
            self.receiverName = [info secureStringForKey:@"receiver.full_name"];
            self.receiverAvatarUrl = [info secureStringForKey:@"receiver.avatar_url"];
            
            //Deprecated. Use itemType
            self.isTeamMessage = [info secureNumberForKey:@"team_message"];
            
            break;
            
        case ActivityFeedTypeTap:
            self.body = [info secureStringForKey:@"body"];
            self.commentsCount = [info secureNumberForKey:@"comments_count" andDefault:@0];
            
            self.behaviourId = [info secureNumberForKey:@"behaviour.id"];
            self.behaviourTitle = [info secureStringForKey:@"behaviour.title"];
            self.behaviourIconUrl = [info secureStringForKey:@"behaviour.icon_url"];
            
            self.receiverId = [info secureNumberForKey:@"receiver.id"];
            self.receiverJob = [info secureStringForKey:@"receiver.job_title"];
            self.receiverName = [info secureStringForKey:@"receiver.full_name"];
            self.receiverAvatarUrl = [info secureStringForKey:@"receiver.avatar_url"];
            
            self.senderId = [info secureNumberForKey:@"sender.id"];
            self.senderAvatarUrl = [info secureStringForKey:@"sender.avatar_url"];
            self.senderName = [info secureStringForKey:@"sender.full_name"];
            
            self.retapsCount = [info secureNumberForKey:@"retaps_count"];
            self.isRetaped = [info secureNumberForKey:@"rettaped"];
            
            //Deprecated. Use itemType
            self.isTeamMessage = [info secureNumberForKey:@"team_message"];
            
            break;
            
        case ActivityFeedTypeTeamMessage:
            self.body = [info secureStringForKey:@"body"];
            self.senderId = [info secureNumberForKey:@"sender.id"];
            self.senderAvatarUrl = [info secureStringForKey:@"sender.avatar_url"];
            self.senderName = [info secureStringForKey:@"sender.full_name"];
            
            //Deprecated. Use itemType
            self.isTeamMessage = [info secureNumberForKey:@"team_message"];
            
        case ActivityFeedTypeChallenge:
            break;
            
        case ActivityFeedTypeLearning:
            break;
    }
}


@end
