#import "Question.h"

@interface Question ()

// Private interface goes here.

@end

@implementation Question

// Custom logic goes here.
- (void) createQuestionWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    
    self.questionId = [info secureNumberForKey:@"id"];
    self.body = [info secureStringForKey:@"body"];
    self.isMultipleAnswer = [info secureNumberForKey:@"multiple"];
    self.answersCount = [info secureNumberForKey:@"answers_count"];
    self.answeredUsersCount = [info secureNumberForKey:@"answered_users_count"];
    self.usersCount = [info secureNumberForKey:@"users_count"];
    
    NSNumber *timestampCreateAt = [info secureNumberForKey:@"created_at"];
    self.createdAt = [NSDate dateWithTimeIntervalSince1970:timestampCreateAt.integerValue];
    
    NSNumber *timestampStartsAt = [info secureNumberForKey:@"starts_at"];
    self.startsAt = [NSDate dateWithTimeIntervalSince1970:timestampStartsAt.integerValue];
    
    for (NSDictionary *answer in [info valueForKey:@"answer_options"]) {
        
        //Fill custom answers
        if ([answer valueForKey:@"custom_answers"] != nil) {
            for (NSString *body in [answer valueForKey:@"custom_answers"]) {
                QuestionCustomAnswer *customAnswer = [QuestionCustomAnswer createWithText:body inContext:context];
                customAnswer.questionId = [info secureNumberForKey:@"id"];
                [self.customAnswersSet addObject:customAnswer];
                self.haveCustomAnswers = [NSNumber numberWithBool:YES];
            }
            self.customAnswersCount = [answer secureNumberForKey:@"answers_count"];
        }
        else {
            //Fill native answers
            QuestionAnswer *nativeAnswer = [QuestionAnswer createWithInfo:answer inContext:context];
            nativeAnswer.questionId = [info secureNumberForKey:@"id"];
            [self.answersSet addObject:nativeAnswer];
            
            self.haveCustomAnswers = [NSNumber numberWithBool:NO];
        }
    }
    
}

-(NSString *) formattedCreateAt {
    return [self.createdAt formattedAsTimeAgo];
}

-(NSString *) formattedStartsAt {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeStyle:NO];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    return [dateFormatter stringFromDate:self.startsAt];
}

-(NSArray *) notEmptyAnswers {
    
    NSMutableArray *result = [NSMutableArray new];
    
    for (int i = 0; i < self.answers.array.count; i++ ) {
        QuestionAnswer *answer = self.answers.array[i];
        
        if (answer.answersCount.integerValue > 0) {
            [result addObject:answer];
        }
    }
    
    return result;
}

@end
