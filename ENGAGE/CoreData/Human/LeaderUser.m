#import "LeaderUser.h"
#import "NSDictionary+SecureValue.h"

@interface LeaderUser ()

// Private interface goes here.

@end

@implementation LeaderUser

-(void) updateUserWithInfo:(NSDictionary *) info {
    
    [super updateUserWithInfo:info];
    
    self.receivedTapsCount = [info secureNumberForKey:@"received_taps_count" andDefault:[NSNumber numberWithInt:0]];
    self.givenTapsCount = [info secureNumberForKey:@"given_taps_count" andDefault:[NSNumber numberWithInt:0]];
}

@end
