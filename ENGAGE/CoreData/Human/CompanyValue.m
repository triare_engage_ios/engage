#import "CompanyValue.h"

@interface CompanyValue ()

// Private interface goes here.

@end

@implementation CompanyValue

- (void) createWithInfo:(NSDictionary *) info {
    
    self.valueId = [info secureNumberForKey:@"id"];
    self.title = [info secureStringForKey:@"title"];
}

@end
