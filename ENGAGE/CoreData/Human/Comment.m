#import "Comment.h"
#import "NSDictionary+SecureValue.h"

@interface Comment ()

// Private interface goes here.

@end

@implementation Comment

-(void) createWithInfo:(NSDictionary *) info {
    
    self.commentId = [info secureNumberForKey:@"id" andDefault:@0];
    self.body = [info secureStringForKey:@"body"];
    NSNumber *timestamp = [info secureNumberForKey:@"created_at"];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];

    
    self.senderId = [info secureNumberForKey:@"user.id"];
    self.senderAvatarUrl = [info secureStringForKey:@"user.avatar_url"];
    self.senderName = [info secureStringForKey:@"user.full_name"];
}

@end
