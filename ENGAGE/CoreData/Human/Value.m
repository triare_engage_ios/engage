#import "Value.h"
#import "NSDictionary+SecureValue.h"

@interface Value ()

// Private interface goes here.

@end

@implementation Value

// Custom logic goes here.

- (void) createValueWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    
    self.valueId = [info secureNumberForKey:@"id"];
    self.title = [info secureStringForKey:@"title"];
    self.iconUrl = [info secureStringForKey:@"icon_url"];
    self.valueDescription = [info secureStringForKey:@"description"];
    
    for (NSDictionary *behaviorInfo in [info valueForKey:@"behaviours"]) {
        Behavior *newBehavior = [Behavior createWithInfo:behaviorInfo inContext:context];
        newBehavior.valueId = [info secureNumberForKey:@"id"];
        [self.behaviorsSet addObject:newBehavior];
    }
}

+(UIImage *) placeholder {
    return [UIImage imageNamed:@"icon-value"];
}

@end
