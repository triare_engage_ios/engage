#import "TopPoint.h"

@interface TopPoint ()

// Private interface goes here.

@end

@implementation TopPoint

// Custom logic goes here.

- (void) createWithInfo:(NSDictionary *) info {
    self.feedId = [info secureNumberForKey:@"id"];
    self.imageUrl = [info secureStringForKey:@"image_url"];
    self.count = [info secureNumberForKey:@"point_count"];
}

@end
