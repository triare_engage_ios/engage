#import "_CDLearning.h"

@interface CDLearning : _CDLearning
// Custom logic goes here.

+ (UIImage *) placeholder;
+ (NSInteger) ratingFromStars:(NSInteger) startsNumber;
+ (NSInteger) starsFromRating:(NSInteger) rating;


- (void) createWithInfo:(NSDictionary *) info;
- (void) createWithInfo:(NSDictionary *) info learningList:(BOOL) isFromLearningList;
- (NSString *) formattedCreateDate;
- (NSInteger) totalRatingStars;

@end
