#import "EvaluateBehavior.h"
#import "NSDictionary+SecureValue.h"


@interface EvaluateBehavior ()

// Private interface goes here.

@end

@implementation EvaluateBehavior

-(void) createWithInfo:(NSDictionary *) info {
    
    [super createWithInfo:info];
    
    self.evaluateState = [info secureNumberForKey:@"state"];
}

@end
