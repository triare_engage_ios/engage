#import "Behavior.h"
#import "NSDictionary+SecureValue.h"
#import <MagicalRecord/MagicalRecord.h>

@interface Behavior ()

// Private interface goes here.

@end

@implementation Behavior

-(void) createWithInfo:(NSDictionary *) info {
    
    self.behaviorId = [info secureNumberForKey:@"id"];
    self.title = [info secureStringForKey:@"title"];
    self.behaviorDescription = [info secureStringForKey:@"description"];
    self.iconUrl = [info secureStringForKey:@"icon_url"];
    self.presentatioFileUrl = [info secureStringForKey:@"presentation_file_url"];
    self.isActive = [info secureNumberForKey:@"active"];
}

-(void) updateWithInfo:(NSDictionary *) info {
    
    if ([info valueForKey:@"id"]) {
        self.behaviorId = [info secureNumberForKey:@"id"];
    }
    
    if ([info valueForKey:@"title"]) {
        self.title = [info secureStringForKey:@"title"];
    }
    
    if ([info valueForKey:@"description"]) {
        self.behaviorDescription = [info secureStringForKey:@"description"];
    }
    
    if ([info valueForKey:@"icon_url"]) {
        self.iconUrl = [info secureStringForKey:@"icon_url"];
    }
    
    if ([info valueForKey:@"presentation_file_url"]) {
        self.presentatioFileUrl = [info secureStringForKey:@"presentation_file_url"];
    }
    
    if ([info valueForKey:@"active"]) {
        self.isActive = [info secureNumberForKey:@"active"];
    }
    
}

+(UIImage *) placeholder {
    return [UIImage imageNamed:@"icon-behaviour"];
}

+(Behavior *) createWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    
    Behavior *newBehavior = [Behavior MR_createEntityInContext:context];
    newBehavior.behaviorId = [info secureNumberForKey:@"id"];
    newBehavior.title = [info secureStringForKey:@"title"];
    newBehavior.behaviorDescription = [info secureStringForKey:@"description"];
    newBehavior.iconUrl = [info secureStringForKey:@"icon_url"];
    newBehavior.presentatioFileUrl = [info secureStringForKey:@"presentation_file_url"];
    newBehavior.isActive = [info secureNumberForKey:@"active"];
    
    return newBehavior;
}

@end
