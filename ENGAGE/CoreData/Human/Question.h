#import "_Question.h"
#import "NSDictionary+SecureValue.h"
#import "NSDate+TimeAgo.h"

@interface Question : _Question
// Custom logic goes here.

- (void) createQuestionWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context;

-(NSString *) formattedCreateAt;
-(NSString *) formattedStartsAt;

-(NSArray*) notEmptyAnswers;

@end
