#import "_Value.h"

@interface Value : _Value
// Custom logic goes here.

- (void) createValueWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context;

+ (UIImage *) placeholder;

@end
