#import "ActivityFeedQuestion.h"

@interface ActivityFeedQuestion ()

// Private interface goes here.

@end

@implementation ActivityFeedQuestion

// Custom logic goes here.

- (void) createWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    
    self.questionId = [info secureNumberForKey:@"id"];
    self.body = [info secureStringForKey:@"body"];
    self.isMultiple = [info secureNumberForKey:@"multiple"];
    
    for (NSDictionary *answerOptionInfo in [info valueForKey:@"answer_options"]) {
        
        ActivityFeedAnswerOption *item = [ActivityFeedAnswerOption createWithInfo:answerOptionInfo inContext:context];
        item.questionId = [info secureNumberForKey:@"id"];
        [self.answerOptionsSet addObject:item];
        
        if (item.isCustomValue) {
            self.haveCustomAnswerOptionValue = YES;
        }
    }
    
}

@end
