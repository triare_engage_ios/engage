#import "UserBehavior.h"
#import "NSDictionary+SecureValue.h"

@interface UserBehavior ()

// Private interface goes here.

@end

@implementation UserBehavior

-(void) createWithInfo:(NSDictionary *) info {
    
    [super createWithInfo:info];
    
    self.count = [info secureNumberForKey:@"count"];
}

@end
