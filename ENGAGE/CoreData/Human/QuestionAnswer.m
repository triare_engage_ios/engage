#import "QuestionAnswer.h"
#import "NSDictionary+SecureValue.h"
#import <MagicalRecord/MagicalRecord.h>

@interface QuestionAnswer ()

// Private interface goes here.

@end

@implementation QuestionAnswer

// Custom logic goes here.

+(QuestionAnswer *) createWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    
    QuestionAnswer *answer = [QuestionAnswer MR_createEntityInContext:context];
    answer.title = [info secureStringForKey:@"title"];
    answer.answersCount = [info secureNumberForKey:@"answers_count"];
    
    return answer;
    
}


@end
