#import "User.h"
#import "NSDictionary+SecureValue.h"
#import "AnalyticsManager.h"

@interface User ()

// Private interface goes here.

@end

@implementation User

-(void) updateUserInfo:(NSDictionary *) info {
    
    self.email = [info secureStringForKey:@"email"];
    self.firstName = [info secureStringForKey:@"first_name"];
    self.lastName = [info secureStringForKey:@"last_name"];
    self.fullName = [info secureStringForKey:@"full_name"];
    self.role = [info secureStringForKey:@"role"];
    [AnalyticsManager setUserProperty:[info secureStringForKey:@"role"] forPropertyType:FireBasePropertyTypeUserRole];
    self.avatarUrl = [info secureStringForKey:@"avatar_url"];
    self.jobTitle = [info secureStringForKey:@"job_title"];
    self.userId = [info secureNumberForKey:@"id"];
    [AnalyticsManager setUserId:[info secureNumberForKey:@"id"].stringValue];
    
    self.company.name = [info secureStringForKey:@"company.name"];
    self.company.backgroundUrl = [info secureStringForKey:@"company.background_url"];
    self.company.logoUrl = [info secureStringForKey:@"company.logo_url"];
    NSNumber *timestamp = [info secureNumberForKey:@"company.created_at"];
    self.company.createdAt = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
    self.company.plan = [info secureStringForKey:@"company_plan" andDefault:@"basic"];
    [AnalyticsManager setUserProperty:[info secureStringForKey:@"company_plan"] forPropertyType:FireBasePropertyTypeCompanyPlan];
    self.company.companyId = [info secureNumberForKey:@"company.id"];
}

-(void) updateSomeUserInfo: (NSDictionary *) info {
    
    if ([info valueForKey:@"first_name"]) {
        self.firstName = [info secureStringForKey:@"first_name"];
    }
    
    if ([info valueForKey:@"last_name"]) {
        self.lastName = [info secureStringForKey:@"last_name"];
    }
    
    if ([info valueForKey:@"job_title"]) {
        self.jobTitle = [info secureStringForKey:@"job_title"];
    }
    
    if ([info valueForKey:@"avatar_url"]) {
        self.avatarUrl = [info secureStringForKey:@"avatar_url"];
    }
    
    if ([info valueForKey:@"email"]) {
        self.email = [info secureStringForKey:@"email"];
    }
    
    if ([info valueForKey:@"role"]) {
        self.role = [info secureStringForKey:@"role"];
        [AnalyticsManager setUserProperty:[info secureStringForKey:@"role"] forPropertyType:FireBasePropertyTypeUserRole];
    }
    
    if ([info valueForKey:@"company_plan"]) {
        self.company.plan = [info secureStringForKey:@"company_plan"];
        [AnalyticsManager setUserProperty:[info secureStringForKey:@"company_plan"] forPropertyType:FireBasePropertyTypeCompanyPlan];
    }
}

-(BOOL) isAdmin {
    return [self.role isEqualToString:@"admin"];
}

-(BOOL) isManager {
    return [self.role isEqualToString:@"manager"];
}

- (NSDictionary *) serialize {
    NSMutableDictionary *dico = [[NSMutableDictionary alloc] init];
    
    [NSDictionary secureSetIn:dico withValue:self.email forKey:@"email"];
    [NSDictionary secureSetIn:dico withValue:self.firstName forKey:@"first_name"];
    [NSDictionary secureSetIn:dico withValue:self.lastName forKey:@"last_name"];
    [NSDictionary secureSetIn:dico withValue:self.fullName forKey:@"full_name"];
    [NSDictionary secureSetIn:dico withValue:self.role forKey:@"role"];
    [NSDictionary secureSetIn:dico withValue:self.avatarUrl forKey:@"avatar_url"];
    [NSDictionary secureSetIn:dico withValue:self.jobTitle forKey:@"job_title"];
    
    NSMutableDictionary *company = [[NSMutableDictionary alloc] init];
    [NSDictionary secureSetIn:company withValue:self.company.name forKey:@"name"];
    [NSDictionary secureSetIn:company withValue:self.company.backgroundUrl forKey:@"background_url"];
    [NSDictionary secureSetIn:company withValue:self.company.logoUrl forKey:@"logo_url"];
    [NSDictionary secureSetIn:company withValue:self.company.createdAt forKey:@"created_at"];
    [NSDictionary secureSetIn:company withValue:self.company.plan forKey:@"company_plan"];
    
    [dico setValue:company forKey:@"company"];
    
    return [dico copy];
}

- (void) updatePushTokenWithData:(NSData *) dataToken {
    
    if (dataToken == nil) {
        return;
    }
    
    NSString *deviceToken = [[dataToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    if (deviceToken.length < 1) {
        return;
    }
    
    self.pushDeviceToken = deviceToken;
}

-(BOOL) isPushTokenActual {
    
    BOOL isActual = NO;
    
    NSString *deviceToken = [[[AppDelegate theApp].deviceTokenForPush description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    if (deviceToken.length > 0 && self.pushDeviceToken.length > 0) {
        if ([deviceToken isEqualToString:self.pushDeviceToken]) {
            isActual = YES;
        }
    }
    
    return isActual;
    
}


@end
