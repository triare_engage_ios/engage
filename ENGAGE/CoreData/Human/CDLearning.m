#import "CDLearning.h"
#import "NSDate+TimeAgo.h"

@interface CDLearning ()

// Private interface goes here.

@end

@implementation CDLearning

#pragma mark - Object Public Methods

- (void) createWithInfo:(NSDictionary *) info {
    
    self.feedId = [info secureNumberForKey:@"id"];
    NSNumber *timeStampCreateDate = [info secureNumberForKey:@"created_at"];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:timeStampCreateDate.integerValue];
    
    self.learningType = [info secureStringForKey:@"ltype"];
    self.siteTitle = [info secureStringForKey:@"title"];
    self.siteDescription = [info secureStringForKey:@"description"];
    self.siteUrl = [info secureStringForKey:@"site_url"];
    self.siteImageUrl = [info secureStringForKey:@"site_image_url"];
    self.rating = [info secureNumberForKey:@"rating"];
    self.userRating = [info secureNumberForKey:@"user_rating"];
    self.userComment = [info secureStringForKey:@"comment"];
    
    self.senderId = [info secureNumberForKey:@"user.id"];
    self.senderFullName = [info secureStringForKey:@"user.full_name"];
    self.senderAvatarUrl = [info secureStringForKey:@"user.avatar_url"];
    self.senderJobTitle = [info secureStringForKey:@"user.job_title"];
    self.isSenderTopPointWinner = [info secureNumberForKey:@"user.top_point_winner"];
    self.isFromLearningList = [NSNumber numberWithBool:NO];
}

- (void) createWithInfo:(NSDictionary *) info learningList:(BOOL) isFromLearningList {
    
    [self createWithInfo:info];
    self.isFromLearningList = [NSNumber numberWithBool:isFromLearningList];
}

-(NSString *) formattedCreateDate {
    return [self.createDate formattedAsTimeAgo];
}

-(NSInteger) totalRatingStars {
    
    NSInteger rating = self.rating.integerValue;
    
    return [CDLearning starsFromRating:rating];
}

#pragma mark - Class Public Methods

+ (UIImage *) placeholder {
    
    NSInteger random = arc4random_uniform(2);
    
    switch (random) {
        case 0:
            return [UIImage imageNamed:@"learning-placeholder-1"];
            break;
            
        case 1:
            return [UIImage imageNamed:@"learning-placeholder-2"];
            break;
            
        case 3:
            return [UIImage imageNamed:@"learning-placeholder-3"];
            break;
            
        default:
            return [UIImage imageNamed:@"learning-placeholder-1"];
            break;
    }
}

+ (NSInteger) ratingFromStars:(NSInteger) startsNumber {
    
    if (startsNumber < 0 || startsNumber > 5) {
        return 0;
    }
    
    NSInteger rating = startsNumber * 20;
    
    return rating;
}

+ (NSInteger) starsFromRating:(NSInteger) rating {
    
    if (rating <= 10) {
        return 0;
    }
    
    if (rating > 10 && rating <= 30) {
        return 1;
    }
    
    if (rating > 30 && rating <= 50 ) {
        return 2;
    }
    
    if (rating > 50 && rating <= 70) {
        return 3;
    }
    
    if (rating > 70 && rating <= 90) {
        return 4;
    }
    
    if (rating > 90) {
        return 5;
    }
    
    return 0;
    
}

@end
