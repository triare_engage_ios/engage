#import "_User.h"

@interface User : _User {}

-(void) updateUserInfo:(NSDictionary *) info;

-(void) updateSomeUserInfo: (NSDictionary *) info;

-(BOOL) isAdmin;

-(BOOL) isManager;

- (NSDictionary *) serialize;

- (void) updatePushTokenWithData:(NSData *) dataToken;

-(BOOL) isPushTokenActual;

@end
