#import "TeamUser.h"
#import "NSDictionary+SecureValue.h"


@implementation TeamUser

-(void) updateUserWithInfo:(NSDictionary *) info {
    
    self.userId = [info secureNumberForKey:@"id"];
    self.email = [info secureStringForKey:@"email"];
    self.unconfirmedEmail = [info secureStringForKey:@"unconfirmed_email"];
    self.firstName = [info secureStringForKey:@"first_name"];
    self.lastName = [info secureStringForKey:@"last_name"];
    self.role = [info secureStringForKey:@"role" andDefault:@"user"];
    self.avatarUrl = [info secureStringForKey:@"avatar_url"];
    self.jobTitle = [info secureStringForKey:@"job_title"];
    self.fullName = [info secureStringForKey:@"full_name"];
    self.unreadSuggestionsCount = [info secureNumberForKey:@"new_suggestions"];
    self.receivedBadEvaluationsCount = [info secureNumberForKey:@"received_bad_evaluations_count"];
    self.receivedGoodEvaluationsCount = [info secureNumberForKey:@"received_good_evaluations_count"];
    self.isTeamMessagesPresent = [info secureNumberForKey:@"team_messages_present"];
    self.isInvationAccepted = [info secureNumberForKey:@"invitation_accepted"];
    self.isQuestionPresent = [info secureNumberForKey:@"questions_present"];
    self.isPointWinner = [info secureNumberForKey:@"top_point_winner"];
    self.haveChallengeBadges = [info secureNumberForKey:@"has_challenge_badges" andDefault:@0];
    self.companyPlan = [info secureStringForKey:@"company_plan" andDefault:@"basic"];
}

-(void) updateSomeUserInfo:(NSDictionary *) info {
    
    if ([info valueForKey:@"avatar_url"]) {
        self.avatarUrl = [info secureStringForKey:@"avatar_url"];
    }
    
    if ([info valueForKey:@"full_name"]) {
        self.fullName = [info secureStringForKey:@"full_name"];
    }
    
    if ([info valueForKey:@"email"]) {
        self.email = [info secureStringForKey:@"email"];
    }
    
    if ([info valueForKey:@"job_title"]) {
        self.jobTitle = [info secureStringForKey:@"job_title"];
    }
    
    if ([info valueForKey:@"role"]) {
        self.role = [info secureStringForKey:@"role"];
    }
    
    if ([info valueForKey:@"invitation_accepted"]) {
        self.isInvationAccepted = [info secureNumberForKey:@"invitation_accepted"]
        ;
    }
    
    if ([info valueForKey:@"top_point_winner"]) {
        self.isPointWinner = [info secureNumberForKey:@"top_point_winner"];
    }
    
    if ([info valueForKey:@"has_challenge_badges"]) {
        self.haveChallengeBadges = [info secureNumberForKey:@"has_challenge_badges"];
    }
    
    if ([info valueForKey:@"company_plan"]) {
        self.companyPlan = [info secureStringForKey:@"company_plan"];
    }
    
}

-(NSString *) shortFullName {
    NSString *name = @"";
    if (self.firstName.length > 0) {
        name = [self.firstName substringToIndex:1];
        name = [name stringByAppendingString:@"."];
    }
    
    return [NSString stringWithFormat:@"%@ %@", name , self.lastName];
}

+(UIImage *) placeholder {
    return [UIImage imageNamed:@"icon-user"];
}

-(BOOL) isManager {
    return [self.role isEqualToString:@"manager"];
}

-(BOOL) isAdmin {
    return [self.role isEqualToString:@"admin"];
}


@end
