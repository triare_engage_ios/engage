#import "BehaviorTeamUserEvaluations.h"
#import "NSDictionary+SecureValue.h"

@interface BehaviorTeamUserEvaluations ()

// Private interface goes here.

@end

@implementation BehaviorTeamUserEvaluations

-(void) createWithInfo:(NSDictionary *) info {
    [super createWithInfo:info];
    
    self.goodEvaluationsCount = [info secureNumberForKey:@"good_evaluations_count"];
    self.badEvaluationsCount = [info secureNumberForKey:@"bad_evaluations_count"];
    self.progress = [self getProgress];
    self.totalEvaluationsCount = [self getTotalEvaluationsCount];
    
}

-(NSNumber *) getProgress {
    
    if (self.goodEvaluationsCount && self.badEvaluationsCount) {
        return [NSNumber numberWithFloat:[self.goodEvaluationsCount floatValue] / ([self.goodEvaluationsCount floatValue] + [self.badEvaluationsCount floatValue])];
    }
    
    else {
        return [NSNumber numberWithFloat:0.0];
    }
    
}

-(NSNumber *) getTotalEvaluationsCount {
    if (self.goodEvaluationsCount && self.badEvaluationsCount) {
        return [NSNumber numberWithInt:(int)([self.goodEvaluationsCount integerValue] + [self.badEvaluationsCount integerValue])];
    }
    else {
        return 0;
    }
}


@end
