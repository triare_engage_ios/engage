#import "_Company.h"

@interface Company : _Company {}

-(UIImage *) logo;
-(UIImage *) background;

@end
