#import "Company.h"
#import "AKFileManager.h"

@interface Company ()

// Private interface goes here.

@end

@implementation Company

-(UIImage *) logo {
    NSString *path = [[AKFileManager companyDirectory] stringByAppendingPathComponent:@"logo.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    if (image) return image;
    
    return [UIImage imageNamed:@"company-logo"];
}

-(UIImage *) background {
    NSString *path = [[AKFileManager companyDirectory] stringByAppendingPathComponent:@"background.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    if (image) return image;
    
    return [UIImage imageNamed:@"company-background"];
}

@end
