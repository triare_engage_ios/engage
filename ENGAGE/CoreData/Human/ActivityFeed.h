#import "_ActivityFeed.h"
#import "TopPoint.h"
#import "Challenge.h"
#import "CDLearning.h"

typedef enum : NSUInteger {
    ActivityFeedTypeUnknown,
    ActivityFeedTypeTopPoint,
    ActivityFeedTypeTap,
    ActivityFeedTypeTeamMessage,
    ActivityFeedTypeChallenge,
    ActivityFeedTypeLearning
} ActivityFeedType;

@interface ActivityFeed : _ActivityFeed {}

+(NSPredicate *) predicateForType:(ActivityFeedType) feedType andId:(NSNumber *) feedId;

-(void) createWithInfo:(NSDictionary *)info inContext:(NSManagedObjectContext *)context;

-(NSString *) formattedCreateDate;
-(ActivityFeedType) feedType;


#pragma mark - Deprecated
//Use createWithInfo: inContext:
-(void) createWithInfo:(NSDictionary *) info __deprecated_msg("Use createWithInfo: inContext:");

@end
