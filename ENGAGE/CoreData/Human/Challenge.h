#import "_Challenge.h"

@interface Challenge : _Challenge
// Custom logic goes here.


- (void) createWithInfo:(NSDictionary *) info;
- (void) createWithInfo:(NSDictionary *) info current:(BOOL) isCurrent;
- (void) createWithInfo:(NSDictionary *) info activityFeed:(BOOL) isFromFeed;

- (void) updateWithInfo:(NSDictionary *) info;

- (NSString *) formattedUntilDate;

@end
