#import "ActivityFeedAnswerOption.h"
#import "NSDictionary+SecureValue.h"
#import <MagicalRecord/MagicalRecord.h>

@interface ActivityFeedAnswerOption ()

// Private interface goes here.

@end

@implementation ActivityFeedAnswerOption

// Custom logic goes here.

+(ActivityFeedAnswerOption *) createWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context {
    
    ActivityFeedAnswerOption *item = [ActivityFeedAnswerOption MR_createEntityInContext:context];
    item.answerOptionId = [info secureNumberForKey:@"id"];
    item.isCustom = [info secureNumberForKey:@"custom"];
    
    if (!item.isCustomValue) {
        item.title = [info secureStringForKey:@"title"];
    }
    
    return item;
}

@end
