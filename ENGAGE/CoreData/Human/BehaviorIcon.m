#import "BehaviorIcon.h"

@interface BehaviorIcon ()

// Private interface goes here.

@end

@implementation BehaviorIcon

-(void) createWithInfo:(NSDictionary *) info {
    
    self.iconId = [info secureNumberForKey:@"id"];
    self.category = [info secureStringForKey:@"category"];
    self.iconImageUrl = [info secureStringForKey:@"url"];
}

@end
