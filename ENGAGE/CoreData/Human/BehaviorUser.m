#import "BehaviorUser.h"
#import "NSDictionary+SecureValue.h"

@interface BehaviorUser ()

// Private interface goes here.

@end

@implementation BehaviorUser

// Custom logic goes here.

-(void) createWithInfo:(NSDictionary *) info {
    
    self.behaviorId = [info secureNumberForKey:@"id"];
    self.avatarUrl = [info secureStringForKey:@"avatar_url"];
    self.fullName = [info secureStringForKey:@"full_name"];
    self.jobTitle = [info secureStringForKey:@"job_title"];
    self.receivedCount = [info secureNumberForKey:@"received_count"];
    self.givenCount = [info secureNumberForKey:@"given_count"];
    self.userId = [info secureNumberForKey:@"id"];
    self.isPointWinner = [info secureNumberForKey:@"top_point_winner"];
}


@end
