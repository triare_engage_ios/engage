#import "LeaderBehavior.h"
#import "NSDictionary+SecureValue.h"

@interface LeaderBehavior ()

// Private interface goes here.

@end

@implementation LeaderBehavior

-(void) createWithInfo:(NSDictionary *) info {
    
    [super createWithInfo:info];
    
    self.tapsCount = [info secureNumberForKey:@"taps_count" andDefault:[NSNumber numberWithInt:0]];
}

@end
