#import "QuestionCustomAnswer.h"
#import "NSDictionary+SecureValue.h"
#import <MagicalRecord/MagicalRecord.h>

@interface QuestionCustomAnswer ()

// Private interface goes here.

@end

@implementation QuestionCustomAnswer

// Custom logic goes here.

+(QuestionCustomAnswer *) createWithText:(NSString *) text inContext:(NSManagedObjectContext *)context {
    
    QuestionCustomAnswer *answer = [QuestionCustomAnswer MR_createEntityInContext:context];
    if (![text isEqual:[NSNull null]]) {
        answer.title = text;
    }
    
    return answer;
}


@end
