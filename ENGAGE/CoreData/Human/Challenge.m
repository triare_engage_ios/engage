#import "Challenge.h"

@interface Challenge ()

// Private interface goes here.

@end

@implementation Challenge

- (void) createWithInfo:(NSDictionary *) info {
    self.feedId = [info secureNumberForKey:@"id"];
    self.borderColorHex = [info secureStringForKey:@"border_color"];
    self.title = [info secureStringForKey:@"title"];
    self.imageIconUrl = [info secureStringForKey:@"image_url"];
    self.backgroundImageUrl = [info secureStringForKey:@"background"];
    
    self.challengeDescription = [info secureStringForKey:@"description"];
    NSNumber *timestamp = [info secureNumberForKey:@"end_date"];
    self.untilDate = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
    
    NSNumber *timeStampCreateDate = [info secureNumberForKey:@"created_at"];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:timeStampCreateDate.integerValue];
    
    self.receiverId = [info secureNumberForKey:@"receiver.id"];
    self.receiverJob = [info secureStringForKey:@"receiver.job_title"];
    self.receiverName = [info secureStringForKey:@"receiver.full_name"];
    self.receiverAvatarUrl = [info secureStringForKey:@"receiver.avatar_url"];
    
    self.isCurrentChallenge = [NSNumber numberWithBool:NO];
    self.isActivityFeedChallenge = [NSNumber numberWithBool:NO];
}

- (void) createWithInfo:(NSDictionary *)info current:(BOOL) isCurrent {
    [self createWithInfo:info];
    self.isCurrentChallenge = [NSNumber numberWithBool:isCurrent];
}

- (void) createWithInfo:(NSDictionary *)info activityFeed:(BOOL) isFromFeed {
    [self createWithInfo:info];
    self.isActivityFeedChallenge = [NSNumber numberWithBool:isFromFeed];
}

- (void) updateWithInfo:(NSDictionary *) info {
    if ([info valueForKey:@"id"]) {
        self.feedId = [info secureNumberForKey:@"id"];
    }
    
    if ([info valueForKey:@"border_color"]) {
        self.borderColorHex = [info secureStringForKey:@"border_color"];
    }
    
    if ([info valueForKey:@"title"]) {
        self.title = [info secureStringForKey:@"title"];
    }
    
    if ([info valueForKey:@"image_url"]) {
        self.imageIconUrl = [info secureStringForKey:@"image_url"];
    }
    
    if ([info valueForKey:@"background"]) {
        self.backgroundImageUrl = [info secureStringForKey:@"background"];
    }
    
    if ([info valueForKey:@"description"]) {
        self.challengeDescription = [info secureStringForKey:@"description"];
    }
    
    if ([info valueForKey:@"end_date"]) {
        NSNumber *timestamp = [info secureNumberForKey:@"end_date"];
        self.untilDate = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
    }
    
    if ([info valueForKey:@"created_at"]) {
        NSNumber *timeStampCreateDate = [info secureNumberForKey:@"created_at"];
        self.createDate = [NSDate dateWithTimeIntervalSince1970:timeStampCreateDate.integerValue];
    }
    
    if ([info valueForKeyPath:@"receiver.id"]) {
        self.receiverId = [info secureNumberForKey:@"receiver.id"];
    }
    
    if ([info valueForKeyPath:@"receiver.job_title"]) {
        self.receiverJob = [info secureStringForKey:@"receiver.job_title"];
    }
    
    if ([info valueForKeyPath:@"receiver.full_name"]) {
        self.receiverName = [info secureStringForKey:@"receiver.full_name"];
    }
    
    if ([info valueForKeyPath:@"receiver.avatar_url"]) {
            self.receiverAvatarUrl = [info secureStringForKey:@"receiver.avatar_url"];
    }
}

- (NSString *) formattedUntilDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyyy"];
    
    NSString *stringFromDate = [formatter stringFromDate:self.untilDate];
    
    return stringFromDate;
}

@end
