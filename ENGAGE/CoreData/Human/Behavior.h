#import "_Behavior.h"

@interface Behavior : _Behavior {}

-(void) createWithInfo:(NSDictionary *) info;
-(void) updateWithInfo:(NSDictionary *) info;

+(UIImage *) placeholder;

+(Behavior *) createWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context;

@end
