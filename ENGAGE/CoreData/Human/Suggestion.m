#import "Suggestion.h"
#import "NSDictionary+SecureValue.h"



@implementation Suggestion

-(void) createWithInfo:(NSDictionary *) info {
    
    self.suggestionId = [info secureNumberForKey:@"id"];
    self.body = [info secureStringForKey:@"body"];
    self.senderId = [info secureNumberForKey:@"user.id"];
    self.senderAvatarUrl = [info secureStringForKey:@"user.avatar_url"];
    self.senderName = [info secureStringForKey:@"user.full_name"];
    
    NSNumber *timestamp = [info secureNumberForKey:@"created_at"];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];

    self.isRead = [info secureNumberForKey:@"readed"];
}

@end
