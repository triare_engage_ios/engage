#import "_ActivityFeedAnswerOption.h"

@interface ActivityFeedAnswerOption : _ActivityFeedAnswerOption
// Custom logic goes here.

+(ActivityFeedAnswerOption *) createWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context;


@end
