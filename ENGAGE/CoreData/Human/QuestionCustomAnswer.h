#import "_QuestionCustomAnswer.h"

@interface QuestionCustomAnswer : _QuestionCustomAnswer
// Custom logic goes here.

+(QuestionCustomAnswer *) createWithText:(NSString *) text inContext:(NSManagedObjectContext *)context;


@end
