#import "_TeamUser.h"

@interface TeamUser : _TeamUser {}

-(void) updateUserWithInfo:(NSDictionary *) info;

-(void) updateSomeUserInfo:(NSDictionary *) info;

-(NSString *) shortFullName;

+(UIImage *) placeholder;

-(BOOL) isManager;

-(BOOL) isAdmin;

@end
