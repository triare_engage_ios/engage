#import "TeamMessage.h"
#import "NSDictionary+SecureValue.h"

@interface TeamMessage ()

// Private interface goes here.

@end

@implementation TeamMessage

// Custom logic goes here.

-(void) createWithInfo:(NSDictionary *) info {
    
    self.body = [info secureStringForKey:@"body"];
    self.messageId = [info secureNumberForKey:@"id" andDefault:@0];

    NSNumber *timestamp = [info secureNumberForKey:@"created_at"];
    self.createDate = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
    
    
}

@end
