#import "_Suggestion.h"

@interface Suggestion : _Suggestion {}

-(void) createWithInfo:(NSDictionary *) info;

@end
