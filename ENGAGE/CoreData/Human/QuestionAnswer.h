#import "_QuestionAnswer.h"

@interface QuestionAnswer : _QuestionAnswer
// Custom logic goes here.

+(QuestionAnswer *) createWithInfo:(NSDictionary *) info inContext:(NSManagedObjectContext *)context;

@end
