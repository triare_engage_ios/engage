//
//  ProfileHelper.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileHelper.h"

@interface ProfileHelper ()

@end

@implementation ProfileHelper

#pragma mark - Profile Cell Display

+(BOOL) isNeedDisplayBadgeForUser:(TeamUser *) user {
    BOOL isNeedDisplay = NO;
    
    if (user.haveChallengeBadgesValue) {
        isNeedDisplay = YES;
    }
    
    return isNeedDisplay;
}

+(BOOL) isNeedDisplayEvaluateAndGiveTap {
    
    return [FeaturesAccessManager isAccessFeature:AppFeatureTypeGiveEvaluation forScreen:AppScreenTypeUserProfile withUser:[DataManager user]];
    
}

+(BOOL) isNeedAccessVirtualSuggestion {
    return [FeaturesAccessManager isAccessFeature:AppFeatureTypeVirtualSuggestion forScreen:AppScreenTypeUserProfile withUser:[DataManager user]];
}

+(BOOL) isNeedAccessQuestion {
    return ([FeaturesAccessManager isAccessFeature:AppFeatureTypeChallenge forScreen:AppScreenTypeUserProfile withUser:[DataManager user]] && [FeaturesAccessManager isAccessFeature:AppFeatureTypeQuestion forScreen:AppScreenTypeUserProfile withUser:[DataManager user]]);
}

@end
