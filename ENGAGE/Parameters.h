//
//  Parameters.h
//  ENGAGE.
//
//  Created by Alex Kravchenko on 03.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

// ======================= LINKS ================================= //
#define EN_API_SERVER_ADDRESS @"http://46.101.159.44"
#define EN_LINK_FORGOT_PASSWORD @"http://engageapp.fasttrack-europe.com/reset_password_instructions"
#define EN_LINK_SIGH_UP @"http://engageapp.fasttrack-europe.com/sign_up"

// ======================= SIZE ================================== //
#define EN_NAVIGATION_BAR_HEIGHT 64
#define EN_NAVIGATION_TAB_BAR_HEIGHT 55

// ====================== Notification ========================== //
#define EN_NOTIFICATION_NAVIGATION_BAR_DROP_MENU_CLOSED @"ENTopDropMenuClosed"
#define EN_NOTIFICATION_NAVIGATION_TAB_BAR_DROP_MENU_CLOSED @"ENBottomDropMenuClosed"
#define EN_NOTIFICATION_LEARNING_DID_CREATE @"ENLearningDidCreate"

// ====================== APP GROUP IDENTIFIER ================== //
#define EN_APP_IDENTIFIER_PREFIX @"8CQ6Z72UAJ"
#define EN_APPGROUP_ID @"group.com.fasttrack.engage"
#define EN_KEYCHAIN_SERVICE_NAME @"com.fasttrack.engage"
#define EN_KEYCHAIN_APPGROUP_ID @"8CQ6Z72UAJ.group.com.fasttrack.engage"

// ===================== PUBLIC TOKEN ========================== //
#define EN_PUBLIC_TOKEN_TEST_FAIRY @"5c3403fa40a28a8fca3d96ead363d3db0244cd5a"
#define EN_PUBLIC_TOKEN_UX_CAM @"facf2151e9a624e"
