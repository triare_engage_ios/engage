//
//  MainViewController.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBar.h"

@interface MainViewController : UIViewController

@property BOOL isAddedNewTap;
@property (nonatomic, strong) UINavigationController *navigationController;

#pragma mark - Action

-(void) userDidLogin;
-(void) logoutUser;
-(void) updateBackground;
-(void) showTabBarView:(BOOL)animated;
-(void) hideTabBarView:(BOOL)animated;

#pragma mark - Navigation

-(void) showGiveTap;
-(void) showGiveTapForUser:(NSNumber *) userId;
-(void) showEvaluate;
-(void) showAddLearning;
-(void) presentGiveSuggestionToAdmin;
-(void) showChallengeBadgeListForCurrentUser;
-(void) showLearningListInActivityFeed;
-(void) presentNewQuestion;
-(void) presentNewTeamMessage;

#pragma mark - Remote Push

-(void) updateActivityFeed;
-(void) showActivityFeed;
-(void) updateActivityFeedAndSetDotOnProfile:(BOOL) needDot;
-(void) newEventForActivityFeed;
-(void) newEventForProfile;
-(void) newEventForProfileEnded;
-(void) challengeWinnerBadgeList;

#pragma mark - Action Button Menu

-(void) showActionButtonMenu;
-(void) hideActionButtonMenu;
-(void) showActivityFeedDropMenu;
-(void) showLearningListDropMenu;
-(void) hideTopDropMenu;

@end
