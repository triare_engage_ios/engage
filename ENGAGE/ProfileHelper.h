//
//  ProfileHelper.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FeaturesAccessManager.h"

@interface ProfileHelper : NSObject

#pragma mark - Profile Cell Display

+(BOOL) isNeedDisplayBadgeForUser:(TeamUser *) user;
+(BOOL) isNeedDisplayEvaluateAndGiveTap;
+(BOOL) isNeedAccessVirtualSuggestion;
+(BOOL) isNeedAccessQuestion;

@end
