//
//  AppDelegate.m
//  ENGAGE.
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "AppDelegate.h"

#import "DataManager.h"
#import "RemotePushManager.h"

#import "TestFairy.h"
#import <UXCam/UXCam.h>
#import "AnalyticsManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (AppDelegate *)theApp {
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (MainViewController *)mainViewController
{
    return (MainViewController *)self.window.rootViewController;
}



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

#if DEBUG
#else
    [TestFairy begin:EN_PUBLIC_TOKEN_TEST_FAIRY];
    [UXCam startWithKey:EN_PUBLIC_TOKEN_UX_CAM];
    //Off shake gesture for sending feedback
    [UIApplication sharedApplication].applicationSupportsShakeToEdit = NO;
    [AnalyticsManager configure];
#endif
    
    [DataManager setupCoreDataStack];
    
    //Setup remote notification
#if TARGET_IPHONE_SIMULATOR
#else
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
    
    //Do not show constaring warnig in console.
    [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"_UIConstraintBasedLayoutLogUnsatisfiable"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //Skip numbers of badge, when open app
    if ([[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken  {
    MLog(@"My token is: %@", deviceToken);
    _deviceTokenForPush = deviceToken;
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    MLog(@"Failed to get token, error: %@", error);
}

- (void) application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

//Handle push when tap on push
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    MLog(@"Push info: %@", userInfo);
    
    //For iOS10. Used when tap of push
    if (application.applicationState == UIApplicationStateInactive) {
            [RemotePushManager parsePushAndActionWithInfo:userInfo forAppState:application.applicationState];
    }
}

//Handle background push
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandle {
    
    MLog(@"Push info: %@", userInfo);
    
    //Logic can find by link: https://goo.gl/NvHsFR
    [RemotePushManager parsePushAndActionWithInfo:userInfo forAppState:application.applicationState];
    
    completionHandle (UIBackgroundFetchResultNewData);
}

#pragma mark - Helpers Methods

-(void) showAlert:(NSString *) title text:(NSString *) text {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:text preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"v1.0") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    
    UIViewController *rootController = self.window.rootViewController;
    
    if (rootController.presentedViewController) {
        rootController = rootController.presentedViewController;
    }
    
    [rootController presentViewController:alertController animated:YES completion:nil];
}



@end
