//
//  MPTextView.h
//
//

#import <UIKit/UIKit.h>

@interface MPTextView : UITextView

// Named .placeholderText, in case UITextView gains
// a .placeholder property (like UITextField) in future iOS versions.
@property (nonatomic, copy) NSString *placeholderText;
@property (nonatomic, copy) UIColor *placeholderColor;

@end
