#import "HFImageEditorViewController+Private.h"
#import "CropImageEditor.h"

static NSString *backgrounds[] = {@"Tuto_enjoy_background", @"BackgroundReminderMorning-iP5", @"BackgroundReminderAfternoon-iP5",@"BackgroundReminderTomorrow-iP5",@"BackgroundReminderNight-iP5"};

@interface CropImageEditor ()

@end

@implementation CropImageEditor
@synthesize overlay;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
        overlay = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        overlay.image = [UIImage imageNamed:@"Tuto_enjoy_background"];
        overlay.alpha = 0.8;
        [overlay setContentMode:UIViewContentModeTopLeft];
        [self.view insertSubview:overlay aboveSubview:self.frameView];
    }
    [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"v1.0") forState:UIControlStateNormal];
    [_doneButton setTitle:NSLocalizedString(@"Done", @"v1.0") forState:UIControlStateNormal];
    [_backgroundButton setTitle:NSLocalizedString(@"Change background", @"v1.0") forState:UIControlStateNormal];
    [_retryButton setTitle:NSLocalizedString(@"Retake picture", @"v90") forState:UIControlStateNormal];
    return self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (IBAction)setSquareAction:(id)sender
{
    self.cropRect = CGRectMake(86, 130, 150, 150);
    self.minimumScale = 0.2;
    self.maximumScale = 10;
    [self reset:NO];
}


-(void)isGalleryPicture:(BOOL)isGalleryPic{
  if(isGalleryPic)
    [_retryButton setTitle:NSLocalizedString(@"Change picture", @"v90") forState:UIControlStateNormal];
  else
    [_retryButton setTitle:NSLocalizedString(@"Retake picture", @"v90") forState:UIControlStateNormal];
}

-(IBAction) done:(id)sender{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CGImageRef resultRef = [self newTransformedImage:self.imageView.transform
                                             sourceImage:self.sourceImage.CGImage
                                              sourceSize:self.sourceImage.size
                                       sourceOrientation:self.sourceImage.imageOrientation
                                             outputWidth:self.outputWidth ? self.outputWidth : self.sourceImage.size.width
                                                cropRect:self.cropRect
                                           imageViewSize:self.imageView.bounds.size];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *transform =  [UIImage imageWithCGImage:resultRef scale:1.0 orientation:UIImageOrientationUp];
            CGImageRelease(resultRef);
            self.view.userInteractionEnabled = YES;
            if(self.doneCallback) {
                self.doneCallback(transform, 0);
            }
        });
    });
}

-(IBAction) cancel:(id)sender{
    if(self.doneCallback) {
        self.doneCallback(nil, 2);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction) retry:(id)sender{
    if(self.doneCallback) {
        self.doneCallback(nil, 1);
    }
}

-(IBAction) switchView:(id)sender{
    currentBackground++;
    currentBackground %= 5;
    overlay.image = [UIImage imageNamed:backgrounds[currentBackground]];
    if (currentBackground==0)
        overlay.alpha = 0.8;
    else
        overlay.alpha = 1.0;
    
    //NSLog(@"Using %@ as back",backgrounds[currentBackground]);
}

@end
