#import "HFImageEditorViewController.h"

@interface CropImageEditor : HFImageEditorViewController{
    int  currentBackground;
}

- (IBAction)setSquareAction:(id)sender;
-(void)isGalleryPicture:(BOOL)isGalleryPic;
@property(strong, nonatomic) UIImageView* overlay;
@property (retain, nonatomic) IBOutlet UIButton *backgroundButton;
@property (retain, nonatomic) IBOutlet UIButton *cancelButton;
@property (retain, nonatomic) IBOutlet UIButton *doneButton;
@property (retain, nonatomic) IBOutlet UIButton *retryButton;

@end
