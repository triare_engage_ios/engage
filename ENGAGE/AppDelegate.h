//
//  AppDelegate.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, readonly) NSData *deviceTokenForPush;
@property (nonatomic) BOOL isNeedShowChallengeBadgeList;

+ (AppDelegate *)theApp;
- (MainViewController *)mainViewController;

-(void) showAlert:(NSString *) title text:(NSString *) text;

@end

