//
//  MainViewController.m
//  ENGAGE.
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "MainViewController.h"

#import "ActivityFeedVC.h"
#import "ProfileVC.h"
#import "BehaviorsBoardVC.h"
#import "GiveTapNavController.h"
#import "GiveTapStep1VC.h"
#import "GiveTapStepMessageVC.h"
#import "BadgeListVC.h"
#import "LearningCreateVC.h"
#import "QuestionsVC.h"

#import "ENDropMenu.h"
#import "ENDropMenuHelper.h"

#import "AnalyticsManager.h"

#define defTabBarAnimationDuration 0.3

@interface MainViewController () <UIGestureRecognizerDelegate>

@property  BOOL testBool;



//Tab bar
@property (weak, nonatomic) IBOutlet UIView *tabBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarBottomSpace;

@property (weak, nonatomic) IBOutlet UIButton *activityFeedButton;
@property (weak, nonatomic) IBOutlet UIButton *leaderboardButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *favorietesButton;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;

@property (nonatomic) BOOL isAuthorized;

//Tab bar selected, when push recived
@property (nonatomic) BOOL isActivityFeedWithDot;
@property (nonatomic) BOOL isProfileWithDot;

//Action Menu Properties
@property (nonatomic, strong) NSArray *actionMenuItems; //Used for bottom menu
@property (nonatomic, strong) NSArray *dropMenuItems; //used for top menu
@property (nonatomic) DropMenuScreen topDropMenuScreen; //used for know top menu screen, where tap it.

@end

@implementation MainViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareTabBar];
    
    [self selectActivityTabElement];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!self.isAuthorized) {
        [self checkAuthorization];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void) checkAuthorization {
    
    User *user = [DataManager user];
    if (user == nil) {
        self.isAuthorized = NO;
        [self showLoginVC];
    } else {
        [[RequestManager sharedManager] setApiToken:[DataManager apiToken] email:[DataManager userEmail]];
        [self userDidLogin];
        self.isAuthorized = YES;
    }
}

#pragma mark - Action

-(void) showLoginVC {
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:NO completion:nil];
}


-(void) updateBackground {
    
    UIViewController *vc = [self.navigationController.viewControllers lastObject];
    
    if ([vc isKindOfClass:[ContentViewController class]]) {
        ((ContentViewController *)vc).backgroundImageView.image = [[DataManager userCompany] background];
    }
}

-(void) userDidLogin {
    [self activityFeedAction:nil];
    
    id vc = self.navigationController.visibleViewController;
    
    if ([vc isKindOfClass:[ActivityFeedVC class]]) {
        
        [(ActivityFeedVC *)vc showLoadingViewBellowView:[(ActivityFeedVC *)vc navigationBarView]];
        [(ActivityFeedVC *)vc loadFeedData];
        [(ActivityFeedVC *)vc refreshBackgroundImage];
    }
}

-(void) logoutUser {
    self.isAuthorized = NO;
    [DataManager logoutUser];
    [AnalyticsManager userDidLogout];
    [self showLoginVC];
    [self cleanNavigationStack];
    
}

- (void) cleanNavigationStack {
    
    NSArray* tempVCA = [self.navigationController viewControllers];
    for(UIViewController *tempVC in tempVCA)
    {
        [tempVC removeFromParentViewController];
    }

}

- (IBAction)giveTapAction:(id)sender {
    self.actionMenuItems = nil;
    [self showActionButtonMenu];
}

-(void) showGiveTap {
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapNavController"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void) showGiveTapForUser:(NSNumber *) userId {
    GiveTapNavController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapNavController"];
    [vc setUserId:userId];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) showEvaluate {
    GiveTapStep1VC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStep1VC"];
    vc.isUsedForEvaluate = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) showAddLearning {
    LearningCreateVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[LearningCreateVC storyboardIdentifier]];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) showChallengeBadgeListForCurrentUser {
    BadgeListVC *vc = [[UIStoryboard userProfileStoryboard] instantiateViewControllerWithIdentifier:[BadgeListVC  storyboardIdentifier]];
    vc.userId = [DataManager user].userId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) presentGiveSuggestionToAdmin {
    GiveTapStepMessageVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStepMessageVC"];
    vc.isSuggestionMessage = YES;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) presentNewQuestion {
    QuestionsVC *vc = [[UIStoryboard mainStoryboard] instantiateViewControllerWithIdentifier:[QuestionsVC storyboardIdentifier]];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) presentNewTeamMessage {
    GiveTapStepMessageVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStepMessageVC"];
    vc.isTeamMessage = YES;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) showActionButtonMenu {
    [AnalyticsManager trackEvent:ENAnalyticCategoryTabNavigationBar action:ENAnalyticActionActionMenuBottom label:ENAnalyticLabelOpen];
    [self showDropMenuWithStyle:DropMenuStyleBottom];
}

- (void) showActivityFeedDropMenu {
    self.dropMenuItems = nil;
    self.topDropMenuScreen = DropMenuScreenActivityFeed;
    [self showDropMenuWithStyle:DropMenuStyleTop];
}

- (void) showLearningListDropMenu {
    self.dropMenuItems = nil;
    self.topDropMenuScreen = DropMenuScreenLearningList;
    [self showDropMenuWithStyle:DropMenuStyleTop];
}

- (void) hideActionButtonMenu {
    [self hideDropMenu:DropMenuStyleBottom];
}

- (void) hideTopDropMenu {
    [self hideDropMenu:DropMenuStyleTop];
}

#pragma mark - Public methods for Push

- (void) updateActivityFeed {
    
    id vc = self.navigationController.visibleViewController;
    
    if ([vc isKindOfClass:[ActivityFeedVC class]]) {
        
        [(ActivityFeedVC *)vc loadFeedData];
    }
    else {
        [self newEventForActivityFeed];
        [AppDelegate theApp].mainViewController.isAddedNewTap = YES;
    }
}

- (void) showActivityFeed {
    
    [AppDelegate theApp].mainViewController.isAddedNewTap = YES;
    
    id vc = self.navigationController.visibleViewController;
    
    if ([vc isKindOfClass:[ActivityFeedVC class]]) {
        
        [(ActivityFeedVC *)vc loadFeedData];
        
    }
    else {
        [self activityFeedAction:nil];
    }
}

- (void) updateActivityFeedAndSetDotOnProfile:(BOOL) needDot {
    id vc = self.navigationController.visibleViewController;
    
    if ([vc isKindOfClass:[ActivityFeedVC class]]) {
        
        [(ActivityFeedVC *)vc loadFeedData];
    }
    else {
        if (needDot) {
            [self newEventForProfile];
        }
        [AppDelegate theApp].mainViewController.isAddedNewTap = YES;
    }
}

- (void) newEventForActivityFeed {
    self.isActivityFeedWithDot = YES;
    [self.activityFeedButton setImage:[UIImage imageNamed:@"main-activityFeedNewEvent"] forState:UIControlStateNormal];
}

- (void) newEventForProfile {
    self.isProfileWithDot = YES;
    [self.profileButton setImage:[UIImage imageNamed:@"main-profileNewEvent"] forState:UIControlStateNormal];
}

- (void) newEventForProfileEnded {
    self.isProfileWithDot = NO;
    
    id vc = self.navigationController.visibleViewController;
    
    if ([vc isKindOfClass:[ProfileVC class]]) {
        [self.profileButton setImage:[UIImage imageNamed:@"main-profileActive"] forState:UIControlStateNormal];
    }
    else {
        [self.profileButton setImage:[UIImage imageNamed:@"main-profile"] forState:UIControlStateNormal];
    }
    
}

- (void) challengeWinnerBadgeList {
    id vc = self.navigationController.visibleViewController;
    
    //If exist
    if (vc != nil) {
        
        UIViewController *vc2 = vc;
        
        //If view controller loaded
        if (vc2.isViewLoaded && vc2.view.window) {
            [[AppDelegate theApp].mainViewController showChallengeBadgeListForCurrentUser];
        }
        
        //If view controller not loaded
        else {
            [AppDelegate theApp].isNeedShowChallengeBadgeList = YES;
        }
    }
}
    
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"navigationControllerSegue"]) {
        self.navigationController = (UINavigationController *)segue.destinationViewController;
    }
}

-(void) setTabRootController:(UIViewController *) controller {
    if (controller == nil) return;
    
    [self.navigationController setViewControllers:@[controller] animated:NO];
}

- (IBAction)activityFeedAction:(id)sender {
    
    if (sender != nil) {
            [AnalyticsManager trackEvent:ENAnalyticCategoryTabNavigationBar action:ENAnalyticActionActivityFeed label:ENAnalyticLabelOpen];
    }

    self.isActivityFeedWithDot = NO;
    [self selectActivityTabElement];
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ActivityFeedVC"];
    [self setTabRootController:vc];
}

-(void) selectActivityTabElement {
    [self deselectAllTabsElement];
    [self.activityFeedButton setImage:[UIImage imageNamed:@"main-activityFeedActive"] forState:UIControlStateNormal];
}

-(void) showLearningListInActivityFeed {
    [self selectActivityTabElement];
    UIViewController *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:@"LearningListVC"];
    [self setTabRootController:vc];
}

- (IBAction)leaderboardAction:(id)sender {
    [AnalyticsManager trackEvent:ENAnalyticCategoryTabNavigationBar action:ENAnalyticActionActivityFeed label:ENAnalyticLabelOpen];
    [self deselectAllTabsElement];
    [self.leaderboardButton setImage:[UIImage imageNamed:@"main-leadersActive"] forState:UIControlStateNormal];
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LeaderBoardVC"];
    [self setTabRootController:vc];
}

- (IBAction)behaviriousAction:(id)sender {
    [AnalyticsManager trackEvent:ENAnalyticCategoryTabNavigationBar action:ENAnalyticActionValuesAndBehaviors label:ENAnalyticLabelOpen];
    [self deselectAllTabsElement];
    [self.favorietesButton setImage:[UIImage imageNamed:@"main-starActive"] forState:UIControlStateNormal];
    
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorsBoardVC"];
    [self setTabRootController:vc];
    
}
- (IBAction)profileAction:(id)sender {
    [AnalyticsManager trackEvent:ENAnalyticCategoryTabNavigationBar action:ENAnalyticActionCurrentProfile label:ENAnalyticLabelOpen];
    self.isProfileWithDot = NO;
    [self deselectAllTabsElement];
    [self.profileButton setImage:[UIImage imageNamed:@"main-profileActive"] forState:UIControlStateNormal];
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    vc.userId = [DataManager user].userId;
    vc.profileTitle = [DataManager user].fullName;
    vc.isPersonalProfile = YES;
    
    [self setTabRootController:vc];
}

#pragma mark Tab bar

-(void) prepareTabBar {
    self.tabBarView.backgroundColor = [UIColor clearColor];
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.tabBarView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.tabBarView addSubview:blurEffectView];
    [self.tabBarView sendSubviewToBack:blurEffectView];
    
    self.tabBarHeight.constant = EN_NAVIGATION_TAB_BAR_HEIGHT;
}

#pragma mark TabBar visability

- (BOOL)isTabBarViewVisible
{
    return (!self.tabBarView.hidden);
}

- (void)showTabBarView:(BOOL)animated
{
    if ([self isTabBarViewVisible]) return;
    
    self.tabBarView.hidden = NO;
    
    self.tabBarBottomSpace.constant = 0;
    [self.view setNeedsUpdateConstraints];

    if (animated) {
        [UIView animateWithDuration:defTabBarAnimationDuration
                         animations: ^{
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
    }

}

- (void)hideTabBarView:(BOOL)animated
{
    if (![self isTabBarViewVisible]) return;
    
    self.tabBarBottomSpace.constant = -EN_NAVIGATION_TAB_BAR_HEIGHT;
    [self.view setNeedsUpdateConstraints];

    if (animated) {
        [UIView animateWithDuration:defTabBarAnimationDuration
                         animations: ^{
                             [self.view layoutIfNeeded];
                         }
                         completion: ^(BOOL finished) {
                             self.tabBarView.hidden = YES;
                         }];
    } else {
        self.tabBarView.hidden = YES;
    }
}

-(void) deselectAllTabsElement {
    if (!self.isActivityFeedWithDot) {
        [self.activityFeedButton setImage:[UIImage imageNamed:@"main-activityFeed"] forState:UIControlStateNormal];
    }
    [self.leaderboardButton setImage:[UIImage imageNamed:@"main-leaders"] forState:UIControlStateNormal];
    [self.favorietesButton setImage:[UIImage imageNamed:@"main-star"] forState:UIControlStateNormal];
    if (!self.isProfileWithDot) {
        [self.profileButton setImage:[UIImage imageNamed:@"main-profile"] forState:UIControlStateNormal];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

#pragma mark - Drop Menu

- (void) showDropMenuWithStyle:(DropMenuStyle) style {
    __weak typeof(self) weakSelf = self;
    
    //Add back View
    UIView *backView = [ENDropMenuHelper backViewForView:self.view style:style];
    [self.view addSubview:backView];
    
    //Add blur effect
    UIVisualEffectView *blurEffectView = [ENDropMenuHelper blurViewForView:self.view style:style];
    [backView addSubview:blurEffectView];
    
    //Add action for hide
    UIButton *backBt;
    if (style == DropMenuStyleBottom) {
        backBt = [ENDropMenuHelper backButtonView:self.view withTarget:weakSelf action:@selector(hideActionButtonMenu) style:DropMenuStyleBottom];
    }
    else {
        backBt = [ENDropMenuHelper backButtonView:self.view withTarget:weakSelf action:@selector(hideTopDropMenu) style:DropMenuStyleTop];
    }
    [backView addSubview:backBt];
    
    //Drop menu size
    CGFloat menuHeight;
    if (style == DropMenuStyleBottom) {
        menuHeight = [ENDropMenu menuHeightWithItems:self.actionMenuItems];
    }
    else {
        menuHeight = [ENDropMenu menuHeightWithItems:self.dropMenuItems];
    }
    CGRect startMenuFrame = [ENDropMenuHelper startMenuFrameForView:self.view withHeight:menuHeight style:style];
    CGRect endMenuFrame = [ENDropMenuHelper endMenuFrameForView:self.view withHeight:menuHeight style:style];
    
    //Drop menu
    ENDropMenu *menu = [ENDropMenuHelper dropMenuWithFrame:startMenuFrame style:style];
    [backView addSubview:menu];
    
    if (style == DropMenuStyleBottom) {
        menu.items = self.actionMenuItems;
        menu.onClose = ^{
            [weakSelf hideActionButtonMenu];
        };
    }
    else {
        menu.items = self.dropMenuItems;
        menu.onClose = ^{
            [weakSelf hideTopDropMenu];
        };
    }
    
    [UIView animateWithDuration:.2 animations:^{
        blurEffectView.alpha = 1.0;
        backBt.alpha = 1.0;
        menu.frame = endMenuFrame;
        menu.alpha = 1.0;
        if (style == DropMenuStyleBottom) {
            self.actionButton.transform = CGAffineTransformMakeRotation((45.0 *M_PI) / 180.0);
        }
    }];
}

- (void) hideDropMenu:(DropMenuStyle) style {
    UIView *coverView = [ENDropMenuHelper backViewForHide:self.view style:style];
    UIVisualEffectView *blurView = [ENDropMenuHelper blurViewForHide:self.view style:style];
    ENDropMenu *menu = [ENDropMenuHelper dropMenuViewForHide:self.view style:style];
    
    CGFloat menuHeight;
    if (style == DropMenuStyleBottom) {
        menuHeight = [ENDropMenu menuHeightWithItems:self.actionMenuItems];
    }
    else {
        menuHeight = [ENDropMenu menuHeightWithItems:self.dropMenuItems];
    }
    
    [ENDropMenuHelper postNotificationForStyle:style];
    
    [UIView animateWithDuration:.2 animations:^{
        menu.frame = [ENDropMenuHelper startMenuFrameForView:self.view withHeight:menuHeight style:style];
        menu.alpha = 0.0;
        blurView.alpha = 0.0;
        if (style == DropMenuStyleBottom) {
                    self.actionButton.transform = CGAffineTransformMakeRotation((0 *M_PI) / 180.0);
        }
    } completion:^(BOOL finished) {
        [coverView removeFromSuperview];
    }];
}

- (NSArray *) actionMenuItems {

    //Return current menu array
    if (_actionMenuItems != nil) {
        return _actionMenuItems;
    }
    
    __weak typeof(self) weakSelf = self;
    
    //Create new item array
    NSMutableArray *items = [NSMutableArray array];
    
    NSArray *itemTypes = [ENDropMenuHelper itemsTypeForUser:[DataManager user]];
    
    for (int i = 0; i < itemTypes.count; i ++) {
        ENDropMenuItem *item = [[ENDropMenuItem alloc] init];
        NSNumber *itemNumber = itemTypes[i];
        item.title = [ENDropMenuHelper titleForItemType:itemNumber.integerValue];
        item.itemType = itemNumber.integerValue;
        item.onSelect = ^(NSInteger itemIndex) {
            [weakSelf hideActionButtonMenu];
            ENDropMenuItem *item = [weakSelf.actionMenuItems objectAtIndex:itemIndex];
            [ENDropMenuHelper showViewControllerForItemType:item.itemType];
        };
        [items addObject:item];
    }
    
    _actionMenuItems = items;
    
    return _actionMenuItems;
}

-(NSArray *) dropMenuItems {
    //Return current menu array
    if (_dropMenuItems != nil) {
        return _dropMenuItems;
    }
    
    __weak typeof(self) weakSelf = self;
    
    //Create new item array
    NSMutableArray *items = [NSMutableArray array];
    
    NSArray *itemTypes = [ENDropMenuHelper itemsTypeForScreen:self.topDropMenuScreen];
    
    for (int i = 0; i < itemTypes.count; i ++) {
        ENDropMenuItem *item = [[ENDropMenuItem alloc] init];
        NSNumber *itemNumber = itemTypes[i];
        item.title = [ENDropMenuHelper titleForItemType:itemNumber.integerValue];
        item.itemType = itemNumber.integerValue;
        item.onSelect = ^(NSInteger itemIndex) {
            [weakSelf hideTopDropMenu];
            ENDropMenuItem *item = [weakSelf.dropMenuItems objectAtIndex:itemIndex];
            [ENDropMenuHelper showViewControllerForItemType:item.itemType];
        };
        [items addObject:item];
    }
    
    _dropMenuItems = items;
    
    return _dropMenuItems;
}



@end
