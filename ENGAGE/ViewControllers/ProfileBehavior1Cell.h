//
//  ProfileBehavior1Cell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 25.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"

@interface ProfileBehavior1Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet ENRoundedImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *badgeLabel;
@property (weak, nonatomic) IBOutlet UIButton *behaviorIconButton;

@property (nonatomic, copy) void (^onShowBehavior)();

-(void) setBehavior:(UserBehavior *) behavior;

@end
