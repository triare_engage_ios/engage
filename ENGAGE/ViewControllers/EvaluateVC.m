//
//  EvaluateVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 30.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "EvaluateVC.h"
#import "EvaluateCollectionCell.h"
#import "ENRoundedImageView.h"

#import <AVFoundation/AVFoundation.h>
#import "MuteChecker.h"


@implementation EvaluateDragGoalView

-(void) changeState:(BOOL) isActive {
    
    if (self.isActive != isActive) {
        if (isActive) {
            [self imageView].contentMode = UIViewContentModeScaleToFill;
        } else {
            [self imageView].contentMode = UIViewContentModeCenter;
        }
        
        self.isActive = isActive;
    }
}

-(UIImageView *) imageView {
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            return (UIImageView *)view;
        }
    }
    
    return nil;
}

@end

@implementation EvaluateDragView

@end





@interface EvaluateVC () <UIGestureRecognizerDelegate>


@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;

@property (strong, nonatomic) IBOutlet UIView *userAreaView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (weak, nonatomic) IBOutlet UILabel *plusEvaluateLabel;
@property (weak, nonatomic) IBOutlet UILabel *minusEvaluateLabel;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *test;

@property (nonatomic, strong) EvaluateDragView *draggableCell;


@property BOOL isMovingState;


@property (weak, nonatomic) IBOutlet EvaluateDragGoalView *plusEvaluateView;
@property (weak, nonatomic) IBOutlet EvaluateDragGoalView *minusEvaluateView;


@property (nonatomic, strong) AVAudioPlayer *myPlayer;
@property (nonatomic, strong) MuteChecker *muteChecker;

@end

@implementation EvaluateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    TeamUser *user = [DataManager teamUserWithId:self.userId];
    NSString *title = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"EVALUATE", @"v1.0"), [user.firstName uppercaseString]];
    [self.navigationBar setTitle:title];
    [self.navigationBar showLeftButton];
    
    
    self.infoLabel.font = [UIFont appFontThin:17];
    self.infoLabel.textColor = [UIColor appTextColor];
    self.infoLabel.text = NSLocalizedString(@"drag and drop behaviors to evaluate", @"v1.0");
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.userAreaView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.userAreaView addSubview:blurEffectView];
    [self.userAreaView sendSubviewToBack:blurEffectView];
    
    self.minusEvaluateLabel.textColor = self.plusEvaluateLabel.textColor = [UIColor appTextColor];
    
    if (IS_IPHONE_5) {
        self.minusEvaluateLabel.font = self.plusEvaluateLabel.font = [UIFont appFontThin:12];
    }
    else {
        self.minusEvaluateLabel.font = self.plusEvaluateLabel.font = [UIFont appFontThin:14];
    }
    
    self.minusEvaluateLabel.text = NSLocalizedString(@"NEED TO WORK ON", @"v1.0");
    self.plusEvaluateLabel.text = NSLocalizedString(@"BEST AT", @"v1.0");
    
    
    
    
    
    
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.collectionView.contentInset = insets;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"EvaluateCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    panGesture.delegate = self;
    [self.collectionView addGestureRecognizer:panGesture];
    
    [self loadEvaluations];
    //[self initMuteChecker];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenEvaluate];
    [AnalyticsManager trackScreen:ENAnalyticScreenEvaluate withScreenClass:NSStringFromClass([self class])];
}

- (void)loadEvaluations {
    
    __weak typeof(self) weakSelf = self;
    
    [[RequestManager sharedManager] getEvaluations:self.userId onCompleate:^(BOOL success, NSArray *list) {
        [DataManager saveEvaluations:list userId:self.userId];
        [weakSelf reloadData];
    }];
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.collectionView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (self.isMovingState) return NO;
    return YES;
}


-(BOOL) isDraggableViewInGoalView:(EvaluateDragGoalView *) view atPoint:(CGPoint) point {
    CGPoint viewPoint = [view convertPoint:point fromView:self.view];
    if ([view pointInside:viewPoint withEvent:nil]) {
        return YES;
    } else {
        return NO;
    }
}


-(void) checkHightlightDragGoals:(CGPoint) point {
    
    [self.plusEvaluateView changeState:[self isDraggableViewInGoalView:self.plusEvaluateView atPoint:point]];
    [self.minusEvaluateView changeState:[self isDraggableViewInGoalView:self.minusEvaluateView atPoint:point]];
}



-(EvaluateDragView *) draggableCellFromCell:(EvaluateCollectionCell *) cell {
    
    EvaluateDragView *view = [[EvaluateDragView alloc] initWithFrame:cell.bounds];
    
    ENRoundedImageView *imView = [[ENRoundedImageView alloc] initWithFrame:cell.cellImageView.frame];
    imView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin| UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
    //[UIView makeAppRoundedView:imView];
    imView.image = cell.cellImageView.image;
    [view addSubview:imView];

    
    view.alpha = .75;
    return view;
}

-(void) changeEvaluateAtIndex:(NSIndexPath *) indexPath state:(NSInteger) state {
    EvaluateBehavior *behaviour = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [DataManager saveState:state forEvaluate:behaviour];
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setValue:[NSNumber numberWithInteger:state] forKey:@"state"];
    [info setValue:self.userId forKey:@"receiver_id"];
    [info setValue:behaviour.behaviorId forKey:@"behaviour_id"];
    
    [[RequestManager sharedManager] changeEvaluations:@{@"evaluation":info} onCompleate:^(BOOL success) {
        
    }];
}

-(void) addPlusAtCell:(NSIndexPath *) indexPath {
    [self changeEvaluateAtIndex:indexPath state:1];
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

-(void) addMinusAtCell:(NSIndexPath *) indexPath {
    [self changeEvaluateAtIndex:indexPath state:-1];
    [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

-(void) addDraggableViewToGoal:(EvaluateDragGoalView *) view {
    
    CGPoint center = [view.superview convertPoint:CGPointMake(view.frame.origin.x+view.frame.size.width/2, view.frame.origin.y+view.frame.size.height/2) toView:self.view];
    
    [UIView animateWithDuration:.2 animations:^{
        self.draggableCell.frame = CGRectMake(center.x - 5, center.y - 5, 10, 10);
    } completion:^(BOOL finished) {
        [self.draggableCell removeFromSuperview];
        [view changeState:NO];
    }];
}

-(void) checkDragGoalsAtPoint:(CGPoint) point {
    if ([self isDraggableViewInGoalView:self.plusEvaluateView atPoint:point]) {
        [self addDraggableViewToGoal:self.plusEvaluateView];
        [self addPlusAtCell:self.draggableCell.cellIndexPath];
        //[self playDragSound];
        //[self makeVibrationIfMuted];
        [self makeVibration];
    } else if ([self isDraggableViewInGoalView:self.minusEvaluateView atPoint:point]) {
        [self addDraggableViewToGoal:self.minusEvaluateView];
        [self addMinusAtCell:self.draggableCell.cellIndexPath];
        //[self playDragSound];
        //[self makeVibrationIfMuted];
        [self makeVibration];
    } else {
        
        NSIndexPath *indexPath = self.draggableCell.cellIndexPath;
        
        if (indexPath == nil) {
            [self.draggableCell removeFromSuperview];
            return;
        }
        
        UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
        CGRect cellRect = attributes.frame;
        
        CGRect dd = [self.view convertRect:cellRect fromView:self.collectionView];
        
        [UIView animateWithDuration:.2 animations:^{
            self.draggableCell.frame = dd;
        } completion:^(BOOL finished) {
            [self.collectionView reloadItemsAtIndexPaths:@[indexPath]];
            [self.draggableCell removeFromSuperview];
        }];
    }
}


-(void)handlePan:(UIPanGestureRecognizer *)panRecognizer {
    
    if (self.isMovingState == NO) {
        return;
    }
    
    
    CGPoint locationPoint2 = [panRecognizer locationInView:self.view];
    
    if (panRecognizer.state == UIGestureRecognizerStateBegan) {
        
        CGPoint collactionViewPoint = [panRecognizer locationInView:self.collectionView];
        
        NSIndexPath *indexPathOfMovingCell = [self.collectionView indexPathForItemAtPoint:collactionViewPoint];
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPathOfMovingCell];
        cell.alpha = .75;
        
        self.draggableCell = [self draggableCellFromCell:(EvaluateCollectionCell *)cell];
        self.draggableCell.center = locationPoint2;
        self.draggableCell.startCenter = locationPoint2;
        self.draggableCell.cellIndexPath = indexPathOfMovingCell;
        [self.view addSubview:self.draggableCell];
    }
    
    if (panRecognizer.state == UIGestureRecognizerStateChanged) {
        [self.draggableCell setCenter:locationPoint2];
        
        [self checkHightlightDragGoals:locationPoint2];
    }
    
    if (panRecognizer.state == UIGestureRecognizerStateEnded) {
        
        self.isMovingState = NO;
        
        [self checkDragGoalsAtPoint:locationPoint2];
        
        //self.test.constant = 80;
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [EvaluateBehavior MR_requestAllSortedBy:@"title" ascending:YES inContext:[DataManager context]];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"teamUser.userId == %@", self.userId];
    NSFetchedResultsController *fetchController = [EvaluateBehavior MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    EvaluateBehavior *behaviour = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    EvaluateCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.alpha = 1;
    cell.cellLabel.text = behaviour.title;
    [cell.cellImageView sd_setImageWithURL:[AppTemplate mediumIconURL:behaviour.iconUrl] placeholderImage:[Behavior placeholder]];
    
    [cell evaluateState:behaviour.evaluateState.integerValue];
    cell.onCancelEvaluate = ^{
        [weakSelf changeEvaluateAtIndex:indexPath state:0];
    };
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [EvaluateCollectionCell sizeForCollectionWidth:collectionView.frame.size.width];
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}


- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {

    self.isMovingState = YES;
    
    EvaluateCollectionCell *cell = (EvaluateCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.cellImageView.alpha = 0.75;
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EvaluateCollectionCell *cell = (EvaluateCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.cellImageView.alpha = 1;
}

#pragma mark - Drags Evaluation Sounds

- (void) playDragSound {
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"evaluate_drag_finished" ofType:@"caf"];
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    
    self.myPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:nil];
    [self.myPlayer prepareToPlay];
    [self.myPlayer play];
}

- (void) initMuteChecker {
    self.muteChecker = [[MuteChecker alloc] initWithCompletionBlk:^(NSTimeInterval lapse, BOOL muted) {
        if (muted) {
            [self makeVibration];
        }
    }];
}

- (void) makeVibrationIfMuted {
    [self.muteChecker check];
}

- (void) makeVibration {
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

@end
