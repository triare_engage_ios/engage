//
//  AnswerCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerCell : UITableViewCell
@property (nonatomic, strong) UIColor *circleColor;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth;

@end
