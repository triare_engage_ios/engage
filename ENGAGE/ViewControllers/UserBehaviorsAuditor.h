//
//  UserBehaviorsAuditor.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 25.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserBehaviorsAuditor : NSObject

@property (nonatomic, strong) TeamUser *user;

@property (nonatomic, strong) NSArray *bjectsType1;
@property (nonatomic, strong) NSArray *bjectsType2;


-(id) initWithUser:(TeamUser *) user;

-(NSArray *) cellsTypesList;
-(NSArray *) bjectsType3ForRow:(NSInteger) index;


@end
