//
//  QuestionsListCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <SWTableViewCell/SWTableViewCell.h>

@interface QuestionsListCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

-(void) addQuestion:(Question *) question withUsersCount:(NSNumber *) usersCount;

@end
