//
//  ChallengeInfoVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ChallengeInfoVC.h"

#import "ContentScrollView.h"

#import "ENCircleView.h"
#import "ENCircleImageView.h"

#import "ENPlaceholderView.h"

@interface ChallengeInfoVC ()
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentScrollView *contentScrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceContentScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightContentView;

@property (weak, nonatomic) IBOutlet UILabel *nameTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIView *backgroundChallengeView;
@property (weak, nonatomic) IBOutlet ENCircleView *backgroundCircleView;
@property (weak, nonatomic) IBOutlet ENCircleImageView *challengeCircleImageView;

@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *actionButtonHeight;

@property (nonatomic, strong) UIColor *backgroundCircleViewColor;

@property (nonatomic, strong) ENPlaceholderView *emptyPlaceholder;

@end

@implementation ChallengeInfoVC

#pragma mark - Public Methods

+(NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self needHideContent:YES forState:self.screenState];
    [self prepareChallengeInfo];
    [self prepareNavigationBar];
    [self prepareScrollView];
    [self prepareLabels];
    [self prepareActionButton];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.backgroundCircleView.layer.borderColor = self.backgroundCircleViewColor.CGColor;
    self.backgroundCircleView.layer.borderWidth = 11.0;
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.screenState == ScreenStateFromNavigationBar) {
        [DataManager deleteActivityFeedQuestion];
    }
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self trackScreenForAnalytic:AnalyticsTypeAll];
}

#pragma mark - Prepare Methods

- (void) prepareChallengeInfo {
    
    if (self.screenState == ScreenStateFromActivityFeed) {
        [self updateChallengeInfoForState:self.screenState];
    }
    
    if (self.screenState == ScreenStateFromNavigationBar) {
        [DataManager deleteCurrentChallenge];
        [self loadCurrentChallenge];
    }
    
    if (self.screenState == ScreenStateFromUserProfile) {
        [self fillWithChallenge:self.challenge];
    }
    
    if (self.screenState == ScreenStateFromActivityFeedCell) {
        [self fillWithChallenge:self.challenge];
    }
}

- (void) updateChallengeInfoForState:(ScreenState) state {
    
    if (state == ScreenStateFromActivityFeed) {
        self.challenge = [DataManager activityFeedChallenge];
        [self fillWithChallenge:self.challenge];
    }
    
    if (state == ScreenStateFromNavigationBar) {
        self.challenge = [DataManager currentChallenge];
        [self fillWithChallenge:self.challenge];
    }
}

- (void) prepareNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.screenState == ScreenStateFromUserProfile || self.screenState == ScreenStateFromNavigationBar || self.screenState == ScreenStateFromActivityFeedCell) {
        [self.navigationBar showLeftButton];
        self.navigationBar.onLeftAction = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    
    [self.navigationBar setTitle:[NSLocalizedString(@"challenge", @"v1.0") uppercaseString]];
}

- (void) prepareScrollView {
    if (self.screenState == ScreenStateFromActivityFeed) {
        UIEdgeInsets insets = self.contentScrollView.contentInset;
        insets.bottom = 0;
        self.contentScrollView.contentInset = insets;
    }
    else if (self.screenState == ScreenStateFromNavigationBar || self.screenState == ScreenStateFromUserProfile || self.screenState == ScreenStateFromActivityFeedCell) {
        self.actionButtonHeight.constant = 0;
        self.bottomSpaceContentScrollView.constant = 0;
    }
}

- (void) prepareLabels {
    self.nameTitleLabel.font = [UIFont appFontBold:30];
    self.nameTitleLabel.textColor = [UIColor appTextColor];
    
    self.timeLabel.font = [UIFont appFontBold:10];
    self.timeLabel.textColor = [UIColor appTextColor];
    
    self.subTitleLabel.font = [UIFont appFontBold:18];
    self.subTitleLabel.textColor = [UIColor appTextColor];
}

- (void) prepareActionButton {
    
    [self.actionButton setTitle:[NSLocalizedString(@"ok", @"v1.0") uppercaseString] forState:UIControlStateNormal];
    [self.actionButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    self.actionButton.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.30];
    self.actionButton.titleLabel.font = [UIFont appFont:18];
    
    switch (self.screenState) {
        case ScreenStateFromActivityFeed:
            self.actionButton.hidden = NO;
            break;
            
        case ScreenStateFromNavigationBar:
            self.actionButton.hidden = YES;
            break;
        
        case ScreenStateFromUserProfile:
            self.actionButton.hidden = YES;
            break;
            
        case ScreenStateFromActivityFeedCell:
            self.actionButton.hidden = YES;
            break;
        
        default:
            break;
    }
}

#pragma mark - Load Data

- (void) loadCurrentChallenge {
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] getCurrentChallengeSuccess:^(BOOL isNotEmpty, NSDictionary *info) {
        if (isNotEmpty) {
            [DataManager saveCurrentChallenge:info];
            [weakSelf updateChallengeInfoForState:weakSelf.screenState];
            [weakSelf needHideContent:NO forState:self.screenState];
        }
        else {
            [self needShowPlaceholder:YES];
        }

        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
    }];
}

- (void) activityFeedChallengeHasBeenShown {
    __weak typeof(self) weakSelf = self;
    
    
    [DataManager deteteActivityFeedChallenge];
    
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] challengeWasShown:self.challenge.feedId success:^(BOOL isSuccess) {
        [weakSelf hideLoadingView];
        //MLog(@"Challenge mark as looked");
    } failure:^(NSString *message) {
        //MLog(@"Challenge looked error");
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithMessage:message];
    }];
}

#pragma mark - Action

- (IBAction)actionButtonPressed:(id)sender {
    [self activityFeedChallengeHasBeenShown];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) fillCustomData {
    self.nameTitleLabel.text = [NSLocalizedString(@"who is the fastest", @"v1.0") uppercaseString];
    self.timeLabel.text = @"till 22.11.2016";
    self.subTitleLabel.text = NSLocalizedString(@"Get 2000$, by due asap, but no quality loss receive taps SPEED The winner will be first who received 1000 taps", @"v1.0");
    [self.challengeCircleImageView sd_setImageWithURL:[NSURL URLWithString:@"http://i.imgur.com/jjIZ4Kn.png"]];
}

- (void) fillWithChallenge:(Challenge *) challenge {
    self.backgroundCircleViewColor = [UIColor colorFromHexString:challenge.borderColorHex];
    [self refreshBackgroundImageWithURL:[NSURL URLWithString:challenge.backgroundImageUrl]];
    [self.challengeCircleImageView sd_setImageWithURL:[NSURL URLWithString:challenge.imageIconUrl] placeholderImage:[Behavior placeholder]];
    self.timeLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"till", @"v1.0"), [challenge formattedUntilDate]];
    
    self.nameTitleLabel.text = challenge.title;
    self.subTitleLabel.text = challenge.challengeDescription;
    
    [self updateContentHeightForState:self.screenState];
}

#pragma mark - UI update

//Use after set text
- (void) updateContentHeightForState:(ScreenState) state {
    
    //Ok button exist
    if (state == ScreenStateFromActivityFeed) {
        CGFloat height = [self subtitleHeight];
        
        if (height < [self spaceToBottomForSubtitile]) {
            height = 0;
        }
        else {
            height = height - [self spaceToBottomForSubtitile];
        }
        
        self.heightContentView.constant = height;
    }
    else {
        CGFloat height = [self subtitleHeight];
        
        if (height < [self spaceToBottomForSubtitile] + 60) {
            height = - 60;
        }
        else {
            height = height - [self spaceToBottomForSubtitile] - 60;
        }
        
        self.heightContentView.constant = height;
    }
}

- (void) needHideContent:(BOOL) needHide forState:(ScreenState) state {
    
    if (self.screenState == ScreenStateFromNavigationBar) {
        [self needHideContent:needHide];
        //Action button always hidden for this state
        self.actionButton.hidden = YES;
    }
}

- (void) needHideContent:(BOOL) needHide {
    self.nameTitleLabel.hidden = needHide;
    self.timeLabel.hidden = needHide;
    self.backgroundChallengeView.hidden = needHide;
    self.subTitleLabel.hidden = needHide;
}

#pragma mark - Properties

-(UIColor *) backgroundCircleViewColor {
    if (_backgroundCircleViewColor == nil) {
        _backgroundCircleViewColor = [UIColor clearColor];
    }
    
    return _backgroundCircleViewColor;
}

#pragma mark - Helpers Methods

-(CGFloat) subtitleHeight {
    [self.subTitleLabel setNeedsLayout];
    [self.subTitleLabel layoutIfNeeded];
    
    return self.subTitleLabel.frame.size.height;
}

-(CGFloat) spaceToBottomForSubtitile {
    if (IS_IPHONE_5) {
        return 110;
    }
    
    if (IS_IPHONE_6) {
        return 270;
    }
    
    if (IS_IPHONE_6P) {
        return 330;
    }
    
    return 180;
}

#pragma mark - Placeholder

- (void) needShowPlaceholder:(BOOL) needShow {
    if (self.emptyPlaceholder == nil) {
            self.emptyPlaceholder = [ENPlaceholderView createCenterPlaceholderWithText:NSLocalizedString(@"No challenge yet..", @"v1.0") andFond:[UIFont appFontBold:22]];
        [self.contentScrollView addSubview:self.emptyPlaceholder];
    }
    self.emptyPlaceholder.hidden = !needShow;
}

-(void) setEmptyPlaceholder:(ENPlaceholderView *)emptyPlaceholder {
    if (emptyPlaceholder == nil) {
        [_emptyPlaceholder removeFromSuperview];
    }
    
    _emptyPlaceholder = emptyPlaceholder;
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) text {
    
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}

- (void) trackScreenForAnalytic:(AnalyticsType) type {
    
    NSString *screenName = @"";
    
    if (self.screenState == ScreenStateFromActivityFeed || self.screenState == ScreenStateFromNavigationBar) {
        screenName = ENAnalyticScreenChallengeCurrent;
    }
    else if (self.screenState == ScreenStateFromActivityFeedCell) {
        screenName = ENAnalyticScreenChallengeCell;
    }
    
    else {
        screenName = ENAnalyticScreenChallengeProfile;
    }
    
    switch (type) {
        case AnalyticsTypeAll:
            [AnalyticsManager trackScreen:screenName];
            [AnalyticsManager trackScreen:screenName withScreenClass:NSStringFromClass([self class])];
            break;
            
        case AnalyticsTypeGoogleAnalytics:
            [AnalyticsManager trackScreen:screenName];
            break;
            
        case AnalyticsTypeFirebase:
            [AnalyticsManager trackScreen:screenName withScreenClass:NSStringFromClass([self class])];
            break;
    }
    
    
}

@end
