//
//  ProfileSortPeriodButtonsCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 17.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileSortPeriodButtonsCell.h"

@implementation ProfileSortPeriodButtonsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.weekButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    self.weekButton.titleLabel.font = [UIFont appFontThin:17];
    [self.weekButton setTitle:NSLocalizedString(@"WEEK", @"v1.0") forState:UIControlStateNormal];
    
    [self.monthButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    self.monthButton.titleLabel.font = [UIFont appFontThin:17];
    [self.monthButton setTitle:NSLocalizedString(@"MONTH", @"v1.0") forState:UIControlStateNormal];
    
    [self.lifetimeButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    self.lifetimeButton.titleLabel.font = [UIFont appFontThin:17];
    [self.lifetimeButton setTitle:NSLocalizedString(@"LIFETIME", @"v1.0") forState:UIControlStateNormal];
    
    [self changeButtonState:self.monthButton isSelected:YES];
}

-(void) changeButtonState:(UIButton *) button isSelected:(BOOL) selected {
    
    button.layer.cornerRadius = 7;
    button.layer.borderWidth = 1;
    
    if (selected) {
        button.layer.borderColor = [UIColor appTextColor].CGColor;
    } else {
        button.layer.borderColor = [UIColor clearColor].CGColor;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) selectButtonAtIndex:(NSInteger) index {
    [self changeButtonState:self.weekButton isSelected:NO];
    [self changeButtonState:self.monthButton isSelected:NO];
    [self changeButtonState:self.lifetimeButton isSelected:NO];
    
    switch (index) {
        case 0:
            [self changeButtonState:self.weekButton isSelected:YES];
            break;
        case 1:
            [self changeButtonState:self.monthButton isSelected:YES];
            break;
        case 2:
            [self changeButtonState:self.lifetimeButton isSelected:YES];
            break;
            
        default:
            break;
    }
}

- (IBAction)weekAction:(id)sender {
    [self selectButtonAtIndex:0];
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onChangeSort) {
        weakSelf.onChangeSort(0);
    }
}
- (IBAction)monthAction:(id)sender {
    [self selectButtonAtIndex:1];
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onChangeSort) {
        weakSelf.onChangeSort(1);
    }
}
- (IBAction)lifetimeAction:(id)sender {
    [self selectButtonAtIndex:2];
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onChangeSort) {
        weakSelf.onChangeSort(2);
    }
}
@end
