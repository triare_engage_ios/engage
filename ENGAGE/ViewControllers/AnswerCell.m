//
//  AnswerCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "AnswerCell.h"
#import "ENCircleView.h"

@interface AnswerCell ()
@property (weak, nonatomic) IBOutlet ENCircleView *leftCircleView;

@end

@implementation AnswerCell

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:17];
}

- (void) prepareForReuse {
    [super prepareForReuse];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
//    self.leftCircleView.layer.cornerRadius = self.leftCircleView.frame.size.width / 2;
//    self.leftCircleView.clipsToBounds = YES;
//    
    self.leftCircleView.layer.borderWidth = 1.0;
    self.leftCircleView.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:1].CGColor;
    
    if (self.circleColor) {
        self.leftCircleView.backgroundColor = self.circleColor;
    } else {
        self.leftCircleView.backgroundColor = [UIColor appTextColor];
    }
    
    self.leftCircleView.clipsToBounds = YES;
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {
 
    CGFloat minHeight = 50;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFont:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 60, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    height += 5; //From title label to top;
    height += 5; //From title label to bottom
    
    if (minHeight > height) {
        height = minHeight;
    }
    
    return height;
}

@end
