//
//  ProfileSettingsJobTitleVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/7/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface ProfileSettingsJobTitleVC : ContentViewController

@property (nonatomic, copy) void (^onBackAction)(NSString *jobTitle);

@end
