//
//  GiveTapStep2_1VC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/22/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "GiveTapStep2_1VC.h"
#import "GiveTapStep1VC.h"
#import "GiveTapNavController.h"
#import "NavigationBar.h"

#import "BehaviorTitleCell.h"
#import "ValueCell.h"

#import "ENPlaceholderView.h"

@interface GiveTapStep2_1VC () <UITableViewDelegate, UITableViewDataSource, NavigationBarSearchDelegate>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerViewLabel;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSString *searchText;

@property (nonatomic, strong) ENPlaceholderView *tablePlaceholder;


@end

@implementation GiveTapStep2_1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupHeaderView];
    [self setupTableView];
    [self loadData];
    
    [self setNeedsStatusBarAppearanceUpdate];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf createTablePlaceholder];
    });
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenGiveATapBehaviorList];
    [AnalyticsManager trackScreen:ENAnalyticScreenGiveATapBehaviorList withScreenClass:NSStringFromClass([self class])];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    //Need for skip first step if give tap from user profile
    if (self.isNeedSkipFirstStepWhenBack) {
        self.navigationBar.onLeftAction = ^{
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        };
        
    }
    else {
        self.navigationBar.onLeftAction = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    
    [self.navigationBar setTitle:NSLocalizedString(@"STEP 2", @"v1.0")];
    
    self.navigationBar.searchDelegate = self;
    [self.navigationBar showRightButton];
    [self.navigationBar showLeftButton];
}

- (void) setupHeaderView {
    self.headerViewLabel.textColor = [UIColor appTextColor];
    self.headerViewLabel.font = [UIFont appFont:17];
    self.headerViewLabel.text = NSLocalizedString(@"What are you tapping them for?", @"v1.0");
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) createTablePlaceholder {
    
    self.tablePlaceholder = [ENPlaceholderView createPlaceholderWithText:NSLocalizedString(@"No result found", @"v1.0") andFont:[UIFont appFont:17]];
    self.tablePlaceholder.frame = CGRectMake(0, 50, SCREEN_WIDTH, self.tablePlaceholder.frame.size.height + 30);
    
    self.tablePlaceholder.hidden = YES;
    [self.contentTableView addSubview:self.tablePlaceholder];
    
}

#pragma mark - Data

- (void) loadData {
    __weak typeof(self) weakSelf = self;
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getValuesListSuccess:^(NSArray *list) {
        [DataManager saveValuesList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
    }];
    
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.contentTableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    GiveTapNavController *navigationController = (GiveTapNavController *) self.navigationController;
    navigationController.behavior = sender;
}

- (void) showNextStepWithBehaviorID:(NSNumber *) behaviorID {
    Behavior *item = [DataManager behaviorByID:behaviorID];
    
    [self performSegueWithIdentifier:@"goToNextStep2" sender:item];
}

#pragma mark - fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSMutableArray *predicates = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [Value MR_requestAllSortedBy:@"title" ascending:YES inContext:[DataManager context]];
    
    if (self.searchText.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"ANY behaviors.title CONTAINS[cd] %@ ", self.searchText];
        [predicates addObject:searchPredicate];
    }
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [Value MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if (number == 0 && self.navigationBar.barView.searchBar.text.length > 0) {
        self.tablePlaceholder.hidden = NO;
    }
    else {
        self.tablePlaceholder.hidden = YES;
    }
    
    return number;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:section];
    NSInteger numberOfRows = (int) ceilf((float)item.behaviorsSet.array.count / (float) 3);
    return numberOfRows + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 40;
    }
    else {
        return SCREEN_WIDTH / 16 * 9 / 1.5;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.section];
    
    if (indexPath.row == 0) {
        BehaviorTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"behaviorTitleCell"];
        
        cell.valueTitle.text = item.title;
        
        return cell;
    }
    else {
        ValueCell *cell = [tableView dequeueReusableCellWithIdentifier:@"valueCell"];
        
        NSInteger itemNumber = (indexPath.row - 1) * 3;
        
        NSMutableArray *arrayOfBehaviors = [[NSMutableArray alloc] init];
        
        if (item.behaviorsSet.array.count > itemNumber) {
            
            Behavior *itemBehavior = item.behaviorsSet.array[itemNumber];
            [arrayOfBehaviors addObject:itemBehavior];
        }
        
        if (item.behaviorsSet.array.count > itemNumber + 1) {
            Behavior *itemBehavior = item.behaviorsSet.array[itemNumber + 1];
            [arrayOfBehaviors addObject:itemBehavior];
        }
        
        if (item.behaviorsSet.array.count > itemNumber + 2) {
            Behavior *itemBehavior = item.behaviorsSet.array[itemNumber + 2];
            [arrayOfBehaviors addObject:itemBehavior];
        }
        
        [cell addBehaviors:arrayOfBehaviors];
        
        cell.onAction = ^(NSNumber *behaviorId) {
            //[weakSelf showBehaviorDetailsById:behaviorId];
            [weakSelf showNextStepWithBehaviorID:behaviorId];
        };
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark NavigationBarSearchDelegate

-(void) checkSearchPlaceholder {
    self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by behavior title", @"v1.0");
}

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    self.searchText = nil;
    [self reloadData];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [self checkSearchPlaceholder];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    self.searchText = searchText;
    
    [self reloadData];
}


@end
