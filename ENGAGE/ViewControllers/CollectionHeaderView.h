//
//  CollectionHeaderView.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
