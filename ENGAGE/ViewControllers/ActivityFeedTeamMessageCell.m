//
//  ActivityFeedTeamMessageCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/13/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ActivityFeedTeamMessageCell.h"
#import "ENRoundedImageView.h"
#import "ENRoundedView.h"
#import "ENCircleView.h"

#define kBorderWidth 1.0

@interface ActivityFeedTeamMessageCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *createDateWidth;
@property (weak, nonatomic) IBOutlet ENCircleView *avatarRoundedView;

@property (nonatomic, strong) NSNumber *borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

@end

@implementation ActivityFeedTeamMessageCell

- (void) addFeed:(ActivityFeed *) feed {
    self.contentLabel.text = feed.body;
    
    [self.avatarImageView sd_setImageWithURL:[AppTemplate mediumIconURL:feed.senderAvatarUrl] placeholderImage:[TeamUser placeholder]];
    self.avatarImageView.backgroundColor = [UIColor whiteColor];
    
    self.titleLabel.text = NSLocalizedString(@"TEAM MESSAGE", @"v1.0");
    
    self.createDateLabel.text = [feed formattedCreateDate];
    
    CGSize dateSize = [self.createDateLabel sizeThatFits:CGSizeZero];
    self.createDateWidth.constant = dateSize.width;
    
    if (feed.isReceiverPointWinnerValue) {
        self.borderWidth = [NSNumber numberWithFloat:3.0];
        self.borderColor = [UIColor avatarBorderColor];
    }
    
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.avatarRoundedView.backgroundColor = [UIColor whiteColor];
    
    self.backgroundRoundedView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    
    self.createDateLabel.textColor = [UIColor appClockColorForTeamMessage];
    self.createDateLabel.font = [UIFont appFontBold:14];
    
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [UIFont appFont:17];
    
    self.contentLabel.textColor = [UIColor blackColor];
    self.contentLabel.font = [UIFont appFontThin:17];
    
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.borderColor = nil;
    self.borderWidth = nil;
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {

    /*
     CGRect rect = [contentText boundingRectWithSize:CGSizeMake(tableWidth - 30, CGFLOAT_MAX) options:(NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [UIFont appFontThin:17]} context:nil];
     */
    
    //bad font, use this way for calculate text height
    
    CGFloat minheight = 21;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFontThin:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 10 - 10 - 45 - 10 - 10 - 15 - 10, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    if (height < minheight) {
        height = minheight;
    }
    
    height += 30; //Top space to background white view
    height += 10; //Top space to avatar image
    height += 21; //Title label height
    height += 5; //Top space to title label height
    height += 5; //height if one line
    
    return height;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.backgroundRoundedView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.backgroundRoundedView.layer.borderWidth = 1.0;
    
    self.avatarImageView.layer.borderWidth = self.borderWidth.floatValue;
    self.avatarImageView.layer.borderColor = self.borderColor.CGColor;
}

- (IBAction)senderIconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowSender) {
        weakSelf.onShowSender (weakSelf);
    }
}

#pragma mark - Properties

-(NSNumber *) borderWidth {
    if (_borderWidth == nil) {
        _borderWidth = [NSNumber numberWithFloat:kBorderWidth];
    }
    
    return _borderWidth;
}

- (UIColor *) borderColor {
    if (_borderColor == nil) {
        _borderColor = [UIColor appLightGrayColor];
    }
    
    return _borderColor;
}

@end
