//
//  ProfileBadgeCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileBadgeCell : UITableViewCell
@property (nonatomic, copy) void (^onAction)();

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

@end
