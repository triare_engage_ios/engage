//
//  ProfileTeamMessageListVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/13/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileTeamMessageListVC.h"
#import "UITableViewCell+Separator.h"
#import "ContentTableView.h"
#import "NavigationBar.h"
#import "TeamMessageListCell.h"
#import "GiveTapStepMessageVC.h"

#import "RequestManager.h"
#import "DataManager.h"

@interface ProfileTeamMessageListVC () <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSMutableArray *fullMessageIds;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation ProfileTeamMessageListVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self setupFullMessageIds];
    
    [self addDownRefresh];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadData];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserTeamMessageList];
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserTeamMessageList withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    
    if (IS_IPHONE_5) {
        self.navigationBar.barView.titleLabel.font = [UIFont appFontBold:16];
    }
    
    [self.navigationBar setTitle:NSLocalizedString(@"TEAM MESSAGES HISTORY", @"v1.0")];
    
    self.navigationBar.onRightAction = ^{
        [weakSelf showAddTeamMessage];
    };
    
    [self.navigationBar changeRightButtonIcon:[UIImage imageNamed:@"icon-teamMessage"]];
    [self.navigationBar showRightButton];
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupFullMessageIds {
    self.fullMessageIds = [NSMutableArray array];
}

#pragma mark - Data

-(void) loadData {
    [self showLoadingViewBellowView:self.navigationBar];
    __weak typeof(self) weakSelf = self;
    [[RequestManager sharedManager] getTeamMessageListSuccess:^(NSArray *list) {
        //
        [DataManager saveTeamMessageList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf.refreshControl endRefreshing];
        [weakSelf showAlertWithTitle:nil andText:message];
    }];
}

- (void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.contentTableView reloadData];
}

#pragma mark - Action

-(void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) showAddTeamMessage {
    GiveTapStepMessageVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStepMessageVC"];
    vc.isTeamMessage = YES;
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [TeamMessage MR_requestAllSortedBy:@"createDate" ascending:NO inContext:[DataManager context]];
    NSFetchedResultsController *fetchController = [ActivityFeed MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TeamMessage *message = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    TeamMessageListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell addTeamMessage:message fullInfo:[self.fullMessageIds containsObject:message.messageId]];
    cell.delegate = self;
    cell.rightUtilityButtons = [self deleteButton];
    
    [cell showSeparatorLine];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat collapsedHeight = 80;
    
    TeamMessage *message = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    CGFloat height = [TeamMessageListCell cellHeight:message.body width:tableView.frame.size.width];
    
    if (height < collapsedHeight) {
        [self.fullMessageIds addObject:message.messageId];
    } else {
        if (![self.fullMessageIds containsObject:message.messageId]) {
            return collapsedHeight;
        }
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    TeamMessage *message = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (![self.fullMessageIds containsObject:message.messageId]) {
        [self.fullMessageIds addObject:message.messageId];
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - SWTableViewCellDelegate

//Can swipe only one row in time (close if swipe another)
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.contentTableView indexPathForCell:cell];
    
    [self showDeleteAlertForIndexPath:indexPath];
}

- (void) deleteTeamMessageAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    TeamMessage *message = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [[RequestManager sharedManager] deleteTeamMessageWithId:message.messageId success:^(BOOL success) {
        [DataManager deleteTeamMessageWithId:message.messageId];
        weakSelf.fetchedResultsController = nil;
        [weakSelf.contentTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        //Used for trigger update of Activity Feed, when open.
        [AppDelegate theApp].mainViewController.isAddedNewTap = YES;
    } failure:^(NSString *message) {
        [weakSelf showAlertWithMessage:message];
    }];
    
}

- (NSArray *) deleteButton {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor redColor] icon:[UIImage imageNamed:@"icon-delete"]];
    
    return rightUtilityButtons;
}

#pragma mark - Alerts

- (void) showDeleteAlertForIndexPath:(NSIndexPath *) indexPath {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to delete team message?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self deleteTeamMessageAtIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Update UI

- (void) hideButtonsForIndexPath: (NSIndexPath *) indexPath {
    
    TeamMessageListCell *cell = [self.contentTableView cellForRowAtIndexPath:indexPath];
    [cell hideUtilityButtonsAnimated:YES];
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.contentTableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadData];
}

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) message {
    
    if (message.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:nil text:message];
}

@end
