//
//  CommentCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 11.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "CommentCell.h"
#import "NSDate+TimeAgo.h"


@implementation CommentCell

+(UIFont *) contentFont {
    return [UIFont appFontThin:17];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeRoundedView:self.senderAvatar];
    //[self.senderAvatar makeCircle];
    
    self.senderNameLabel.textColor = [UIColor appTextColor];
    self.senderNameLabel.font = [UIFont appFont:17];
    
    self.dateLabel.textColor = [UIColor appTextColor];
    self.dateLabel.font = [UIFont appFont:14];
    
    self.contentLabel.textColor = [UIColor appTextColor];
    self.contentLabel.font = [CommentCell contentFont];
    
    self.showMoreLabel.text = NSLocalizedString(@"show more", @"v1.0");
    self.showMoreLabel.textColor = [UIColor appTextColor];
    self.showMoreLabel.font = [UIFont appFont:14];
    self.showMoreLabel.hidden = YES;
}

-(void)prepareForReuse {
    [super prepareForReuse];
    
    self.showMoreLabel.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) addComment:(Comment *) comment fullInfo:(BOOL) fullInfo {
    self.contentLabel.text = comment.body;
    
    [self.senderAvatar sd_setImageWithURL:[AppTemplate mediumIconURL:comment.senderAvatarUrl] placeholderImage:[TeamUser placeholder]];
    self.senderNameLabel.text = comment.senderName;
    
    self.dateLabel.text = [comment.createDate formattedAsTimeAgo];
    
    
    self.showMoreLabel.hidden = fullInfo;
}

-(void) addSuggestion:(Suggestion *) suggestion fullInfo:(BOOL) fullInfo {
    self.contentLabel.text = suggestion.body;
    self.dateLabel.text = [suggestion.createDate formattedAsTimeAgo];
    
    if (suggestion.senderId) {
        [self.senderAvatar sd_setImageWithURL:[AppTemplate mediumIconURL:suggestion.senderAvatarUrl] placeholderImage:[TeamUser placeholder]];
        self.senderNameLabel.text = suggestion.senderName;
    } else {
        [self.senderAvatar setImage:[UIImage imageNamed:@"icon-userAnonymous"]];
        self.senderNameLabel.text = NSLocalizedString(@"Incognito", @"v1.0");
    }
    
    self.showMoreLabel.hidden = fullInfo;
}



+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {

    CGFloat minHeight = 75;
    
    /*
     CGRect rect = [contentText boundingRectWithSize:CGSizeMake(tableWidth - 70 - 15, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [CommentCell contentFont]} context:nil];
     */
    
    //bad font, use this way for calculate text height
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [CommentCell contentFont];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 70 - 15, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    
    height += 36;//sender text
    height += 15;//text padding
    height += 1;//separator height
    
    if (height < minHeight) height = minHeight;
    
    return height;
}

- (IBAction)senderIconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowSender) {
        weakSelf.onShowSender (weakSelf);
    }
}

@end
