//
//  ValueDescriptionCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 10/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ValueDescriptionCell.h"

@implementation ValueDescriptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.descriptionLabel.textColor = [UIColor appTextColor];
    self.descriptionLabel.font = [UIFont appFont:17];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {
    
    CGFloat minHeight = 50;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFont:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 15 - 15, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    height += 10; //Top constraint
    height += 10; //Bottom constraint
    height += 10;
    
    if (height < minHeight) {
        height = minHeight;
    }
    
    return height;
}

@end
