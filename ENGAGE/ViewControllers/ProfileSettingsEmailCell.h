//
//  ProfileSettingsEmailCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+Separator.h"

@interface ProfileSettingsEmailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *rightUnconfirmedEmail;
@property (weak, nonatomic) IBOutlet UILabel *leftUnconfirmedEmail;
@property (weak, nonatomic) IBOutlet UILabel *leftTitle;
@property (weak, nonatomic) IBOutlet UITextField *rightTextField;


- (void) needShowUnconfirmedEmail:(BOOL) show;

@end
