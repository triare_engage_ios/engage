//
//  ValueCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"

@interface ValueCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel1;
@property (weak, nonatomic) IBOutlet ENRoundedImageView *imageView1;
@property (nonatomic, strong) NSNumber *behaviorId1;
@property (weak, nonatomic) IBOutlet UIButton *button1;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel2;
@property (weak, nonatomic) IBOutlet ENRoundedImageView *imageView2;
@property (nonatomic, strong) NSNumber *behaviorId2;
@property (weak, nonatomic) IBOutlet UIButton *button2;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel3;
@property (weak, nonatomic) IBOutlet ENRoundedImageView *imageView3;
@property (nonatomic, strong) NSNumber *behaviorId3;
@property (weak, nonatomic) IBOutlet UIButton *button3;


@property (nonatomic, copy) void (^onAction)(NSNumber *behaviorId);

-(void) addBehaviors:(NSArray *) behaviors;

@end
