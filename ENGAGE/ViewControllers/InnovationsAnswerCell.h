//
//  InnovationsAnswerCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    InnovationsAnswerCellStyleStart,
    InnovationsAnswerCellStyleStop,
    InnovationsAnswerCellStyleNeed
} InnovationsAnswerCellStyle;

@interface InnovationsAnswerCell : UITableViewCell
@property (nonatomic, readonly) InnovationsAnswerCellStyle cellStyle;
@property (nonatomic, copy) void (^onChangeText)(NSString *text, InnovationsAnswerCell *cell);
@property (nonatomic, copy) void (^onDidBeginChangingText)(InnovationsAnswerCell *cell);

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

-(void) applyCellStyle:(InnovationsAnswerCellStyle) style;

@end
