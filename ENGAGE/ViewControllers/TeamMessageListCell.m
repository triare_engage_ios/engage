//
//  TeamMessageListCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/13/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "TeamMessageListCell.h"
#import "NSDate+TimeAgo.h"

@implementation TeamMessageListCell

+(UIFont *) contentFont {
    return [UIFont appFontThin:17];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.dateLabel.textColor = [UIColor appTextColor];
    self.dateLabel.font = [UIFont appFont:14];
    
    self.contentLabel.textColor = [UIColor appTextColor];
    self.contentLabel.font = [TeamMessageListCell contentFont];
    
    self.showMoreLabel.text = NSLocalizedString(@"show more", @"v1.0");
    self.showMoreLabel.textColor = [UIColor appTextColor];
    self.showMoreLabel.font = [UIFont appFont:14];
    self.showMoreLabel.hidden = YES;
}

-(void) addTeamMessage:(TeamMessage *) message fullInfo:(BOOL) fullInfo {
    self.contentLabel.text = message.body;
    
    self.dateLabel.text = [message.createDate formattedAsTimeAgo];
    
    self.showMoreLabel.hidden = fullInfo;
}

-(void)prepareForReuse {
    [super prepareForReuse];
    
    self.showMoreLabel.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {
    
    CGFloat minHeight = 75;
    
    /*
     CGRect rect = [contentText boundingRectWithSize:CGSizeMake(tableWidth - 70 - 15, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [CommentCell contentFont]} context:nil];
     */
    
    //bad font, use this way for calculate text height
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [TeamMessageListCell contentFont];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 15 - 70 - 15 - 10, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    
    height += 15; //top constraint
    height += 15; //bottom constraint
    
    if (height < minHeight) height = minHeight;
    
    return height;
}

@end
