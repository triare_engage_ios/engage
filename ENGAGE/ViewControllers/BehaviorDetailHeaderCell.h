//
//  BehaviorDetailHeaderCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"

@interface BehaviorDetailHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENRoundedImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth;

@end
