//
//  LeaderBoardBehaviorListVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface LeaderBoardBehaviorListVC : ContentViewController
@property (nonatomic, strong) NSNumber *behaviorId;
@property (nonatomic, strong) NSString *behaviorTitle;
@property (nonatomic, strong) NSString *iconUrl;

@end
