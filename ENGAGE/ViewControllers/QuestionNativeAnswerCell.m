//
//  QuestionNativeAnswerCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionNativeAnswerCell.h"

@implementation QuestionNativeAnswerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:17];
    
    self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle"];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    [super prepareForReuse];

    self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle"];
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {
    CGFloat minHeight = 50;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFont:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 25 - 31 - 15, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    if (height < minHeight) {
        height = minHeight;
    }
    
    return height;
}

- (void) changeCheckmarkToSelected:(BOOL)state {
    if (state) {
        self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle-selected"];
    }
    else {
        self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle"];
    }
}

@end
