//
//  ActivityFeedVC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationBar.h"

@interface ActivityFeedVC : ContentViewController

-(void) loadFeedData;

-(NavigationBar *) navigationBarView;

@end
