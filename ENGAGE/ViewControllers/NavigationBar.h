//
//  NavigationBar.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NavigationBarView.h"


@class NavigationBar;
@protocol NavigationBarSearchDelegate <NSObject>
@optional
- (void)navigationBarDidCancelSearch:(NavigationBar *)barView;
- (void)navigationBarDidStartSearch:(NavigationBar *)barView;
- (void)searchViewDidCancel:(UISearchBar *)view;
- (void)searchViewSearchButtonClicked:(UISearchBar *)view;
- (void)searchViewDidBeginEditing:(UISearchBar *)view;
- (void)searchViewDidEndEditing:(UISearchBar *)view;
- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText;
@end


@interface NavigationBar : UIView

@property (nonatomic, weak) id <NavigationBarSearchDelegate> searchDelegate;

@property (nonatomic, strong) NavigationBarView *barView;
@property (nonatomic) BOOL isCenterButtonTapped;

@property (nonatomic, copy) void (^onLeftAction)();
@property (nonatomic, copy) void (^onRightAction)();
@property (nonatomic, copy) void (^onCenterAction)();

- (void)setTitle:(NSString *)title;

-(void) showLeftButton;
-(void) changeLeftButtonIcon:(UIImage *) image;

-(void) showRightButton;
-(void) hideRightButton;
-(void) changeRightButtonIcon:(UIImage *) image;

-(void) rightButtonEnable:(BOOL) enable;
-(void) setRightButtonText:(NSString *) text;

-(void) addCenterTitleIcon:(UIImage *) image; //Add image after add title (setTitle:)
-(void) centerInteractionEnable:(BOOL) enable;

@end
