//
//  BehaviorListHeaderCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"

@interface BehaviorListHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENRoundedImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
