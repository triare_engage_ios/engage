//
//  BehaviorIconsListVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 10/6/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface BehaviorIconsListVC : ContentViewController
@property (nonatomic, copy) void (^onAction)(BehaviorIcon *behaviorIcon);

@end
