//
//  ProfileSettingsJobTitleCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileSettingsJobTitleCell.h"

@interface ProfileSettingsJobTitleCell ()

@end

@implementation ProfileSettingsJobTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.leftLabel.textColor = [UIColor appTextColor];
    self.leftLabel.font = [UIFont appFont:17];
    
    self.rightLabel.textColor = [UIColor appTextFieldColor];
    self.rightLabel.font = [UIFont appFont:17];
}

- (void) prepareForReuse {
    self.leftLabel.text = nil;
    self.rightLabel.text = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
