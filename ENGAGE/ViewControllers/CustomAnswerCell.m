//
//  CustomAnswerCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "CustomAnswerCell.h"

@implementation CustomAnswerCell

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:17];
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {
    CGFloat minHeight = 50;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFont:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    height += 10; //From title label to top;
    height += 10; //From title label to bottom
    
    if (minHeight > height) {
        height = minHeight;
    }
    
    return height;
}

@end
