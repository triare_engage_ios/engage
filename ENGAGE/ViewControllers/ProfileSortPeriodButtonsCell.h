//
//  ProfileSortPeriodButtonsCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 17.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileSortPeriodButtonsCell : UITableViewCell

@property (nonatomic, copy) void (^onChangeSort)(NSInteger buttonIndex);

@property (weak, nonatomic) IBOutlet UIButton *weekButton;
@property (weak, nonatomic) IBOutlet UIButton *monthButton;
@property (weak, nonatomic) IBOutlet UIButton *lifetimeButton;

-(void) selectButtonAtIndex:(NSInteger) index;

@end
