//
//  ProfileCurrentUserEvaluationVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/2/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileCurrentUserEvaluationVC.h"
#import "ProfileCurrentUserEvaluationCell.h"
#import "GiveTapStepMessageVC.h"
#import "UITableViewCell+Separator.h"
#import "LoadingCell.h"

#import "RequestManager.h"
#import "DataManager.h"
#import "BehaviorDetailVC.h"

#define kDefaultHeaderViewHeight 22.0

@interface ProfileCurrentUserEvaluationVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *rightHeaderLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftHeaderLabel;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation ProfileCurrentUserEvaluationVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupBoundsOfTableView];
    [self setupTableView];
    [self setupLabels];
    [self setupHeaderView];
    [self loadCurrentUserEvaluation];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self trackScreen];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:NSLocalizedString(@"EVALUATIONS", @"v1.0")];
}

- (void) setupBoundsOfTableView {
    CGRect headerViewBounds = CGRectMake(0, 0, SCREEN_WIDTH, kDefaultHeaderViewHeight);
    self.headerView.bounds = headerViewBounds;
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
    
    UIEdgeInsets insets = self.contentTableView.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT + self.headerView.bounds.size.height;
    insets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.contentTableView.contentInset = insets;
}

- (void) setupHeaderView {
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.headerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.headerView addSubview:blurEffectView];
    [self.headerView sendSubviewToBack:blurEffectView];
}

- (void) setupLabels {
    self.rightHeaderLabel.textColor = [UIColor appTextColor];
    self.rightHeaderLabel.font = [UIFont appFontBold:13];
    self.rightHeaderLabel.text = NSLocalizedString(@"NEED TO WORK ON", @"v1.0");
    
    self.leftHeaderLabel.textColor = [UIColor appTextColor];
    self.leftHeaderLabel.font = [UIFont appFontBold:13];
    self.leftHeaderLabel.text = NSLocalizedString(@"BEST AT", @"v1.0");
}

- (void) loadCurrentUserEvaluation {
    [self showLoadingViewBellowView:self.navigationBar];
    __weak typeof(self) weakSelf = self;
    [[RequestManager sharedManager] getEvaluationsForUser:self.userId complete:^(BOOL success, NSArray *list, NSString *errorMessage) {
        if (success) {
            [DataManager saveCurrentUserEvaluationList:list];
            [weakSelf reloadData];
        }
        else {
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
        }
        [weakSelf hideLoadingView];
    }];
    
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.contentTableView reloadData];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [BehaviorTeamUserEvaluations MR_requestAllSortedBy:@"goodEvaluationsCount" ascending:NO inContext:[DataManager context]];
//    NSFetchRequest *fetchRequest = [BehaviorTeamUserEvaluations MR_requestAllSortedBy:@"progress" ascending:NO inContext:[DataManager context]];
//    fetchRequest.includesSubentities = NO;
//    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"totalEvaluationsCount" ascending:NO],
//                                     [NSSortDescriptor sortDescriptorWithKey:@"progress" ascending:NO]];
    
    NSFetchedResultsController *fetchController = [BehaviorTeamUserEvaluations MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Action

-(void) showBehaviorDetailForItemAtIndex:(NSIndexPath *) indexPath {
    BehaviorTeamUserEvaluations *evaluation = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    Behavior *item = [DataManager behaviorByID:evaluation.behaviorId];
    
    BehaviorDetailVC * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorDetailVC"];
    vc.item = item;
    vc.onHideAction = nil;
    
    [self showViewController:vc sender:nil];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        //__weak typeof(self) weakSelf = self;
        
        BehaviorTeamUserEvaluations *evaluation = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        ProfileCurrentUserEvaluationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"currentUserEvaluateCell"];
        [cell.evaluateImageView sd_setImageWithURL:[AppTemplate mediumIconURL:evaluation.iconUrl] placeholderImage:[BehaviorTeamUserEvaluations placeholder]];
        cell.evaluateTitleLabel.text = evaluation.title;
        cell.goodCountLabel.text = [evaluation.goodEvaluationsCount stringValue];
        cell.badCountLabel.text = [evaluation.badEvaluationsCount stringValue];
        [cell showSeparatorLine];
        
        return cell;
    } else {
        return [LoadingCell cell];
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self showBehaviorDetailForItemAtIndex:indexPath];
}


#pragma mark - Navigation

-(void) closeVC {
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

- (void) trackScreen {
    
    if (self.userId.integerValue == [DataManager user].userId.integerValue) {
        [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserEvaluateList];
        [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserEvaluateList withScreenClass:NSStringFromClass([self class])];
    }
    else {
        [AnalyticsManager trackScreen:ENAnalyticScreenProfileEvaluateList];
        [AnalyticsManager trackScreen:ENAnalyticScreenProfileEvaluateList withScreenClass:NSStringFromClass([self class])];
    }
    
}

@end
