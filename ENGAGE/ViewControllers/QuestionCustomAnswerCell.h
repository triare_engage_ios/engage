//
//  QuestionCustomAnswerCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionCustomAnswerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImageView;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (nonatomic, copy) void (^onChangeText)(NSString *text, QuestionCustomAnswerCell *cell);

- (void) changeCheckmarkToSelected:(BOOL)state;

@end
