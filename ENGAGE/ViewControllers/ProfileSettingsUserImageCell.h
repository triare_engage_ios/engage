//
//  ProfileSettingsUserImageCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedView.h"
#import "UITableViewCell+Separator.h"

@interface ProfileSettingsUserImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

@end
