//
//  ProfileSettingsJobTitleVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/7/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileSettingsJobTitleVC.h"
#import "CustomJobTitleCell.h"
#import "PredefinedJobTitleCell.h"

#import "AppDelegate.h"

#import "UITableViewCell+Separator.h"
#import "UIViewController+Keyboard.h"

#import <SVProgressHUD/SVProgressHUD.h>

@interface JobTitle : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *jobId;

@end

@implementation JobTitle

@end


@interface ProfileSettingsJobTitleVC () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonatomic, strong) NSArray *jobsList;
@property (nonatomic, strong) UITextField *customJobTextTextField;

@end

@implementation ProfileSettingsJobTitleVC

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self setupTextField];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self reloadJobTitleList];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserJobtitleList];
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserJobtitleList withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    
    self.navigationBar.onRightAction = ^{
        [weakSelf saveChanges];
    };
    
    [self.navigationBar setTitle:NSLocalizedString(@"CHOOSE JOB TITLE", @"v1.0")];
    
    [self.navigationBar.barView.rightButton setTitle:[NSString stringWithFormat:@"%@ ", NSLocalizedString(@"Save", @"v1.0")] forState:UIControlStateNormal];
    
    //Prepare right button for text display
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    self.navigationBar.barView.rightButton.titleLabel.font = [UIFont appFont:17];
    [self.navigationBar.barView.rightButton setBackgroundColor:[UIColor clearColor]];
    [self.navigationBar.barView.rightButton setImage:nil forState:UIControlStateNormal];
    //[self.navigationBar showRightButton];
    //[self.navigationBar.barView.rightButton setEnabled:NO];
    [self.navigationBar hideRightButton];
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupTextField {
    self.customJobTextTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
}

#pragma mark - Load Data

-(void) reloadJobTitleList {
    __weak typeof(self) weakSelf = self;
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] getCompanyJobsOnSuccess:^(NSArray *list) {
        NSMutableArray *jobsList = [NSMutableArray array];
        for (NSDictionary *jobTitleInfo in list) {
            JobTitle *job = [[JobTitle alloc] init];
            job.jobId = [jobTitleInfo valueForKey:@"id"];
            job.title = [jobTitleInfo valueForKey:@"title"];
            [jobsList addObject:job];
        }
        weakSelf.jobsList = jobsList;
        [weakSelf reloadData];
        
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
    }];
}

- (void) reloadData {
    [self.contentTableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) saveChanges {
    
    CustomJobTitleCell *cell = [self.contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    self.customJobTextTextField = cell.jobTextField;
    
    if (self.customJobTextTextField.text.length == 0) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Job Title", @"v1.0") text:NSLocalizedString(@"Job Title field cannot be empty", @"v1.0")];
        //[self.navigationBar rightButtonEnable:NO];
        [self.navigationBar hideRightButton];
        return;
    }
    
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if ([self.customJobTextTextField.text stringByTrimmingCharactersInSet:set].length == 0) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Job Title", @"v1.0") text:NSLocalizedString(@"Job Title field cannot consist only space", @"v1.0")];
        //[self.navigationBar rightButtonEnable:NO];
        [self.navigationBar hideRightButton];
        return;
    }
    
    [self updateCustomJobTitle:self.customJobTextTextField.text];
    
    MLog(@"Save changes");
}

#pragma mark - Action
- (void) updateCustomJobTitle:(NSString *) jobTitle {
    
    NSDictionary *attributes = @{@"title":jobTitle};
    
    __weak typeof(self) weakSelf = self;
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] updateUserWithInfo:@{@"job_attributes":attributes}success:^(BOOL success, NSDictionary *userInfo) {
        [weakSelf hideLoadingView];
        [weakSelf.view endEditing:YES];
        //[weakSelf.navigationBar rightButtonEnable:NO];
        [self.navigationBar hideRightButton];
        [weakSelf showSentAlert];
        [DataManager updateSomeUserInfo:@{@"job_title":jobTitle}];
        if (weakSelf.onBackAction) {
            weakSelf.onBackAction (jobTitle);
        }
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self closeVC];
//        });
        [weakSelf closeVC];
    } failure:^(NSString *message) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Job Title update", @"v1.0") text:message];
        [weakSelf hideLoadingView];
        [weakSelf.view endEditing:YES];
    }];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.jobsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (indexPath.row == 0) {
        return [self customJobTitleCell:tableView indexPath:indexPath];
    }
    
    return [self predefinedJobTitleCell:tableView indexPath:indexPath];
}

-(CustomJobTitleCell *) customJobTitleCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    CustomJobTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"customJobTitle"];
    cell.jobTextField.delegate = self;
    [cell showSeparatorLine];
    
    return cell;
}

-(PredefinedJobTitleCell *) predefinedJobTitleCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    JobTitle *job = [self.jobsList objectAtIndex:indexPath.row - 1];
    
    PredefinedJobTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:@"predefinedJobTitle"];
    cell.numberLabel.text = [NSString stringWithFormat:@"%d", (int)indexPath.row];
    cell.centerLabel.text = job.title;
    [cell showSeparatorLine];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == 0) {
        return;
    }
    
    JobTitle *job = [self.jobsList objectAtIndex:indexPath.row - 1];
    NSNumber *jobId = job.jobId;
    NSString *jobTitle = job.title;
    
    [self showLoadingViewBellowView:self.navigationBar];
    __weak typeof(self) weakSelf = self;
    [[RequestManager sharedManager] updateUserWithInfo:@{@"job_id":jobId} success:^(BOOL success, NSDictionary *userInfo) {
        [weakSelf hideLoadingView];
        [weakSelf.view endEditing:YES];
        //[weakSelf.navigationBar rightButtonEnable:NO];
        [self.navigationBar hideRightButton];
        [weakSelf showSentAlert];
        [DataManager updateSomeUserInfo:@{@"job_title":jobTitle}];
            if (weakSelf.onBackAction) {
                weakSelf.onBackAction (jobTitle);
            }
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [self closeVC];
//        });
        [weakSelf closeVC];
    } failure:^(NSString *message) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Job Title update", @"v1.0") text:message];
        [weakSelf hideLoadingView];
        [weakSelf.view endEditing:YES];
    }];
    
    MLog(@"Select JobTitle: %@", job.title);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //New textfield string
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textField.text.length == 0 && [proposedNewString stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    if (![self isCharacterText:string]) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Job Title", @"v1.0") text:NSLocalizedString(@"This field accepts only characters, space and hyphen", @"v1.0")];
        return NO;
    }
    
    if (proposedNewString.length < 2) {
        //[self.navigationBar rightButtonEnable:NO];
        [self.navigationBar hideRightButton];
    }
    else {
        //[self.navigationBar rightButtonEnable:YES];
        [self.navigationBar showRightButton];
    }
    
    return YES;
}

//Check is text is character, space and "-"
- (BOOL) isCharacterText:(NSString *) text {
    
    BOOL isLetter = YES;
    
    NSMutableCharacterSet *symbols = [NSMutableCharacterSet letterCharacterSet];
    [symbols addCharactersInString:@" -"];
    
    if ([text rangeOfCharacterFromSet:[symbols invertedSet]].location != NSNotFound) {
        isLetter = NO;
    }
    
    return isLetter;
}



#pragma mark - Helpers Methods

-(void) showSentAlert {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Job title has been updated", @"v1.0")];
}



@end
