//
//  QuestionDateCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionDateCell.h"

@implementation QuestionDateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:17];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.titleLabel.text = nil;
    self.onAction = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)buttonAction:(id)sender {
    if (self.onAction) {
        self.onAction();
    }
}

@end
