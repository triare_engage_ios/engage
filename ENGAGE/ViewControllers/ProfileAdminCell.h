//
//  ProfileAdminCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 23.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENStokeLightButton.h"
#import "BadgeLabel.h"

@interface ProfileAdminCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENStokeLightButton *suggestionButton;
@property (weak, nonatomic) IBOutlet BadgeLabel *suggestionBadge;
@property (weak, nonatomic) IBOutlet ENStokeLightButton *questionsButton;
@property (weak, nonatomic) IBOutlet ENStokeLightButton *peopleButton;
@property (weak, nonatomic) IBOutlet ENStokeLightButton *privatButton;

@property (nonatomic, copy) void (^onAction)(NSInteger index);

-(void) setNewSuggestionCount:(NSInteger) count;

@end
