//
//  GiveTapStepMessageVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 06.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "GiveTapStepMessageVC.h"
#import "NavigationBar.h"
#import "ENButton.h"
#import "GiveTapNavController.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface GiveTapStepMessageVC () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *textViewBackground;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet ENButton *sendAndMailButton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewBottomSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sendButtonLeftSpace;

@end

@implementation GiveTapStepMessageVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.registerKeyboardNotifications = YES;

    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        if (weakSelf.navigationController) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
        else {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }
    };
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:NSLocalizedString(@"STEP 3", @"v1.0")];
    
    self.infoLabel.text = NSLocalizedString(@"Add your comment", @"v1.0");
    self.infoLabel.textColor = [UIColor appTextColor];
    self.infoLabel.font = [UIFont appFontThin:18];
    
    self.textView.font = [UIFont appFontThin:17];
    self.textView.textColor = [UIColor blackColor];
        
    self.textViewBackground.image = [[UIImage imageNamed:@"message-textBackground.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [self.sendButton setTitle:NSLocalizedString(@"SEND", @"v1.0") forState:UIControlStateNormal];
    //self.sendButton.titleLabel.font = [UIFont appFontBold:12];
    [self.sendAndMailButton setTitle:NSLocalizedString(@"SEND INCOGNITO", @"v1.0") forState:UIControlStateNormal];
    self.sendAndMailButton.titleLabel.font = [UIFont appFontBold:12];
    self.sendAndMailButton.style = ENButtonStyleWhite;
    
    self.textView.delegate = self;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    if (self.isSuggestionMessage) {
        [self.navigationBar setTitle:NSLocalizedString(@"SUGGESTION", @"v1.0")];
        self.infoLabel.text = NSLocalizedString(@"Give your suggestion to admin", @"v1.0");
        
        self.navigationBar.onLeftAction = ^{
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        };
        
        self.sendButtonLeftSpace.constant = self.view.frame.size.width - 140;
        self.sendAndMailButton.hidden = NO;
        [self.sendAndMailButton needHideImage:YES];
    }
    
    if (self.isTeamMessage) {
        [self.navigationBar setTitle:NSLocalizedString(@"TEAM MESSAGE", @"v1.0")];
        self.infoLabel.text = NSLocalizedString(@"add your message", @"v1.0");
        
        self.navigationBar.onLeftAction = ^{
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        };
        
        [self.sendAndMailButton needHideImage:YES];
        
        self.sendButtonLeftSpace.constant = self.view.frame.size.width - 140;
        self.sendAndMailButton.hidden = NO;
        
        [self.sendAndMailButton setTitle:NSLocalizedString(@"SEND IN APP AND EMAIL", @"v1.0") forState:UIControlStateNormal];
        
        self.sendAndMailButton.titleLabel.font = [UIFont appFontBold:12];
        self.sendButton.titleLabel.font = [UIFont appFontBold:12];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.textView becomeFirstResponder];
    [self trackScreen];
}

- (IBAction)sendAction:(id)sender {
    
    //Validation for text length
    if (self.textView.text.length == 0) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add text", @"v1.0")];
        return;
    }
    
    if (self.isSuggestionMessage) {
        [self sendSuggestion:NO];
    }
    else if (self.isTeamMessage) {
        [self sendTeamMessageWithEmail:NO];
    }
    else {
        [self sendTap:NO];
    }
}

- (IBAction)sendWithMailAction:(id)sender {
    
    //Validation for text length
    if (self.textView.text.length == 0) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add text", @"v1.0")];
        return;
    }
    
    if (self.isSuggestionMessage) {
        [self sendSuggestion:YES];
    }
    else if (self.isTeamMessage) {
        [self sendTeamMessageWithEmail:YES];
    }
    else {
        [self sendTap:YES];
    }
}

-(void) showSentAlert {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Tap has been sent", @"v1.0")];
}

-(void) showSentSuggestionAlert {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Successfully sent", @"v1.0")];
}

-(void) showTeamMessageAlert {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Message has been sent", @"v1.0")];
}

-(void) sendTap:(BOOL) anonymous {
    
    __weak typeof(self) weakSelf = self;
    
    GiveTapNavController *navController = (GiveTapNavController *) self.navigationController;
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setValue:self.textView.text forKey:@"body"];
    [info setValue:navController.user.userId forKey:@"receiver_id"];
    [info setValue:navController.behavior.behaviorId forKey:@"behaviour_id"];
    if (anonymous) {
        [info setValue:@YES forKey:@"anonymous"];
    }
    
    [self showLoadingViewBellowView:nil style:LoadingViewStyleCoverContent];
    
    [[RequestManager sharedManager] giveTap:info onCompleate:^(BOOL success, NSString *errorMessage) {
        if (success) {
            
            [weakSelf.textView resignFirstResponder];
            
            [AppDelegate theApp].mainViewController.isAddedNewTap = YES;
            [weakSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
            
            [weakSelf showSentAlert];
        } else if (errorMessage) {
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
        }
        [weakSelf hideLoadingView];
    }];
}


-(void) sendSuggestion:(BOOL) anonymous {

    __weak typeof(self) weakSelf = self;
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    [info setValue:self.textView.text forKey:@"body"];
    if (anonymous) {
        [info setValue:@YES forKey:@"anonymous"];
    }
    
    [self showLoadingViewBellowView:nil style:LoadingViewStyleCoverContent];
    
    [[RequestManager sharedManager] sendSuggestion:info onCompleate:^(BOOL success, NSString *errorMessage) {
        if (success) {
            
            [weakSelf.textView resignFirstResponder];
            
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
            
            [weakSelf showSentSuggestionAlert];
        } else if (errorMessage) {
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
        }
        [weakSelf hideLoadingView];
    }];
}

- (void) sendTeamMessageWithEmail:(BOOL)emailState {
    
    __weak typeof(self) weakSelf = self;
    
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if ([self.textView.text stringByTrimmingCharactersInSet:set].length == 0) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Message", @"v1.0") text:NSLocalizedString(@"Message cannot consist only space", @"v1.0")];
        [self.navigationBar rightButtonEnable:NO];
        return;
    }
    
    [self showLoadingViewBellowView:nil style:LoadingViewStyleCoverContent];
    [[RequestManager sharedManager] sendTeamMessage:self.textView.text withEmail:emailState success:^(BOOL success) {
        [weakSelf.textView resignFirstResponder];
        [weakSelf hideLoadingView];
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
        [weakSelf showTeamMessageAlert];
        
    } failure:^(NSString *message) {
        [[AppDelegate theApp] showAlert:nil text:message];
        [weakSelf hideLoadingView];
    }];
}

#pragma mark - Text View Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //New textview string
    NSString * newString = [[textView text] stringByReplacingCharactersInRange:range withString:text];
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textView.text.length == 0 && [newString stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    return YES;
}



#pragma mark Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    CGRect keyboardFrame = [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.buttonsViewBottomSpace.constant = keyboardFrame.size.height;
    [self.view setNeedsLayout];
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    self.buttonsViewBottomSpace.constant = 0;
    [self.view setNeedsLayout];
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

#pragma mark - Helpers Methods

- (void) trackScreen {
    
    NSString *screenName = @"";
    
    if (self.isTeamMessage) {
        screenName = ENAnalyticScreenTeamMessageCreate;
    }
    else if (self.isSuggestionMessage) {
        screenName = ENAnalyticScreenTeamMessageCreate;
    }
    else {
        screenName = ENAnalyticScreenGiveATapCreate;
    }
    
    [AnalyticsManager trackScreen:screenName];
    [AnalyticsManager trackScreen:screenName withScreenClass:NSStringFromClass([self class])];
    
    
}



@end
