//
//  QuestionCustomAnswerCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionCustomAnswerCell.h"

@interface QuestionCustomAnswerCell () <UITextFieldDelegate>

@end

@implementation QuestionCustomAnswerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.textField.delegate = self;
    self.textField.textColor = [UIColor appTextColor];
    self.textField.font = [UIFont appFont:17];
    
    self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle"];
    
    //UIColor *color = [UIColor appGrayColor];
    UIColor *color = [UIColor colorWithRed:204/255. green:204/255. blue:204/255. alpha:1];
    UIFont *font = [UIFont appFont:17];
    
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Write your answer", @"v1.0") attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
    
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    __weak typeof(self) weakSelf = self;
    
    //New textfield string
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textField.text.length == 0 && [proposedNewString stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    //send text changes
    if (weakSelf.onChangeText) {
        weakSelf.onChangeText (proposedNewString, weakSelf);
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

- (void) changeCheckmarkToSelected:(BOOL)state {
    if (state) {
        self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle-selected"];
    }
    else {
        self.checkmarkImageView.image = [UIImage imageNamed:@"icon-checkmark-circle"];
    }
}

@end
