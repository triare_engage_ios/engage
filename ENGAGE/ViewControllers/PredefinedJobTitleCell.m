//
//  PredefinedJobTitleCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/7/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "PredefinedJobTitleCell.h"

@implementation PredefinedJobTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.centerLabel.textColor = [UIColor appTextColor];
    self.centerLabel.font = [UIFont appFont:17];
    
    self.numberLabel.textColor = [UIColor appTextColor];
    self.numberLabel.font = [UIFont appFontBold:22];

}

- (void) prepareForReuse {
    
    self.centerLabel.text = nil;
    self.numberLabel.text = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
