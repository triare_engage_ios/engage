//
//  ProfileManagePeopleVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileManagePeopleVC.h"
#import "ENStokeLightButton.h"

#import "UITableViewCell+Separator.h"
#import "ProfileManagePeopleCell.h"

#import "ProfileVC.h"

#import <SVProgressHUD/SVProgressHUD.h>


@interface ProfileManagePeopleVC () <UITextFieldDelegate, NavigationBarSearchDelegate, UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

//Header
@property (weak, nonatomic) IBOutlet ENStokeLightButton *actionHeaderButton;
@property (weak, nonatomic) IBOutlet UITextField *headerTextField;
@property (weak, nonatomic) IBOutlet UIView *separatorHeaderView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSString *searchText;

@end

@implementation ProfileManagePeopleVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self setupHeaderView];
    [self loadData];
    [self addDownRefresh];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenManagePeople];
    [AnalyticsManager trackScreen:ENAnalyticScreenManagePeople withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:[NSLocalizedString(@"manage people", @"v1.0") uppercaseString]];
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    
    [self.navigationBar showLeftButton];
    [self.navigationBar showRightButton];
    
    self.navigationBar.searchDelegate = self;
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupHeaderView {
    [self.actionHeaderButton setTitle:[NSLocalizedString(@"send", @"v1.0") uppercaseString] forState:UIControlStateNormal];
    self.headerTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Add new user", @"v1.0") attributes:@{NSForegroundColorAttributeName: [UIColor appGrayColor],NSFontAttributeName: [UIFont appFont:15]}];
    self.headerTextField.textColor = [UIColor appTextColor];
    self.headerTextField.font = [UIFont appFont:17];
    self.separatorHeaderView.backgroundColor = [UIColor colorWithWhite:1 alpha:.5];
    self.headerTextField.delegate = self;
    self.activityIndicator.hidden = YES;
}

#pragma mark - Data

- (void) loadData {
    __weak typeof(self) weakSelf = self;
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getTeamUsersListSuccess:^(NSArray *list) {
        [DataManager updateTeamUserList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithMessage:message];
    }];
    
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.contentTableView reloadData];
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [TeamUser MR_requestAllSortedBy:@"fullName" ascending:YES inContext:[DataManager context]];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"isInvationAccepted" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"fullName" ascending:YES]];

    NSMutableArray *predicates = [NSMutableArray array];
    
    if (self.searchText.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"fullName CONTAINS[cd] %@ ", self.searchText];
        [predicates addObject:searchPredicate];
    }
    
    //NSPredicate *currentUserPredicate = [NSPredicate predicateWithFormat:@"NOT (userId CONTAINS %@)", [DataManager user].userId];
        //[predicates addObject:currentUserPredicate];
    NSPredicate *userPredicate = [NSPredicate predicateWithFormat:@"userId != %@", [DataManager user].userId];
    [predicates addObject:userPredicate];
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [TeamUser MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Action

- (IBAction)actionHeaderButtonPressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    //Email validation
    if (![self isValidEmail:self.headerTextField.text]) {
        [self showAlertWithMessage:NSLocalizedString(@"Add valid email", @"v1.0")];
        return;
    }
    
    self.actionHeaderButton.hidden = YES;
    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = NO;
    [[RequestManager sharedManager] inviteTeamUserToCompanyWithEmail:self.headerTextField.text success:^(NSDictionary *userInfo) {
        [DataManager updateTeamUser:[userInfo valueForKey:@"id"] withInfo:userInfo];
        [weakSelf reloadData];
        [weakSelf showMessageWithText:NSLocalizedString(@"User invited", @"v1.0")];
        //[weakSelf showAlertWithMessage:NSLocalizedString(@"User invited!", @"v1.0")];
        weakSelf.headerTextField.text = @"";
        weakSelf.activityIndicator.hidden = YES;
        weakSelf.actionHeaderButton.hidden = NO;
        [weakSelf.activityIndicator stopAnimating];
        [weakSelf.headerTextField resignFirstResponder];
    } failure:^(NSString *message) {
        [weakSelf showAlertWithMessage:message];
        weakSelf.activityIndicator.hidden = YES;
        weakSelf.actionHeaderButton.hidden = NO;
        [weakSelf.activityIndicator stopAnimating];
    }];
    
}

- (void) giveManagerRightsTeamUserForIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [[RequestManager sharedManager] giveManagerRightsToTeamUser:user.userId success:^(NSDictionary *userInfo) {
        [DataManager updateTeamUser:user.userId withInfo:userInfo];
        MLog(@"Give manager rights for user: %@", user.fullName);
        [weakSelf hideButtonsForIndexPath:indexPath];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [weakSelf reloadRowAtIndexPath:indexPath];
        });
    } failure:^(NSString *message) {
        [weakSelf showAlertWithMessage:message];
    }];
}

- (void) removeManagerRightsTeamUserForIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [[RequestManager sharedManager] removeManagerRightsFromTeamUser:user.userId success:^(NSDictionary *userInfo) {
        [DataManager updateTeamUser:user.userId withInfo:userInfo];
        MLog(@"Remove manager rights for user: %@", user.fullName);
        [weakSelf hideButtonsForIndexPath:indexPath];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf reloadRowAtIndexPath:indexPath];
        });
    } failure:^(NSString *message) {
        [weakSelf showAlertWithMessage:message];
        [weakSelf hideButtonsForIndexPath:indexPath];
    }];
    
}

- (void) deleteFromCompanyTeamUserForIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [[RequestManager sharedManager] deleteFromCompanyTeamUser:user.userId success:^(BOOL success) {
        MLog(@"Delete from company: %", user.fullName);
        [DataManager deleteFromCompanyTeamUser:user.userId];
        weakSelf.fetchedResultsController = nil;
        [weakSelf.contentTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    } failure:^(NSString *message) {
        [weakSelf showAlertWithMessage:message];
    }];
    
}

#pragma mark - Update UI

- (void) hideButtonsForIndexPath: (NSIndexPath *) indexPath {
    
    ProfileManagePeopleCell *cell = [self.contentTableView cellForRowAtIndexPath:indexPath];
    [cell hideUtilityButtonsAnimated:YES];
}

- (void) reloadRowAtIndexPath:(NSIndexPath *) indexPath {
    
    self.fetchedResultsController = nil;
    [self.contentTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ProfileManagePeopleCell *cell = (ProfileManagePeopleCell *)[tableView dequeueReusableCellWithIdentifier:@"manageCell"];
    
    cell.positionLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
    
    if (user.isInvationAcceptedValue) {
        [cell.userImageView sd_setImageWithURL:[AppTemplate mediumIconURL:user.avatarUrl] placeholderImage:[TeamUser placeholder]];
        cell.titleLabel.text = user.fullName;
        cell.subtitleLabel.text = user.jobTitle;
    }
    else if ([user isAdmin]) {
        [cell.userImageView sd_setImageWithURL:[AppTemplate mediumIconURL:user.avatarUrl] placeholderImage:[TeamUser placeholder]];
        cell.titleLabel.text = user.fullName;
        cell.subtitleLabel.text = user.jobTitle;
    }
    else {
        [cell.userImageView sd_setImageWithURL:[AppTemplate mediumIconURL:user.avatarUrl] placeholderImage:[TeamUser placeholder]];
        cell.titleLabel.text = user.email;
        cell.subtitleLabel.text = NSLocalizedString(@"User did not accept invite yet", @"v1.0");
    }
    
    if ([user isManager] && [DataManager user].isAdmin) {
        cell.rightUtilityButtons = [self rightButtonsManager];
    }
    else if ([user isAdmin]){
        cell.rightUtilityButtons = nil;
    }
    else if (!user.isInvationAcceptedValue) {
        cell.rightUtilityButtons = [self rightButtonsNotInvited];
    }
    else if ([DataManager user].isManager) {
        cell.rightUtilityButtons = [self rightButtonsNotInvited];
    }
    else {
        cell.rightUtilityButtons = [self rightButtonsNotManager];
    }
    
    cell.delegate = self;
    
    [cell showSeparatorLine];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    
    vc.userId = user.userId;
    vc.profileTitle = user.fullName;
    
    [self showViewController:vc sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.contentTableView indexPathForCell:cell];
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    //Not invited users
    if (!user.isInvationAcceptedValue && ![user isAdmin]) {
        if (index == 0) {
            //[self deleteFromCompanyTeamUserForIndexPath:indexPath];
            [self showDeleteAlertForIndexPath:indexPath];
        }
    }
    //Invited users
    else {
        switch (index) {
            case 0:
            {
                if ([user isManager]) {
                    [self showRemoveManagerRightsAlertForIndexPath:indexPath];
                    //[self removeManagerRightsTeamUserForIndexPath:indexPath];
                }
                else {
                    [self showGiveManagerRightsAlertForIndexPath:indexPath];
                    //[self giveManagerRightsTeamUserForIndexPath:indexPath];
                }
            }
                break;
                
            case 1:
            {
                [self showDeleteAlertForIndexPath:indexPath];
                //[self deleteFromCompanyTeamUserForIndexPath:indexPath];
            }
                
                break;
                
            default:
                break;
        }
    }
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
    
    NSIndexPath *indexPath = [self.contentTableView indexPathForCell:cell];
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([user isAdmin]) {
        return NO;
    }
    else {
        return YES;
    }
}

//Can swipe only one row in time (close if swipe another)
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

- (NSArray *)rightButtonsManager {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor appYellowColor] icon:[UIImage imageNamed:@"icon-starFilled"]];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor redColor] icon:[UIImage imageNamed:@"icon-delete"]];
    
    return rightUtilityButtons;
}

- (NSArray *) rightButtonsNotManager {
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor appYellowColor] icon:[UIImage imageNamed:@"icon-star"]];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor redColor] icon:[UIImage imageNamed:@"icon-delete"]];
    
    return rightUtilityButtons;
}

- (NSArray *) rightButtonsNotInvited {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor redColor] icon:[UIImage imageNamed:@"icon-delete"]];
    
    return rightUtilityButtons;
}

#pragma mark NavigationBarSearchDelegate

-(void) checkSearchPlaceholder {
    self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search among colleagues", @"v1.0");
}

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    self.searchText = nil;
    [self reloadData];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [self checkSearchPlaceholder];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    self.searchText = searchText;
    
    [self reloadData];
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - Navigation

-(void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) message {
    
    if (message.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:nil text:message];
}

- (void) showMessageWithText:(NSString *) text {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:text];
}

- (BOOL)isValidEmail:(NSString *)checkString {
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - Alerts

- (void) showDeleteAlertForIndexPath:(NSIndexPath *) indexPath {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Delete user from your team?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self deleteFromCompanyTeamUserForIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) showRemoveManagerRightsAlertForIndexPath:(NSIndexPath *) indexPath {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to cancel manager rights?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self removeManagerRightsTeamUserForIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) showGiveManagerRightsAlertForIndexPath:(NSIndexPath *) indexPath {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to give manager rights?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self giveManagerRightsTeamUserForIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.contentTableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadData];
}

@end
