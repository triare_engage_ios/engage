//
//  ProfileVC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 16.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVC : ContentViewController

@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *profileTitle;

@property BOOL isPersonalProfile;

@end
