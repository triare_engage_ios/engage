//
//  ActivityFeedPointCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/11/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityFeedPointCell : UITableViewCell

@property (nonatomic, copy) void (^onShowReceiver)(ActivityFeedPointCell *cell);
@property (nonatomic, copy) void (^onShowTopPoints)(ActivityFeedPointCell *cell);

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

-(void) addFeed:(ActivityFeed *) feed;
-(void) fillWithDefaultData;

@end
