//
//  GiveTapStepMessageVC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 06.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiveTapStepMessageVC : ContentViewController

@property BOOL isSuggestionMessage;
@property BOOL isTeamMessage;

@end
