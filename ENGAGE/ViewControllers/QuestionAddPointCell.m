//
//  QuestionAddPointCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionAddPointCell.h"

@implementation QuestionAddPointCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFontBold:17];
    
    self.subtitleLabel.textColor = [UIColor appTextColor];
    self.subtitleLabel.font = [UIFont appFont:10];
    self.subtitleLabel.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    [self.addButton setEnabled:YES];
    self.addButton.hidden = NO;
    
    
    self.onAction = nil;
}

- (IBAction)addButtonAction:(id)sender {
    if (self.onAction) {
        self.onAction();
    }
}

@end
