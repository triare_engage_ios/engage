//
//  CompanyValuesCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/24/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyValuesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
