//
//  ProfileCurrentUserEvaluationVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/2/16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ContentViewController.h"

@interface ProfileCurrentUserEvaluationVC : ContentViewController
@property (nonatomic, strong) NSNumber *userId;

@end
