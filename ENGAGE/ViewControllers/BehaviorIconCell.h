//
//  BehaviorIconCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 10/6/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"

@interface BehaviorIconCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet ENRoundedImageView *iconImageView;

+ (CGSize) sizeForCollectionWidth:(CGFloat) width;

@end
