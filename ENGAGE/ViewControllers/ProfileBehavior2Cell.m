//
//  ProfileBehavior2Cell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 25.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileBehavior2Cell.h"

@implementation ProfileBehavior2Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeAppRoundedView:self.cellImageView1];
    
    self.cellTitleLabel1.font = [UIFont appFont:17];
    self.cellTitleLabel1.textColor = [UIColor appTextColor];
    
    
    //[UIView makeAppRoundedView:self.cellImageView2];
    
    self.cellTitleLabel2.font = [UIFont appFont:17];
    self.cellTitleLabel2.textColor = [UIColor appTextColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setBehaviors:(NSArray *) items {
    UserBehavior *b1 = items.firstObject;
    self.cellTitleLabel1.text = b1.title;
    [self.cellImageView1 sd_setImageWithURL:[AppTemplate bigIconURL:b1.iconUrl] placeholderImage:[Behavior placeholder]];
    self.badgeLabel1.text = [NSString stringWithFormat:@"%@", b1.count];
    
    UserBehavior *b2 = items.lastObject;
    self.cellTitleLabel2.text = b2.title;
    [self.cellImageView2 sd_setImageWithURL:[AppTemplate bigIconURL:b2.iconUrl] placeholderImage:[Behavior placeholder]];
    self.badgeLabel2.text = [NSString stringWithFormat:@"%@", b2.count];
}

- (IBAction)leftIconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowBehavior) {
        weakSelf.onShowBehavior(0);
    }
}

- (IBAction)rightIconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowBehavior) {
        weakSelf.onShowBehavior(1);
    }
}


@end
