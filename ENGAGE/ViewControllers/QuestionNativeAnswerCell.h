//
//  QuestionNativeAnswerCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionNativeAnswerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth;

- (void) changeCheckmarkToSelected:(BOOL)state;

@end
