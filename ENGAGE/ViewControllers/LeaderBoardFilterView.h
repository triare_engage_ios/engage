//
//  LeaderBoardFilterView.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 19.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum : NSUInteger {
    FilterStateNew,
    FilterStateFromSelect,
    FilterStateToSelect,
    FilterStateFiltered
} FilterState;


@class LeaderBoardFilterView;
@protocol LeaderBoardFilterDelegate <NSObject>
- (void)leaderBoardFilteDidApplyFilter:(LeaderBoardFilterView *)view;
- (void)leaderBoardFilteDidCancelFilter:(LeaderBoardFilterView *)view;
- (void)leaderBoardFilteOnSelectDate:(LeaderBoardFilterView *)view;
@end



@interface LeaderBoardFilterView : UIView

@property (nonatomic, weak) id <LeaderBoardFilterDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIButton *applyButton;

@property (strong, nonatomic) NSDate *fromDate;
@property (strong, nonatomic) NSDate *toDate;

@property (weak, nonatomic) IBOutlet UIButton *fromButton;
@property (weak, nonatomic) IBOutlet UIButton *toButton;


@property (nonatomic) FilterState state;

@end
