//
//  InnovationsAnswerVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface InnovationsAnswerVC : ContentViewController

+(NSString *) storyboardIdentifier;

@end
