//
//  GiveTapStep1VC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiveTapStep1VC : ContentViewController

@property (nonatomic) BOOL isUsedForEvaluate;

@end
