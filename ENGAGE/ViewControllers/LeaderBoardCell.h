//
//  LeaderBoardCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 18.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENCircleImageView.h"

@interface LeaderBoardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leadPositionLabel;
@property (weak, nonatomic) IBOutlet ENCircleImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobLabel;

@property (nonatomic, strong) NSNumber *userBorderWidth;
@property (nonatomic, strong) UIColor *userBorderColor;

@end


@interface LeaderBoardMoreCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *moreLabel;

@end
