//
//  QuestionTextViewCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPTextView.h"

@interface QuestionTextViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet MPTextView *textView;

@end
