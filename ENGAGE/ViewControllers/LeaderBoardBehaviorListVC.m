//
//  LeaderBoardBehaviorListVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "LeaderBoardBehaviorListVC.h"
#import "UITableViewCell+Separator.h"

#import "BehaviorListHeaderCell.h"
#import "BehaviorListCell.h"

#import "AKLineMenuView.h"

#import "BehaviorDetailVC.h"
#import "ProfileVC.h"

typedef enum : NSUInteger {
    ListStateTapsRecieved,
    ListStateTapsGiven
} ListState;

@interface LeaderBoardBehaviorListVC () <NavigationBarSearchDelegate, AKLineMenuViewDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;
@property (weak, nonatomic) IBOutlet AKLineMenuView *lineMenu;
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;

@property ListState listState;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation LeaderBoardBehaviorListVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupLineMenu];
    [self setupTableView];
    
    self.listState = ListStateTapsRecieved;
    
    [self loadData];
    [self addDownRefresh];
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self trackScreenForAnalytic:AnalyticsTypeGoogleAnalytics];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self trackScreenForAnalytic:AnalyticsTypeFirebase];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction =^{
        [weakSelf closeVC];
    };
    
    [self.navigationBar setTitle:self.behaviorTitle];
    [self.navigationBar showLeftButton];
    
    self.navigationBar.searchDelegate = self;
}

- (void) setupLineMenu {
    self.lineMenu.delegate = self;
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.lineMenu.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.lineMenu addSubview:blurEffectView];
    [self.lineMenu sendSubviewToBack:blurEffectView];
    
    [self.lineMenu addMenuItems:@[NSLocalizedString(@"TAPS RECEIVED", @"v1.0"), NSLocalizedString(@"TAPS GIVEN", @"v1.0")]];
    
    [self addTableTopInset:self.lineMenu.frame.size.height];
}

-(void) addTableTopInset:(CGFloat) topInset {
    UIEdgeInsets insets = self.contentTableView.contentInset;
    insets.top += topInset;
    self.contentTableView.contentInset = insets;
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

#pragma mark - Data

- (void) loadData {
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getBehaviorUsersListsByBehaviorId:self.behaviorId success:^(NSArray *list) {
        [DataManager saveBehaviorUsersList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Behaviors update", @"v1.0") text:message];
        [weakSelf hideLoadingView];
        [weakSelf.refreshControl endRefreshing];
    }];
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.contentTableView reloadData];
    
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    if (self.listState == ListStateTapsRecieved) {
        _fetchedResultsController = [self tapsReceivedResultsController];
    } else {
        _fetchedResultsController = [self tapsGivenResultsController];
    }
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSFetchedResultsController *) tapsReceivedResultsController {
    
    NSMutableArray *predicates = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [BehaviorUser MR_requestAllSortedBy:@"receivedCount" ascending:NO inContext:[DataManager context]];
    fetchRequest.includesSubentities = NO;
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"receivedCount" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"fullName" ascending:YES]];
    
    //Filter without zero
    NSPredicate *zeroPredicate = [NSPredicate predicateWithFormat:@"receivedCount > %@", @0];
    [predicates addObject:zeroPredicate];
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [BehaviorUser MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    return _fetchedResultsController;
}

- (NSFetchedResultsController *) tapsGivenResultsController {
    
    NSMutableArray *predicates = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [BehaviorUser MR_requestAllSortedBy:@"givenCount" ascending:NO inContext:[DataManager context]];
    fetchRequest.includesSubentities = NO;
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"givenCount" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"fullName" ascending:YES]];
    
    //Filter without zero
    NSPredicate *zeroPredicate = [NSPredicate predicateWithFormat:@"givenCount > %@", @0];
    [predicates addObject:zeroPredicate];
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [BehaviorUser MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    return _fetchedResultsController;
}

#pragma mark - Action 

-(void) showBehaviorDetails {
    
    Behavior *item = [DataManager behaviorByID:self.behaviorId];
    
    BehaviorDetailVC * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorDetailVC"];
    vc.item = item;
    vc.onHideAction = nil;
    
    [self showViewController:vc sender:nil];
}

-(void) showUserProfileForItemAtIndex:(NSIndexPath *) indexPath {
    BehaviorUser *behavior = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row - 1];
    
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    vc.userId = behavior.userId;
    vc.profileTitle = behavior.fullName;
    
    [self showViewController:vc sender:nil];
}


#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return 150;
    }
    
    return 75;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.fetchedResultsController.fetchedObjects.count == 0) {
        MLog(@"No objects");
    }
    
    if (indexPath.row == 0) {
        BehaviorListHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        [cell.headerImageView sd_setImageWithURL:[AppTemplate bigIconURL:self.iconUrl] placeholderImage:[Behavior placeholder]];
        cell.titleLabel.text = self.behaviorTitle;
        [cell showSeparatorLine];
        
        return cell;
    } else {
        
        BehaviorUser *behavior = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row - 1];
        
        BehaviorListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"behaviorCell"];
        cell.leadPositionLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
        [cell.userImageView sd_setImageWithURL:[AppTemplate mediumIconURL:behavior.avatarUrl] placeholderImage:[TeamUser placeholder]];
        
        if (self.listState == ListStateTapsRecieved) {
            cell.pointsLabel.text = [behavior.receivedCount stringValue];
        }
        else {
            cell.pointsLabel.text = [behavior.givenCount stringValue];
        }
        
        cell.nameLabel.text = behavior.fullName;
        cell.jobLabel.text = behavior.jobTitle;
        
        if (behavior.isPointWinnerValue) {
            cell.userBorderColor = [UIColor avatarBorderColor];
            cell.userBorderWidth = [NSNumber numberWithFloat:3.0];
        }
        
        [cell showSeparatorLine];
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        [self showBehaviorDetails];
    }
    else {
        [self showUserProfileForItemAtIndex:indexPath];
    }
    
}

#pragma mark AKLineMenuView

- (void)lineMenu:(AKLineMenuView *) menu didSelectItem:(NSInteger)itemIndex {
    
    self.listState = itemIndex;
    
    [self reloadData];
    
    [self.contentTableView scrollTableToTop:YES];
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.contentTableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadData];
}


#pragma mark - Navigation

- (void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Helpers Methods

- (void) trackScreenForAnalytic:(AnalyticsType) type {
    
    if (self.listState == ListStateTapsGiven) {
        switch (type) {
            case AnalyticsTypeAll:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsGivenList];
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsGivenList withScreenClass:NSStringFromClass([self class])];
                break;
                
            case AnalyticsTypeGoogleAnalytics:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsGivenList];
                break;
                
            case AnalyticsTypeFirebase:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsGivenList withScreenClass:NSStringFromClass([self class])];
                break;
        }
    }
    else {
        switch (type) {
            case AnalyticsTypeAll:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsReceivedList];
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsReceivedList withScreenClass:NSStringFromClass([self class])];
                break;
                
            case AnalyticsTypeGoogleAnalytics:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsReceivedList];
                break;
            
            case AnalyticsTypeFirebase:
                 [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehaviorTapsReceivedList withScreenClass:NSStringFromClass([self class])];
                 break;
        }
    }
    
}

@end
