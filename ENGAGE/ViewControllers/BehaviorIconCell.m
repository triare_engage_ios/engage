//
//  BehaviorIconCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 10/6/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorIconCell.h"

@implementation BehaviorIconCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

-(void) layoutSubviews {
    [super layoutSubviews];
    
    self.iconImageView.layer.cornerRadius = 4.0;
    self.iconImageView.clipsToBounds = YES;
}

+ (CGSize) sizeForCollectionWidth:(CGFloat) width {
    CGFloat cellWidth = width/5;
    return CGSizeMake(cellWidth, cellWidth+20);
}

@end
