//
//  AnswerDetailVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface AnswerDetailVC : ContentViewController
@property (nonatomic, strong) Question *question;

@end
