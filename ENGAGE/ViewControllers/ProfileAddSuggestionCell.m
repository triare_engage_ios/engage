//
//  ProfileAddSuggestionCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 29.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileAddSuggestionCell.h"

@implementation ProfileAddSuggestionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.leftButton setTitle:NSLocalizedString(@"VIRTUAL SUGGESTIONS", @"v1.0") forState:UIControlStateNormal];
    [self.rightButton setTitle:NSLocalizedString(@"EVALUATE", @"v1.0") forState:UIControlStateNormal];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state

}

- (IBAction)leftButtonAction:(id)sender {
    if (self.onLeftAction) {
        self.onLeftAction();
    }
}

- (IBAction)rightButtonAction:(id)sender {
    if (self.onRightAction) {
        self.onRightAction();
    }
}


@end
