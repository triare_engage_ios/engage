//
//  ActivityFeedPointCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/11/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ActivityFeedPointCell.h"

#import "ENCircleView.h"
#import "ENCircleImageView.h"

#define kDefaultCellHeight 110.0
#define kDefaultCellIdentifier @"pointCell"

@interface ActivityFeedPointCell ()
@property (weak, nonatomic) IBOutlet UIView *topSeparatorView;
@property (weak, nonatomic) IBOutlet UIView *backgroundTransparentView;
@property (weak, nonatomic) IBOutlet UIView *bottomSeparatorView;

@property (weak, nonatomic) IBOutlet ENCircleView *avatarBackgroundCircleView;
@property (weak, nonatomic) IBOutlet ENCircleImageView *avatarImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *congratulationLabel;


@property (weak, nonatomic) IBOutlet ENCircleImageView *badgeCircleImageView;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

@end

@implementation ActivityFeedPointCell

#pragma mark - Public methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

-(void) addFeed:(ActivityFeed *) feed {
    if ([feed feedType] == ActivityFeedTypeTopPoint) {
        [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:feed.receiverAvatarUrl] placeholderImage:[TeamUser placeholder]];
        [self.badgeCircleImageView sd_setImageWithURL:[NSURL URLWithString:feed.topPoint.imageUrl] placeholderImage:[Behavior placeholder]];
        self.nameLabel.text = feed.receiverName;
        self.congratulationLabel.text = NSLocalizedString(@"is the winner of last quarterly\nreceived", @"v1.0");
        self.pointsLabel.text = [NSString stringWithFormat:@"%d %@", (int) feed.topPoint.count.integerValue, [NSLocalizedString(@"points", @"v1.0") uppercaseString]];
    }
}

-(void) fillWithDefaultData {
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:@"http://i.imgur.com/GP8vvZE.png"] placeholderImage:[TeamUser placeholder]];
    [self.badgeCircleImageView sd_setImageWithURL:[NSURL URLWithString:@"http://i.imgur.com/nS3Dbhx.png"] placeholderImage:[Behavior placeholder]];
    self.nameLabel.text = @"Mark Twen";
    self.congratulationLabel.text = NSLocalizedString(@"is the winner of last quarterly\nhe received", @"v1.0");
    self.pointsLabel.text = NSLocalizedString(@"650 TAPS", @"v1.0");
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundTransparentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    self.topSeparatorView.backgroundColor = [UIColor whiteColor];
    self.bottomSeparatorView.backgroundColor = [UIColor whiteColor];
    
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.font = [UIFont appFont:17];
    
    self.congratulationLabel.textColor = [UIColor grayColor];
    self.congratulationLabel.font = [UIFont appFontBold:14];
    
    self.pointsLabel.textColor = [UIColor blackColor];
    self.pointsLabel.font = [UIFont appFontBold:12];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.avatarBackgroundCircleView.layer.borderWidth = 3.0;
    self.avatarBackgroundCircleView.layer.borderColor = [UIColor avatarBorderColor].CGColor;
    
    self.avatarImageView.layer.borderWidth = 3.0;
    self.avatarImageView.layer.borderColor = [UIColor avatarBorderColor].CGColor;
    
    self.badgeCircleImageView.layer.borderWidth = 1.0;
    self.badgeCircleImageView.layer.borderColor = [UIColor whiteColor].CGColor;
}

#pragma mark - Action

- (IBAction)avatarReceiverIconPressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowReceiver) {
        weakSelf.onShowReceiver (weakSelf);
    }
    
}

- (IBAction)badgeIconPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowTopPoints) {
        weakSelf.onShowTopPoints (weakSelf);
    }
    
}


@end
