//
//  CompanyValuesCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/24/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "CompanyValuesCell.h"

@implementation CompanyValuesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:17];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
