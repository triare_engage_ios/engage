//
//  CommentsVC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 11.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsVC : ContentViewController

@property (nonatomic, strong) ActivityFeed *feed;
@property (nonatomic, copy) void (^needRefresh)(BOOL isNeedRefresh);

@end
