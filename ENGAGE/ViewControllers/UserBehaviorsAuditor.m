//
//  UserBehaviorsAuditor.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 25.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "UserBehaviorsAuditor.h"

@interface UserBehaviorsAuditor ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;



@end

@implementation UserBehaviorsAuditor

-(id) initWithUser:(TeamUser *) user {
    if ([super init]) {
        self.user = user;
        
        [self prepareList];
    }
    
    return self;
}

-(void) prepareList {
    NSArray *objects = self.fetchedResultsController.fetchedObjects;
    
    self.bjectsType1 = [self objectsForType:1 fromIndex:0 fromList:objects];
    self.bjectsType2 = [self objectsForType:2 fromIndex:self.bjectsType1.count fromList:objects];
    
    //Not show second line if only two objects will be
    if (self.bjectsType2.count == 1) {
        self.bjectsType2 = nil;
    }
    
    self.fetchedResultsController = nil;
}

-(NSArray *) objectsForType:(NSInteger) type fromIndex:(NSInteger) fromIndex fromList:(NSArray *) objects {

    NSMutableArray *list = [NSMutableArray array];
    NSInteger count = 0;

    for (NSInteger i = fromIndex; i < objects.count; i++) {
        UserBehavior *b = [objects objectAtIndex:i];
        if (count == 0) {
            count = b.count.integerValue;
            [list addObject:b];
        } else {
            if (count == b.count.integerValue) {
                [list addObject:b];
            } else {
                break;
            }
        }
    }
    
    if (list.count <= type) {
        return list;
    }
    
    return nil;
}

-(NSArray *) cellsTypesList {
    NSMutableArray *list = [NSMutableArray array];
    
    if (self.bjectsType1.count) {
        [list addObject:@"behaviorType1"];
    }
    
    if (self.bjectsType2.count) {
        [list addObject:@"behaviorType2"];
    }
    
    NSInteger otherCells = ceil(self.fetchedResultsController.fetchedObjects.count / 3.);
    for (NSInteger i = 0; i < otherCells; i++) {
        [list addObject:@"behaviorType3"];
    }

    return list;
}


- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [UserBehavior MR_requestAllSortedBy:@"count" ascending:NO inContext:[DataManager context]];
    
    if (self.bjectsType1.count || self.bjectsType2.count) {
        NSMutableArray *ids = [NSMutableArray array];
        
        for (UserBehavior *b in self.bjectsType1) {
            [ids addObject:b.behaviorId];
        }
        
        for (UserBehavior *b in self.bjectsType2) {
            [ids addObject:b.behaviorId];
        }
        
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"teamUser.userId == %@ AND NOT (behaviorId IN %@)", self.user.userId, ids];
    } else {
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"teamUser.userId == %@", self.user.userId];
    }
    
    NSFetchedResultsController *fetchController = [UserBehavior MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

-(NSArray *) bjectsType3ForRow:(NSInteger) index {
    NSArray *objects = self.fetchedResultsController.fetchedObjects;
    
    NSInteger rowIndex = index * 3;
    
    NSArray *u;
    
    if (objects.count > rowIndex + 3) {
        u = [objects subarrayWithRange:NSMakeRange(rowIndex, 3)];
    } else {
        u = [objects subarrayWithRange:NSMakeRange(rowIndex, objects.count - rowIndex)];
    }
    
    return u;
}

@end
