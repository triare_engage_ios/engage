//
//  QuestionsVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionsVC.h"

#import "QuestionTextViewCell.h"
#import "QuestionSwitcherCell.h"
#import "QuestionAddPointCell.h"
#import "QuestionTextFieldCell.h"
#import "QuestionDateCell.h"
#import "QuestionActionCell.h"
#import "UITableViewCell+Separator.h"

#import "QuestionForSend.h"

#import "QuestionDataPickerView.h"

#import <KLCPopup/KLCPopup.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface QuestionsVC () <UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) QuestionForSend *currentQuestion;

//@property (nonatomic, strong) NSIndexPath *activeIndexPath;

@property (nonatomic, strong) QuestionDataPickerView *dataPickerView;
@property (nonatomic, strong) KLCPopup *popup;

@end//

@implementation QuestionsVC

#pragma mark - Public Methods

+(NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self setupQuestion];
    [self prepareDataPicker];
    
    [self reloadData];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self addKeyboardNotification];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionCreate];
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionCreate withScreenClass:NSStringFromClass([self class])];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeKeyboardNotification];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:NSLocalizedString(@"NEW QUESTION", @"v1.0")];
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupQuestion {
    self.currentQuestion = [[QuestionForSend alloc] init];
}

#pragma mark - Data

- (void) reloadData {
    NSMutableArray *list = [NSMutableArray new];
    [list addObject:@"questionBody"]; //textfield
    [list addObject:@"singeChoice"];
    [list addObject:@"answerWithText"];
    [list addObject:@"points"];
    [list addObject:@"scheduleTime"];
    [list addObject:@"sendAction"];
    
    self.items = list;
    
    [self.contentTableView reloadData];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSString *index = self.items[section];
    
    if ([index isEqualToString:@"questionBody"]) {
        return 1;
    }
    
    if ([index isEqualToString:@"singeChoice"]) {
        return 1;
    }
    
    if ([index isEqualToString:@"answerWithText"]) {
        return 1;
    }
    
    if ([index isEqualToString:@"points"]) {
        return self.currentQuestion.answers.count + 1;
        //return 1;
    }
    
    if ([index isEqualToString:@"scheduleTime"]) {
        if (self.currentQuestion.isSchedule) {
            return 2;
        }
        else {
            return 1;
        }
    }
    
    if ([index isEqualToString:@"sendAction"]) {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *index = self.items[indexPath.section];
    
    if ([index isEqualToString:@"questionBody"]) {
        return 160;
    }
    
    if ([index isEqualToString:@"singeChoice"]) {
        return 50;
    }
    
    if ([index isEqualToString:@"answerWithText"]) {
        return 50;
    }
    
    if ([index isEqualToString:@"points"]) {
        if (indexPath.row == 0) {
            return 50;
        }
        else {
            return 50;
        }
    }
    
    if ([index isEqualToString:@"scheduleTime"]) {
        if (indexPath.row == 0) {
            return 60;
        }
        else {
            return 50;
        }
    }
    
    if ([index isEqualToString:@"sendAction"]) {
        return 60;
    }
    
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *index = self.items[indexPath.section];
    
    if ([index isEqualToString:@"questionBody"]) {
        return [self questionBodyCell:tableView indexPath:indexPath];
    }
    
    if ([index isEqualToString:@"singeChoice"]) {
        return [self multipleSwitcherCell:tableView indexPath:indexPath];
    }
    
    if ([index isEqualToString:@"answerWithText"]) {
        return [self multipleSwitcherCell:tableView indexPath:indexPath];
    }
    
    if ([index isEqualToString:@"points"]) {
        if (indexPath.row == 0) {
            return [self addPointCell:tableView indexPath:indexPath];
        }
        else {
            return [self addTextFieldCell:tableView indexPath:indexPath];
        }
    }
    
    if ([index isEqualToString:@"scheduleTime"]) {
        if (indexPath.row == 0) {
            return [self multipleSwitcherCell:tableView indexPath:indexPath];
        }
        else {
            return [self scheduleCell:tableView indexPath:indexPath];
        }
    }
    
    if ([index isEqualToString:@"sendAction"]) {
        return [self actionCell:tableView indexPath:indexPath];
    }
    
    return nil;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Cells

-(QuestionTextViewCell *) questionBodyCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    QuestionTextViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"textViewCell"];
    cell.textView.delegate = self;
    
    return cell;
}

-(QuestionSwitcherCell *) multipleSwitcherCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    NSString *index = self.items[indexPath.section];
    
    __weak typeof(self) weakSelf = self;
    
    QuestionSwitcherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"switcherCell"];
    
    if ([index isEqualToString:@"singeChoice"]) {
        cell.titleLabel.text = NSLocalizedString(@"SINGLE CHOICE", @"v1.0");
        cell.subtitleLabel.text = NSLocalizedString(@"user can choose just one answer from all variants", @"v1.0");
        cell.onAction = ^(NSInteger index){
            if (index == 0) {
                [weakSelf multipleChoise:NO];
                [weakSelf changeChoiseTextForIndexPath:indexPath andState:NO];
            }
            
            if (index == 1) {
                [weakSelf multipleChoise:YES];
                [weakSelf changeChoiseTextForIndexPath:indexPath andState:YES];
            }
        };
    }
    
    if ([index isEqualToString:@"answerWithText"]) {
        cell.titleLabel.text = NSLocalizedString(@"ANSWER WITH TEXT", @"v1.0");
        cell.subtitleLabel.text = NSLocalizedString(@"add text field where user can answer with his own text", @"v1.0");
        
        cell.onAction = ^(NSInteger index){
            if (index == 0) {
                [weakSelf customAnswer:NO];
            }
            
            if (index == 1) {
                [weakSelf customAnswer:YES];
            }
        };
    }
    
    if ([index isEqualToString:@"scheduleTime"]) {
        if (indexPath.row == 0) {
            cell.titleLabel.text = NSLocalizedString(@"ADD SCHEDULE", @"v1.0");
            cell.subtitleLabel.text = NSLocalizedString(@"choose date when question appears on users devices", @"v1.0");
            
            cell.titleLabelTopSpace.constant = 15;
            
            cell.onAction = ^(NSInteger index){
                if (index == 0) {
                    [weakSelf scheduleEnable:NO];
                    [weakSelf hideDateRow];
                }
                
                if (index == 1) {
                    [weakSelf.view endEditing:YES];
                    [weakSelf scheduleEnable:YES];
                    [weakSelf showDateRow];
                    [weakSelf showDataPicker];
                }
            };
        }
    }
    
    return cell;
}

-(QuestionAddPointCell *) addPointCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    QuestionAddPointCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addPointCell"];
    
    cell.titleLabel.text = NSLocalizedString(@"ADD POINT", @"v1.0");
    cell.onAction = ^ {
        [weakSelf addPointRowForSection:indexPath.section];
    };
    
    if ([self.currentQuestion isAnswersFull]) {
        cell.addButton.hidden = YES;
    }
    else {
        cell.addButton.hidden = NO;
    }
    
    //cell.subtitleLabel.text = NSLocalizedString(@"minimum two answers needed", @"v1.0");
    
    return cell;
}

-(QuestionTextFieldCell *) addTextFieldCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    QuestionTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"textFieldCell"];
    cell.textField.text = self.currentQuestion.answers[indexPath.row - 1];
    
    cell.deleteButton.hidden = NO;
    
    if (![self.currentQuestion isAnswersMinimum]) {
        cell.deleteButton.hidden = YES;
    }
    
    cell.index = indexPath.row - 1;
    cell.onAction = ^ (QuestionTextFieldCell *cellReturned) {
        NSIndexPath *indexPathReturned = [weakSelf.contentTableView indexPathForCell:cellReturned];
        [weakSelf deleteAnswerForIndex:indexPathReturned.row - 1];
        [weakSelf deleteRowOfTextFieldAtRow:indexPathReturned.row - 1];
    };
    
    cell.onChangeText = ^ (NSString *text, QuestionTextFieldCell *cellReturned) {
        NSIndexPath *indexPathReturned = [weakSelf.contentTableView indexPathForCell:cellReturned];
        [weakSelf replaceAnswerAtIndex:indexPathReturned.row - 1 withText:text];
    };
    
    [cell showSeparatorLine];
    
    return cell;
}

-(QuestionDateCell *) scheduleCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    QuestionDateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dateCell"];
    
    if (self.currentQuestion.isSchedule && self.currentQuestion.dateTime) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd.MM.YYYY HH:mm"];
        cell.titleLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:self.currentQuestion.dateTime]];
    }
    else {
        cell.titleLabel.text = @"00.00.0000 00:00";
    }
    
    cell.onAction = ^ {
        [weakSelf showDataPicker];
    };
    
    return cell;
}

-(QuestionActionCell *) actionCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    QuestionActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionCell"];
    cell.onAction = ^{
        [weakSelf.view endEditing:YES];
        [weakSelf sendQuestion];
    };
    
    return cell;
}

#pragma mark - Action

- (void) sendQuestion {
    
    __weak typeof(self) weakSelf = self;
    
    //Body validation
    if (weakSelf.currentQuestion.body.length == 0) {
        [weakSelf showAlertWithText:NSLocalizedString(@"Add your question", @"v1.0")];
        return;
    }
    
    //Empty point validation
    for (NSString *answer in self.currentQuestion.answers) {
        //Empty points validation
        if ([answer isEqualToString:@""]) {
            //Need fill empty point or delete it
            if (self.currentQuestion.haveCustomAnswer) {
                [weakSelf showAlertWithText:NSLocalizedString(@"Fill in answer point or remove them by pressing minus", @"v1.0")];
                return;
            }
            
            //Need set minimum two answers
            if (self.currentQuestion.answers.count < 2 && !self.currentQuestion.haveCustomAnswer) {
                [weakSelf showAlertWithText:NSLocalizedString(@"Minimum two answers needed", @"v1.0")];
                return;
            }
            
            //Need fill point or remove them
            if (self.currentQuestion.answers.count > 2 && !self.currentQuestion.haveCustomAnswer) {
                [weakSelf showAlertWithText:NSLocalizedString(@"Fill in answer point or remove them by pressing minus", @"v1.0")];
                return;
            }
            
            //Need fill point
            if (self.currentQuestion.answers.count == 2 && !self.currentQuestion.haveCustomAnswer) {
                [weakSelf showAlertWithText:NSLocalizedString(@"Fill in answer point", @"v1.0")];
                return;
            }
        }
        
    }
    
    //Validation for empty text and answer
    if (!self.currentQuestion.haveCustomAnswer && self.currentQuestion.answers.count < 2) {
        [weakSelf showAlertWithText:NSLocalizedString(@"Please select answer with text or add two answers points", @"v1.0")];
        return;
    }
    
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] sendQuestionWithInfo:[weakSelf.currentQuestion serializeForSend] success:^(BOOL success) {
        if (weakSelf.onBackAction) {
            weakSelf.onBackAction();
        }
        [weakSelf hideLoadingView];
        [weakSelf closeVC];
        [weakSelf showSentAlert];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithText:message];
    }];
}

- (void) closeVC {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Question changes

- (void) multipleChoise:(BOOL) choice {
    self.currentQuestion.haveMultipleChoice = choice;
}

- (void) customAnswer:(BOOL) answer {
    self.currentQuestion.haveCustomAnswer = answer;
    [self reloadFirstRowInSection:3];
    [self showMinusInRowsIfNeed];
}

- (void) deleteAnswerForIndex:(NSInteger) index {
    [self.currentQuestion.answers removeObjectAtIndex:index];
}

- (void) addEmpteyAnswer {
    [self.currentQuestion.answers addObject:@""];
}

- (void) replaceAnswerAtIndex:(NSInteger) index withText:(NSString *) text {
    [self.currentQuestion.answers replaceObjectAtIndex:index withObject:text];
}

- (void) scheduleEnable:(BOOL) enable {
    self.currentQuestion.isSchedule = enable;
    if (!enable) {
        self.currentQuestion.dateTime = nil;
    }
}

- (void) addDate:(NSDate *) date {
    self.currentQuestion.dateTime = date;
}


#pragma mark - Cell Actions

- (void) changeChoiseTextForIndexPath:(NSIndexPath *) indexPath andState:(BOOL) isOn {
    
    NSString *index = self.items[indexPath.section];
    
    if ([index isEqualToString:@"singeChoice"]) {
        if (isOn) {
            QuestionSwitcherCell *cell = [self.contentTableView cellForRowAtIndexPath:indexPath];
            cell.titleLabel.text = NSLocalizedString(@"MULTIPLE CHOICE", @"v1.0");
            cell.subtitleLabel.text = NSLocalizedString(@"user can choose more than one answer", @"v1.0");
        }
        else {
            QuestionSwitcherCell *cell = [self.contentTableView cellForRowAtIndexPath:indexPath];
            cell.titleLabel.text = NSLocalizedString(@"SINGLE CHOICE", @"v1.0");
            cell.subtitleLabel.text = NSLocalizedString(@"user can choose just one answer from all variants", @"v1.0");
        }
    }
}

- (void) changeSwitcherEnable:(BOOL)state forIndexPath:(NSIndexPath *) indexPath {
    
    NSString *index = self.items[indexPath.section];
    
    if ([index isEqualToString:@"answerWithText"]) {
        QuestionSwitcherCell *cell = [self.contentTableView cellForRowAtIndexPath:indexPath];
        [cell changeSwitcherStateEnable:state];
    }
    
}

- (void) addPointRowForSection:(NSInteger)section {
    if (![self.currentQuestion isAnswersFull]) {
        [self addEmpteyAnswer];
    }
    
    //Reload row, because wee need to hide @+@ if limit 5 and 4
    if ([self.currentQuestion isAnswersFull]) {
        QuestionAddPointCell *cell = [self.contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
        cell.addButton.hidden = YES;
    }
    
    //Update switcher for multiple choice if needed
    if ([self.currentQuestion isAnswersFull]) {
        [self changeSwitcherEnable:NO forIndexPath:[NSIndexPath indexPathForRow:0 inSection:section - 1]];
    }
    else {
        [self changeSwitcherEnable:YES forIndexPath:[NSIndexPath indexPathForRow:0 inSection:section - 1]];
    }
    
    //Insert row
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.currentQuestion.answers.count inSection:3]];
    [self.contentTableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
    [self.contentTableView scrollToRowAtIndexPath:paths.firstObject atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    [self showMinusInRowsIfNeed];
}

- (void) showMinusInRowsIfNeed {
    //Hide minus
    if (![self.currentQuestion isAnswersMinimum]) {
        for (int i = 0; i <= self.currentQuestion.answers.count; i++ ) {
            QuestionTextFieldCell *cell = [self.contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i + 1 inSection:3]];
            cell.deleteButton.hidden = YES;
        }
        //Show minus
    } else {
        for (int i = 0; i <= self.currentQuestion.answers.count; i++ ) {
            QuestionTextFieldCell *cell = [self.contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i + 1 inSection:3]];
            cell.deleteButton.hidden = NO;
        }
    }
}

- (void) reloadFirstRowInSection:(NSInteger) section {
    NSArray *path = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:section]];
    [self.contentTableView reloadRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationNone];
}

- (void) deleteRowOfTextFieldAtRow:(NSInteger) indexRow {
    [self.contentTableView beginUpdates];
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexRow + 1 inSection:3]];
    [self.contentTableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationBottom];
    [self.contentTableView endUpdates];
    
    if ([self.currentQuestion isAnswersFull]) {
        [self changeSwitcherEnable:NO forIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    }
    else {
        [self changeSwitcherEnable:YES forIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    }
    
    if (![self.currentQuestion isAnswersFull]) {
        QuestionAddPointCell *cell = [self.contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
        cell.addButton.hidden = NO;
    }
    
    [self showMinusInRowsIfNeed];
}

- (void) hideDateRow {
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:4]];
    [self.contentTableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
}

- (void) showDateRow {
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:4]];
    NSArray *pathForScroll = [NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:5], nil];
    [self.contentTableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
//    [self.contentTableView scrollToRowAtIndexPath:paths.firstObject atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        [self.contentTableView scrollToRowAtIndexPath:pathForScroll.firstObject atScrollPosition:UITableViewScrollPositionBottom animated:YES];

}

- (void) updateDateRow {
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:4]];
    [self.contentTableView reloadRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationNone];
}

- (void) updateSwitcherRow {
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:4]];
    
    QuestionSwitcherCell *cell = [self.contentTableView cellForRowAtIndexPath:paths.firstObject];
    [cell changeSwitcherToOn:NO];
}

#pragma mark - Text View Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //__weak typeof(self) weakSelf = self;
    
    //New textfield string
    NSString * proposedNewText = [[textView text] stringByReplacingCharactersInRange:range withString:text];
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textView.text.length == 0 && [proposedNewText stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    if ([self isTextChanges:self.currentQuestion.body withText:proposedNewText]) {
        self.currentQuestion.body = proposedNewText;
    }
    
    return YES;
}

- (BOOL) isTextChanges:(NSString *)text withText:(NSString *) textToCompare {
    
    BOOL isChanges = NO;
    
    if (![text isEqualToString:textToCompare] && textToCompare != 0) {
        isChanges = YES;
    }
    
    return isChanges;
}

#pragma mark - Data Picker

#define DATA_PICKER_HEIGHT 245

- (void) prepareDataPicker {
    
    __weak typeof(self) weakSelf = self;
    
    self.dataPickerView = [[NSBundle mainBundle] loadNibNamed:@"QuestionDataPickerView" owner:self options:nil].lastObject;
    
    CGRect f = CGRectMake(0, 0, SCREEN_WIDTH, DATA_PICKER_HEIGHT);
    self.dataPickerView.frame = f;
    self.dataPickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.dataPickerView.onOkAction = ^(NSDate *date){
        [weakSelf addDate:date];
        [weakSelf hideDataPicker];
        [weakSelf updateDateRow];
    };
    
    self.dataPickerView.onCancelAction = ^{
        [weakSelf hideDataPicker];
        [weakSelf scheduleEnable:NO];
        [weakSelf hideDateRow];
        [weakSelf updateSwitcherRow];
    };
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.dataPickerView.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.dataPickerView addSubview:blurEffectView];
    [self.dataPickerView sendSubviewToBack:blurEffectView];
    
    self.popup = [KLCPopup popupWithContentView:self.dataPickerView];
    self.popup.showType = KLCPopupShowTypeSlideInFromTop;
    self.popup.dismissType = KLCPopupDismissTypeSlideOutToTop;
    self.popup.maskType = KLCPopupMaskTypeDimmed;
    self.popup.shouldDismissOnBackgroundTouch = YES;
    self.popup.shouldDismissOnContentTouch = NO;
}

- (void) showDataPicker {
    if (self.currentQuestion.dateTime) {
        self.dataPickerView.startDate = self.currentQuestion.dateTime;
    }
    [self.popup show];
}

- (void) hideDataPicker {
    [self.popup dismiss:YES];
}

#pragma mark - Helpers Methods

-(void) showSentAlert {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Question has been sent", @"v1.0")];
}

- (void) showAlertWithText:(NSString *) text {
    
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:nil text:text];
}

#pragma mark - Keyboard Movement

#define kDeltaKeyboardMovement 0

- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(EN_NAVIGATION_BAR_HEIGHT, 0.0, kbRect.size.height + kDeltaKeyboardMovement + EN_NAVIGATION_TAB_BAR_HEIGHT, 0.0);
    self.contentTableView.contentInset = contentInsets;
    self.contentTableView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    
//    if (self.activeIndexPath) {
//        [self.contentTableView scrollToRowAtIndexPath:self.activeIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
//    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = self.contentTableView.contentInset;
    contentInsets.top = EN_NAVIGATION_BAR_HEIGHT;
    contentInsets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.contentTableView.contentInset = contentInsets;
    self.contentTableView.scrollIndicatorInsets = contentInsets;
}


@end
