//
//  ProfileBadgeCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileBadgeCell.h"

#import "ENStokeLightButton.h"

#define kDefaultCellHeight 65.0
#define kDefaultCellIdentifier @"badgeCell"

@interface ProfileBadgeCell ()
@property (weak, nonatomic) IBOutlet ENStokeLightButton *badgeActionButton;

@end

@implementation ProfileBadgeCell

#pragma mark - Public Methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.badgeActionButton setTitle:[NSLocalizedString(@"badge", @"v1.0") uppercaseString] forState:UIControlStateNormal];
}

#pragma mark - Action

- (IBAction)badgeActionPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onAction) {
        weakSelf.onAction();
    }
}


@end
