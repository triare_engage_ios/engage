//
//  ProfileVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 16.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileVC.h"

#import "UITableViewCell+Separator.h"
#import "ProfileUserCell.h"
#import "ProfileEvaluationCell.h"
#import "ProfileTapsButtonsCell.h"
#import "ProfileSortPeriodButtonsCell.h"
#import "ProfileSortPeriodInfoCell.h"
#import "ProfileAddSuggestionCell.h"
#import "ProfileAdminCell.h"
#import "ProfileBadgeCell.h"
#import "ProfileBehavior1Cell.h"
#import "ProfileBehavior2Cell.h"
#import "ProfileBehavior3Cell.h"

#import "GiveTapStepMessageVC.h"
#import "ProfileCurrentUserEvaluationVC.h"
#import "ProfileSettingsVC.h"
#import "EvaluateVC.h"
#import "GiveTapStep1VC.h"
#import "BehaviorDetailVC.h"
#import "BadgeListVC.h"

#import "UserBehaviorsAuditor.h"
#import "ProfileHelper.h"

typedef enum : NSUInteger {
    SortingPeriodWeek = 0,
    SortingPeriodMonth,
    SortingPeriodLifetime
} BehaviorsSortingPeriod;

typedef enum : NSUInteger {
    SortingTapsReceived,
    SortingTapsGiven
} BehaviorsSortingTaps;

@interface BehaviorsSorting : NSObject

@property BehaviorsSortingTaps taps;
@property BehaviorsSortingPeriod period;

@end

@implementation BehaviorsSorting


@end






@interface ProfileVC ()

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) TeamUser *user;
@property (nonnull, strong) NSArray *cells;

@property (nonatomic, strong) UserBehaviorsAuditor *behaviorsAuditor;
@property (nonatomic, strong) BehaviorsSorting *behaviorsSorting;

@property BOOL isCurrentUserProfile;

@property BOOL isQuestionListExist;

@end

@implementation ProfileVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.behaviorsSorting = [[BehaviorsSorting alloc] init];
    self.behaviorsSorting.taps = SortingTapsReceived;
    self.behaviorsSorting.period = SortingPeriodWeek;
    
    [self.tableView registerNib:[UINib nibWithNibName:[ProfileBadgeCell cellNimbName] bundle:nil] forCellReuseIdentifier:[ProfileBadgeCell cellReuseIdentifier]];
    
    if ([self.userId isEqualToNumber:[DataManager user].userId]) {
        self.isCurrentUserProfile = YES;
    }
    
    __weak typeof(self) weakSelf = self;
    if (!self.isPersonalProfile) {
        self.navigationBar.onLeftAction = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        [self.navigationBar showLeftButton];
    }
    
    if (self.isCurrentUserProfile) {
        self.navigationBar.onRightAction = ^{
            [weakSelf showProfileSettings];
        };
        [self.navigationBar changeRightButtonIcon:[UIImage imageNamed:@"icon-settings"]];
        [self.navigationBar showRightButton];
    }

    [self.navigationBar setTitle:[self.profileTitle uppercaseString]];
    
    //[self loadUserData];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self isUserAdmin]) {
        NSInteger index = [self.cells indexOfObject:@"adminButtons"];
        if (self.tableView.visibleCells.count) {
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    
    [self trackScreenForAnalytic:AnalyticsTypeAll];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self loadDataWithDelay];
}

#pragma mark - Load Data

- (void) loadDataWithDelay {
    [self loadUserData];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf loadBehaviorsData];
    });
}

-(void) loadUserData {
    
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] getUserProfile:self.userId success:^(NSDictionary *userInfo, NSArray *behaviors) {
        
        [DataManager saveTeamUser:userInfo];
        [DataManager saveUserBehaviors:behaviors userId:weakSelf.userId];
        weakSelf.user = [DataManager teamUserWithId:weakSelf.userId];
        [weakSelf updateTitleName];
        
        if (weakSelf.isPersonalProfile) {
            [DataManager updateSomeUserInfo:userInfo];
        }
    
        [weakSelf reloadData];
        
        //[self hideLoadingView];
    } failure:^(NSString *message) {
        //[self hideLoadingView];
    }];
}

-(void) loadBehaviorsData {
    
    __weak typeof(self) weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSString *taps = @"given";
    if (self.behaviorsSorting.taps == SortingTapsReceived) taps = @"received";
    [params setValue:taps forKey:@"behaviours"];
    

    NSString *period = @"lifetime";
    if (self.behaviorsSorting.period == SortingPeriodMonth) period = @"month";
    if (self.behaviorsSorting.period == SortingPeriodWeek) period = @"week";
    [params setValue:period forKey:@"range"];

    
    //[self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] getUserBehaviors:self.userId sort:params success:^(NSArray *list) {
        
        [DataManager saveUserBehaviors:list userId:self.userId];
        [weakSelf reloadData];
        
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithTitle:nil andText:message];
    }];
}

- (void)reloadData {
    
    NSMutableArray *list = [NSMutableArray array];
    [list addObject:@"user"];
    if ((self.isCurrentUserProfile && ![self isEvaluationEmpety]) || ([[DataManager user] isAdmin] && ![self isEvaluationEmpety]) || ([[DataManager user] isManager] && ![self isEvaluationEmpety])) {
        [list addObject:@"evaluation"];
    }
    
    if ([self isUserAdmin] || [self isUserManager]) {
        [list addObject:@"adminButtons"];
    }
    
    if (self.isCurrentUserProfile && ![self isUserAdmin] && ![self isUserManager]) {
        [list addObject:@"suggestionAndEvaluate"];
    }
    
    if ([ProfileHelper isNeedDisplayBadgeForUser:self.user]) {
        [list addObject:@"badgeButton"];
    }
    
    [list addObject:@"tapsButtons"];
    [list addObject:@"periodButtons"];
    if (self.behaviorsSorting.period != SortingPeriodLifetime) {
    //    [list addObject:@"periodInfo"];
    }
    
    self.cells = list;
    
    self.behaviorsAuditor = [[UserBehaviorsAuditor alloc] initWithUser:self.user];
    self.cells = [self.cells arrayByAddingObjectsFromArray:[self.behaviorsAuditor cellsTypesList]];
    
    [self.tableView reloadData];
}

- (NSInteger) behaviorIndexInCells {
    return [self.cells indexOfObject:@"behaviorType3"];
}

-(BOOL) isUserAdmin {
    return ([self.userId isEqualToNumber:[DataManager user].userId] && [[DataManager user] isAdmin]);
}

-(BOOL) isUserManager {
    return ([self.userId isEqualToNumber:[DataManager user].userId] && [[DataManager user] isManager]);
}

-(BOOL) isEvaluationEmpety {
    return (self.user.receivedBadEvaluationsCount.integerValue == 0 && self.user.receivedGoodEvaluationsCount.integerValue == 0);
}

#pragma mark - UI Update

- (void) updateTitleName {
    self.profileTitle = self.user.fullName;
    [self.navigationBar setTitle:[self.profileTitle uppercaseString]];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showAddEvaluate"]) {
        EvaluateVC *dest = segue.destinationViewController;
        dest.userId = self.userId;
    }
    
    if ([segue.identifier isEqualToString:@"showProfileEvaluation"]) {
        ProfileCurrentUserEvaluationVC *dest = segue.destinationViewController;
        dest.userId = self.userId;
    }
    
    if ([segue.identifier isEqualToString:@"showProfileSettings"]) {
        ProfileSettingsVC *dest = segue.destinationViewController;
        dest.userId = self.userId;
    }
    
}


-(void) sendSuggestion {
    GiveTapStepMessageVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStepMessageVC"];
    vc.isSuggestionMessage = YES;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void) giveTap {
    [[AppDelegate theApp].mainViewController showGiveTapForUser:self.userId];
}

-(void) showProfileSettings {
    [self performSegueWithIdentifier:@"showProfileSettings" sender:nil];
}

-(void) evaluateUser {
    [self performSegueWithIdentifier:@"showAddEvaluate" sender:nil];
}

-(void) showSuggestions {
    [self performSegueWithIdentifier:@"showSuggestions" sender:nil];
}

-(void) showQuestion {
    [self performSegueWithIdentifier:@"showQuestion" sender:nil];
}

-(void) showQuestionsList {
    if (self.user.isQuestionPresentValue) {
        [self performSegueWithIdentifier:@"showQuestionsList" sender:nil];
    }
    else {
        [self showQuestion];
    }
}

-(void) showTeamMessage {
    [self performSegueWithIdentifier:@"showTeamMessageList" sender:nil];
}

-(void) createTeamMessage {
    GiveTapStepMessageVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStepMessageVC"];
    vc.isTeamMessage = YES;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void) showManagePeople {
    [self performSegueWithIdentifier:@"showManagePeople" sender:nil];
}

-(void) showProfileEvaluation {
    [self performSegueWithIdentifier:@"showProfileEvaluation" sender:nil];
}

-(void) sendEvaluate {
    GiveTapStep1VC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStep1VC"];
    vc.isUsedForEvaluate = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    //[self presentViewController:vc animated:YES completion:nil];
}

- (void) showBehaviorDetailForId:(NSNumber *) behaviorId {
    
    Behavior *item = [DataManager behaviorByID:behaviorId];
    
    BehaviorDetailVC * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorDetailVC"];
    vc.item = item;
    vc.onHideAction = nil;
    
    [self showViewController:vc sender:nil];
}

- (void) showBadgeList {
    BadgeListVC *vc = [[UIStoryboard userProfileStoryboard] instantiateViewControllerWithIdentifier:[BadgeListVC  storyboardIdentifier]];
    vc.userId = self.userId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Action

-(void) changeTapsSort:(BOOL) received {
    self.behaviorsSorting.taps = SortingTapsGiven;
    if (received) self.behaviorsSorting.taps = SortingTapsReceived;
    
    [self loadBehaviorsData];
}

-(void) changePeriodSort:(BehaviorsSortingPeriod) period {
    self.behaviorsSorting.period = period;
    [self loadBehaviorsData];
}


#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.user == nil) return 0;
    
    return self.cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    __weak typeof(self) weakSelf = self;
    
    NSString *index = [self.cells objectAtIndex:indexPath.row];
    
    if ([index isEqualToString:@"user"]) {
        
        ProfileUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userCell"];
        cell.usernameLabel.text = [self.user fullName];
        cell.userJobLabel.text = self.user.jobTitle;
        
        [cell.userImageView sd_setImageWithURL:[AppTemplate bigIconURL:self.user.avatarUrl] placeholderImage:[TeamUser placeholder]];
        
        if (self.user.isPointWinnerValue) {
            [cell showBorderForAvatar];
        }
        
        [cell showSeparatorLine];
        
        if ([self isUserAdmin] || [self isPersonalProfile]) {
            [cell isPersonalProfile:YES];
        }
        else if ([self.userId integerValue] == [[DataManager user].userId integerValue]) {
            [cell hideGiveTapAndEvaluation];
        }
        else {
            [cell isPersonalProfile:NO];
            
            if ([self.userId isEqualToNumber:[DataManager user].userId]) {
                [cell setSuggestOnLeftButton];
                cell.onLeftAction = ^{
                    [weakSelf sendSuggestion];
                };
            } else {
                cell.onLeftAction = ^{
                    [weakSelf giveTap];
                };
            }
            
            cell.onRigntAction = ^{
                [weakSelf evaluateUser];
            };
        }
        
        if (![ProfileHelper isNeedDisplayEvaluateAndGiveTap]) {
            [cell hideGiveTapAndEvaluation];
        }
        
        return cell;
    }
    
    if ([index isEqualToString:@"evaluation"]) {
        ProfileEvaluationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"evaluationCell"];
        cell.leftNumberLabel.text = [self.user.receivedGoodEvaluationsCount stringValue];
        cell.rightNumberLabel.text = [self.user.receivedBadEvaluationsCount stringValue];
        cell.leftNumberLabel.textColor = [UIColor appGrayColor];
        cell.rightNumberLabel.textColor = [UIColor appGrayColor];
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"suggestionAndEvaluate"]) {
        ProfileAddSuggestionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addSuggestionCell"];
        cell.leftButton.titleLabel.font = [UIFont appFontBold:11];
        cell.rightButton.titleLabel.font = [UIFont appFontBold:11];
        cell.onLeftAction = ^{
            
            if ([ProfileHelper isNeedAccessVirtualSuggestion]) {
                [weakSelf sendSuggestion];
            }
            else {
                [weakSelf showAlertWithTitle:nil andText:NSLocalizedString(@"This functionality is not available in your plan", @"v1.0")];
            }
            
        };
        
        cell.onRightAction = ^{
            
            if ([ProfileHelper isNeedAccessVirtualSuggestion]) {
                [weakSelf sendEvaluate];
            }
            else {
                [weakSelf showAlertWithTitle:nil andText:NSLocalizedString(@"This functionality is not available in your plan", @"v1.0")];
            }
            
        };
        
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"badgeButton"]) {
        return [self badgeCellForTableView:tableView atIndexPath:indexPath];
    }
    
    if ([index isEqualToString:@"tapsButtons"]) {
        
        ProfileTapsButtonsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tapsButtonsCell"];
        [cell showSeparatorLine];
        if (self.behaviorsSorting.taps == SortingTapsReceived) {
            [cell isReceived:YES];
        } else {
            [cell isReceived:NO];
        }
        
        
        cell.onChangeSort = ^(BOOL received) {
            [weakSelf changeTapsSort:received];
        };
        
        return cell;
    }
    
    if ([index isEqualToString:@"periodButtons"]) {
        
        ProfileSortPeriodButtonsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"periodButtonsCell"];
        [cell showSeparatorLine];
        [cell selectButtonAtIndex:self.behaviorsSorting.period];
        cell.onChangeSort = ^(NSInteger buttonIndex) {
            [weakSelf changePeriodSort:buttonIndex];
        };
        
        return cell;
    }
    
    if ([index isEqualToString:@"periodInfo"]) {
        
        ProfileSortPeriodInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"periodInfoCell"];
        
        return cell;
    }
    
    if ([index isEqualToString:@"adminButtons"]) {
        
        ProfileAdminCell *cell = [tableView dequeueReusableCellWithIdentifier:@"adminCell"];
        cell.onAction = ^(NSInteger index) {
            
            if (index == 0) {
                if ([ProfileHelper isNeedAccessVirtualSuggestion]) {
                    [weakSelf showSuggestions];
                }
                else {
                    [weakSelf showAlertWithTitle:nil andText:NSLocalizedString(@"This functionality is not available in your plan", @"v1.0")];
                }
            }
            
            if (index == 1) {
                if ([ProfileHelper isNeedAccessQuestion]) {
                    [weakSelf showQuestionsList];
                }
                else {
                    [weakSelf showAlertWithTitle:nil andText:NSLocalizedString(@"This functionality is not available in your plan", @"v1.0")];
                }
            }
            
            if (index == 2) {
                [weakSelf showManagePeople];
            }
            
            if (index == 3 && self.user.isTeamMessagesPresentValue) {
                [weakSelf showTeamMessage];
            }
            
            if (index == 3 && !self.user.isTeamMessagesPresentValue) {
                [weakSelf createTeamMessage];
            }
        };
        [cell setNewSuggestionCount:self.user.unreadSuggestionsCount.integerValue];
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"behaviorType1"]) {
        
        ProfileBehavior1Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"behaviorCell"];
        [cell setBehavior:[self.behaviorsAuditor.bjectsType1 firstObject]];
        cell.onShowBehavior = ^ {
            UserBehavior *b = [weakSelf.behaviorsAuditor.bjectsType1 firstObject];
            [weakSelf showBehaviorDetailForId:b.behaviorId];
        };
        
        return cell;
    }
    
    if ([index isEqualToString:@"behaviorType2"]) {
        
        ProfileBehavior2Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"behavior2Cell"];
        [cell setBehaviors:self.behaviorsAuditor.bjectsType2];
        cell.onShowBehavior = ^(NSInteger index) {
            if (index == 0) {
                UserBehavior *b = [weakSelf.behaviorsAuditor.bjectsType2 firstObject];
                [weakSelf showBehaviorDetailForId:b.behaviorId];
            }
            
            if (index == 1) {
                UserBehavior *b = [weakSelf.behaviorsAuditor.bjectsType2 lastObject];
                [weakSelf showBehaviorDetailForId:b.behaviorId];
            }
        };
        
        return cell;
    }
    
    if ([index isEqualToString:@"behaviorType3"]) {

        NSInteger cellIndex = indexPath.row - [self behaviorIndexInCells];
        
        ProfileBehavior3Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"behavior3Cell"];
        [cell setBehaviors:[self.behaviorsAuditor bjectsType3ForRow:cellIndex]];
        cell.onShowBehavior = ^(NSInteger index) {
            if ([NSNumber numberWithInteger:index] != nil) {
                UserBehavior *b = [weakSelf.behaviorsAuditor bjectsType3ForRow:cellIndex][index];
                [weakSelf showBehaviorDetailForId:b.behaviorId];
            }
        };
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *index = [self.cells objectAtIndex:indexPath.row];
    
    if ([index isEqualToString:@"evaluation"]) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [self showProfileEvaluation];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *index = [self.cells objectAtIndex:indexPath.row];

    if ([index isEqualToString:@"user"]) {
        return 230;
    }
    
    if ([index isEqualToString:@"evaluation"]) {
        return 100;
    }
    
    if ([index isEqualToString:@"tapsButtons"]) {
        return 65;
    }
    if ([index isEqualToString:@"periodButtons"]) {
        return 65;
    }
    
    if ([index isEqualToString:@"periodInfo"]) {
        return 70;
    }
    
    if ([index isEqualToString:@"adminButtons"]) {
        return 110;
    }
    
    if ([index isEqualToString:@"badgeButton"]) {
        return [ProfileBadgeCell cellHeight];
    }
    
    if ([index isEqualToString:@"behaviorType1"]) {
        return 240;
    }
    
    if ([index isEqualToString:@"behaviorType2"]) {
        return 180;
    }
    
    if ([index isEqualToString:@"behaviorType3"]) {
        return 140;
    }

    return 65;
}

#pragma mark - Cells

- (ProfileBadgeCell *) badgeCellForTableView:(UITableView *) tableView atIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ProfileBadgeCell *cell = [tableView dequeueReusableCellWithIdentifier:[ProfileBadgeCell cellReuseIdentifier]];
    cell.onAction = ^{
        [weakSelf showBadgeList];
    };
    
    [cell showSeparatorLine];
    
    return cell;
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

- (void) trackScreenForAnalytic:(AnalyticsType) type {
    
    if (self.isCurrentUserProfile || self.isPersonalProfile) {
        switch (type) {
            case AnalyticsTypeAll:
                [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUser];
                [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUser withScreenClass:NSStringFromClass([self class])];
                break;
                
            case AnalyticsTypeFirebase:
                [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUser withScreenClass:NSStringFromClass([self class])];
                break;
                
            case AnalyticsTypeGoogleAnalytics:
                [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUser];
                break;
        }
    }
    else {
        switch (type) {
            case AnalyticsTypeAll:
                [AnalyticsManager trackScreen:ENAnalyticScreenProfile];
                [AnalyticsManager trackScreen:ENAnalyticScreenProfile withScreenClass:NSStringFromClass([self class])];
                break;
                
            case AnalyticsTypeGoogleAnalytics:
                [AnalyticsManager trackScreen:ENAnalyticScreenProfile];
                break;
                
            case AnalyticsTypeFirebase:
                [AnalyticsManager trackScreen:ENAnalyticScreenProfile withScreenClass:NSStringFromClass([self class])];
                break;
        }
    }
    
}

@end
