//
//  ProfileSortPeriodInfoCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 17.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileSortPeriodInfoCell.h"

@implementation ProfileSortPeriodInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.periodLabel.font = [UIFont appFontBold:17];
    self.periodLabel.textColor = [UIColor appTextColor];
}
- (IBAction)leftAction:(id)sender {
}
- (IBAction)rightAction:(id)sender {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
