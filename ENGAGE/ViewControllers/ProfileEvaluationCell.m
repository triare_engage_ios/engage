//
//  ProfileEvaluationCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/1/16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileEvaluationCell.h"

@implementation ProfileEvaluationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.leftTitleLabel.text = NSLocalizedString(@"BEST AT", @"v1.0");
    self.rightTitleLabel.text = NSLocalizedString(@"NEED TO WORK ON", @"v1.0");
    self.leftTitleLabel.font = [UIFont appFontBold:[self fontSize]];
    self.leftTitleLabel.textColor = [UIColor appTextColor];
    self.rightTitleLabel.font = [UIFont appFontBold:[self fontSize]];
    self.rightTitleLabel.textColor = [UIColor appTextColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat) fontSize {
    if (IS_IPHONE_5) {
        return 14;
    }
    else {
        return 17;
    }
}

@end
