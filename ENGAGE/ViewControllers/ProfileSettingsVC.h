//
//  ProfileSettingsVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface ProfileSettingsVC : ContentViewController

@property (nonatomic, strong) NSNumber *userId;

@end
