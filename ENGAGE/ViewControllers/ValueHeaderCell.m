//
//  ValueHeaderCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ValueHeaderCell.h"

@implementation ValueHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFontBold:22];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.minimumScaleFactor = 0.7;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    self.descriptionLabel.textColor = [UIColor appTextColor];
    self.descriptionLabel.font = [UIFont appFont:17];
    
    [self.customBackgroundView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2f]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.imageView.image = nil;
    self.titleLabel.text = nil;
    self.descriptionLabel.text = nil;
    
    self.descriptionLabel.hidden = NO;
}

@end
