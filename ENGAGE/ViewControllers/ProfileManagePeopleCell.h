//
//  ProfileManagePeopleCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/26/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <SWTableViewCell/SWTableViewCell.h>
#import "ENRoundedImageView.h"
#import "ENCircleImageView.h"

@interface ProfileManagePeopleCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet ENCircleImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end
