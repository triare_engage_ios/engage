//
//  ProfileTapsButtonsCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 17.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENButton.h"

@interface ProfileTapsButtonsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ENButton *givenButton; //recieved
@property (weak, nonatomic) IBOutlet ENButton *recievedButton; //given

@property (nonatomic, copy) void (^onChangeSort)(BOOL received);

-(void) isReceived:(BOOL) isReseived;

@end
