//
//  GiveTapNavController.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiveTapNavController : UINavigationController

@property (nonatomic, strong) TeamUser *user;
@property (nonatomic, strong) Behavior *behavior;

-(void) setUserId:(NSNumber *) userId;

@end
