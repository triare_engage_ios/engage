//
//  AnswerDetailVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "AnswerDetailVC.h"

#import "ENRoundedLabel.h"

#import "AnswerCell.h"
#import "CustomAnswerHeaderCell.h"
#import "CustomAnswerCell.h"
#import "UITableViewCell+Separator.h"

#import "ENCircleLabel.h"

#import <PNChart/PNChart.h>

#define kPieChartSize 290.0

@interface AnswerDetailVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *backgroundPieView;
@property (nonatomic) PNPieChart *pieChart;
@property (weak, nonatomic) IBOutlet UILabel *headerViewTitle;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonatomic, strong) ENCircleLabel *circleLabel;

@end

@implementation AnswerDetailVC

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupTabelView];
    [self setupPieChart];
    [self setupLabels];
    [self setupHeaderView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionAnswerDetail];
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionAnswerDetail withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Layout

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^ {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    [self.navigationBar showLeftButton];
    
    [self.navigationBar setTitle:NSLocalizedString(@"ANSWERS", @"v1.0")];
    
}

- (void) setupTabelView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupPieChart {

    NSMutableArray *itemsArray = [NSMutableArray new];
    
    int customAnswerColor = 0;
    
    for (int i = 0; i < [self.question notEmptyAnswers].count; i++ ) {
        QuestionAnswer *answer = [self.question notEmptyAnswers][i];
        
        PNPieChartDataItem * pieItem = [PNPieChartDataItem dataItemWithValue:answer.answersCount.floatValue color:[self pieItemColorFromIndex:i]];
        [itemsArray addObject:pieItem];
        
        customAnswerColor = i + 1;
    }
    
    if (self.question.haveCustomAnswersValue) {
        if (self.question.customAnswersCount.integerValue > 0) {
            PNPieChartDataItem * pieItem = [PNPieChartDataItem dataItemWithValue:self.question.customAnswersCount.floatValue color:[self pieItemColorFromIndex:customAnswerColor]];
            [itemsArray addObject:pieItem];
        }
    }
    
    self.pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(0, 0, kPieChartSize, kPieChartSize) items:[itemsArray copy]];
    
    self.pieChart.descriptionTextColor = [UIColor appTextColor];
    self.pieChart.descriptionTextFont = [UIFont appFont:17];
    self.pieChart.showAbsoluteValues = NO;
    self.pieChart.showOnlyValues = YES;
    self.pieChart.duration = 1.5;
    self.pieChart.shouldHighlightSectorOnTouch = NO;
    //For changing radius looks at PNPieChart+RadiusChanges.h
    
    [self.pieChart strokeChart];
    
    [self.backgroundPieView addSubview:self.pieChart];
}

- (void) setupLabels {
    
    //Rounded label
    CGFloat width = CGRectGetWidth(self.pieChart.bounds) / 2;
    CGFloat height = width;
    
    self.circleLabel = [[ENCircleLabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    NSString *number1 = [NSString stringWithFormat:@"%ld", (long)self.question.answeredUsersCount.integerValue];
    NSString *number2 = [NSString stringWithFormat:@"%ld", (long)self.question.usersCount.integerValue];
    NSString *text = NSLocalizedString(@"answers from", @"v1.0");
    
    if (number1.integerValue == 1) {
        text = NSLocalizedString(@"answer from", @"v1.0");
    }
    
    self.circleLabel.text = [NSString stringWithFormat:@"%@\n%@\n%@", number1, text, number2];
    
    self.backgroundPieView.bounds = CGRectMake(0, 0, kPieChartSize, kPieChartSize);
    
    [self.backgroundPieView addSubview:self.circleLabel];
    
    self.circleLabel.center = [self.backgroundPieView convertPoint:self.backgroundPieView.center fromView:self.backgroundPieView.superview];
    
    //Title label
    self.headerViewTitle.textColor = [UIColor appTextColor];
    self.headerViewTitle.font = [UIFont appFont:17];
    self.headerViewTitle.text = self.question.body;
}

- (void) setupHeaderView {
    CGFloat headerViewheight = 0;
    headerViewheight += 20;
    headerViewheight += 290;
    headerViewheight += 10;
    headerViewheight += 10;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFont:17];
    l.text = self.question.body;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake([UIScreen mainScreen].bounds.size.width - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    headerViewheight += height;
    
    self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, headerViewheight);
}

#pragma mark - Helpers Methods

-(UIColor *) pieItemColorFromIndex:(int) index {
    
    switch (index) {
        case 0:
            return [UIColor appPieChartBlueColor];
            break;
            
        case 1:
            return [UIColor appPieChartGreenColor];
            break;
            
        case 2:
            return [UIColor appPieChartOrangeColor];
            break;
            
        case 3:
            return [UIColor appPieChartDarkOrangeColor];
            break;
            
        case 4:
            return [UIColor appPieChartDarkRedColor];
            break;
            
        default:
            return [UIColor appPieChartBlueColor];
            break;
    }
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!self.question.haveCustomAnswersValue) {
        return 1;
    }
    else {
        if (self.question.customAnswers.array.count == 0) {
            return 1;
        }
        else {
           return 2;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        if (self.question.haveCustomAnswersValue) {
            return [self.question notEmptyAnswers].count + 1;
        }
        else {
            return [self.question notEmptyAnswers].count;
        }
    }
    else {
        return self.question.customAnswers.array.count + 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        if (self.question.haveCustomAnswersValue) {
            if (indexPath.row == [self.question notEmptyAnswers].count) {
                return 50;
            }
        }
        else {
            QuestionAnswer *answer = [self.question notEmptyAnswers][indexPath.row];
            return [AnswerCell cellHeight:answer.title width:tableView.frame.size.width];
        }
        
    }
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 50;
        }
        else {
            QuestionCustomAnswer *answer = self.question.customAnswers.array[indexPath.row - 1];
            return [CustomAnswerCell cellHeight:answer.title width:tableView.frame.size.width];
        }
    }
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        if (self.question.haveCustomAnswersValue && indexPath.row == [self.question notEmptyAnswers].count) {
            
            AnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"answerCell"];
            cell.circleColor = [self pieItemColorFromIndex:(int)indexPath.row];
            cell.titleLabel.text = NSLocalizedString(@"Custom answers", @"v1.0");
            
            [cell showSeparatorLine];
            
            return cell;
            
        }
        else {
            QuestionAnswer *answer = [self.question notEmptyAnswers][indexPath.row];
            
            AnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"answerCell"];
            cell.circleColor = [self pieItemColorFromIndex:(int)indexPath.row];
            cell.titleLabel.text = answer.title;
            
            if (!self.question.haveCustomAnswersValue && indexPath.row == [self.question notEmptyAnswers].count - 1) {
                [cell showSeparatorLine];
            }
            
            return cell;
        }
    }
    else {
        if (indexPath.row == 0) {
            CustomAnswerHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
            cell.titleLabel.text = NSLocalizedString(@"CUSTOM ANSWERS", @"v1.0");
            
            return cell;
        }
        else {
            QuestionCustomAnswer *answer = self.question.customAnswers.array[indexPath.row - 1];
            
            CustomAnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customAnswerCell"];
            cell.titleLabel.text = answer.title;
            [cell showSeparatorLine];
            
            return cell;
        }
    }
    
    return nil;
    
}




@end
