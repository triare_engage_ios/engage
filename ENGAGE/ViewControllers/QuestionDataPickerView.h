//
//  QuestionDataPickerView.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENButton.h"

@interface QuestionDataPickerView : UIView
@property (weak, nonatomic) IBOutlet UIDatePicker *dataPicker;
@property (weak, nonatomic) IBOutlet ENButton *cancelButton;
@property (weak, nonatomic) IBOutlet ENButton *okButton;

@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *startDate;

@property (nonatomic, copy) void (^onCancelAction)();
@property (nonatomic, copy) void (^onOkAction)(NSDate *date);

@end
