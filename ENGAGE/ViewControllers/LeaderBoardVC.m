//
//  LeaderBoardVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 18.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "LeaderBoardVC.h"
#import "UITableViewCell+Separator.h"
#import "LeaderBoardCell.h"
#import "LeaderBoardBehaviorCell.h"
#import "AKLineMenuView.h"

#import "LeaderBoardFilterView.h"
#import "ProfileVC.h"

#import "LeaderBoardBehaviorListVC.h"
#import "ENPlaceholderView.h"

#import "AnalyticsManager.h"

typedef enum : NSUInteger {
    LeadListStateTapsRecieved,
    LeadListStateTapsGiven,
    LeadListStateBehaviors,
} LeadListState;

#define MAX_HIDE_LIST_POSITION 5
#define TOP_ITEMS_BEFORE_HIDE 3

@interface LeaderBoardVC () <AKLineMenuViewDelegate, LeaderBoardFilterDelegate, NavigationBarSearchDelegate>

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;

@property (weak, nonatomic) IBOutlet UIView *tapsButtonsBar;
@property (weak, nonatomic) IBOutlet UIButton *givenButton;
@property (weak, nonatomic) IBOutlet UIButton *receivedButton;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSArray *leadPositions;
@property BOOL showAllListState;
@property BOOL isNeedShowAllList;

@property (weak, nonatomic) IBOutlet ContentTableView *tableView;

@property (weak, nonatomic) IBOutlet AKLineMenuView *lineMenu;

@property LeadListState listSatate;

@property BOOL isLoadTapsData;
@property BOOL isLoadBehaviorsData;

@property (weak, nonatomic) IBOutlet UIView *filterViewArea;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterAreaHeight;

@property (strong, nonatomic) LeaderBoardFilterView *filterView;
@property BOOL isFilteredData;

@property (nonatomic, strong) NSString *searchText;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) ENPlaceholderView *tablePlaceholder;

@end

@implementation LeaderBoardVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    [self prepareNavigationBar];
    
    //TODO: Find way to fix this
    CGRect lineMenu = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    
    self.lineMenu.delegate = self;
    self.lineMenu.bounds = lineMenu;
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.lineMenu.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.lineMenu addSubview:blurEffectView];
    [self.lineMenu sendSubviewToBack:blurEffectView];
    
    [self.lineMenu addMenuItems:@[NSLocalizedString(@"TAPS RECEIVED",nil), NSLocalizedString(@"TAPS GIVEN",nil),NSLocalizedString(@"BEHAVIOR",nil)]];
    
    [self addTableTopInset:self.lineMenu.frame.size.height];
    
    self.listSatate = LeadListStateTapsRecieved;
    
    
    [self prepareFilter];
    
    [self loadDataIfNeed];
    [self addDownRefresh];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf createTablePlaceholder];
    });
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self trackScreenIsFirebase:NO];
}

-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self trackScreenIsFirebase:YES];
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.filterView.fromButton.layer.borderWidth = 1;
    self.filterView.fromButton.layer.cornerRadius = self.filterView.fromButton.bounds.size.height/2;
    self.filterView.fromButton.clipsToBounds = YES;
    
    self.filterView.toButton.layer.borderWidth = 1;
    self.filterView.toButton.layer.cornerRadius = self.filterView.fromButton.bounds.size.height/2;
    self.filterView.toButton.clipsToBounds = YES;
}

-(void) addTableTopInset:(CGFloat) topInset {
    UIEdgeInsets insets = self.tableView.contentInset;
    insets.top += topInset;
    self.tableView.contentInset = insets;
    
}

- (void) createTablePlaceholder {
    
    self.tablePlaceholder = [ENPlaceholderView createPlaceholderWithText:NSLocalizedString(@"No result found", @"v1.0") andFont:[UIFont appFont:17]];
    
    self.tablePlaceholder.hidden = YES;
    [self.tableView addSubview:self.tablePlaceholder];
    
}

#pragma mark - Prepare Methods

-(void) prepareNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf showFilter];
    };
    [self.navigationBar setTitle:NSLocalizedString(@"LEADERBOARD", @"v1.0")];
    [self.navigationBar showLeftButton];
    [self.navigationBar changeLeftButtonIcon:[UIImage imageNamed:@"icon-filter"]];
    [self.navigationBar showRightButton];
    self.navigationBar.searchDelegate = self;
    
}

#pragma mark - Action

-(void) showUserProfile:(LeaderUser *) user {
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    vc.userId = user.userId;
    vc.profileTitle = user.fullName;
    
    [self showViewController:vc sender:nil];
}

-(void) showGivenList {
    self.listSatate = LeadListStateTapsGiven;
    
    [self.lineMenu selectItemAtIndex:0 animate:YES];
}

#pragma mark - Data

-(void) loadData {
    
    self.isLoadTapsData = NO;
    self.isLoadBehaviorsData = NO;
    
    [self loadDataIfNeed];
}

-(void) loadDataIfNeed {
    
    if (self.listSatate == LeadListStateBehaviors) {
        if (!self.isLoadBehaviorsData) {
            [self loadBehaviorsData];
        }
    } else {
        if (!self.isLoadTapsData) {
            [self loadTapsData];
        }
    }
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

-(void) loadTapsData {
    
    __weak typeof(self) weakSelf = self;
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] getUserLeaderboard:[self filterDates] onCompleate:^(BOOL success, NSArray *list, NSString *errorMessage) {
        if (success) {
            [DataManager saveLeaderUserList:list];
        }
        
        weakSelf.isLoadTapsData = YES;
        
        [weakSelf reloadData];
        
        [weakSelf hideLoadingView];
        
        if (!success) {
            [weakSelf.refreshControl endRefreshing];
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
        }
    }];
}

-(void) loadBehaviorsData {
    
    __weak typeof(self) weakSelf = self;
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] getBehaviorLeaderboard:[self filterDates] onCompleate:^(BOOL success, NSArray *list, NSString *errorMessage) {
        if (success) {
            [DataManager saveLeaderBehaviorList:list];
        }
        
        weakSelf.isLoadBehaviorsData = YES;
        
        [weakSelf reloadData];
        
        [weakSelf hideLoadingView];
        
        if (!success) {
            [weakSelf.refreshControl endRefreshing];
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
        }
    }];
}

-(NSInteger) userLeadPosition {
    NSInteger p = -1;
    if (self.leadPositions.count > 0) {
        p = [self.leadPositions indexOfObject:[DataManager user].userId];
    }
    return p;
}

-(void) saveLeadPositions:(NSFetchRequest *) request {
    
    NSManagedObjectContext *context = [DataManager context];
    
    [request setResultType:NSDictionaryResultType];
    [request setReturnsDistinctResults:YES];
    [request setPropertiesToFetch:@[@"userId"]];
    
    
    NSError *errorForObject;
    NSArray *objects = [context executeFetchRequest:request error:&errorForObject];
    
    NSMutableArray *ids = [NSMutableArray array];
    
    for (NSDictionary *i in objects) {
        [ids addObject:[i valueForKey:@"userId"]];
    }

    self.leadPositions = ids;
}

#pragma mark - FetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    if (self.listSatate == LeadListStateBehaviors) {
        _fetchedResultsController = [self behaviorsFetchedResultsController];
    } else {
        _fetchedResultsController = [self tapsFetchedResultsController];
    }

    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }

    return _fetchedResultsController;
}

- (NSFetchedResultsController *)tapsFetchedResultsController {
    
    self.showAllListState = YES;

    NSMutableArray *predicates = [NSMutableArray array];
    
    NSString *sort = @"givenTapsCount";
    if (self.listSatate == LeadListStateTapsRecieved) sort = @"receivedTapsCount";
    
    NSManagedObjectContext *context = [DataManager context];
    
    NSEntityDescription *entity = [NSEntityDescription  entityForName:@"LeaderUser" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:sort ascending:NO],
                                [NSSortDescriptor sortDescriptorWithKey:@"fullName" ascending:YES]];
    [request setEntity:entity];
    
    if (self.searchText || self.isFilteredData) {
        self.isNeedShowAllList = YES;
    }
    
    if (self.searchText.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"fullName CONTAINS[cd] %@ ", self.searchText];
        [predicates addObject:searchPredicate];
    }
    
    
    [self saveLeadPositions:[request copy]];
    
    if (!self.isNeedShowAllList) {
        
        NSInteger userPosition = [self userLeadPosition];
        if (userPosition > MAX_HIDE_LIST_POSITION) {
            
            self.showAllListState = NO;
            
            NSMutableArray *ids = [NSMutableArray array];
            
            [ids addObjectsFromArray:[self.leadPositions subarrayWithRange:NSMakeRange(0, TOP_ITEMS_BEFORE_HIDE)]];
            if (userPosition < self.leadPositions.count-1) {
                [ids addObjectsFromArray:[self.leadPositions subarrayWithRange:NSMakeRange(userPosition, self.leadPositions.count-userPosition)]];
            } else {
                [ids addObject:[self.leadPositions lastObject]];
            }
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId IN %@", ids];
            [predicates addObject:predicate];
        }
    }
    
    if (predicates.count) {
        request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    
    return [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                               managedObjectContext:context
                                                 sectionNameKeyPath:nil
                                                          cacheName:nil];
}

- (NSFetchedResultsController *)behaviorsFetchedResultsController {
    
    self.showAllListState = YES;
    
    NSManagedObjectContext *context = [DataManager context];
    
    NSEntityDescription *entity = [NSEntityDescription  entityForName:@"LeaderBehavior" inManagedObjectContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"tapsCount" ascending:NO],
                                [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES]];
    [request setEntity:entity];
    
    if (self.searchText.length) {
        request.predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", self.searchText];
    }
        
    return [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                               managedObjectContext:context
                                                 sectionNameKeyPath:nil
                                                          cacheName:nil];
}


#pragma mark - UITableView Delegate & DataSource methods

-(NSInteger) objectIndexForPath:(NSIndexPath *)indexPath {
    
    NSInteger index = indexPath.row;
    if (!self.showAllListState) {
        if (index > (TOP_ITEMS_BEFORE_HIDE - 1)) {
            index--;
        }
    }
    return index;
}

-(BOOL) isShowMoreCell:(NSIndexPath *)indexPath {
    
    NSInteger index = indexPath.row;
    if (!self.showAllListState) {
        if (index == TOP_ITEMS_BEFORE_HIDE) {
            return YES;
        }
    }
    
    return NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if (!self.showAllListState) {
        number++;
    }
    
    if (number == 0 && self.navigationBar.barView.searchBar.text.length > 0) {
        self.tablePlaceholder.hidden = NO;
    }
    else {
        self.tablePlaceholder.hidden = YES;
    }

    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listSatate == LeadListStateBehaviors) {
        return [self behaviorCell:tableView indexPath:indexPath];
    } else {
        if ([self isShowMoreCell:indexPath]) {
            return [self moreCell:tableView indexPath:indexPath];
        } else {
            return [self userCell:tableView indexPath:indexPath];
        }
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self isShowMoreCell:indexPath]) {
        return 50;
    }
    
    return 75;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (self.listSatate != LeadListStateBehaviors) {
        if ([self isShowMoreCell:indexPath]) {
            self.isNeedShowAllList = YES;
            [self reloadData];
        } else {
            LeaderUser *u = [self.fetchedResultsController.fetchedObjects objectAtIndex:[self objectIndexForPath:indexPath]];
            [self showUserProfile:u];
        }
    }
    else if (self.listSatate == LeadListStateBehaviors) {
        LeaderBehavior *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
        
        LeaderBoardBehaviorListVC * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LeaderBoardBehaviorListVC"];
        vc.behaviorId = item.behaviorId;
        vc.behaviorTitle = item.title;
        vc.iconUrl = item.iconUrl;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.navigationBar.barView.searchBar resignFirstResponder];
}

#pragma mark - Cells

-(LeaderBoardMoreCell *) moreCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    LeaderBoardMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"moreCell"];
    [cell showSeparatorLine];
    return cell;
}

-(LeaderBoardCell *) userCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    LeaderUser *u = [self.fetchedResultsController.fetchedObjects objectAtIndex:[self objectIndexForPath:indexPath]];
    
    LeaderBoardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.nameLabel.text = u.fullName;
    cell.jobLabel.text = u.jobTitle;
    [cell.userImageView sd_setImageWithURL:[AppTemplate mediumIconURL:u.avatarUrl] placeholderImage:[TeamUser placeholder]];
    
    NSNumber *points = u.givenTapsCount;
    if (self.listSatate == LeadListStateTapsRecieved) points = u.receivedTapsCount;
    cell.pointsLabel.text = [NSString stringWithFormat:@"%@", points];
    
    NSInteger leadPosition = [self.leadPositions indexOfObject:u.userId];
    
    cell.leadPositionLabel.text = [NSString stringWithFormat:@"%d", (int)(leadPosition + 1)];
    
    if (u.isPointWinnerValue) {
        cell.userBorderColor = [UIColor avatarBorderColor];
        cell.userBorderWidth = [NSNumber numberWithFloat:3.0];
    }
    
    [cell showSeparatorLine];
    
    return cell;
}

-(LeaderBoardBehaviorCell *) behaviorCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    LeaderBehavior *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
    
    LeaderBoardBehaviorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"behaviorCell"];
    cell.cellTitleCell.text = item.title;
    [cell.cellImageView sd_setImageWithURL:[AppTemplate mediumIconURL:item.iconUrl] placeholderImage:[Behavior placeholder]];
    
    cell.leadLabel.text = [NSString stringWithFormat:@"%d", (int)(indexPath.row + 1)];
    cell.pointsLabel.text = [NSString stringWithFormat:@"%@", item.tapsCount];
    
    [cell showSeparatorLine];
    
    return cell;
}

#pragma mark AKLineMenuView

- (void)lineMenu:(AKLineMenuView *) menu didSelectItem:(NSInteger)itemIndex {

    self.listSatate = itemIndex;
    [self trackScreen];
    
    [self loadDataIfNeed];
    
    [self reloadData];
    
    [self.tableView scrollTableToTop:YES];
    
    [self checkSearchPlaceholder];
}

#pragma mark Filter

#define FILTER_HEIGHT 300
#define FILTER_HEIGHT_ACTIVE 45
#define FILTER_ANIMATION_DURATION .2


-(NSDictionary *) filterDates {
    if (!self.filterViewArea.hidden) {
        if (self.filterView.fromDate && self.filterView.toDate) {
            return @{@"from":self.filterView.fromDate, @"to":self.filterView.toDate};
        }
    }
    
    return nil;
}

-(void) prepareFilter {
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.filterViewArea.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.filterViewArea addSubview:blurEffectView];
    [self.filterViewArea sendSubviewToBack:blurEffectView];
    
    self.filterViewArea.hidden = YES;
    self.filterViewArea.backgroundColor = [UIColor clearColor];
    self.filterViewArea.clipsToBounds = YES;
    self.filterAreaHeight.constant = 0;
    
    self.filterView = [[NSBundle mainBundle] loadNibNamed:@"LeaderBoardFilterView" owner:self options:nil].lastObject;
    CGRect f = self.filterView.frame;
    f.size.width = self.filterViewArea.bounds.size.width;
    self.filterView.frame = f;
    self.filterView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.filterViewArea addSubview:self.filterView];
    self.filterView.delegate = self;
}

-(void) cancelFilter {
    if (!self.filterViewArea.hidden) {
        [AnalyticsManager trackEvent:ENAnalyticCategoryLeaderboard action:ENAnalyticActionFilterData label:ENAnalyticLabelClose];
        [self addTableTopInset:-FILTER_HEIGHT_ACTIVE];
        
        self.filterAreaHeight.constant = 0;
        
        [self.filterViewArea setNeedsLayout];
        [UIView animateWithDuration:FILTER_ANIMATION_DURATION
                         animations:^{
                             [self.filterViewArea layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             self.filterViewArea.hidden = YES;
                             self.filterView.state = FilterStateNew;
                             
                             self.tableView.hidden = NO;
                             
                         }];
    }
    
    if (self.isFilteredData) {
        self.isFilteredData = NO;
        [self loadData];
    }
}


-(void) showFilter {
    
    if (self.filterViewArea.hidden) {
        [AnalyticsManager trackEvent:ENAnalyticCategoryLeaderboard action:ENAnalyticActionFilterData label:ENAnalyticLabelOpen];
        self.tableView.hidden = YES;
        
        self.filterViewArea.hidden = NO;
        
        self.filterAreaHeight.constant = FILTER_HEIGHT;
        
        [self.filterViewArea setNeedsLayout];
        [UIView animateWithDuration:FILTER_ANIMATION_DURATION
                         animations:^{
                             [self.filterViewArea layoutIfNeeded];
                         } completion:^(BOOL finished) {
                         }];
        
        self.tableView.hidden = YES;
        [self addTableTopInset:FILTER_HEIGHT_ACTIVE];
        
    } else {
        [self cancelFilter];
    }
}

- (void)leaderBoardFilteDidApplyFilter:(LeaderBoardFilterView *)view {
    self.filterAreaHeight.constant = 45;
    
    [self.filterViewArea setNeedsLayout];
    [UIView animateWithDuration:FILTER_ANIMATION_DURATION animations:^{
                         [self.filterViewArea layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         self.tableView.hidden = NO;
                     }];
    
    
    self.isFilteredData = YES;
    
    self.isLoadTapsData = NO;
    self.isLoadBehaviorsData = NO;
    
    [self loadDataIfNeed];
}
- (void)leaderBoardFilteDidCancelFilter:(LeaderBoardFilterView *)view {
    [self cancelFilter];
}

- (void)leaderBoardFilteOnSelectDate:(LeaderBoardFilterView *)view {
    
    self.tableView.hidden = YES;
    
    self.filterAreaHeight.constant = FILTER_HEIGHT;
    
    [self.filterViewArea setNeedsLayout];
    [UIView animateWithDuration:FILTER_ANIMATION_DURATION
                     animations:^{
                         [self.filterViewArea layoutIfNeeded];
                     } completion:^(BOOL finished) {
                     }];
}


#pragma mark NavigationBarSearchDelegate

-(void) checkSearchPlaceholder {
    if (self.listSatate == LeadListStateTapsRecieved | self.listSatate == LeadListStateTapsGiven) {
        self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by username", @"v1.0");
    } else {
        self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by behavior title", @"v1.0");
    }
}

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    self.searchText = nil;
    [self reloadData];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [self checkSearchPlaceholder];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    self.searchText = searchText;
    
    [self reloadData];
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadData];
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

- (void) trackScreenIsFirebase:(BOOL) firebase {
    
    if (firebase) {
        switch (self.listSatate) {
            case LeadListStateTapsGiven:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardTapsGiven withScreenClass:NSStringFromClass([self class])];
                break;
                
            case LeadListStateTapsRecieved:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardTapsReceived withScreenClass:NSStringFromClass([self class])];
                break;
                
            case LeadListStateBehaviors:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehavior withScreenClass:NSStringFromClass([self class])];
                break;
        }
        
    }
    else {
        switch (self.listSatate) {
            case LeadListStateTapsGiven:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardTapsGiven];
                break;
                
            case LeadListStateTapsRecieved:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardTapsReceived];
                break;
                
            case LeadListStateBehaviors:
                [AnalyticsManager trackScreen:ENAnalyticScreenLeaderBoardBehavior];
                break;
        }
        
        
    }
    
    
}

- (void) trackScreen {
    
    [self trackScreenIsFirebase:YES];
    [self trackScreenIsFirebase:NO];
    
}

@end
