//
//  ManagePeopleCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <SWTableViewCell/SWTableViewCell.h>
#import "ENCircleImageView.h"

@interface ManagePeopleCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet ENCircleImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet UIButton *switcherButton;

@property (nonatomic, copy) void (^onAction)(NSInteger state, ManagePeopleCell *cell);

@end
