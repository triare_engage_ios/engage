//
//  QuestionActionCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENButton.h"

@interface QuestionActionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENButton *ActionButton;

@property (nonatomic, copy) void (^onAction)();

@end
