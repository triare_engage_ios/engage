//
//  ProfileSortPeriodInfoCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 17.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileSortPeriodInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *periodLabel;
@end
