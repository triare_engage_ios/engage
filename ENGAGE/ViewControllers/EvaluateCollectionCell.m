//
//  EvaluateCollectionCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "EvaluateCollectionCell.h"
#import "UIView+Extends.h"

@implementation EvaluateCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeAppRoundedView:self.cellImageView];
    //[UIView makeAppRoundedView:self.evaluateView];
    
    self.cellLabel.textColor = [UIColor appTextColor];
    self.cellLabel.font = [UIFont appFontThin:16];
}

-(void)prepareForReuse {
    [super prepareForReuse];
    
    [self evaluateState:0];
}

+ (CGSize) sizeForCollectionWidth:(CGFloat) width {
    CGFloat cellWidth = width/3;
    return CGSizeMake(cellWidth, cellWidth+30);
}

-(void) evaluateState:(NSInteger) state {
    if (state != 0) {
        self.evaluateView.hidden = NO;
        self.cancelEvaluateButton.hidden = NO;
    } else {
        self.evaluateView.hidden = YES;
        self.cancelEvaluateButton.hidden = YES;
    }
    
    if (state < 0) {
        self.evaluateLabel.text = @"-";
    }
    
    if (state > 0) {
        self.evaluateLabel.text = @"+";
    }
}

- (IBAction)cancelAction:(id)sender {
    
    [self evaluateState:0];
    
    if (self.onCancelEvaluate) {
        self.onCancelEvaluate();
    }
}

@end
