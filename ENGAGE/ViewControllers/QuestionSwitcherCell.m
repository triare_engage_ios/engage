//
//  QuestionSwitcherCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionSwitcherCell.h"

@interface QuestionSwitcherCell ()
@property (weak, nonatomic) IBOutlet UISwitch *switcher;

@end

@implementation QuestionSwitcherCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFontBold:17];
    
    self.subtitleLabel.textColor = [UIColor appTextColor];
    self.subtitleLabel.font = [UIFont appFont:10];
    
    self.switcher.onTintColor = [UIColor appOrangeColor];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.titleLabel.text = nil;
    self.subtitleLabel.text = nil;
    
    self.onAction = nil;
    
    self.titleLabelTopSpace.constant = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchChanges:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.switcher.isOn) {
        if (weakSelf.onAction) {
            weakSelf.onAction (1);
        }
    }
    else {
        if (weakSelf.onAction) {
            weakSelf.onAction (0);
        }
    }
}

- (void) changeSwitcherStateEnable:(BOOL)state {
    [self.switcher setEnabled:state];
}

- (void) changeSwitcherToOn:(BOOL) isOn {
    [self.switcher setOn:isOn];
}

@end
