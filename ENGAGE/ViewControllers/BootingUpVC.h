//
//  BootingUpVC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Company.h"

@interface BootingUpVC : UIViewController

@property (nonatomic, strong) Company *company;

@end
