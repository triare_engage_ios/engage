//
//  CompanyValuesListVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "CompanyValuesListVC.h"

#import "UIViewController+LoadingView.h"
#import "UITableViewCell+Separator.h"

#import "CompanyValuesCell.h"

@interface CompanyValuesListVC () <UITableViewDelegate, UITableViewDataSource, NavigationBarSearchDelegate>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) NSString *searchText;

@end

@implementation CompanyValuesListVC


#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self addDownRefresh];
    [self loadData];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenValueCompanyList];
    [AnalyticsManager trackScreen:ENAnalyticScreenValueCompanyList withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:[NSLocalizedString(@"company values", @"v1.0") uppercaseString]];
    [self.navigationBar showRightButton];
    self.navigationBar.searchDelegate = self;
    
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

#pragma mark - Data

- (void) loadData {
    
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getValuesTitlesAndIdsSuccess:^(NSArray *list) {
        [DataManager saveValueCompanyList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf showAlertWithText:message];
    }];
    
}

- (void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.contentTableView reloadData];
}

#pragma mark - Navigation

- (void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSMutableArray *predicates = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [CompanyValue MR_requestAllSortedBy:@"title" ascending:YES inContext:[DataManager context]];
    
    if (self.searchText.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@ ", self.searchText];
        [predicates addObject:searchPredicate];
    }
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [CompanyValue MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}


#pragma mark - Helpers Methods

- (void) showAlertWithText:(NSString*) text {
    
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:NSLocalizedString(@"Profile update", @"v1.0") text:text];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CompanyValue *value = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    CompanyValuesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"valueCell"];
    
    cell.titleLabel.text = value.title;
    [cell showSeparatorLine];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CompanyValue *value = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onBackAction) {
        weakSelf.onBackAction(value.title, value.valueId);
    }
    
    [self closeVC];
}

#pragma mark - NavigationBarSearchDelegate

-(void) checkSearchPlaceholder {
    self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by Values", @"v1.0");
}

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    self.searchText = nil;
    [self reloadData];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [self checkSearchPlaceholder];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    self.searchText = searchText;
    
    [self reloadData];
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.contentTableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadData];
}




@end
