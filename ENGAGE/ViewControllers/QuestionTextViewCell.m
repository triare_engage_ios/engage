//
//  QuestionTextViewCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionTextViewCell.h"

@interface QuestionTextViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation QuestionTextViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backgroundImageView.image = [[UIImage imageNamed:@"message-textBackground.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    self.textView.font = [UIFont appFontThin:17];
    self.textView.textColor = [UIColor blackColor];
    
    self.textView.placeholderText = NSLocalizedString(@"Enter your question...", @"v1.0");
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.backgroundImageView.layer.borderColor = [UIColor appTextColor].CGColor;
    self.backgroundImageView.layer.borderWidth = 1.0;
    self.backgroundImageView.layer.cornerRadius = 10.0;
    self.backgroundImageView.clipsToBounds = YES;
}



@end
