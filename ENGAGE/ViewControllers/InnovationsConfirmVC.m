//
//  InnovationsConfirmVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "InnovationsConfirmVC.h"

#import "InnovationsConfirmCell.h"
#import "InnovationsActionCell.h"

#import <SVProgressHUD/SVProgressHUD.h>

typedef enum : NSUInteger {
    CellItemTypeAnswer,
    CellItemTypeAction
} CellItemType;

@interface InnovationsConfirmVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (nonatomic, strong) NSArray *itemsCell;

@end

@implementation InnovationsConfirmVC

#pragma mark - Public methods

+ (NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self prepareAnswerForState:self.screenState];
    [self prepareNavigationBarState:self.screenState];
    [self prepareHeaderView];
    [self prepareTableView];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Prepare Methods

- (void) prepareAnswerForState:(InnovationConfirmScreenState) state {
    if (state == InnovationConfirmScreenStateStep1) {
        self.answer = [[InnovationConfirmAnswer alloc] init];
    }
}

- (void) prepareNavigationBarState:(InnovationConfirmScreenState) screenState {
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:[NSLocalizedString(@"innovations", @"v1.0") uppercaseString]];
    
    if (screenState == InnovationConfirmScreenStateStep2) {
        [self.navigationBar showLeftButton];
        self.navigationBar.onLeftAction = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        return;
    }
    
    if (screenState == InnovationConfirmScreenStateStep3) {
        [self.navigationBar showLeftButton];
        self.navigationBar.onLeftAction = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        return;
    }
}

-(void) prepareHeaderView {
    self.headerTitleLabel.font = [UIFont appFont:15];
    self.headerTitleLabel.textColor = [UIColor appTextColor];
    self.headerTitleLabel.text = NSLocalizedString(@"Have you performed your plans?", @"v1.0");
}

- (void) prepareTableView {
    [self.tableView registerNib:[UINib nibWithNibName:[InnovationsConfirmCell cellNimbName] bundle:nil] forCellReuseIdentifier:[InnovationsConfirmCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[InnovationsActionCell cellNimbName] bundle:nil] forCellReuseIdentifier:[InnovationsActionCell cellReuseIdentifier]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets.bottom = 0;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Data

-(NSArray *) itemsCell {
    if (_itemsCell == nil) {
        NSArray *result = @[@(CellItemTypeAnswer), @(CellItemTypeAction)];
        _itemsCell = result;
    }
    
    return _itemsCell;
}

-(void) reloadData {
    self.itemsCell = nil;
    [self.tableView reloadData];
}

#pragma mark - Table View Delegate & Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsCell.count;
}


#define kTempText @"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32."

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *itemNumber = self.itemsCell[indexPath.row];
    CellItemType item = itemNumber.integerValue;
    
    if (item == CellItemTypeAction) {
        return [InnovationsActionCell cellHeight];
    }
    
    //TODO: Change logic for Height using text
    if (item == CellItemTypeAnswer) {
        if (self.screenState == InnovationConfirmScreenStateStep1) {
            return [InnovationsConfirmCell cellHeightForText:kTempText withTableWidth:tableView.frame.size.width];
        }
        else if (self.screenState == InnovationConfirmScreenStateStep2) {
            return [InnovationsConfirmCell cellHeightForText:kTempText withTableWidth:tableView.frame.size.width];
        }
        else {
            return [InnovationsConfirmCell cellHeightForText:kTempText withTableWidth:tableView.frame.size.width];
        }
    }
    
    return 80; //Default Value, not used
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *itemNumber = self.itemsCell[indexPath.row];
    CellItemType item = itemNumber.integerValue;
    
    if (item == CellItemTypeAction) {
        return [self actionCell:tableView atIndexPath:indexPath screenState:self.screenState];
    }
    else if (item == CellItemTypeAnswer) {
        return [self descriptionConfirmCell:tableView atIndexPath:indexPath screenState:self.screenState];
    }
    
    return nil;
    
}

#pragma mark - Cells

-(InnovationsActionCell *) actionCell:(UITableView *) tableView atIndexPath:(NSIndexPath *) indexPath screenState:(InnovationConfirmScreenState) state {
    
    __weak typeof(self) weakSelf = self;
    
    InnovationsActionCell *cell = [tableView dequeueReusableCellWithIdentifier:[InnovationsActionCell cellReuseIdentifier]];
    
    [cell applyButtonsLeftTitle:[NSLocalizedString(@"no", @"v1.0") uppercaseString] andRightTitle:[NSLocalizedString(@"yes", @"v1.0") uppercaseString]];
    
    
    cell.onAction = ^(NSInteger actionState, InnovationsActionCell *returnedCell) {
        [weakSelf changePlansStateToFinished:[NSNumber numberWithInteger:actionState].boolValue forScreenState:weakSelf.screenState];
    };
    
    return cell;
}

-(InnovationsConfirmCell *) descriptionConfirmCell:(UITableView *) tableView atIndexPath:(NSIndexPath *) indexPath screenState:(InnovationConfirmScreenState) state {
    
    InnovationsConfirmCell *cell = [tableView dequeueReusableCellWithIdentifier:[InnovationsConfirmCell cellReuseIdentifier]];
    
    switch (state) {
        case InnovationConfirmScreenStateStep1:
            [cell applyCellStyle:InnovationsConfirmCellStyleStart];
            break;
            
        case InnovationConfirmScreenStateStep2:
            [cell applyCellStyle:InnovationsConfirmCellStyleStop];
            break;
            
        case InnovationConfirmScreenStateStep3:
            [cell applyCellStyle:InnovationsConfirmCellStyleNeed];
            break;
    }
    
    //TODO: Make logic fot text desription
    [cell setInnovationText:kTempText];
    
    return cell;
}

#pragma mark - Action

- (void) changePlansStateToFinished:(BOOL) finished forScreenState:(InnovationConfirmScreenState) state {
    [self isInnovationFinished:finished forState:state];
    [self showAlertForInnovationPlan:finished];
    [self showNextStepForState:state];
}

- (void) isInnovationFinished:(BOOL) finished forState:(InnovationConfirmScreenState) state {
    
    switch (state) {
        case InnovationConfirmScreenStateStep1:
            self.answer.isStartDoingDone = finished;
            break;
            
        case InnovationConfirmScreenStateStep2:
            self.answer.isStopDoingDone = finished;
            break;
            
        case InnovationConfirmScreenStateStep3:
            self.answer.isNeedDoingDone = finished;
            break;
    }
}

- (void) showNextStepForState:(InnovationConfirmScreenState) state {
    
    if (state == InnovationConfirmScreenStateStep1 || state == InnovationConfirmScreenStateStep2) {
        InnovationsConfirmVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[InnovationsConfirmVC storyboardIdentifier]];
        vc.screenState = state + 1;
        vc.answer = self.answer;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    if (state == InnovationConfirmScreenStateStep3) {
        //TODO: Logic for sending data
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
}

#pragma mark - Alert

- (void) showAlertForInnovationPlan:(BOOL) finished {
    
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    NSString *alertText;
    NSString *firstName = [DataManager user].firstName;
    
    if (finished) {
        alertText = [ [NSString stringWithFormat:@"%@, %@! %@!", NSLocalizedString(@"great job", @"v1.0"), firstName, NSLocalizedString(@"keep it up", @"v1.0")] uppercaseString];
    }
    else {        
        alertText = [NSString stringWithFormat:@"%@, %@!", NSLocalizedString(@"Let’s try to improve next time", @"v1.0"), [firstName uppercaseString]];
    }
    
    [SVProgressHUD showSuccessWithStatus:alertText];
    
}


@end
