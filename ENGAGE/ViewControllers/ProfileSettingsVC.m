//
//  ProfileSettingsVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileSettingsVC.h"
#import "ProfileSettingsUserImageCell.h"
#import "ProfileSettingsTextFieldCell.h"
#import "ProfileSettingsJobTitleCell.h"
#import "ProfileSettingsEmailCell.h"
#import "ProfileSettingsActionsCell.h"
#import "ProfileSettingsJobTitleVC.h"

#import "AppDelegate.h"
#import "AKFileManager.h"

#import "UITableViewCell+Separator.h"
#import "UIViewController+Keyboard.h"
#import "UIImage+Extends.h"
#import "TakePictureVC.h"

#import <SVProgressHUD/SVProgressHUD.h>
#import <RSKImageCropper/RSKImageCropper.h>

#define kFirstNameTextFieldTag 111
#define kSecondNameTextFieldTag 222
#define kJobTitileTextFieldTag 333
#define kEmailTextFieldTag 444
#define kChangePasswordTextField 555
#define kNewPasswordTextField 666
#define kConfirmPasswordTextField 777

#define kDeltaKeyboardMovement 20

#define kChangePasswordStartSymbols @"12345"

@interface ProfileSettingsVC () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonnull, strong) NSArray *cells;

@property (weak, nonatomic) UITextField *activeField;

//Properties, that capture current (NOT saved) data of cells
@property (nonatomic, strong) TeamUser *user;

@property (nonatomic, strong) NSString *avatarUrl;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *currentEmail;
@property (nonatomic, strong) NSString *notConfirmedEmail;
@property (nonatomic, strong) NSString *jobTitle;
@property (nonatomic, strong) NSString *currentPassword;
@property (nonatomic, strong) NSString *confirmPassword;
@property (nonatomic, strong) NSString *confirm2Password;

@property (nonatomic) BOOL isPasswordsRowInserted; //Use for indicate that new rows (confirm password and confirm2password is shown)
@property (nonatomic) BOOL isNeedShowCurrentPasswordRow; //Use for indicate that change password cell renamed to current password

//Edit photo
@property (nonatomic, strong) UIImagePickerController *imagePicker;

@end

@implementation ProfileSettingsVC

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self setupTextField];
    
    [self reloadData];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self addKeyboardNotification];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserSettings];
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileCurrentUserSettings withScreenClass:NSStringFromClass([self class])];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeKeyboardNotification];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    
    self.navigationBar.onRightAction = ^{
        [weakSelf saveChanges];
    };
    
    [self.navigationBar setTitle:NSLocalizedString(@"SETTINGS", @"v1.0")];
    
    [self.navigationBar.barView.rightButton setTitle:[NSString stringWithFormat:@"%@ ", NSLocalizedString(@"Save", @"v1.0")] forState:UIControlStateNormal];
    
    //Prepare right button for text display
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    self.navigationBar.barView.rightButton.titleLabel.font = [UIFont appFont:17];
    [self.navigationBar.barView.rightButton setBackgroundColor:[UIColor clearColor]];
    [self.navigationBar.barView.rightButton setImage:nil forState:UIControlStateNormal];
    //[self.navigationBar showRightButton];
    //[self.navigationBar.barView.rightButton setEnabled:NO];
    [self.navigationBar hideRightButton];
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupTextField {
    //[[UITextField appearance] setTintColor:[UIColor appTextColor]];
}

- (void)reloadData {
    
    self.user = [DataManager teamUserWithId:self.userId];
    
//    self.avatarUrl = [DataManager user].avatarUrl;
//    self.firstName = [DataManager user].firstName;
//    self.lastName = [DataManager user].lastName;
//    self.jobTitle = [DataManager user].jobTitle;
//    self.currentEmail = [DataManager user].email;
    
    self.avatarUrl = self.user.avatarUrl;
    self.firstName = self.user.firstName;
    self.lastName = self.user.lastName;
    self.jobTitle = self.user.jobTitle;
    self.currentEmail = self.user.email;
    //self.currentEmail = [DataManager user].email;
    self.notConfirmedEmail = self.user.unconfirmedEmail;
    
    NSMutableArray *list = [NSMutableArray array];
    [list addObject:@"userImage"]; //0
    [list addObject:@"userFirstName"]; //1
    [list addObject:@"userLastName"]; //2
    [list addObject:@"userJobTitle"]; //3
    [list addObject:@"userEmail"]; //4
    [list addObject:@"userChangePassword"]; //5
    [list addObject:@"userLogout"]; //6
    
    self.cells = list;
    
    [self.contentTableView reloadData];
}

#pragma mark - Changes in Table View

//Add confirm password rows
- (void) addConfirmPasswordData {
    NSMutableArray *list = [self.cells mutableCopy];
//    [list insertObject:@"userNewPassword" atIndex:5];
//    [list insertObject:@"userConfirmNewPassword" atIndex:6];
    [list insertObject:@"userNewPassword" atIndex:6];
    [list insertObject:@"userConfirmNewPassword" atIndex:7];
    
    self.cells = list;
    
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:6 inSection:0]];
    NSArray *paths2 = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:7 inSection:0]];
    
    [self.contentTableView beginUpdates];
    [self.contentTableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationBottom];
    [self.contentTableView insertRowsAtIndexPaths:paths2 withRowAnimation:UITableViewRowAnimationBottom];
    
    [self.contentTableView endUpdates];
}

//Delete confirm password rows
- (void) deleteConfirmPasswordData {
    NSMutableArray *list = [self.cells mutableCopy];
    //    [list removeObjectAtIndex:5];
    //    [list removeObjectAtIndex:6];
    [list removeObjectAtIndex:6];
    [list removeObjectAtIndex:7];
    
    self.cells = list;
    
    NSArray *paths = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:6 inSection:0]];
    NSArray *paths2 = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:7 inSection:0]];
    
    [self.contentTableView beginUpdates];
    [self.contentTableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationTop];
    [self.contentTableView deleteRowsAtIndexPaths:paths2 withRowAnimation:UITableViewRowAnimationTop];
    
    [self.contentTableView endUpdates];
}

//Change left title for row @Change password@, when pressed first time
- (void) updateChangePasswordRow {
    ProfileSettingsTextFieldCell *cell = [self.contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    cell.leftTitle.text = NSLocalizedString(@"Current Password", @"v1.0");
}

//Show checkmark for cell
- (void) showCheckMarkForPasswordCell:(BOOL)needShow {
    ProfileSettingsTextFieldCell *cell = [self.contentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    [cell needShowCheckmark:needShow];
}

//Update image cell after download
- (void) updateImageRow {
    NSArray *path = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]];
    [self.contentTableView reloadRowsAtIndexPaths:path withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showJobTitle"]) {
        __weak typeof(self) weakSelf = self;
        
        ProfileSettingsJobTitleVC *dest = segue.destinationViewController;
        dest.onBackAction = ^(NSString* job) {
            if (job.length > 0) {
                [weakSelf updateJobTitle:job];
            }
        };
        
    }
}

- (void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) editUserImage {
    MLog(@"Change Avatar");
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* takePhoto = [UIAlertAction
                         actionWithTitle:NSLocalizedString (@"Take a photo", @"v1.0")
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self takePhotoAction];
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* photoroll = [UIAlertAction
                             actionWithTitle:NSLocalizedString (@"Choose from photoroll", @"v1.0")
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self chooseFromPhotoRollAction];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* cancel = [UIAlertAction
                                actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleCancel
                                handler:^(UIAlertAction * action)
                                {
                                    [view dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    
    
    [view addAction:takePhoto];
    [view addAction:photoroll];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

- (void) editJobTitle {
    [self performSegueWithIdentifier:@"showJobTitle" sender:nil];
}

#pragma mark - Action

- (void) updateJobTitle:(NSString *)job {
    self.jobTitle = job;
    
    NSArray *jobPath = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:3 inSection:0]];
    
    [self.contentTableView reloadRowsAtIndexPaths:jobPath withRowAnimation:UITableViewRowAnimationNone];
}

//Used when press Save button
- (void) saveChanges {
    
    //First and last name validation
    if ((self.firstName.length == 0 && ![self.firstName isEqualToString:@" "]) || (self.lastName.length == 0 && ![self.lastName isEqualToString:@" "])) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Profile update", @"v1.0") text:NSLocalizedString(@"This field cannot be empety", @"v1.0")];
        return;
    }
    
    //Change password validation
    if (![self.currentPassword isEqualToString:kChangePasswordStartSymbols] && self.isPasswordsRowInserted) {
        if ([self.confirmPassword isEqualToString:@""] || [self.confirm2Password isEqualToString:@""] || self.confirmPassword.length < 6 || self.confirm2Password.length < 6) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Password update", @"v1.0") text:NSLocalizedString(@"New password & password confirmation fields must be at least 6 symbols", @"v1.0")];
            return;
        }
        
        else if (![self.confirmPassword isEqualToString:self.confirm2Password]) {
            [[AppDelegate theApp] showAlert:NSLocalizedString(@"Password update", @"v1.0") text:NSLocalizedString(@"New password & password confirmation fields should contents the same symbols", @"v1.0")];
            return;
        }
    }
    
    //E-mail validation
    if (![self isValidEmail:self.currentEmail]) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Email update", @"v1.0") text:NSLocalizedString(@"Email is not valid", @"v1.0")];
        return;
    }
    
    //Check parrameters, that was changed
    NSDictionary *parameters = [self parametersForUpdate];
    
    //Send request for update
    if ([parameters allKeys].count > 0) {
        __weak typeof(self) weakSelf = self;
        [self showLoadingViewBellowView:self.navigationBar];
        [[RequestManager sharedManager] updateUserWithInfo:parameters success:^(BOOL success, NSDictionary *userInfo) {
            [DataManager updateSomeUserInfo:parameters];
            [weakSelf hideLoadingView];
            [weakSelf.view endEditing:YES];
            //[weakSelf.navigationBar rightButtonEnable:NO];
            [weakSelf.navigationBar hideRightButton];
            [weakSelf showSentAlert];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf closeVC];
            });
        } failure:^(NSString *message) {
            [[AppDelegate theApp] showAlert:NSLocalizedString(@"Profile update", @"v1.0") text:message];
            [weakSelf hideLoadingView];
            [weakSelf.view endEditing:YES];
        }];
    }
}

//Check what need to update and make dictionary for request
- (NSDictionary *) parametersForUpdate {
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    if (![[DataManager user].firstName isEqualToString:self.firstName]) {
        [result setValue:self.firstName forKey:@"first_name"];
    }
    
    if (![[DataManager user].lastName isEqualToString:self.lastName]) {
        [result setValue:self.lastName forKey:@"last_name"];
    }
    
//    if (![[DataManager user].jobTitle isEqualToString:self.jobTitle]) {
//        [result setValue:self.jobTitle forKey:@"job_title"];
//    }
    
    if (self.currentPassword.length > 0 && self.confirmPassword > 0 && self.confirm2Password > 0) {
        [result setValue:self.currentPassword forKey:@"current_password"];
        [result setValue:self.confirmPassword forKey:@"password"];
        [result setValue:self.confirm2Password forKey:@"password_confirmation"];
    }
    
    if (![self.currentEmail isEqualToString:self.user.email]) {
        [result setValue:self.currentEmail forKey:@"email"];
    }
    
    return result;
}

//Logout
- (void) logout {
    [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionLogout label:ENAnalyticLabelSuccess];
    [[[AppDelegate theApp] mainViewController] logoutUser];
}

- (void) chooseFromPhotoRollAction {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //[self presentViewController:self.imagePicker animated:YES completion:nil];
    //[self.navigationController setViewControllers:@[self.imagePicker] animated:NO];
    //[self showViewController:self.imagePicker sender:nil];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void) takePhotoAction {
    TakePictureVC *pic = [[TakePictureVC alloc] initWithNibName:@"TakePictureVC" bundle:nil];
    pic.imageWidth = 500;
    pic.onSelectPicture = ^(UIImage *image) {
        RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeCustom];
        imageCropVC.delegate = self;
        imageCropVC.dataSource = self;
        [[[AppDelegate theApp] mainViewController] hideTabBarView:NO];
        [self.navigationController pushViewController:imageCropVC animated:YES];
    };
    [self presentViewController:pic animated:YES completion:nil];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    return self.cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    NSString *index = [self.cells objectAtIndex:indexPath.row];
    
    if ([index isEqualToString:@"userImage"]) {
        
        ProfileSettingsUserImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userImageCell"];
        [cell.userImage sd_setImageWithURL:[AppTemplate bigIconURL:self.avatarUrl] placeholderImage:[TeamUser placeholder]];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userFirstName"]) {
        
        ProfileSettingsTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userTextFieldCell"];
        cell.leftTitle.text = NSLocalizedString(@"First name", @"v1.0");
        cell.rightTextField.text = self.firstName;
        cell.rightTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        cell.rightTextField.tag = kFirstNameTextFieldTag;
        cell.rightTextField.delegate = self;
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userLastName"]) {
        ProfileSettingsTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userTextFieldCell"];
        cell.leftTitle.text = NSLocalizedString(@"Last name", @"v1.0");
        cell.rightTextField.text = self.lastName;
        cell.rightTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        cell.rightTextField.tag = kSecondNameTextFieldTag;
        cell.rightTextField.delegate = self;
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userJobTitle"]) {
        //
        ProfileSettingsJobTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userJobTitleCell"];
        cell.leftLabel.text = NSLocalizedString(@"Job title", @"v1.0");
        cell.rightLabel.text = self.jobTitle;
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userEmail"]) {
        ProfileSettingsEmailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userEmailCell"];
        cell.leftTitle.text = NSLocalizedString(@"Email", @"v1.0");
        cell.rightTextField.text = self.currentEmail;
        cell.rightTextField.tag = kEmailTextFieldTag;
        cell.rightTextField.delegate = self;
        if (self.notConfirmedEmail.length > 0) {
            cell.leftUnconfirmedEmail.text = NSLocalizedString(@"Unconfirmed E-mail", @"v1.0");
            cell.rightUnconfirmedEmail.text = self.notConfirmedEmail;
            [cell needShowUnconfirmedEmail:YES];
        }
        else {
            [cell needShowUnconfirmedEmail:NO];
        }
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userChangePassword"]) {
        ProfileSettingsTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userTextFieldCell"];
        cell.leftTitle.text = NSLocalizedString(@"Change password", @"v1.0");
        cell.rightTextField.placeholder = NSLocalizedString(@"6 symbols or more", @"v1.0");
        cell.rightTextField.text = kChangePasswordStartSymbols;
        cell.rightTextField.tag = kChangePasswordTextField;
        cell.rightTextField.delegate = self;
        [cell.rightTextField setSecureTextEntry:YES];
        [cell needShowCheckmark:NO];
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userNewPassword"]) {
        ProfileSettingsTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userTextFieldCell"];
        cell.leftTitle.text = NSLocalizedString(@"New password", @"v1.0");
        cell.rightTextField.placeholder = NSLocalizedString(@"6 symbols or more", @"v1.0");
        cell.rightTextField.text = @"";
        cell.rightTextField.tag = kNewPasswordTextField;
        cell.rightTextField.delegate = self;
        [cell.rightTextField setSecureTextEntry:YES];
        [cell needShowCheckmark:NO];
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userConfirmNewPassword"]) {
        ProfileSettingsTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userTextFieldCell"];
        cell.leftTitle.text = NSLocalizedString(@"Password confirmation", @"v1.0");
        cell.rightTextField.placeholder = NSLocalizedString(@"6 symbols or more", @"v1.0");
        cell.rightTextField.text = @"";
        cell.rightTextField.tag = kConfirmPasswordTextField;
        cell.rightTextField.delegate = self;
        [cell.rightTextField setSecureTextEntry:YES];
        [cell needShowCheckmark:NO];
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([index isEqualToString:@"userLogout"]) {
        ProfileSettingsActionsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userActionCell"];
        [cell.rightButton setTitle:NSLocalizedString(@"LOG OUT", @"v1.0") forState:UIControlStateNormal];
        cell.onRightAction = ^{
            [weakSelf logout];
        };
        
        return cell;
        
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *index = [self.cells objectAtIndex:indexPath.row];
    
    if ([index isEqualToString:@"userImage"]) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [self editUserImage];
    }
    
    if ([index isEqualToString:@"userJobTitle"]) {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [self editJobTitle];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *index = [self.cells objectAtIndex:indexPath.row];
    
    if ([index isEqualToString:@"userImage"]) {
        return 150;
    }
    
    if ([index isEqualToString:@"userEmail"] && self.notConfirmedEmail.length > 0) {
        return 70;
    }
    
    return 50;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Text Field Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //Used for moving rows
    self.activeField = textField;
    
    //Update left title of cell
    if (textField.tag == kChangePasswordTextField) {
        if (!self.isNeedShowCurrentPasswordRow) {
            self.isNeedShowCurrentPasswordRow = YES;
            [self updateChangePasswordRow];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //Used for moving rows
    self.activeField = nil;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    __weak typeof(self) weakSelf = self;
    
    //New textfield string
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    //First name check
    if (textField.tag == kFirstNameTextFieldTag) {
        
        //Validation for first symbol space
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        if (textField.text.length == 0 && [proposedNewString stringByTrimmingCharactersInSet:set].length == 0) {
            return NO;
        }
        
        //Check for changes in text and hide save button
        [self checkTextForChangeAndChangeButton:self.firstName withText:proposedNewString];
        if ([self isTextChanges:self.firstName withText:proposedNewString]) {
            self.firstName = proposedNewString;
        }
        
        //Accept only letters, space and "-"
        if (![self isCharacterText:string]) {
            [[AppDelegate theApp] showAlert:NSLocalizedString(@"First name", @"v1.0") text:NSLocalizedString(@"This field accepts only characters and space", @"v1.0")];
            return NO;
        }
        
        //Accept if length more than 0
        if (proposedNewString.length == 0) {
            //[self.navigationBar rightButtonEnable:NO];
            [self.navigationBar hideRightButton];
        }
        
        return YES;
    }
    
    //Last name check
    if (textField.tag == kSecondNameTextFieldTag) {
        //Validation for first symbol space
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        if (textField.text.length == 0 && [proposedNewString stringByTrimmingCharactersInSet:set].length == 0) {
            return NO;
        }
        
        //Check for changes in text and hide save button
        [self checkTextForChangeAndChangeButton:self.lastName withText:proposedNewString];
        if ([self isTextChanges:self.lastName withText:proposedNewString]) {
            self.lastName = proposedNewString;
        }
        
        //Accept only letters, space and "-"
        if (![self isCharacterText:string]) {
            [[AppDelegate theApp] showAlert:NSLocalizedString(@"Last name", @"v1.0") text:NSLocalizedString(@"This field accepts only characters and space", @"v1.0")];
            return NO;
        }
        
        //Accept if length more than 0
        if (proposedNewString.length == 0) {
            //[self.navigationBar rightButtonEnable:NO];
            [self.navigationBar hideRightButton];
        }
        
        return YES;
    }
    
    //E-mail
    if (textField.tag == kEmailTextFieldTag) {
        //Check for changes in text and hide save button
        [self checkTextForChangeAndChangeButton:self.currentEmail withText:proposedNewString];
        if ([self isTextChanges:self.currentEmail withText:proposedNewString]) {
            self.currentEmail = proposedNewString;
        }
        
        //Accept if length more than 0
        if (proposedNewString.length == 0) {
            //[self.navigationBar rightButtonEnable:NO];
            [self.navigationBar hideRightButton];
        }
        
        return YES;
    }
    
    //Current password
    if (textField.tag == kChangePasswordTextField) {
        self.currentPassword = proposedNewString;
        
        //Start check correct password 
        if (proposedNewString.length >= 6) {
            [[RequestManager sharedManager] checkPassword:proposedNewString success:^(BOOL success, BOOL isCorrect, NSString *password) {
                MLog(@"Pass: %@ Correct: %@", password, [NSNumber numberWithBool:isCorrect]);
                if (isCorrect) {
                    MLog(@"Correct password");
                    //If correct show checkmark on cell
                    [weakSelf showCheckMarkForPasswordCell:YES];
                    
                    //Show new rows if need
                    if (!weakSelf.isPasswordsRowInserted) {
                        [weakSelf addConfirmPasswordData];
                        weakSelf.isPasswordsRowInserted = YES;
                    }
                } else {
                    //Hide rows if password is not correct
                    if (weakSelf.isPasswordsRowInserted) {
                        [weakSelf deleteConfirmPasswordData];
                        weakSelf.isPasswordsRowInserted = NO;
                    }
                    //Hide checkmark is password is not correct
                    [weakSelf showCheckMarkForPasswordCell:NO];
                }
            } failure:^(NSString *message) {
                [[AppDelegate theApp] showAlert:NSLocalizedString(@"Password update", @"v1.0") text:message];
            }];
        }
        return YES;
    }
    
    //New password
    if (textField.tag == kNewPasswordTextField) {
        self.confirmPassword = proposedNewString;
        //[self.navigationBar rightButtonEnable:YES];
        [self.navigationBar showRightButton];
        
        return YES;
    }
    
    //Confirm new password
    if (textField.tag == kConfirmPasswordTextField) {
        self.confirm2Password = proposedNewString;
        //[self.navigationBar rightButtonEnable:YES];
        [self.navigationBar showRightButton];
        
        return YES;
    }
    
    return NO;
}

#pragma mark - UIImagePickerController Delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary <NSString *, id> *)info
{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    UIImage *resizedImage = [UIImage imageWithImage:image scaledToWidth:500];
    
    RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:resizedImage cropMode:RSKImageCropModeCustom];
    imageCropVC.delegate = self;
    imageCropVC.dataSource = self;
    [self.navigationController pushViewController:imageCropVC animated:YES];
    [[[AppDelegate theApp] mainViewController] hideTabBarView:YES];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController setViewControllers:@[self] animated:NO];
    [self dismissViewControllerAnimated:YES completion:nil]; //Return to activity feed
}

#pragma mark - Text

//Compare to not empety text
- (BOOL) isTextChanges:(NSString *)text withText:(NSString *) textToCompare {
    
    BOOL isChanges = NO;
    
    if (![text isEqualToString:textToCompare] && textToCompare != 0) {
        isChanges = YES;
    }
   
    return isChanges;
}

//Compare text and change button state
- (void) checkTextForChangeAndChangeButton:(NSString *)text withText:(NSString *) textToCompare {
    if (![text isEqualToString:textToCompare] && textToCompare.length != 0) {
        //[self.navigationBar rightButtonEnable:YES];
        [self.navigationBar showRightButton];
    }
    else {
        //[self.navigationBar rightButtonEnable:NO];
        [self.navigationBar hideRightButton];
    }
}

//Check is text is character, space and "-"
- (BOOL) isCharacterText:(NSString *) text {
    
    BOOL isLetter = YES;
    
    NSMutableCharacterSet *symbols = [NSMutableCharacterSet letterCharacterSet];
    [symbols addCharactersInString:@" -"];
    
    if ([text rangeOfCharacterFromSet:[symbols invertedSet]].location != NSNotFound) {
        isLetter = NO;
    }
    
    return isLetter;
    
}

#pragma mark - RSKImageCropViewController Delegate & DataSource methods

- (CGRect)imageCropViewControllerCustomMaskRect:(RSKImageCropViewController *)controller
{
    CGFloat cropWidth = SCREEN_WIDTH - 40;
    CGFloat cropHeight = SCREEN_WIDTH - 40;
    
    return CGRectMake(20, 90, cropWidth, cropHeight);
}

- (UIBezierPath *)imageCropViewControllerCustomMaskPath:(RSKImageCropViewController *)controller
{
    CGFloat cropWidth = SCREEN_WIDTH - 40;
    CGFloat cropHeight = SCREEN_WIDTH - 40;
    
    return [UIBezierPath bezierPathWithRect:CGRectMake(20, 90, cropWidth, cropHeight)];
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller
{
    [[[AppDelegate theApp] mainViewController] showTabBarView:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)imageCropViewController:(RSKImageCropViewController *)controller didCropImage:(UIImage *)croppedImage usingCropRect:(CGRect)cropRect
{
    __weak typeof(self) weakSelf = self;
    [[[AppDelegate theApp] mainViewController] showTabBarView:YES];
    [weakSelf.navigationController popViewControllerAnimated:YES];
    
    NSString *filePathOfImage = [self saveAvatarImage:croppedImage];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePathOfImage]) {
        [self showLoadingViewBellowView:self.navigationBar];
        [[RequestManager sharedManager] updateUserAvatarWithImagePath:filePathOfImage success:^(BOOL success, NSString *avatarUrl) {
            [DataManager updateSomeUserInfo:@{@"avatar_url":avatarUrl}];
            weakSelf.avatarUrl = avatarUrl;
            [weakSelf updateImageRow];
            [weakSelf hideLoadingView];
        } failure:^(NSString *message) {
            [weakSelf hideLoadingView];
            [[AppDelegate theApp] showAlert:NSLocalizedString(@"Profile update", @"v1.0") text:NSLocalizedString(@"Cannot update avatar image. Try again later", @"v1.0")];
        }];
    }
}


#pragma mark - Keyboard Movement

- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(EN_NAVIGATION_BAR_HEIGHT, 0.0, kbRect.size.height + kDeltaKeyboardMovement + EN_NAVIGATION_TAB_BAR_HEIGHT, 0.0);
    self.contentTableView.contentInset = contentInsets;
    self.contentTableView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    if (!CGRectContainsPoint(aRect, self.activeField.frame.origin) ) {
        [self.contentTableView scrollRectToVisible:self.activeField.frame animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = self.contentTableView.contentInset;
    contentInsets.top = EN_NAVIGATION_BAR_HEIGHT;
    contentInsets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.contentTableView.contentInset = contentInsets;
    self.contentTableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Helpers Methods

-(void) showSentAlert {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Profile has been updated", @"v1.0")];
}

-(NSString *) saveAvatarImage:(UIImage *) image {
    
    //Current date
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSInteger minutes = [components minute];
    NSInteger seconds = [components second];
    
    NSString *currentTime = [NSString stringWithFormat:@"%ld%ld%ld%ld%ld", (long)day, (long)month, (long)year, (long)minutes, (long)seconds];

    //Image name for save
    NSString *fileName = [NSString stringWithFormat:@"UserAvatar_%@", currentTime];
    
    //Image path
    NSString *imageFilePath = [[AKFileManager temporaryDirectory] stringByAppendingPathComponent:fileName];
    
    //Image Data
    NSData *dataImage = [UIImage dataWithImage:image];
    
    //Save file to image
    [dataImage writeToFile:imageFilePath atomically:YES];
    
    return imageFilePath;
}

- (BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
