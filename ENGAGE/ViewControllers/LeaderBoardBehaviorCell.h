//
//  LeaderBoardBehaviorCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 19.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"

@interface LeaderBoardBehaviorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leadLabel;
@property (weak, nonatomic) IBOutlet ENRoundedImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellTitleCell;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

@end
