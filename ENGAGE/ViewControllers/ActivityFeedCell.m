//
//  ActivityFeedCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 08.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ActivityFeedCell.h"
#import "ENRoundedImageView.h"
#import "ENCircleImageView.h"

#define kBorderWidth 1.0

@interface ActivityFeedCell ()


@property (weak, nonatomic) IBOutlet ENCircleImageView *receiverImageView;
@property (weak, nonatomic) IBOutlet UILabel *receiverLabel;

@property (weak, nonatomic) IBOutlet UILabel *receiverJobLabel;

@property (weak, nonatomic) IBOutlet ENRoundedImageView *behaviourImageView;
@property (weak, nonatomic) IBOutlet UILabel *behaiourLabel;


@property (weak, nonatomic) IBOutlet UILabel *contentLabel;


@property (weak, nonatomic) IBOutlet ENCircleImageView *authorImageView;

@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *createDateWidth;

@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UIButton *retapsCount;
@property (weak, nonatomic) IBOutlet UIButton *retapButton;

@property (weak, nonatomic) IBOutlet UIButton *receiverIconButton;
@property (weak, nonatomic) IBOutlet UIButton *senderIconButton;
@property (weak, nonatomic) IBOutlet UIButton *behaviorIconButton;

@property (nonatomic, strong) NSNumber *borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

@end



@implementation ActivityFeedCell


-(void) addFeed:(ActivityFeed *) feed {
    
    self.contentLabel.text = feed.body;
    
    self.receiverLabel.text = feed.receiverName;
    self.receiverJobLabel.text = feed.receiverJob;
    
    [self.receiverImageView sd_setImageWithURL:[AppTemplate mediumIconURL:feed.receiverAvatarUrl] placeholderImage:[TeamUser placeholder]];
    
    self.behaiourLabel.text = feed.behaviourTitle;
    [self.behaviourImageView sd_setImageWithURL:[AppTemplate bigIconURL:feed.behaviourIconUrl] placeholderImage:[Behavior placeholder]];
    
    [self.authorImageView sd_setImageWithURL:[AppTemplate smallIconURL:feed.senderAvatarUrl] placeholderImage:[TeamUser placeholder]];
    
    self.createDateLabel.text = [feed formattedCreateDate];
    CGSize dateSize = [self.createDateLabel sizeThatFits:CGSizeZero];
    self.createDateWidth.constant = dateSize.width;
    
    if (feed.retapsCount.integerValue > 0) {
        [self.retapsCount setTitle:[NSString stringWithFormat:@"%@ +%@", NSLocalizedString(@"Retaps", @"v1.0"), feed.retapsCount] forState:UIControlStateNormal];
        self.retapsCount.enabled = YES;
        [self.retapsCount setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    } else {
        [self.retapsCount setTitle:[NSString stringWithFormat:@"%@", feed.senderName] forState:UIControlStateNormal];
        self.retapsCount.enabled = NO;
        [self.retapsCount setTitleColor:[UIColor appGrayColor] forState:UIControlStateNormal];
    }
    
    if (feed.commentsCount.integerValue == 0) {
        [self.commentsButton setTitle:nil forState:UIControlStateNormal];
    } else {
        [self.commentsButton setTitle:[NSString stringWithFormat:@"  +%ld",(long)feed.commentsCount.integerValue] forState:UIControlStateNormal];
    }
    
    self.commentsButton.titleLabel.font = [UIFont appFontBold:14];
    self.commentsButton.titleLabel.textColor = [UIColor appOrangeColor];
    
    
    if (feed.isRetaped.boolValue) {
        [self.retapButton setImage:[UIImage imageNamed:@"icon-retaped"] forState:UIControlStateNormal];
    } else {
        [self.retapButton setImage:[UIImage imageNamed:@"icon-retap"] forState:UIControlStateNormal];
    }
    
    if ([feed.senderId isEqualToNumber:[DataManager user].userId] || [feed.receiverId isEqualToNumber:[DataManager user].userId]) {
        self.retapButton.enabled = NO;
    } else {
        self.retapButton.enabled = YES;
    }
    
    if (feed.isReceiverPointWinnerValue) {
        self.borderWidth = [NSNumber numberWithFloat:3.0];
        self.borderColor = [UIColor avatarBorderColor];
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.receiverLabel.textColor = [UIColor blackColor];
    self.receiverLabel.font = [UIFont appFont:17];
    
    self.receiverJobLabel.textColor = [UIColor appGrayColor];
    self.receiverJobLabel.font = [UIFont appFontThin:17];
    
    self.behaiourLabel.textColor = [UIColor blackColor];
    self.behaiourLabel.font = [UIFont appFontBold:17];
    
    
    self.contentLabel.textColor = [UIColor appGrayColor];
    self.contentLabel.font = [UIFont appFontThin:17];
    
    [self.retapsCount setTitleColor:[UIColor appGrayColor] forState:UIControlStateNormal];
    self.retapsCount.titleLabel.font = [UIFont appFont:14];
    
    self.createDateLabel.textColor = [UIColor appGrayColor];
    self.createDateLabel.font = [UIFont appFontBold:14];
    
    //For iOS10
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.commentsButton.titleLabel.textColor = [UIColor appOrangeColor];
    
    self.borderWidth = nil;
    self.borderColor = nil;
}

+(CGFloat) cellHeight:(NSString *) contentText andBehaviorText:(NSString *) behaviorText width:(CGFloat) tableWidth {
    
    /*
     CGRect rect = [contentText boundingRectWithSize:CGSizeMake(tableWidth - 30, CGFLOAT_MAX) options:(NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [UIFont appFontThin:17]} context:nil];
     */
    
    //bad font, use this way for calculate text height
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFontThin:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    
    //Calculate behavior label
    UILabel *behaviorSampleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    behaviorSampleLabel.font = [UIFont appFontBold:17];
    behaviorSampleLabel.text = behaviorText;
    behaviorSampleLabel.numberOfLines = 0;
    
    CGSize behaviorSize = [behaviorSampleLabel sizeThatFits:CGSizeMake(tableWidth - 30, CGFLOAT_MAX)];
    CGFloat behaviorHeight = ceilf(behaviorSize.height);
    
    height += 65;//reciever view
    height += behaviorHeight + 20; //behavior label height
    height += 30;//text padding
    height += 100;//bottom height
    height += 20;//separator height
    
    return height;
}



- (IBAction)retapAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.onRetap) {
        self.onRetap(weakSelf);
    }
}

- (IBAction)addCommentAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.onComments) {
        self.onComments(weakSelf);
    }
}
- (IBAction)showRetaps:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (self.onShowRetaps) {
        self.onShowRetaps(weakSelf);
    }
}

- (IBAction)showReceiver:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowReceiver) {
        weakSelf.onShowReceiver(weakSelf);
    }
}

- (IBAction)showSender:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowSender) {
        weakSelf.onShowSender (weakSelf);
    }
}

- (IBAction)showBehavior:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowBehavior) {
        weakSelf.onShowBehavior(weakSelf);
    }
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.behaviourImageView.imageBorderWidth = kBorderWidth;
    self.behaviourImageView.imageBorderColor = [UIColor appLightGrayColor];
    
    self.receiverImageView.layer.borderWidth = self.borderWidth.floatValue;
    self.receiverImageView.layer.borderColor = self.borderColor.CGColor;
    
    self.authorImageView.layer.borderWidth = 1.0;
    self.authorImageView.layer.borderColor = [UIColor appLightGrayColor].CGColor;
}

#pragma mark - Properties

-(NSNumber *) borderWidth {
    if (_borderWidth == nil) {
        _borderWidth = [NSNumber numberWithFloat:kBorderWidth];
    }
    
    return _borderWidth;
}

- (UIColor *) borderColor {
    if (_borderColor == nil) {
        _borderColor = [UIColor appLightGrayColor];
    }
    
    return _borderColor;
}


@end
