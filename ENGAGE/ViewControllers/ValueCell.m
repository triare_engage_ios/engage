//
//  ValueCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ValueCell.h"

@interface ValueCell ()
@property (weak, nonatomic) IBOutlet UIView *firstBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *secondBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *thirdBackgroundView;


@end

@implementation ValueCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel1.hidden = YES;
    self.titleLabel2.hidden = YES;
    self.titleLabel3.hidden = YES;
    
    self.imageView1.hidden = YES;
    self.imageView2.hidden = YES;
    self.imageView3.hidden = YES;
    
    self.button1.hidden = YES;
    self.button2.hidden = YES;
    self.button3.hidden = YES;
    
    self.titleLabel1.textColor = [UIColor appTextColor];
    self.titleLabel1.font = [UIFont appFont:14];
    
    self.titleLabel2.textColor = [UIColor appTextColor];
    self.titleLabel2.font = [UIFont appFont:14];
    
    self.titleLabel3.textColor = [UIColor appTextColor];
    self.titleLabel3.font = [UIFont appFont:14];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.behaviorId1 = nil;
    self.behaviorId2 = nil;
    self.behaviorId3 = nil;
    
    self.titleLabel1.text = nil;
    self.titleLabel2.text = nil;
    self.titleLabel3.text = nil;
    
    [self.imageView1 setImage:nil];
    [self.imageView2 setImage:nil];
    [self.imageView3 setImage:nil];
    
    self.titleLabel1.hidden = YES;
    self.titleLabel2.hidden = YES;
    self.titleLabel3.hidden = YES;
    
    self.imageView1.hidden = YES;
    self.imageView2.hidden = YES;
    self.imageView3.hidden = YES;
    
    self.button1.hidden = YES;
    self.button2.hidden = YES;
    self.button3.hidden = YES;
    
    self.onAction = nil;
}

-(void) addBehaviors:(NSArray *) behaviors {
   
    for (int i = 0; i < behaviors.count; i ++) {
        Behavior *item = behaviors[i];
        [self addBehavior:item toIndex:i];
    }
}

- (void) addBehavior: (Behavior *) behavior toIndex:(NSInteger) index {
    
    switch (index) {
        case 0:
            self.titleLabel1.text = behavior.title;
            [self.imageView1 sd_setImageWithURL:[AppTemplate bigIconURL:behavior.iconUrl] placeholderImage:[Behavior placeholder]];
            self.behaviorId1 = behavior.behaviorId;
            self.titleLabel1.hidden = NO;
            self.imageView1.hidden = NO;
            self.button1.hidden = NO;
            
            break;
            
        case 1:
            self.titleLabel2.text = behavior.title;
            [self.imageView2 sd_setImageWithURL:[AppTemplate bigIconURL:behavior.iconUrl] placeholderImage:[Behavior placeholder]];
            self.behaviorId2 = behavior.behaviorId;
            self.titleLabel2.hidden = NO;
            self.imageView2.hidden = NO;
            self.button2.hidden = NO;
            
            break;
            
        case 2:
            self.titleLabel3.text = behavior.title;
            [self.imageView3 sd_setImageWithURL:[AppTemplate bigIconURL:behavior.iconUrl] placeholderImage:[Behavior placeholder]];
            self.behaviorId3 = behavior.behaviorId;
            self.titleLabel3.hidden = NO;
            self.imageView3.hidden = NO;
            self.button3.hidden = NO;
            
            break;
            
        default:
            break;
    }
}

- (IBAction)button1Action:(id)sender {
    if (self.onAction) {
        self.onAction(self.behaviorId1);
    }
}

- (IBAction)button2Action:(id)sender {
    if (self.onAction) {
        self.onAction(self.behaviorId2);
    }
}

- (IBAction)button3Action:(id)sender {
    if (self.onAction) {
        self.onAction(self.behaviorId3);
    }
}



@end
