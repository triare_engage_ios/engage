//
//  BehaviorsManageVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

typedef enum : NSUInteger {
    BehaviorScreenState,
    ValueScreenState
} ScreenState;

typedef enum : NSUInteger {
    ImageStateFromPhone,
    ImageStateFromLibrary
} ImageState;

@interface BehaviorsManageVC : ContentViewController
@property (nonatomic) ScreenState screenState;
@property (nonatomic) ImageState imageState;
@property (nonatomic, copy) void (^onSendAction)();

@end
