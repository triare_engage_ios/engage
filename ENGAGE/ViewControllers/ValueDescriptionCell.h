//
//  ValueDescriptionCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 10/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

//Identifier in storyboard: valueDescriptionCell

@interface ValueDescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth;

@end
