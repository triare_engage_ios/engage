//
//  BehaviorIconsListVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 10/6/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorIconsListVC.h"
#import "BehaviorIconCell.h"

@interface BehaviorIconsListVC () <UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) PaginationInfo *pagination;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation BehaviorIconsListVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupCollectionView];
    [self setupPagination];
    [self addDownRefresh];
    [self showLoadingViewBellowView:self.navigationBar];
    [self loadIconsData];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenBehaviorIconsList];
    [AnalyticsManager trackScreen:ENAnalyticScreenBehaviorIconsList withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:NSLocalizedString(@"LIBRARY", @"v1.0")];
    
}

- (void) setupCollectionView {
    
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT;
    insets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.collectionView.contentInset = insets;
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"BehaviorIconCell" bundle:nil] forCellWithReuseIdentifier:@"iconCell"];
}

- (void) setupPagination {
    self.pagination = [[PaginationInfo alloc] init];
    self.pagination.elementsPerPage = 70;
}

#pragma mark - Data

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.collectionView reloadData];
}

- (void) loadIconsData {
    
    self.pagination.currentPage = 0;
    self.pagination.totalPages = 1;
    
    [self loadIconsDataForPage:self.pagination.currentPage + 1];
}

- (void) loadNextIconsData {
    if (self.pagination.isLoading) return;
    
    if (self.pagination.totalPages > self.pagination.currentPage) {
        [self loadIconsDataForPage:self.pagination.currentPage + 1];
    }
}

-(void) loadIconsDataForPage:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    weakSelf.pagination.isLoading = YES;
    
    [[RequestManager sharedManager] getBehaviorIconsForPage:page iconsPerPage:self.pagination.elementsPerPage success:^(NSArray *list, PaginationInfo *pagination) {
        weakSelf.pagination.currentPage = pagination.currentPage;
        weakSelf.pagination.totalPages = pagination.totalPages;
        
        if (weakSelf.pagination.currentPage == 1) {
            [DataManager saveBehaviorsIconList:list];
        }
        else {
            [DataManager appendBehaviorsIconList:list];
        }
        
        weakSelf.pagination.isLoading = NO;
        [weakSelf reloadData];
        
        [weakSelf hideLoadingView];
        
    } failure:^(NSString *message) {
        weakSelf.pagination.isLoading = NO;
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithMessage:message];
    }];
    
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [BehaviorIcon MR_requestAllSortedBy:@"category" ascending:YES];
    fetchRequest.includesSubentities = NO;
    
    NSFetchedResultsController *fetchController = [BehaviorIcon MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    return number;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BehaviorIcon *iconItem = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    BehaviorIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"iconCell" forIndexPath:indexPath];
    [cell.iconImageView sd_setImageWithURL:[AppTemplate smallIconURL:iconItem.iconImageUrl] placeholderImage:[Behavior placeholder]];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [BehaviorIconCell sizeForCollectionWidth:collectionView.frame.size.width];
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BehaviorIcon *iconItem = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [self closeVC];
    
    if (self.onAction) {
        self.onAction(iconItem);
    }
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Start loading new list before next row
    if (indexPath.row == self.fetchedResultsController.fetchedObjects.count - 6) {
        [self loadNextIconsData];
    }
}

#pragma mark - Navigation

-(void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadIconsData];
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) text {
    
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}

@end
