//
//  ProfileBehavior3Cell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 25.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"
#import "BadgeLabel.h"

@interface ProfileBehavior3Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellTitleLabel1;
@property (weak, nonatomic) IBOutlet ENRoundedImageView *cellImageView1;
@property (weak, nonatomic) IBOutlet BadgeLabel *badgeLabel1;

@property (weak, nonatomic) IBOutlet UILabel *cellTitleLabel2;
@property (weak, nonatomic) IBOutlet ENRoundedImageView *cellImageView2;
@property (weak, nonatomic) IBOutlet BadgeLabel *badgeLabel2;

@property (weak, nonatomic) IBOutlet UILabel *cellTitleLabel3;
@property (weak, nonatomic) IBOutlet ENRoundedImageView *cellImageView3;
@property (weak, nonatomic) IBOutlet BadgeLabel *badgeLabel3;

@property (nonatomic, copy) void (^onShowBehavior)(NSInteger index);

-(void) setBehaviors:(NSArray *) items;

@end
