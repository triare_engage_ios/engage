//
//  ProfileCurrentUserEvaluationCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/2/16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileCurrentUserEvaluationCell.h"

@interface ProfileCurrentUserEvaluationCell ()

@end

@implementation ProfileCurrentUserEvaluationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.evaluateTitleLabel.font = [UIFont appFont:17];
    self.evaluateTitleLabel.textColor = [UIColor appTextColor];
    
    self.goodCountLabel.font = [UIFont appFontBold:22];
    self.goodCountLabel.textColor = [UIColor appTextColor];
    
    self.badCountLabel.font = [UIFont appFontBold:22];
    self.badCountLabel.textColor = [UIColor appTextColor];

}

- (void)prepareForReuse {
    [super prepareForReuse];

    self.evaluateTitleLabel.text = nil;
    self.goodCountLabel.text = nil;
    self.badCountLabel.text = nil;
    self.evaluateImageView.image = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
