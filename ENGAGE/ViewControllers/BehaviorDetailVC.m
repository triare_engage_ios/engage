//
//  BehaviorDetailVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorDetailVC.h"

#import "BehaviorDetailHeaderCell.h"
#import "BehaviorDetailDescriptionCell.h"

#import "UITableViewCell+Separator.h"

#import <SVProgressHUD/SVProgressHUD.h>

#import "AnalyticsManager.h"

@interface BehaviorDetailVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;

@property (nonatomic, strong) NSArray *items;

@end

@implementation BehaviorDetailVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupNavigationBar];
    [self setupTableView];
    
    [self setupData];
    [self.contentTableView reloadData];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenBehaviorDetail];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenBehaviorDetail withScreenClass:NSStringFromClass([self class])];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup

- (void) setupNavigationBar {
    [self.navigationBar setTitle:NSLocalizedString(@"Behavior", @"v1.0")];
    
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    
    [self.navigationBar showLeftButton];
    
    if (([[DataManager user] isAdmin] || [[DataManager user] isManager]) && self.item.isActiveValue) {
        [self.navigationBar.barView.rightButton setTitle:[NSString stringWithFormat:@"%@ ", NSLocalizedString(@"HIDE", @"v1.0")] forState:UIControlStateNormal];
        
        //Prepare right button for text display
        [self.navigationBar.barView.rightButton setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
        [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self.navigationBar.barView.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
        self.navigationBar.barView.rightButton.titleLabel.font = [UIFont appFontBold:12];
        [self.navigationBar.barView.rightButton setBackgroundColor:[UIColor clearColor]];
        [self.navigationBar.barView.rightButton setImage:nil forState:UIControlStateNormal];
        [self.navigationBar showRightButton];
        
        self.navigationBar.onRightAction = ^{
            [weakSelf showHideAlert];
        };
    }
    
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupData {
    NSMutableArray *array = [NSMutableArray new];
    
    [array addObject:@"headerCell"];
    [array addObject:@"descriptionCell"];
    
    self.items = [array copy];
}

#pragma mark - Action

- (void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) showHideAlert {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Hide", @"v1.0") message:NSLocalizedString(@"This option allows you hide behavior from the app, but you can still find behavior on the web page and activate it again.", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self hideBehavior];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) hideBehavior {
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] deactivateBehaviorWithId:self.item.behaviorId success:^(BOOL success) {
        [weakSelf hideLoadingView];
        [DataManager updateBehaviorID:self.item.behaviorId withInfo:@{@"active":@NO}];
        if (weakSelf.onHideAction) {
            weakSelf.onHideAction();
        }
        [weakSelf closeVC];
        [weakSelf showAlertBehaviroDidHidden];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithMessage:message];
    }];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *item = self.items[indexPath.row];
    
    if ([item isEqualToString:@"headerCell"]) {
        return [BehaviorDetailHeaderCell cellHeight:self.item.title width:tableView.frame.size.width];
    }
    else {
        return (SCREEN_HEIGHT - EN_NAVIGATION_TAB_BAR_HEIGHT - EN_NAVIGATION_BAR_HEIGHT) / 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *item = self.items[indexPath.row];
    
    if ([item isEqualToString:@"headerCell"]) {
        BehaviorDetailHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        cell.headerTitleLabel.text = self.item.title;
        [cell.headerImageView sd_setImageWithURL:[AppTemplate bigIconURL:self.item.iconUrl] placeholderImage:[Behavior placeholder]];
        
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if ([item isEqualToString:@"descriptionCell"]) {
        BehaviorDetailDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"descriptionCell"];
        cell.descriptionLabel.text = self.item.behaviorDescription;
        
        return cell;
        
    }
    
    return nil;
    
}

#pragma mark - Helpers Methods

-(void) showAlertBehaviroDidHidden {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"«Behavior» hidden", @"v1.0")];
}

- (void) showAlertWithMessage:(NSString *) message {
    if (message.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:nil text:message];
}

@end
