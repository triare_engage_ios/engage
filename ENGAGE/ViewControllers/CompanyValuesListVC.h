//
//  CompanyValuesListVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface CompanyValuesListVC : ContentViewController
@property (nonatomic, copy) void (^onBackAction)(NSString *valueTitle, NSNumber *valueId);

@end
