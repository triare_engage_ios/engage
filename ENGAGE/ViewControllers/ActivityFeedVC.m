//
//  ActivityFeedVC.m
//  ENGAGE.
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ActivityFeedVC.h"
#import "ActivityView.h"
#import "UIViewController+LoadingView.h"
#import "NSString+URLValidation.h"
#import "ActivityFeedCell.h"
#import "ActivityFeedTeamMessageCell.h"
#import "ActivityFeedPointCell.h"
#import "ActivityFeedChallengeCell.h"
#import "LearningLinkCell.h"
#import "LearningCommentCell.h"
#import "PaginationInfo.h"
#import "NavigationBar.h"
#import "LoadingCell.h"
#import "ENPlaceholderView.h"

#import "CommentsVC.h"
#import "RetapsListVC.h"
#import "QuestionAnswerVC.h"

#import "ProfileVC.h"
#import "BehaviorDetailVC.h"

#import "ChallengeInfoVC.h"
#import "InnovationsAnswerVC.h"
#import "InnovationsConfirmVC.h"
#import "LearningRatingVoteVC.h"

#import "FeaturesAccessManager.h"
#import "SFSafariManager.h"

@interface ActivityFeedVC () <NavigationBarSearchDelegate, SWTableViewCellDelegate, SFSafariViewControllerDelegate>

#pragma mark - UI
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableBottomSpace;

#pragma mark - Data
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) PaginationInfo *pagination;
@property (nonatomic, strong) PaginationInfo *searchPagination;
@property BOOL isSearchState;
@property (nonatomic, strong) NSMutableArray *fullLearningCommentsIds;

#pragma mark - Refresh
@property (nonatomic) BOOL isNeedRefreshAfterNewComment;

#pragma mark - Show Feed
@property BOOL needShowActivityFeedQuestion;
@property BOOL needShowActivityFeedChallenge;
@property BOOL needShowActivityFeedInnovationAnswer;
@property BOOL needShowActivityFeedInnovationConfirm;

#pragma mark - Placeholders
@property (nonatomic, strong) ENPlaceholderView *tablePlaceholder;
@property (nonatomic, strong) ENPlaceholderView *emptyPlaceholder;

@end

@implementation ActivityFeedVC

#pragma mark - Life Cycle

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareNavigationBar];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ActivityFeedCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.tableView registerNib:[UINib nibWithNibName:[ActivityFeedPointCell cellNimbName] bundle:nil] forCellReuseIdentifier:[ActivityFeedPointCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[ActivityFeedChallengeCell cellNimbName] bundle:nil] forCellReuseIdentifier:[ActivityFeedChallengeCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[LearningLinkCell cellNimbName] bundle:nil]forCellReuseIdentifier:[LearningLinkCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[LearningCommentCell cellNimbName] bundle:nil]forCellReuseIdentifier:[LearningCommentCell cellReuseIdentifier]];
    
    self.pagination = [[PaginationInfo alloc] init];
    [self addDownRefresh];
    
    self.registerKeyboardNotifications = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenMainActivityFeed];
    
    [self cleanPlaceholders];
    
    if (self.isNeedRefreshAfterNewComment) {
        [self loadFeedData];
        self.isNeedRefreshAfterNewComment = NO;
    }
    
    if ([AppDelegate theApp].mainViewController.isAddedNewTap) {
        [AppDelegate theApp].mainViewController.isAddedNewTap = NO;
        [self loadFeedData];
    }
    
    [self.tableView reloadData];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([AppDelegate theApp].mainViewController.isAddedNewTap) {
        [AppDelegate theApp].mainViewController.isAddedNewTap = NO;
        
        [self.tableView scrollTableToTop:YES];
    }
    
    if (self.fetchedResultsController.fetchedObjects.count == 0 && !self.isSearchState) {
        if (self.emptyPlaceholder == nil) {
            [self createTablePlaceholderForEmptyFeed];
        }
        //Fix for showing list of feed.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.fetchedResultsController.fetchedObjects.count == 0 && !self.isSearchState) {
                self.emptyPlaceholder.hidden = NO;
            }
            else {
                self.emptyPlaceholder.hidden = YES;
            }
        });
    }
    
    [AnalyticsManager trackScreen:ENAnalyticScreenMainActivityFeed withScreenClass:NSStringFromClass([self class])];

    [self showBadgeListIfNeeded];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    //When used search, no result, and move to other screen
    if (self.fetchedResultsController.fetchedObjects.count == 0) {
        [self loadFeedData];
    }
    
    //
    [self cleanPlaceholders];
}

#pragma mark - Prepare Methods

- (void) prepareNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:NSLocalizedString(@"ACTIVITY FEED", @"v1.0")];
    
    if ([self isNeedAccessChallenge]) {
        [self.navigationBar showLeftButton];
        [self.navigationBar changeLeftButtonIcon:[UIImage imageNamed:@"icon-challenge"]];
        self.navigationBar.onLeftAction = ^{
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionChallenge label:ENAnalyticLabelNavigationBar];
            [weakSelf showCurrentChallenge];
        };
    };

    [self.navigationBar showRightButton];
    
    if ([self isNeedAccessCenterNavigationBar]) {
        self.navigationBar.onCenterAction = ^{
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionMenuTop label:ENAnalyticLabelNavigationBar];
            [[[AppDelegate theApp] mainViewController] showActivityFeedDropMenu];
        };
        [self.navigationBar centerInteractionEnable:YES];
        [self.navigationBar addCenterTitleIcon:[UIImage imageNamed:@"icon-arrowDown"]];
    }
    
    self.navigationBar.searchDelegate = self;
    self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search here", @"v1.0");
    
}

#pragma mark - Placeholders

- (void) createTablePlaceholder {
    
    self.tablePlaceholder = [ENPlaceholderView createPlaceholderWithText:NSLocalizedString(@"No result found", @"v1.0") andFont:[UIFont appFont:17]];
    
    self.tablePlaceholder.hidden = YES;
    [self.tableView addSubview:self.tablePlaceholder];
    
}

- (void) createTablePlaceholderForEmptyFeed {
    
    self.emptyPlaceholder = [ENPlaceholderView createCenterPlaceholderWithText:NSLocalizedString(@"No feeds so far, stay tuned", @"v1.0") andFond:[UIFont appFontBold:22]];
    self.emptyPlaceholder.hidden = YES;
    [self.tableView addSubview:self.emptyPlaceholder];
}

- (void) cleanPlaceholders {
    [self.emptyPlaceholder removeFromSuperview];
    [self.tablePlaceholder removeFromSuperview];
    self.tablePlaceholder = nil;
    self.emptyPlaceholder = nil;
}

#pragma mark - Refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadFeedData];
}

#pragma mark - Data

- (void)reloadData {

    self.fullLearningCommentsIds = nil;
    self.fetchedResultsController = nil;
    
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

-(void) loadFeedData {
    
    self.pagination.currentPage = 0;
    self.pagination.totalPages = 1;
    
    [self loadFeedDataForPage:self.pagination.currentPage + 1];
}

-(void) loadNextFeedData {
    
    if (self.pagination.isLoading) return;
    
    if (self.pagination.totalPages > self.pagination.currentPage) {
        [self loadFeedDataForPage:self.pagination.currentPage + 1];
    }
}

-(void) loadFeedDataForPage:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    weakSelf.pagination.isLoading = YES;
    
    [[RequestManager sharedManager] getActivityFeedForPage:page onCompleate:^(BOOL success, PaginationInfo *pagination, NSDictionary *info, NSString *errorMessage) {
        //
        
        if (success) {
            weakSelf.pagination.currentPage = pagination.currentPage;
            weakSelf.pagination.totalPages = pagination.totalPages;
            
            if (weakSelf.pagination.currentPage == 1) {
                [DataManager saveActivityFeedWithInfo:info];
                if ([DataManager activityFeedQuestion] != nil) {
                    weakSelf.needShowActivityFeedQuestion = YES;
                }
                
                if ([DataManager activityFeedChallenge] != nil) {
                    weakSelf.needShowActivityFeedChallenge = YES;
                }
                
                //weakSelf.needShowActivityFeedInnovationAnswer = YES;
                //weakSelf.needShowActivityFeedInnovationConfirm = YES;
            }
            else {
                [DataManager appendTopPointWithInfo:[info valueForKey:@"top_point"]];
                [DataManager appendActivityFeedList:[info valueForKey:@"feeds"]];
            }
            
            [weakSelf reloadData];
        }
        
        weakSelf.pagination.isLoading = NO;
        [weakSelf hideLoadingView];
        
        //Logic for showing current question or challenge
        [weakSelf showChallengeOrQuestionIfNeed];
        
        if (!success && errorMessage.length > 0) {
            [weakSelf showAlertWithMessage:errorMessage];
            [weakSelf.refreshControl endRefreshing];
        }
    }];
    
}

- (NSMutableArray *) fullLearningCommentsIds {
    if (_fullLearningCommentsIds == nil) {
        _fullLearningCommentsIds = [NSMutableArray array];
    }
    
    return _fullLearningCommentsIds;
}

#pragma mark - Action

-(void) showCommentsForItemAtIndexPath:(NSIndexPath *)indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [self performSegueWithIdentifier:@"commentsList" sender:feed];
}

-(void) retapItemAtIndexPath:(NSIndexPath *)indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (feed.isRetaped.boolValue) {
        [self deleteRetapAtIndexPath:indexPath];
        return;
    }
    
    [DataManager saveRetapForFeed:feed.feedId isRetaped:YES];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
    __weak typeof(self) weakSelf = self;
    [[RequestManager sharedManager] retapFeed:feed.feedId onCompleate:^(BOOL success, NSString *errorMessage) {
        if (!success) {
            [DataManager saveRetapForFeed:feed.feedId isRetaped:NO];
            [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            [weakSelf showAlertWithMessage:errorMessage];
        }
    }];
}

- (void) deleteRetapAtIndexPath:(NSIndexPath *)indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (feed.isRetaped.boolValue) {
        //Save to DB
        [DataManager saveRetapForFeed:feed.feedId isRetaped:NO];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        __weak typeof(self) weakSelf = self;
        
        //Send data to API
        [[RequestManager sharedManager] deleteRetapWithId:feed.feedId success:^(BOOL success) {
            //
        } failure:^(NSString *message) {
            //If error - reverse changes
            [DataManager saveRetapForFeed:feed.feedId isRetaped:YES];
            [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [weakSelf showAlertWithMessage:message];
        }];
    }
}

-(void) showRetapsForItemAtIndexPath:(NSIndexPath *)indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [self performSegueWithIdentifier:@"retapsList" sender:feed];
}

-(void) showReceiverUserProfileForItemAtIndex:(NSIndexPath *) indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    vc.userId = feed.receiverId;
    vc.profileTitle = feed.receiverName;
    
    [self showViewController:vc sender:nil];
}

-(void) showSenderUserProfileForItemAtIndex:(NSIndexPath *) indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    NSNumber *senderId = nil;
    NSString *senderName = nil;
    
    if ([feed feedType] == ActivityFeedTypeLearning) {
        senderId = feed.learning.senderId;
        senderName = feed.learning.senderFullName;
    }
    else {
        senderId = feed.senderId;
        senderName = feed.senderName;
    }
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    vc.userId = senderId;
    vc.profileTitle = senderName;
    
    [self showViewController:vc sender:nil];
}

-(void) showBehaviorDetailForItemAtIndex:(NSIndexPath *) indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    Behavior *item = [DataManager behaviorByID:feed.behaviourId];
    
    BehaviorDetailVC * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorDetailVC"];
    vc.item = item;
    vc.onHideAction = nil;
    
    [self showViewController:vc sender:nil];
}

-(void) showChallengeDetailForItemAtIndex:(NSIndexPath *) indexPath {
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ChallengeInfoVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[ChallengeInfoVC storyboardIdentifier]];
    
    vc.screenState = ScreenStateFromActivityFeedCell;
    vc.challenge = feed.challenge;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) deleteFeedAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([feed feedType] == ActivityFeedTypeTap) {
        [[RequestManager sharedManager] deleteTapWithId:feed.feedId success:^(BOOL success) {
            [DataManager deleteFeedWithId:feed.feedId forType:ActivityFeedTypeTap];
            weakSelf.fetchedResultsController = nil;
            [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        } failure:^(NSString *message) {
            [weakSelf showAlertWithMessage:message];
        }];
        return;
    }
    
    if ([feed feedType] == ActivityFeedTypeLearning) {
        [[RequestManager sharedManager] deleteLearningWithId:feed.learning.feedId success:^(BOOL success) {
            [DataManager deleteFeedWithId:feed.feedId forType:ActivityFeedTypeLearning];
            weakSelf.fetchedResultsController = nil;
            [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        } failure:^(NSString *message) {
            [weakSelf showAlertWithMessage:message];
        }];
    }

}

- (void) showChallengeOrQuestionIfNeed {
    if (self.needShowActivityFeedChallenge) {
        [self showActivityFeedChallengeInfo];
    }
    else if (self.needShowActivityFeedQuestion) {
        [self showQuestionForAnswer];
    }
    else if (self.needShowActivityFeedInnovationAnswer) {
        [self showInnovationAnswer];
    }
    else if (self.needShowActivityFeedInnovationConfirm) {
        [self showInnovationConfirm];
    }
}

- (void) showFullInfoAtIndexPath:(NSIndexPath *) indexPath {
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if (![self.fullLearningCommentsIds containsObject:feed.learning.feedId]) {
            [self.fullLearningCommentsIds addObject:feed.learning.feedId];
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (void) showUrlLinkAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (feed.learning.siteUrl.length > 0 && [feed.learning.siteUrl isValidURL] ) {
        [SFSafariManager openURLWithSafariController:feed.learning.siteUrl forController:weakSelf];
    }
    
}

- (void) showVoteRateAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    LearningRatingVoteVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[LearningRatingVoteVC storyboardIdentifier]];
    vc.learning = feed.learning;
    vc.isFromLearningList = NO;
    
    vc.onBackAction = ^() {
        weakSelf.fetchedResultsController = nil;
        
        if ([weakSelf.tableView numberOfRowsInSection:0] > indexPath.row && indexPath != nil) {
            [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    
    };
    
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}

-(PaginationInfo *) currentPagination {
    if (self.isSearchState) {
        return self.searchPagination;
    } else {
        return self.pagination;
    }
}

-(NavigationBar *) navigationBarView {
    return self.navigationBar;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"commentsList"]) {
        __weak typeof(self) weakSelf = self;
        
        CommentsVC *dest = segue.destinationViewController;
        dest.feed = sender;
        dest.needRefresh = ^(BOOL isNeedRefresh) {
            weakSelf.isNeedRefreshAfterNewComment = YES;
        };
        
    }
    
    if ([segue.identifier isEqualToString:@"retapsList"]) {
        RetapsListVC *dest = segue.destinationViewController;
        dest.feed = sender;
    }
}

- (void) showQuestionForAnswer {
    QuestionAnswerVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"QuestionAnswerVC"];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    self.needShowActivityFeedQuestion = NO;
}

- (void) showActivityFeedChallengeInfo {
    ChallengeInfoVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[ChallengeInfoVC storyboardIdentifier]];
    vc.screenState = ScreenStateFromActivityFeed;
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    self.needShowActivityFeedChallenge = NO;
}

- (void) showCurrentChallenge {
    ChallengeInfoVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[ChallengeInfoVC storyboardIdentifier]];
    vc.screenState = ScreenStateFromNavigationBar;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) showInnovationAnswer {
    InnovationsAnswerVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[InnovationsAnswerVC storyboardIdentifier]];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
    self.needShowActivityFeedInnovationAnswer = NO;
}

- (void) showInnovationConfirm {
    UIViewController *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:@"InnovationsConfirmNC"];

    [self presentViewController:vc animated:YES completion:nil];
    self.needShowActivityFeedInnovationConfirm = NO;
}

- (void) showBadgeListIfNeeded {
    if ([AppDelegate theApp].isNeedShowChallengeBadgeList) {
        [AppDelegate theApp].isNeedShowChallengeBadgeList = NO;
        [[AppDelegate theApp].mainViewController showChallengeBadgeListForCurrentUser];
    }
}

#pragma mark - Fetch Result Controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [ActivityFeed MR_requestAllSortedBy:@"createDate" ascending:NO inContext:[DataManager context]];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"isTopPoint" ascending:NO],[NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO]];
    NSFetchedResultsController *fetchController = [ActivityFeed MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
     
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}


#pragma mark - UITableView Delegate & DataSource methods
 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if ([self currentPagination].currentPage < [self currentPagination].totalPages) {
        self.tablePlaceholder.hidden = YES;
        self.emptyPlaceholder.hidden = YES;
        return number + 1;
    }
    
    //Show empty search result
    if (number == 0 && self.navigationBar.barView.searchBar.text.length > 0) {
        if (self.tablePlaceholder == nil) {
            [self createTablePlaceholder];
        }
        self.tablePlaceholder.hidden = NO;
    }
    //Show if empty feed
    else if (number == 0 && !self.isSearchState) {
        if (self.emptyPlaceholder == nil) {
            [self createTablePlaceholderForEmptyFeed];
        }
        //[self.tableView numberOfRowsInSection:0];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([self.tableView numberOfRowsInSection:0] == 0 && !self.isSearchState) {
                self.emptyPlaceholder.hidden = NO;
            }
            else {
                self.emptyPlaceholder.hidden = YES;
            }
        });
        
    }
    //Hide all
    else {
        self.tablePlaceholder.hidden = YES;
        self.emptyPlaceholder.hidden = YES;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        
        ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if ([feed feedType] == ActivityFeedTypeTap) {
            return [self activityFeedCell:tableView indexPath:indexPath];
        }
        
        if ([feed feedType] == ActivityFeedTypeTeamMessage) {
            return [self teamMessageFeedCell:tableView indexPath:indexPath];
        }
        
        if ([feed feedType] == ActivityFeedTypeTopPoint) {
            return [self pointWinnerFeedCell:tableView indexPath:indexPath];
        }
        
        if ([feed feedType] == ActivityFeedTypeChallenge) {
            return [self challengeWinnerFeedCell:tableView indexPath:indexPath];
        }
        
        if ([feed feedType] == ActivityFeedTypeLearning) {
            //Link
            if (feed.learning.siteUrl.length > 0) {
                return [self learningLinkCell:tableView indexPath:indexPath];
            }
            //Comment
            else {
                return [self learningCommentCell:tableView indexPath:indexPath];
            }
        }
        
    } else {
        return [LoadingCell cell];
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        
        ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if ([feed feedType] == ActivityFeedTypeTap) {
            return [ActivityFeedCell cellHeight:feed.body andBehaviorText:feed.behaviourTitle width:tableView.frame.size.width];
        }
        
        if ([feed feedType] == ActivityFeedTypeTeamMessage) {
            return [ActivityFeedTeamMessageCell cellHeight:feed.body width:tableView.frame.size.width];
        }
        
        if ([feed feedType] == ActivityFeedTypeTopPoint) {
            return [ActivityFeedPointCell cellHeight];
        }
        
        if ([feed feedType] == ActivityFeedTypeChallenge) {
            return [ActivityFeedChallengeCell cellHeight];
        }
        
        if ([feed feedType] == ActivityFeedTypeLearning) {
            if (feed.learning.siteUrl.length > 0) {
                return [self heightForCellWithIdentifier:[LearningLinkCell cellReuseIdentifier] atIndexPath:indexPath];
            }
            else {
                return [self heightForCellWithIdentifier:[LearningCommentCell cellReuseIdentifier] atIndexPath:indexPath];
            }
        }
    }
    
    return 80; //loading cell height
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if ([feed feedType] == ActivityFeedTypeTap) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeTap];
            return;
        }
        
        if ([feed feedType] == ActivityFeedTypeTeamMessage) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeTeamMessage];
            return;
        }
        
        if ([feed feedType] == ActivityFeedTypeTopPoint) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeTopPoints];
            return;
        }
        
        if ([feed feedType] == ActivityFeedTypeChallenge) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeChallenge];
            return;
        }
        
        if ([feed feedType] == ActivityFeedTypeLearning) {
            if (feed.learning.siteUrl.length > 0) {
                [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeLearningLink];
                return;
            }
            else {
                [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeLearningComment];
                return;
            }
        }
        
    }
    else {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypePaginationSpinner];
        return;
        
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isLoadingCell]) {
        if (self.isSearchState) {
            [self loadNextSearchData];
        } else {
            [self loadNextFeedData];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.navigationBar.barView.searchBar resignFirstResponder];
}

#pragma mark - Cells

-(ActivityFeedCell *) activityFeedCell :(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ActivityFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    [cell addFeed:feed];
    
    //Detecting new indexPath row need when some cell was deleted (not reload all table view) and then try to show some info of them.
    cell.onComments = ^ (ActivityFeedCell *commentReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionComments label:ENAnalyticLabelFeedTypeTap];
        NSIndexPath *indexPathCommentReturned = [weakSelf.tableView indexPathForCell:commentReturnedCell];
        [weakSelf showCommentsForItemAtIndexPath:indexPathCommentReturned];
    };
    cell.onRetap = ^ (ActivityFeedCell *retapReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionRetapConfirm label:ENAnalyticLabelFeedTypeTap];
        NSIndexPath *indexPathRetapReturned = [weakSelf.tableView indexPathForCell:retapReturnedCell];
        [weakSelf retapItemAtIndexPath:indexPathRetapReturned];
    };
    cell.onShowRetaps = ^ (ActivityFeedCell *showRetapReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionRetapList label:ENAnalyticLabelFeedTypeTap];
        NSIndexPath *indexPathShowRetapsReturned = [weakSelf.tableView indexPathForCell:showRetapReturnedCell];
        [weakSelf showRetapsForItemAtIndexPath:indexPathShowRetapsReturned];
    };
    cell.onShowReceiver = ^ (ActivityFeedCell *showReceiverReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionReceiverProfile label:ENAnalyticLabelFeedTypeTap];
        NSIndexPath *indexPathShowReceiverReturned = [weakSelf.tableView indexPathForCell:showReceiverReturnedCell];
        [weakSelf showReceiverUserProfileForItemAtIndex:indexPathShowReceiverReturned];
    };
    cell.onShowSender = ^ (ActivityFeedCell *showSenderReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionSenderProfile label:ENAnalyticLabelFeedTypeTap];
        NSIndexPath *indexPathShowSenderReturned = [weakSelf.tableView indexPathForCell:showSenderReturnedCell];
        [weakSelf showSenderUserProfileForItemAtIndex:indexPathShowSenderReturned];
    };
    cell.onShowBehavior = ^ (ActivityFeedCell *showBehaviorReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionBehavior label:ENAnalyticLabelFeedTypeTap];
        NSIndexPath *indexPathShowBehaviorReturned = [weakSelf.tableView indexPathForCell:showBehaviorReturnedCell];
        [weakSelf showBehaviorDetailForItemAtIndex:indexPathShowBehaviorReturned];
    };
    
    if ([self canDeleteFeed:feed]) {
        cell.rightUtilityButtons = [self deleteButton];
    }
    
    cell.delegate = self;
    
    return cell;
}

-(ActivityFeedTeamMessageCell *) teamMessageFeedCell :(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ActivityFeedTeamMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"teamMessageCell"];
    [cell addFeed:feed];
    
    cell.onShowSender = ^ (ActivityFeedTeamMessageCell *showSenderReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionSenderProfile label:ENAnalyticLabelFeedTypeTeamMessage];
        NSIndexPath *indexPathShowSender = [weakSelf.tableView indexPathForCell:showSenderReturnedCell];
        [weakSelf showSenderUserProfileForItemAtIndex:indexPathShowSender];
    };
    
    return cell;
}

- (ActivityFeedPointCell *) pointWinnerFeedCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ActivityFeedPointCell *cell = [tableView dequeueReusableCellWithIdentifier:[ActivityFeedPointCell cellReuseIdentifier]];
    [cell addFeed:feed];
    cell.onShowReceiver = ^ (ActivityFeedPointCell *showReceiverReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionReceiverProfile label:ENAnalyticLabelFeedTypeTopPoints];
        NSIndexPath *indexPathShowReceiver = [weakSelf.tableView indexPathForCell:showReceiverReturnedCell];
        [weakSelf showReceiverUserProfileForItemAtIndex:indexPathShowReceiver];
    };
    cell.onShowTopPoints = ^ (ActivityFeedPointCell *showTopPointReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionTapTryPointsIcon label:ENAnalyticLabelFeedTypeTopPoints];
    };
    
    return cell;
}

- (ActivityFeedChallengeCell *) challengeWinnerFeedCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {

    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ActivityFeedChallengeCell *cell = [tableView dequeueReusableCellWithIdentifier:[ActivityFeedChallengeCell cellReuseIdentifier]];
    [cell addFeed:feed];
    
    cell.onShowReceiver = ^ (ActivityFeedChallengeCell *showReceiverReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionReceiverProfile label:ENAnalyticLabelFeedTypeChallenge];
        NSIndexPath *indexPathShowReceiver = [weakSelf.tableView indexPathForCell:showReceiverReturnedCell];
        [weakSelf showReceiverUserProfileForItemAtIndex:indexPathShowReceiver];
    };
    
    cell.onShowChallenge = ^(ActivityFeedChallengeCell *showChallengeReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionChallenge label:ENAnalyticLabelFeedTypeChallenge];
        NSIndexPath *indexPathShowChallenge = [weakSelf.tableView indexPathForCell:showChallengeReturnedCell];
        [weakSelf showChallengeDetailForItemAtIndex:indexPathShowChallenge];
    };
    
    return cell;
}

- (LearningLinkCell *) learningLinkCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    LearningLinkCell *cell = [tableView dequeueReusableCellWithIdentifier:[LearningLinkCell cellReuseIdentifier]];
    
    if ([self.fullLearningCommentsIds containsObject:feed.learning.feedId]) {
        [cell addLearning:feed.learning fullInfo:YES];
    }
    else {
        [cell addLearning:feed.learning fullInfo:NO];
    }
    
    cell.onShowMore = ^(LearningLinkCell *showMoreReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionShowMore label:ENAnalyticLabelFeedTypeLearningLink];
        NSIndexPath *indexPathShowMore = [weakSelf.tableView indexPathForCell:showMoreReturnedCell];
        [weakSelf showFullInfoAtIndexPath:indexPathShowMore];
    };
    
    cell.onShowUrlLink = ^(LearningLinkCell *urlLinkReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionLink label:ENAnalyticLabelFeedTypeLearningLink];
        NSIndexPath *indexPathUrlLink = [weakSelf.tableView indexPathForCell:urlLinkReturnedCell];
        [weakSelf showUrlLinkAtIndexPath:indexPathUrlLink];
    };
    
    cell.onShowSenderInfo = ^(LearningLinkCell *senderInfoReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionSenderProfile label:ENAnalyticLabelFeedTypeLearningLink];
        NSIndexPath *senderInfoIndexPath = [weakSelf.tableView indexPathForCell:senderInfoReturnedCell];
        [weakSelf showSenderUserProfileForItemAtIndex:senderInfoIndexPath];
    };
    
    cell.onShowVote = ^(LearningLinkCell *voteReturnedCell) {
        NSIndexPath *voteIndexPath = [weakSelf.tableView indexPathForCell:voteReturnedCell];
        
        if (feed.learning.senderIdValue == [DataManager user].userIdValue) {
            [weakSelf showAlertWithMessage:NSLocalizedString(@"You can't rate your article", @"v1.0")];
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionRateVoteTry label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [weakSelf showVoteRateAtIndexPath:voteIndexPath];
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionRateVoteConfirm label:ENAnalyticLabelFeedTypeLearningLink];
        }
    };
    
    if ([self canDeleteLearning:feed.learning]) {
        cell.rightUtilityButtons = [self deleteButton];
    }
    
    cell.delegate = self;
    
    return cell;
}

- (LearningCommentCell *) learningCommentCell:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    LearningCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[LearningCommentCell cellReuseIdentifier]];
    
   ;
    
    if ([self.fullLearningCommentsIds containsObject:feed.learning.feedId]) {
        [cell addLearning:feed.learning fullInfo:YES];
    }
    else {
        [cell addLearning:feed.learning fullInfo:NO];
    }
    
    cell.onShowMore = ^(LearningCommentCell *showMoreReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionShowMore label:ENAnalyticLabelFeedTypeLearningComment];
        NSIndexPath *indexPathShowMore = [weakSelf.tableView indexPathForCell:showMoreReturnedCell];
        [weakSelf showFullInfoAtIndexPath:indexPathShowMore];
    };
    
    cell.onShowSenderInfo = ^(LearningCommentCell*senderInfoReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionSenderProfile label:ENAnalyticLabelFeedTypeLearningComment];
        NSIndexPath *senderInfoIndexPath = [weakSelf.tableView indexPathForCell:senderInfoReturnedCell];
        [weakSelf showSenderUserProfileForItemAtIndex:senderInfoIndexPath];
    };
    
    cell.onShowVote = ^(LearningCommentCell *voteReturnedCell) {
        NSIndexPath *voteIndexPath = [weakSelf.tableView indexPathForCell:voteReturnedCell];
        
        if (feed.learning.senderIdValue == [DataManager user].userIdValue) {
            [weakSelf showAlertWithMessage:NSLocalizedString(@"You can't rate your article", @"v1.0")];
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionRateVoteTry label:ENAnalyticLabelFeedTypeLearningComment];
        }
        else {
            [weakSelf showVoteRateAtIndexPath:voteIndexPath];
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionRateVoteConfirm label:ENAnalyticLabelFeedTypeLearningComment];
        }
    };
    
    if ([self canDeleteLearning:feed.learning]) {
        cell.rightUtilityButtons = [self deleteButton];
    }
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - Cells Height

- (CGFloat) heightForCellWithIdentifier:(NSString *) identifier atIndexPath:(NSIndexPath *) indexPath {
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([identifier isEqualToString:[LearningLinkCell cellReuseIdentifier]]) {
        
        CGFloat height = [LearningLinkCell cellHeightForLearning:feed.learning withFullInfo:YES];
        CGFloat collapsedHeight = [LearningLinkCell cellHeightForLearning:feed.learning withFullInfo:NO];
        
        if (height < collapsedHeight) {
            [self.fullLearningCommentsIds addObject:feed.learning.feedId];
        }
        else {
            if (![self.fullLearningCommentsIds containsObject:feed.learning.feedId]) {
                return collapsedHeight;
            }
        }
        return height;
    }
    
    if ([identifier isEqualToString:[LearningCommentCell cellReuseIdentifier]]) {
        
        CGFloat height = [LearningCommentCell cellHeightForLearning:feed.learning fullInfo:YES];
        CGFloat collapsedHeight = [LearningCommentCell cellHeightForLearning:feed.learning fullInfo:NO];
        
        if (height < collapsedHeight && feed.learning.feedId != nil) {
            [self.fullLearningCommentsIds addObject:feed.learning.feedId];
        }
        else {
            if (![self.fullLearningCommentsIds containsObject:feed.learning.feedId]) {
                return collapsedHeight;
            }
        }
        
        return height;
    }
    
    else {
        return 80; //default height
    }
    
}


#pragma mark - SWTableViewCellDelegate

//Can swipe only one row in time (close if swipe another)
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([feed feedType] == ActivityFeedTypeTap) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteSwipeConfirm label:ENAnalyticLabelFeedTypeTap];
        [self showDeleteAlertForIndexPath:indexPath];
        return;
    }
    
    if ([feed feedType] == ActivityFeedTypeLearning) {
        if (feed.learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteSwipeConfirm label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteSwipeConfirm label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
        [self showDeleteAlertForLearningAtIndexPath:indexPath];
        return;
    }

}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([feed feedType] == ActivityFeedTypeTap && [self canDeleteFeed:feed]) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteSwipe label:ENAnalyticLabelFeedTypeTap];
        return YES;
    }
    
    if ([feed feedType] == ActivityFeedTypeLearning && [self canDeleteLearning:feed.learning]) {
        if (feed.learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteSwipe label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteSwipe label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
        return YES;
    }
    
    return NO;
    
}

- (NSArray *) deleteButton {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor clearColor] icon:[UIImage imageNamed:@"icon-delete2"]];
    
    return rightUtilityButtons;
}

#pragma mark - Alerts

- (void) showDeleteAlertForIndexPath:(NSIndexPath *) indexPath {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to delete tap?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteAlertCancel label:ENAnalyticLabelFeedTypeTap];
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteAlertConfirm label:ENAnalyticLabelFeedTypeTap];
        [self deleteFeedAtIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) showDeleteAlertForLearningAtIndexPath:(NSIndexPath *) indexPath {
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to delete learning?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (feed.learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteAlertCancel label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteAlertCancel label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        if (feed.learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteAlertConfirm label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionDeleteAlertConfirm label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
        [self deleteFeedAtIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Update UI

- (void) hideButtonsForIndexPath: (NSIndexPath *) indexPath {
    
    ActivityFeed *feed = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([feed feedType] == ActivityFeedTypeTap) {
        ActivityFeedCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell hideUtilityButtonsAnimated:YES];
        return;
    }
    
    if ([feed feedType] == ActivityFeedTypeLearning) {
        SWTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell hideUtilityButtonsAnimated:YES];
        return;
    }
}


#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    CGRect keyboardFrame = [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.tableBottomSpace.constant = keyboardFrame.size.height - self.tableView.contentInset.bottom;
    [self.view setNeedsLayout];
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    self.tableBottomSpace.constant = 0;
    [self.view setNeedsLayout];
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

#pragma mark NavigationBarSearchDelegate

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    [self stopSearch];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [AnalyticsManager trackEvent:ENAnalyticScreenMainActivityFeed action:ENAnalyticActionSearch label:ENAnalyticLabelNavigationBar];
    [self startSearch];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    [self searchText:searchText page:self.searchPagination.currentPage];
}

#pragma mark Search

-(void) startSearch {
    self.searchPagination = [[PaginationInfo alloc] init];
    self.searchPagination.currentPage = 1;
    self.searchPagination.totalPages = 1;
    
    self.isSearchState = YES;
    
    [self removeDownRefresh];
}

-(void) stopSearch {
    self.tablePlaceholder.hidden = YES;
    self.navigationBar.barView.searchBar.text = nil;
    self.searchPagination = nil;
    
    self.isSearchState = NO;
    
    
    [self addDownRefresh];
    [self loadFeedData];
}

-(void) searchText:(NSString *) text page:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    weakSelf.searchPagination.isLoading = YES;
    [[RequestManager sharedManager] searchInActivityFeed:text page:page onCompleate:^(BOOL success, NSArray *list, PaginationInfo *pagination) {

        if (success) {
            weakSelf.searchPagination.currentPage = pagination.currentPage;
            weakSelf.searchPagination.totalPages = pagination.totalPages;
            
            if (weakSelf.searchPagination.currentPage == 1) {
                [DataManager saveActivityFeedList:list];
            } else {
                [DataManager appendActivityFeedList:list];
            }
        }
        
        weakSelf.searchPagination.isLoading = NO;
        
        [weakSelf reloadData];
    }];
}

-(void) loadNextSearchData {
    
    if (self.searchPagination.isLoading) return;
    
    NSString *text = self.navigationBar.barView.searchBar.text;
    NSInteger page = self.searchPagination.currentPage + 1;
    
    [self searchText:text page:page];
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) text {
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}

- (BOOL) canDeleteFeed:(ActivityFeed *) feed {
    BOOL canDelete = NO;
    
    //Admin and manager can delete any comments
    if ([DataManager user].isAdmin || [DataManager user].isManager) {
        canDelete = YES;
    }
    
    //User can delete his comments
    if ([DataManager user].userIdValue == feed.senderIdValue) {
        canDelete = YES;
    }
    
    return canDelete;
}

- (BOOL) canDeleteLearning:(CDLearning *) learning {
    BOOL canDelete = NO;
    
    //Admin and manager can delete any comments
    if ([DataManager user].isAdmin || [DataManager user].isManager) {
        canDelete = YES;
    }
    
    //User can delete his comments
    if ([DataManager user].userIdValue == learning.senderIdValue) {
        canDelete = YES;
    }
    
    return canDelete;
}

- (BOOL) isNeedAccessCenterNavigationBar {
    
    if ([FeaturesAccessManager isAccessDropMenuStyle:DropMenuStyleTop forScreen:AppScreenTypeActivityFeed withUser:[DataManager user]]) {
        return YES;
    }
    
    return NO;
}

- (BOOL) isNeedAccessChallenge {
    
    if ([FeaturesAccessManager isAccessFeature:AppFeatureTypeChallenge forScreen:AppScreenTypeActivityFeed withUser:[DataManager user]]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - SFSafariViewControllerDelegate

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
