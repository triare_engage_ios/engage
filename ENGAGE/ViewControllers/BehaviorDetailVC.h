//
//  BehaviorDetailVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface BehaviorDetailVC : ContentViewController
@property (nonatomic, strong) Behavior *item;
@property (nonatomic, copy) void (^onHideAction)();

@end
