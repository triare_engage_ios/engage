//
//  QuestionTextFieldCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionTextFieldCell.h"

@interface QuestionTextFieldCell () <UITextFieldDelegate>

@end

@implementation QuestionTextFieldCell

#pragma mark - Public

- (void) setAttributePlaceholderText:(NSString *) text {
    UIColor *color = [UIColor appGrayColor];
    UIFont *font = [UIFont appFont:17];
    
    self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName: color, NSFontAttributeName: font}];
}

#pragma mark - Private

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.textField.delegate = self;
    self.deleteButton.hidden = NO;
    
    self.textField.textColor = [UIColor appTextColor];
    self.textField.font = [UIFont appFont:17];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.deleteButton.hidden = NO;
    self.index = -1;
    
    self.textField.placeholder = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteButtoAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
     
    if (weakSelf.onAction) {
        weakSelf.onAction (weakSelf);
    }
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    __weak typeof(self) weakSelf = self;
    
    //New textfield string
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    //send text changes
    if (weakSelf.onChangeText) {
        weakSelf.onChangeText (proposedNewString, weakSelf);
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.textField resignFirstResponder];
    
    return YES;
}



@end
