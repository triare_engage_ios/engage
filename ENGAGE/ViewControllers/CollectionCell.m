//
//  CollectionCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "CollectionCell.h"
#import "UIView+Extends.h"

@interface CollectionCell ()

@property (nonatomic, strong) UIColor *borderColor;

@end

@implementation CollectionCell

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeAppRoundedView:self.cellImageView];
    
    self.cellLabel.textColor = [UIColor appTextColor];
    self.cellLabel.font = [UIFont appFontThin:16];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    if (self.cellType == CollectionCellBadge) {
        self.cellImageView.layer.borderColor = self.borderColor.CGColor;
        self.cellImageView.layer.borderWidth = 3.0;
    }
}

- (void) prepareForReuse {
    [super prepareForReuse];
    self.borderColor = nil;
    self.cellType = CollectionCellDefault;
}

#pragma mark - Public Methods

+ (CGSize) sizeForCollectionWidth:(CGFloat) width {
    CGFloat cellWidth = width/3;
    return CGSizeMake(cellWidth, cellWidth+30);
}

- (void) fillCustomData {
    [self.cellImageView sd_setImageWithURL:[NSURL URLWithString:@"http://i.imgur.com/jjIZ4Kn.png"] placeholderImage:[Behavior placeholder]];
    self.cellLabel.text = NSLocalizedString(@"The Fastest", @"v1.0");
    self.borderColor = [UIColor redColor];
}

- (void) addChallenge:(Challenge *) challenge {
    [self.cellImageView sd_setImageWithURL:[NSURL URLWithString:challenge.imageIconUrl] placeholderImage:[Behavior placeholder]];
    self.cellLabel.text = challenge.title;
    self.borderColor = [UIColor colorFromHexString:challenge.borderColorHex];
}

#pragma mark - Properties

-(UIColor *) borderColor {
    if (_borderColor == nil) {
        _borderColor = [UIColor yellowColor];
    }
    
    return _borderColor;
}

@end
