//
//  GiveTapNavController.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "GiveTapNavController.h"
#import "GiveTapStep2VC.h"
#import "GiveTapStep1VC.h"
#import "GiveTapStep2_1VC.h"

@implementation GiveTapNavController

-(void) setUserId:(NSNumber *) userId {
    TeamUser *user = [DataManager teamUserWithId:userId];
    
    if (user == nil) {
        return;
    }
    
    self.user = user;
    
    GiveTapStep2_1VC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GiveTapStep2_1VC"];
    
    //Need for skip first step if give tap from user profile
    vc.isNeedSkipFirstStepWhenBack = YES;
    [self removeFirstStep];
    
    [self showViewController:vc sender:nil];
}

- (void) removeFirstStep {
    NSMutableArray* tempVCA = [NSMutableArray arrayWithArray:[self viewControllers]];
    
    NSMutableArray *toDelete = [NSMutableArray array];
    
    for(UIViewController *tempVC in tempVCA)
    {
        if ([tempVC isKindOfClass:[GiveTapStep1VC class]]) {
            [toDelete addObject:tempVC];
        }
    }
    [tempVCA removeObjectsInArray:toDelete];
    
    self.viewControllers = tempVCA;
}

@end
