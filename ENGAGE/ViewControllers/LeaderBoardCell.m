//
//  LeaderBoardCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 18.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "LeaderBoardCell.h"

#define kDefaultUserBorderWidth 0.0
#define kDefaultUserBorderColor [UIColor clearColor]

@interface LeaderBoardCell ()

@end

@implementation LeaderBoardCell

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeRoundedView:self.userImageView];
    //[self.userImageView makeCircle];
    
    self.nameLabel.textColor = [UIColor appTextColor];
    self.nameLabel.font = [UIFont appFont:17];
    
    self.jobLabel.textColor = [UIColor appTextColor];
    self.jobLabel.font = [UIFont appFontThin:17];
    
    self.pointsLabel.textColor = [UIColor appTextColor];
    self.pointsLabel.font = [UIFont appFont:17];
    
    self.leadPositionLabel.textColor = [UIColor appTextColor];
    self.leadPositionLabel.font = [UIFont appFontBold:22];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.userImageView.layer.borderWidth = self.userBorderWidth.floatValue;
    self.userImageView.layer.borderColor = self.userBorderColor.CGColor;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.userBorderColor = nil;
    self.userBorderWidth = nil;
}

#pragma mark - Properties

- (NSNumber *) userBorderWidth {
    if (_userBorderWidth == nil) {
        _userBorderWidth = [NSNumber numberWithFloat:kDefaultUserBorderWidth];
    }
    
    return _userBorderWidth;
}

- (UIColor *) userBorderColor {
    if (_userBorderColor == nil) {
        _userBorderColor = kDefaultUserBorderColor;
    }
    
    return _userBorderColor;
}

@end



@implementation LeaderBoardMoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.moreLabel.text = NSLocalizedString(@"SHOW ALL", @"v1.0");
    self.moreLabel.textColor = [UIColor appOrangeColor];
    self.moreLabel.font = [UIFont appFontBold:17];

}

@end
