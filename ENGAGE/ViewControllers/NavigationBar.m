//
//  NavigationBar.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "NavigationBar.h"

#define SEARCH_ANIMATION_DURATION .3


@interface NavigationBar () <UISearchBarDelegate>

@end



@implementation NavigationBar

#pragma mark - Life Cycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:blurEffectView];
    [self sendSubviewToBack:blurEffectView];
    
    self.barView = [[NSBundle mainBundle] loadNibNamed:@"NavigationBarView" owner:self options:nil].lastObject;
    self.barView.frame = self.bounds;
    self.barView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self addSubview:self.barView];
    
    [self.barView.leftButton addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [self.barView.rightButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.backgroundColor = [UIColor clearColor];
    self.barView.backgroundColor = [UIColor clearColor];
    self.barView.titleLabel.textColor = [UIColor appNavigationTextColor];
    self.barView.titleLabel.font = [UIFont appFontBold:18];
    
    //Center action
    self.barView.titleLabel.hidden = YES;
    [self.barView.titleButton setTitleColor:[UIColor appNavigationTextColor] forState:UIControlStateNormal];
    self.barView.titleButton.titleLabel.font = [UIFont appFontBold:18];
    self.barView.titleButton.userInteractionEnabled = NO;
    [self.barView.titleButton addTarget:self action:@selector(centerAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.superview bringSubviewToFront:self];
    
    self.barView.leftButton.hidden = YES;
    self.barView.rightButton.hidden = YES;
    
    [self applyHeight];
    
    [self initSearchView];
    
    [self registerForMenuNotification];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:EN_NOTIFICATION_NAVIGATION_BAR_DROP_MENU_CLOSED object:nil];
}

#pragma mark - Action

- (void)leftAction {
    if (self.onLeftAction) {
        self.onLeftAction();
    }
}

- (void) centerAction {
    if (self.onCenterAction) {
        [self rotateCenterButtonImage];
        self.onCenterAction();
    }
}

- (void)rightAction {
    if (self.onRightAction) {
        self.onRightAction();
    } else {
        [self showSearchBar];
    }
}

- (void)setTitle:(NSString *)title {
    [self.barView.titleButton setTitle:title forState:UIControlStateNormal];
    //self.barView.titleLabel.text = title;
}

-(void) showLeftButton {
    self.barView.leftButton.hidden = NO;
}

-(void) showRightButton {
    self.barView.rightButton.hidden = NO;
}

-(void) hideRightButton {
    self.barView.rightButton.hidden = YES;
}

-(void) rightButtonEnable:(BOOL) enable {
    [self.barView.rightButton setEnabled:enable];
}

-(void) setRightButtonText:(NSString *) text {
    [self.barView.rightButton setTitle:text forState:UIControlStateNormal];
}

-(void) changeLeftButtonIcon:(UIImage *) image {
    [self.barView.leftButton setImage:image forState:UIControlStateNormal];
}

-(void) changeRightButtonIcon:(UIImage *) image {
    [self.barView.rightButton setImage:image forState:UIControlStateNormal];
}

-(void) applyHeight {
    NSLayoutConstraint *heightConstraint;
    for (NSLayoutConstraint *constraint in self.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            heightConstraint = constraint;
            break;
        }
    }
    heightConstraint.constant = EN_NAVIGATION_BAR_HEIGHT;
}

#pragma mark - Center part

-(void) centerInteractionEnable:(BOOL) enable {
    self.barView.titleButton.userInteractionEnabled = enable;
}

-(void) addCenterTitleIcon:(UIImage *) image {
    [self.barView.titleButton setImage:image forState:UIControlStateNormal];
    [self.barView.titleButton sizeToFit];
    self.barView.titleButton.titleEdgeInsets = UIEdgeInsetsMake(0, -self.barView.titleButton.imageView.frame.size.width, 0, self.barView.titleButton.imageView.frame.size.width);
    self.barView.titleButton.imageEdgeInsets = UIEdgeInsetsMake(0, self.barView.titleButton.titleLabel.frame.size.width + 5, 0, -self.barView.titleButton.titleLabel.frame.size.width - 5);
}

-(void) registerForMenuNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotateCenterButtonImageBack) name:EN_NOTIFICATION_NAVIGATION_BAR_DROP_MENU_CLOSED object:nil];
}

-(void) rotateCenterButtonImageBack {
    [UIView animateWithDuration:.2 animations:^{
        self.barView.titleButton.imageView.transform = CGAffineTransformMakeRotation((0 *M_PI) / 180.0);
    }];
}

- (void) rotateCenterButtonImage {
    [UIView animateWithDuration:.2 animations:^{
        self.barView.titleButton.imageView.transform = CGAffineTransformMakeRotation((-180.0 *M_PI) / 180.0);
    }];
}


#pragma mark - Search

-(void) initSearchView {
    [self.barView.searchCancelBt setTitleColor:[UIColor appTextColor] forState:UIControlStateNormal];
    [self.barView.searchCancelBt setTitle:NSLocalizedString(@"Cancel", @"v1.0") forState:UIControlStateNormal];
    [self.barView.searchCancelBt addTarget:self action:@selector(hideSearchAction) forControlEvents:UIControlEventTouchUpInside];
    self.barView.searchCancelBt.titleLabel.font = [UIFont appFont:16];
    
    UIImage *im = [[UIImage imageNamed:@"searchBackground"] resizableImageWithCapInsets:UIEdgeInsetsMake(14, 30, 14, 30)];
    self.barView.searchBar.backgroundImage = [UIImage new];
    [self.barView.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"searchBackgroundClear"] forState:UIControlStateNormal];
    self.barView.searchBarBackground.image = im;
    
    self.barView.searchBar.delegate = self;
}

-(CGFloat) hiddenSearchBarWidth {
    return self.bounds.size.width - 140;
}

-(void) showSearchBar {
    self.barView.contentView.hidden = YES;
    
    self.barView.searchView.hidden = NO;
    self.barView.searchView.alpha = 0;
    
    self.barView.searchViewLeftSpace.constant = [self hiddenSearchBarWidth];
    [self.barView updateConstraints];
    
    self.barView.searchViewLeftSpace.constant = 0;
    [self.barView setNeedsLayout];
    
    
    [UIView animateWithDuration:SEARCH_ANIMATION_DURATION
                     animations:^{
                         self.barView.searchView.alpha = 1;
                         [self.barView layoutIfNeeded];
                         
                         [self.barView.searchBar becomeFirstResponder];
                     } completion:^(BOOL finished) {
                         if ([self.searchDelegate respondsToSelector:@selector(navigationBarDidStartSearch:)])
                         {
                             [self.searchDelegate navigationBarDidStartSearch:self];
                         }
                     }];
}

-(void) hideSearchAction {
    
    [self.barView.searchBar resignFirstResponder];
    
    if ([self.searchDelegate respondsToSelector:@selector(navigationBarDidCancelSearch:)])
    {
        [self.searchDelegate navigationBarDidCancelSearch:self];
    }
    
    self.barView.searchBar.text = nil;
    
    [self hideSearchBar:YES];
}

-(void) hideSearchBar:(BOOL) animated {
    
    self.barView.searchViewLeftSpace.constant = [self hiddenSearchBarWidth];
    
    [self.barView setNeedsLayout];
    
    CGFloat duration = SEARCH_ANIMATION_DURATION;
    if (!animated) duration = 0;
    
    [UIView animateWithDuration:duration
                     animations:^{
                         self.barView.searchView.alpha = 0;
                         [self.barView layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         self.barView.searchView.hidden = YES;
                         self.barView.contentView.hidden = NO;
                     }];
}

#pragma mark UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if ([self.searchDelegate respondsToSelector:@selector(searchViewDidBeginEditing:)])
    {
        [self.searchDelegate searchViewDidBeginEditing:searchBar];
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if ([self.searchDelegate respondsToSelector:@selector(searchViewDidEndEditing:)])
    {
        [self.searchDelegate searchViewDidEndEditing:searchBar];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([self.searchDelegate respondsToSelector:@selector(searchView:textDidChange:)])
    {
        [self.searchDelegate searchView:searchBar textDidChange:searchText];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if ([self.searchDelegate respondsToSelector:@selector(searchViewSearchButtonClicked:)])
    {
        [self.searchDelegate searchViewSearchButtonClicked:searchBar];
    }
}



@end
