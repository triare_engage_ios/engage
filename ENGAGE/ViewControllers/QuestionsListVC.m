//
//  QuestionsListVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionsListVC.h"

#import "QuestionsVC.h"
#import "AnswerDetailVC.h"

#import "QuestionsListCell.h"
#import "UITableViewCell+Separator.h"

@interface QuestionsListVC () <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation QuestionsListVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self loadData];
    [self addDownRefresh];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionList];
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionList withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:NSLocalizedString(@"QUESTIONS HISTORY", @"v1.0")];
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    
    self.navigationBar.onRightAction = ^{
        [weakSelf showAddQuestion];
    };
    [self.navigationBar changeRightButtonIcon:[UIImage imageNamed:@"icon-teamMessage"]];
    [self.navigationBar showRightButton];
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

#pragma mark - Data

- (void) loadData {
    
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getQuestionsListSuccess:^(NSArray *list) {
        [DataManager saveQuestionsList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf.refreshControl endRefreshing];
        [weakSelf showAlertWithTitle:nil andText:message];
    }];
}

- (void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.contentTableView reloadData];
}

#pragma mark - Action

- (void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) showAddQuestion {
    __weak typeof(self) weakSelf = self;
    
    QuestionsVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"QuestionsVC"];
    vc.onBackAction = ^{
        [weakSelf loadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) showQuestionDetail:(Question *) question {
    AnswerDetailVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AnswerDetailVC"];
    vc.question = question;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [Question MR_requestAllSortedBy:@"createdAt" ascending:NO inContext:[DataManager context]];
    NSFetchedResultsController *fetchController = [Question MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Question *question = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    QuestionsListCell *cell = (QuestionsListCell *)[tableView dequeueReusableCellWithIdentifier:@"questionCell"];
    [cell addQuestion:question withUsersCount:question.usersCount];
    [cell showSeparatorLine];
    
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    Question *question = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (question.answeredUsersCount.integerValue > 0) {
        [self showQuestionDetail:question];
    }
    else {
        [self showAlertWithTitle:nil andText:NSLocalizedString(@"There are no any answers yet", @"v1.0")];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    __weak typeof(self) weakSelf = self;
    
    switch (index) {
        case 0:
        {
            NSIndexPath *indexPath = [self.contentTableView indexPathForCell:cell];
            Question *question = self.fetchedResultsController.fetchedObjects[indexPath.row];
            
            [[RequestManager sharedManager] deleteQuestionById:question.questionId success:^(BOOL success) {
                [DataManager deleteQuestionById:question.questionId];
                weakSelf.fetchedResultsController = nil;
                [weakSelf.contentTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            } failure:^(NSString *message) {
                [[AppDelegate theApp] showAlert:NSLocalizedString(@"Question", @"v1.0") text:message];
            }];
        }
            break;
            
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
    
    if (state == kCellStateRight) {
        return YES;
    }
    else {
        return NO;
    }
}

//Can swipe only one row in time (close if swipe another)
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

- (NSArray *)rightButtons {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor redColor] icon:[UIImage imageNamed:@"icon-delete"]];
    
    return rightUtilityButtons;
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.contentTableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadData];
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}


@end
