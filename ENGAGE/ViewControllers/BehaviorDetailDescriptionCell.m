//
//  BehaviorDetailDescriptionCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorDetailDescriptionCell.h"

@implementation BehaviorDetailDescriptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.descriptionLabel.textColor = [UIColor appTextColor];
    self.descriptionLabel.font = [UIFont appFont:17];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
