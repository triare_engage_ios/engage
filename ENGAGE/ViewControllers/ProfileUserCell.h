//
//  ProfileUserCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 16.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENCircleImageView.h"
#import "ENCircleView.h"

@interface ProfileUserCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ENCircleView *userCenterView;
@property (weak, nonatomic) IBOutlet ENCircleImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userJobLabel;
@property (weak, nonatomic) IBOutlet UIButton *evaluateButton;
@property (weak, nonatomic) IBOutlet UILabel *evaluateLabel;
@property (weak, nonatomic) IBOutlet UIButton *giveTapButton;
@property (weak, nonatomic) IBOutlet UILabel *giveTapLabel;



@property (nonatomic, copy) void (^onLeftAction)();
@property (nonatomic, copy) void (^onRigntAction)();

-(void) setSuggestOnLeftButton;
-(void) isPersonalProfile:(BOOL) isPersonal;
-(void) hideGiveTapAndEvaluation;
-(void) showBorderForAvatar;

@end
