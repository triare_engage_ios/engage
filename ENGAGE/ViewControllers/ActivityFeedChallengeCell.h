//
//  ActivityFeedChallengeCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityFeedChallengeCell : UITableViewCell

@property (nonatomic, copy) void (^onShowReceiver)(ActivityFeedChallengeCell *cell);
@property (nonatomic, copy) void (^onShowChallenge)(ActivityFeedChallengeCell *cell);

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

-(void) addFeed:(ActivityFeed *) feed;
-(void) fillWithDefaultData;

@end
