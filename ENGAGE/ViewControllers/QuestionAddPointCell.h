//
//  QuestionAddPointCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionAddPointCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@property (nonatomic, copy) void (^onAction)();

@end
