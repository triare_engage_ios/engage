//
//  Question.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionForSend.h"

@implementation QuestionForSend

-(instancetype) init {
    if ((self = [super init]))
    {
        self.body = @"";
        self.haveMultipleChoice = NO;
        self.haveCustomAnswer = NO;
        self.answers = [[NSMutableArray alloc] init];
        //[self.answers addObject:@""];
        //[self.answers addObject:@""];
        self.isSchedule = NO;
    }
    return self;
}

-(NSDictionary *) serializeForSend {
    
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    [result setValue:self.body forKey:@"body"];
    
    if (self.haveMultipleChoice) {
        [result setValue:@YES forKey:@"multiple"];
    }
    
    if (self.dateTime) {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString = [dateFormatter stringFromDate:self.dateTime];
        [result setValue:dateString forKey:@"starts_at"];
    }
    
    //Set answers
    NSMutableArray *answerArray = [[NSMutableArray alloc] init];
    
    for (NSString *answer in self.answers) {
        NSDictionary *answerDict = @{@"title":answer};
        [answerArray addObject:answerDict];
    }
    
    
    if (self.haveCustomAnswer) {
        NSDictionary *answerDict = @{@"custom":@YES};
        [answerArray addObject:answerDict];
    }
    
    [result setValue:answerArray forKey:@"answer_options_attributes"];
    
    return [result copy];
}

- (BOOL) isAnswersFull {
    
    BOOL isFull = NO;
    
    if (self.answers.count == 4 && self.haveCustomAnswer) {
        isFull = YES;
    }
    
    if (self.answers.count == 5) {
        isFull = YES;
    }
    
    return isFull;
}

- (BOOL) isAnswersMinimum {
    BOOL isStart = YES;
    
    if (self.answers.count <= 2 && !self.haveCustomAnswer) {
        isStart = NO;
    }
    
    return isStart;
}

@end
