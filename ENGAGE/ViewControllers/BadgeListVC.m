//
//  BadgeListVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BadgeListVC.h"
#import "ChallengeInfoVC.h"

#import "CollectionCell.h"
#import "CollectionHeaderView.h"

@interface BadgeListVC () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation BadgeListVC

#pragma mark - Public Methods

+ (NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareNavigationBar];
    [self prepareCollectionView];
    [self loadBadges];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileBadgeList];
    [AnalyticsManager trackScreen:ENAnalyticScreenProfileBadgeList withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Prepare Methods

- (void) prepareNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:[NSLocalizedString(@"badges", @"v1.0") uppercaseString]];
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    [self.navigationBar showLeftButton];
}

- (void) prepareCollectionView {
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT;
    insets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.collectionView.contentInset = insets;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
}

#pragma mark - Data

- (void) loadBadges {
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getChallengeThatWonUser:self.userId success:^(NSArray *list) {
        [DataManager saveChallengeList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithMessage:message];
    }];
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.collectionView reloadData];
}

#pragma mark - Navigation

- (void) showChallengeDetailForIndexPath:(NSIndexPath *) indexPath {
    
    Challenge *c = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ChallengeInfoVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[ChallengeInfoVC storyboardIdentifier]];
    vc.screenState = ScreenStateFromUserProfile;
    vc.challenge = c;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [Challenge MR_requestAllSortedBy:@"createDate" ascending:NO inContext:[DataManager context]];
    
    NSMutableArray *predicates = [NSMutableArray array];
    NSPredicate *userPredicate = [NSPredicate predicateWithFormat:@"receiverId == %@", self.userId];
    NSPredicate *currentChallengePredicate = [NSPredicate predicateWithFormat:@"isCurrentChallenge == %@", [NSNumber numberWithBool:NO]];
    NSPredicate *activityFeedChallengePredicate = [NSPredicate predicateWithFormat:@"isActivityFeedChallenge == %@", [NSNumber numberWithBool:NO]];
    [predicates addObject:userPredicate];
    [predicates addObject:currentChallengePredicate];
    [predicates addObject:activityFeedChallengePredicate];
    fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    
    NSFetchedResultsController *fetchController = [Challenge MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Challenge *c = self.fetchedResultsController.fetchedObjects[indexPath.row];
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.cellType = CollectionCellBadge;
    [cell addChallenge:c];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [CollectionCell sizeForCollectionWidth:collectionView.frame.size.width];
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self showChallengeDetailForIndexPath:indexPath];
}

//header
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                        UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    
    return headerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    return CGSizeMake(200, 40);
}

#pragma mark - Helpers Methods 

- (void) showAlertWithMessage:(NSString *) text {
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}

@end
