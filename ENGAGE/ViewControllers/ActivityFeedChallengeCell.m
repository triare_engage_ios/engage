//
//  ActivityFeedChallengeCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ActivityFeedChallengeCell.h"

#import "ENCircleView.h"
#import "ENCircleImageView.h"


#define kDefaultCellHeight 110.0
#define kDefaultCellIdentifier @"challengeCell"

@interface ActivityFeedChallengeCell ()
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;

@property (weak, nonatomic) IBOutlet ENCircleImageView *receiverImageView;
@property (weak, nonatomic) IBOutlet UILabel *receiverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiverSubtitleLabel;

@property (weak, nonatomic) IBOutlet ENCircleView *challengeBackgroundView;
@property (weak, nonatomic) IBOutlet ENCircleImageView *challengeImageView;
@property (weak, nonatomic) IBOutlet UILabel *challengeTitleLabel;

@property (nonatomic, strong) UIColor *challengeBorderColor;
@property (nonatomic, strong) UIColor *receiverBorderColor;
@property (nonatomic, strong) NSNumber *receiverBorderWidth;

@end

@implementation ActivityFeedChallengeCell

#pragma mark - Public methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

-(void) addFeed:(ActivityFeed *) feed {
    if ([feed feedType] == ActivityFeedTypeChallenge) {
        
        [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:feed.challenge.backgroundImageUrl]];
        [self.receiverImageView sd_setImageWithURL:[NSURL URLWithString:feed.challenge.receiverAvatarUrl] placeholderImage:[TeamUser placeholder]];
        
        [self.challengeImageView sd_setImageWithURL:[NSURL URLWithString:feed.challenge.imageIconUrl] placeholderImage:[Behavior placeholder]];
        
        self.receiverNameLabel.text = feed.challenge.receiverName;
        self.receiverSubtitleLabel.text = NSLocalizedString(@"is the winner of the CHALLENGE", @"v1.0");
        
        self.challengeTitleLabel.text = [feed.challenge.title uppercaseString];
        
        self.challengeBorderColor = [UIColor colorFromHexString:feed.challenge.borderColorHex];
        
        if (feed.isReceiverPointWinnerValue) {
            self.receiverBorderColor = [UIColor avatarBorderColor];
            self.receiverBorderWidth = [NSNumber numberWithFloat:3.0];
        }
    }
}

-(void) fillWithDefaultData {
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:@"http://i.imgur.com/GH67jzu.png"]];
    [self.receiverImageView sd_setImageWithURL:[NSURL URLWithString:@"http://i.imgur.com/1ELkRRt.png"] placeholderImage:[TeamUser placeholder]];
    [self.challengeImageView sd_setImageWithURL:[NSURL URLWithString:@"http://i.imgur.com/jjIZ4Kn.png"]];
    
    self.receiverNameLabel.text = NSLocalizedString(@"Margery Stewart", @"v1.0");
    
    self.receiverSubtitleLabel.text = NSLocalizedString(@"is the winner of the CHALLENGE", @"v1.0");

    self.challengeTitleLabel.text = [NSLocalizedString(@"who is the fastest", @"v1.0") uppercaseString];
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.receiverNameLabel.textColor = [UIColor blackTextColor];
    self.receiverNameLabel.font = [UIFont appFont:14];
    
    self.receiverSubtitleLabel.textColor = [UIColor appTextColor];
    self.receiverSubtitleLabel.font = [UIFont appFontBold:12];
    
    self.challengeTitleLabel.textColor = [UIColor appTextColor];
    self.challengeTitleLabel.font = [UIFont appFontBold:15];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.receiverImageView.layer.borderColor = self.receiverBorderColor.CGColor;
    self.receiverImageView.layer.borderWidth = self.receiverBorderWidth.floatValue;
    
    self.challengeBackgroundView.layer.borderColor = self.challengeBorderColor.CGColor;
    self.challengeBackgroundView.layer.borderWidth = 3.0;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.challengeBorderColor = nil;
    self.receiverBorderWidth = nil;
    self.receiverBorderColor = nil;
    
    self.receiverImageView = nil;
}

#pragma mark - Action
- (IBAction)receiverImagePressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowReceiver) {
        weakSelf.onShowReceiver (weakSelf);
    }
}

- (IBAction)challengeImagePressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowChallenge) {
        weakSelf.onShowChallenge (weakSelf);
    }
    
}

#pragma mark - Properties

-(UIColor *) challengeBorderColor {
    if (_challengeBorderColor == nil) {
        _challengeBorderColor = [UIColor yellowColor];
    }
    
    return _challengeBorderColor;
}

- (NSNumber *) receiverBorderWidth {
    if (_receiverBorderWidth == nil) {
        _receiverBorderWidth = [NSNumber numberWithFloat:1.0];
    }
    
    return _receiverBorderWidth;
}

- (UIColor *) receiverBorderColor {
    if (_receiverBorderColor == nil) {
        _receiverBorderColor = [UIColor whiteColor];
    }
    
    return _receiverBorderColor;
}


@end
