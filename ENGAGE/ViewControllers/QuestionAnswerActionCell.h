//
//  QuestionAnswerActionCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENButton.h"

@interface QuestionAnswerActionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENButton *leftButton;
@property (weak, nonatomic) IBOutlet ENButton *rightButton;

@property (nonatomic, copy) void (^onAction)(NSInteger actionState);

@end
