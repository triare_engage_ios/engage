//
//  QuestionActionCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionActionCell.h"

@implementation QuestionActionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.ActionButton setTitle:NSLocalizedString(@"SEND", @"v1.0") forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionButton:(id)sender {
    if (self.onAction) {
        self.onAction();
    }
}


@end
