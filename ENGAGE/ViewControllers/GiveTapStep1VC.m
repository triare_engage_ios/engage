//
//  GiveTapStep1VC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "GiveTapStep1VC.h"
#import "GiveTapNavController.h"
#import "NavigationBar.h"
#import "CollectionCell.h"
#import "CollectionHeaderView.h"
#import "EvaluateVC.h"

#import "ENPlaceholderView.h"

@interface GiveTapStep1VC () <NavigationBarSearchDelegate>

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSString *searchText;

@property (nonatomic, strong) ENPlaceholderView *tablePlaceholder;

@end


@implementation GiveTapStep1VC

-(void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:NSLocalizedString(@"STEP 1", @"v1.0")];
    self.navigationBar.searchDelegate = self;
    [self.navigationBar showRightButton];
    
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT;
    self.collectionView.contentInset = insets;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionCell" bundle:nil] forCellWithReuseIdentifier:@"userCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf createTablePlaceholder];
    });
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.isDataLoaded) {
        self.isDataLoaded = YES;
        [self loadUsers];
    }
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self trackScreen];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void) createTablePlaceholder {
    
    self.tablePlaceholder = [ENPlaceholderView createPlaceholderWithText:NSLocalizedString(@"No result found", @"v1.0") andFont:[UIFont appFont:17]];
    self.tablePlaceholder.frame = CGRectMake(0, 60, SCREEN_WIDTH, self.tablePlaceholder.frame.size.height + 30);
    
    self.tablePlaceholder.hidden = YES;
    [self.collectionView addSubview:self.tablePlaceholder];
    
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.collectionView reloadData];
}

-(void) loadUsers {
    
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getTeam:^(BOOL success, NSArray *list) {
        if (success) {
            [DataManager saveTeamUsersList:list];
            [weakSelf reloadData];
        }
        [weakSelf hideLoadingView];
    }];
}

-(void) closeVC {
    
    if (self.isUsedForEvaluate) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
       [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    GiveTapNavController *navigationController = (GiveTapNavController *) self.navigationController;
    navigationController.user = sender;
}

- (void) showEvaluateForUserId:(NSNumber *) userId {
    
    EvaluateVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EvaluateVC"];
    vc.userId = userId;
    [self.navigationController pushViewController:vc animated:YES];
     
     //[self presentViewController:vc animated:YES completion:nil];
}


#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSMutableArray *predicates = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [TeamUser MR_requestAllSortedBy:@"firstName" ascending:YES inContext:[DataManager context]];
    fetchRequest.includesSubentities = NO;
    
    if (self.searchText.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"fullName CONTAINS[cd] %@ ", self.searchText];
        [predicates addObject:searchPredicate];
    }
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    
    NSFetchedResultsController *fetchController = [TeamUser MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if (number == 0 && self.navigationBar.barView.searchBar.text.length > 0) {
        self.tablePlaceholder.hidden = NO;
    }
    else {
        self.tablePlaceholder.hidden = YES;
    }
    
    return number;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"userCell" forIndexPath:indexPath];
    cell.cellLabel.text = [user shortFullName];
    [cell.cellImageView sd_setImageWithURL:[AppTemplate mediumIconURL:user.avatarUrl] placeholderImage:[TeamUser placeholder]];
    //[cell roundImageView];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [CollectionCell sizeForCollectionWidth:collectionView.frame.size.width];
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    TeamUser *user = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (self.isUsedForEvaluate) {
        [self showEvaluateForUserId:user.userId];
    }
    else {
        [self performSegueWithIdentifier:@"goToNextStep2" sender:user];
    }
}

//header
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                        UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    
    if (self.isUsedForEvaluate) {
        headerView.label.text = NSLocalizedString(@"Select a person you want to evaluate", @"v1.0");
    }
    else {
        headerView.label.text = NSLocalizedString(@"Select a person you want to give a tap", @"v1.0");
    }
    
    return headerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    return CGSizeMake(200, 60);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.navigationBar.barView.searchBar resignFirstResponder];
}

#pragma mark NavigationBarSearchDelegate

-(void) checkSearchPlaceholder {
    self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by college", @"v1.0");
}

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    self.searchText = nil;
    [self reloadData];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [self checkSearchPlaceholder];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    self.searchText = searchText;
    
    [self reloadData];
}

#pragma mark - Helpers Methods

- (void) trackScreen {
    if (self.isUsedForEvaluate) {
        [AnalyticsManager trackScreen:ENAnalyticScreenEvaluateUserList];
        [AnalyticsManager trackScreen:ENAnalyticScreenEvaluateUserList withScreenClass:NSStringFromClass([self class])];
    }
    else {
        [AnalyticsManager trackScreen:ENAnalyticScreenGiveATapUserList];
        [AnalyticsManager trackScreen:ENAnalyticScreenGiveATapUserList withScreenClass:NSStringFromClass([self class])];
    }
}

@end
