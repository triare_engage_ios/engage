//
//  LeaderBoardFilterView.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 19.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "LeaderBoardFilterView.h"

@implementation LeaderBoardFilterView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.datePicker.datePickerMode=UIDatePickerModeDate;
    
    [self.datePicker setMaximumDate:[NSDate date]];
    [self.datePicker setValue:[UIColor appTextColor] forKey:@"textColor"];
    
    
    self.state = FilterStateNew;
}

-(void) changeButton:(UIButton *) bt hightlight:(BOOL) hightlight {
    
    UIColor *color = [UIColor appTextColor];
    if (hightlight) {
        color = [UIColor appOrangeColor];
    }
    
    bt.layer.borderColor = color.CGColor;
    [bt setTitleColor:color forState:UIControlStateNormal];
}

-(void) setDatePickerMinimumDate:(NSDate *) date {
    if (date == nil) {
        [self.datePicker setMinimumDate:[DataManager userCompany].createdAt];
    } else {
        [self.datePicker setMinimumDate:date];
    }
}

- (IBAction)cancelAction:(id)sender {
    self.state = FilterStateNew;
    [self.delegate leaderBoardFilteDidCancelFilter:self];
}
- (IBAction)firstDateAction:(id)sender {
    
    self.state = FilterStateFromSelect;
    
    [self.delegate leaderBoardFilteOnSelectDate:self];
}

- (IBAction)lastDateAction:(id)sender {
    self.state = FilterStateToSelect;
    
    [self.delegate leaderBoardFilteOnSelectDate:self];
}

- (IBAction)applyAction:(id)sender {
    
    if (self.state == FilterStateFromSelect || self.state == FilterStateNew) {
        self.fromDate = self.datePicker.date;
        
        if (self.fromDate && self.toDate) {
            [self applyFilter];
        } else {
            self.state = FilterStateToSelect;
        }
    } else {
        self.toDate = self.datePicker.date;
        
        if (self.fromDate && self.toDate) {
            [self applyFilter];
        } else {
            self.state = FilterStateFromSelect;
        }
    }
}

-(void) applyFilter {
    self.state = FilterStateFiltered;
    [self.delegate leaderBoardFilteDidApplyFilter:self];
}

-(NSString *) stringState:(NSDate *) date {
    
    if (date == nil) {
        return nil;
    }
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeStyle:NO];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    
    return [dateFormatter stringFromDate:date];
}

-(void) setState:(FilterState)state {
    
    [self.applyButton setTitle:NSLocalizedString(@"NEXT", @"v1.0") forState:UIControlStateNormal];
    
    NSString *fromTitle = [self stringState:self.fromDate];
    if (fromTitle == nil) fromTitle = NSLocalizedString(@"From", @"v1.0");
    
    NSString *toTitle = [self stringState:self.toDate];
    if (toTitle == nil) toTitle = NSLocalizedString(@"To", @"v1.0");
    
    switch (state) {
        case FilterStateNew: {
            
            [self setDatePickerMinimumDate:nil];

            self.fromDate = nil;
            self.toDate = nil;
            
            [self.fromButton setTitle:fromTitle forState:UIControlStateNormal];
            [self changeButton:self.fromButton hightlight:YES];
            
            [self.toButton setTitle:toTitle forState:UIControlStateNormal];
            [self changeButton:self.toButton hightlight:NO];
            
            break;
        }
         
        case FilterStateFromSelect: {
            
            [self setDatePickerMinimumDate:self.toDate];
            
            if (self.fromDate) self.datePicker.date = self.fromDate;
            
            [self.fromButton setTitle:fromTitle forState:UIControlStateNormal];
            [self changeButton:self.fromButton hightlight:YES];
            
            [self.toButton setTitle:toTitle forState:UIControlStateNormal];
            [self changeButton:self.toButton hightlight:NO];
            
            
            if (self.toDate || self.fromDate) {
                [self.applyButton setTitle:NSLocalizedString(@"APPLY FILTER", @"v1.0") forState:UIControlStateNormal];
            }
            
            break;
        }
        
        case FilterStateToSelect: {
            
            [self setDatePickerMinimumDate:self.fromDate];
            
            if (self.toDate) self.datePicker.date = self.toDate;
            
            [self.fromButton setTitle:fromTitle forState:UIControlStateNormal];
            [self changeButton:self.fromButton hightlight:NO];
            
            [self.toButton setTitle:toTitle forState:UIControlStateNormal];
            [self changeButton:self.toButton hightlight:YES];
            
            
            
            if (self.toDate || self.fromDate) {
                [self.applyButton setTitle:NSLocalizedString(@"APPLY FILTER", @"v1.0") forState:UIControlStateNormal];
            }
            
            break;
        }
            
        case FilterStateFiltered: {
            
            [self setDatePickerMinimumDate:nil];
            
            [self.fromButton setTitle:fromTitle forState:UIControlStateNormal];
            [self changeButton:self.fromButton hightlight:YES];
            
            [self.toButton setTitle:toTitle forState:UIControlStateNormal];
            [self changeButton:self.toButton hightlight:YES];
            
            break;
        }
            
        default:
            break;
    }
    
    _state = state;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
//    self.fromButton.layer.borderWidth = 1;
//    self.fromButton.layer.cornerRadius = self.fromButton.bounds.size.height/2;
//    self.fromButton.clipsToBounds = YES;
//    
//    self.toButton.layer.borderWidth = 1;
//    self.toButton.layer.cornerRadius = self.fromButton.bounds.size.height/2;
//    self.toButton.clipsToBounds = YES;
}




@end
