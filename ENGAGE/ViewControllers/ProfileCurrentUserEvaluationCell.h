//
//  ProfileCurrentUserEvaluationCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/2/16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"
#import "ENRoundedProgressView.h"

@interface ProfileCurrentUserEvaluationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENRoundedImageView *evaluateImageView;
@property (weak, nonatomic) IBOutlet UILabel *evaluateTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *badCountLabel;


@end
