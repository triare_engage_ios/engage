//
//  CustomJobTitleCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/7/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "CustomJobTitleCell.h"

@implementation CustomJobTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.jobTextField.backgroundColor = [UIColor clearColor];
    self.jobTextField.textColor = [UIColor appTextColor];
    self.jobTextField.font = [UIFont appFont:17];
    self.jobTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Add new job title", @"v1.0") attributes:@{NSForegroundColorAttributeName: [UIColor appGrayColorForPlaceHolder],NSFontAttributeName: [UIFont appFont:17]}];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
