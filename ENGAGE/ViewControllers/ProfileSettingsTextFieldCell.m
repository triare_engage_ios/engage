//
//  ProfileSettingsTextFieldCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileSettingsTextFieldCell.h"

#define kDefaultImageViewWidth 25

@interface ProfileSettingsTextFieldCell ()
@property (weak, nonatomic) IBOutlet UIImageView *leftCheckmarkImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidth;

@end

@implementation ProfileSettingsTextFieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.leftTitle.textColor = [UIColor appTextColor];
    self.leftTitle.font = [UIFont appFont:17];
    
    self.rightTextField.textColor = [UIColor appTextFieldColor];
    self.rightTextField.font = [UIFont appFont:17];
    
    self.leftCheckmarkImageView.image = [UIImage imageNamed:@"icon-checkmark"];
    self.leftCheckmarkImageView.hidden = YES;
}

- (void)prepareForReuse {
    self.leftTitle.text = nil;
    self.rightTextField.text = nil;
    self.rightTextField.delegate = nil;
    self.rightTextField.tag = 0;
    self.rightTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.rightTextField.keyboardType = UIKeyboardTypeDefault;
    [self.rightTextField setSecureTextEntry:NO];
    
    self.leftCheckmarkImageView.hidden = YES;
    self.imageViewWidth.constant = kDefaultImageViewWidth;
}

- (void) needShowCheckmark:(BOOL) show {
    if (show) {
        self.imageViewWidth.constant = kDefaultImageViewWidth;
    }
    else {
        self.imageViewWidth.constant = 1;
    }
    self.leftCheckmarkImageView.hidden = !show;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
