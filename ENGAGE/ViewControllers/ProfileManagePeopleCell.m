//
//  ProfileManagePeopleCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/26/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileManagePeopleCell.h"

@implementation ProfileManagePeopleCell

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.positionLabel.textColor = [UIColor appTextColor];
    self.positionLabel.font = [UIFont appFontBold:22];
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:17];
    
    self.subtitleLabel.textColor = [UIColor appTextColor];
    self.subtitleLabel.font = [UIFont appFontThin:15];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.rightUtilityButtons = nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
