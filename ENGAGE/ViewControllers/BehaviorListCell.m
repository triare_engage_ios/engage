//
//  BehaviorListCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorListCell.h"

#define kDefaultUserBorderWidth 0.0
#define kDefaultUserBorderColor [UIColor clearColor]

@implementation BehaviorListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //[self.userImageView makeCircle];
    
    self.nameLabel.textColor = [UIColor appTextColor];
    self.nameLabel.font = [UIFont appFont:17];
    
    self.jobLabel.textColor = [UIColor appTextColor];
    self.jobLabel.font = [UIFont appFontThin:17];
    
    self.pointsLabel.textColor = [UIColor appTextColor];
    self.pointsLabel.font = [UIFont appFont:17];
    
    self.leadPositionLabel.textColor = [UIColor appTextColor];
    self.leadPositionLabel.font = [UIFont appFontBold:22];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    //self.userImageView.layer.borderWidth = 0.0;
    
    self.userImageView.layer.borderWidth = self.userBorderWidth.floatValue;
    self.userImageView.layer.borderColor = self.userBorderColor.CGColor;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.userBorderColor = nil;
    self.userBorderWidth = nil;
}

#pragma mark - Properties

- (NSNumber *) userBorderWidth {
    if (_userBorderWidth == nil) {
        _userBorderWidth = [NSNumber numberWithFloat:kDefaultUserBorderWidth];
    }
    
    return _userBorderWidth;
}

- (UIColor *) userBorderColor {
    if (_userBorderColor == nil) {
        _userBorderColor = kDefaultUserBorderColor;
    }
    
    return _userBorderColor;
}

@end
