//
//  BehaviorDetailDescriptionCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BehaviorDetailDescriptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
