//
//  ProfileSettingsActionsCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/6/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENActionButton.h"

@interface ProfileSettingsActionsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENActionButton *rightButton;

@property (nonatomic, copy) void (^onRightAction)();

@end
