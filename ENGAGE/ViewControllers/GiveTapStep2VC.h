//
//  GiveTapStep2VC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

//Deprecated. Use: GiveTapStep2_1VC

@interface GiveTapStep2VC : ContentViewController

@property (nonatomic) BOOL isNeedSkipFirstStepWhenBack; //Need for skip first step if give tap from user profile

@end
