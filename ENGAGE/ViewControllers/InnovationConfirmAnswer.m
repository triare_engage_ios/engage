//
//  InnovationConfirmAnswer.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "InnovationConfirmAnswer.h"

@interface InnovationConfirmAnswer ()

@end

@implementation InnovationConfirmAnswer

-(instancetype) init {
    if (self = [super init]) {
        _isStartDoingDone = NO;
        _isStopDoingDone = NO;
        _isNeedDoingDone = NO;
    }
    
    return self;
}

- (NSDictionary *) serializeForSend {
    return nil;
}

@end
