//
//  QuestionDataPickerView.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionDataPickerView.h"

@implementation QuestionDataPickerView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    [self.dataPicker setMinimumDate:[NSDate date]];
    [self.dataPicker setValue:[UIColor appTextColor] forKey:@"textColor"];
    //[self.dataPicker setValue:@NO forKey:@"highlightsToday"];
    
    //Trick for changing color of AM. First rendering down timer, than rendering needed timer.
    self.dataPicker.datePickerMode = UIDatePickerModeCountDownTimer;
    self.dataPicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    if (self.startDate) {
        [self.dataPicker setDate:self.startDate];
    }
    
    [self.okButton setTitle:NSLocalizedString(@"OK", @"v1.0") forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", @"v1.0") forState:UIControlStateNormal];
}

- (IBAction)cancelButtonAction:(id)sender {
    
    if (self.onCancelAction) {
        self.onCancelAction();
    }
    
}

- (IBAction)okButtonAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    self.fromDate = self.dataPicker.date;
    
    if (weakSelf.onOkAction) {
        weakSelf.onOkAction(weakSelf.fromDate);
    }
}

@end
