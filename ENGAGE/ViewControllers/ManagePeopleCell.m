//
//  ManagePeopleCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ManagePeopleCell.h"

@implementation ManagePeopleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.switcher.onTintColor = [UIColor appOrangeColor];
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:17];
    
    self.subtitleLabel.textColor = [UIColor appTextColor];
    self.subtitleLabel.font = [UIFont appFontThin:15];
    
    self.positionLabel.textColor = [UIColor appTextColor];
    self.positionLabel.font = [UIFont appFontBold:22];
                                 
}

- (void) prepareForReuse {
    self.delegate = nil;
    self.onAction = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    //NSIndexPath *indexPathReturned = [weakSelf.contentTableView indexPathForCell:cellReturned]
    
    
    if (!self.switcher.isOn) {
        if (weakSelf.onAction) {
            weakSelf.onAction (0, weakSelf);
        }
    }
    else {
        if (weakSelf.onAction) {
            weakSelf.onAction (1, weakSelf);
        }
    }
}

- (IBAction)swticherStateChange:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (self.switcher.isOn) {
        if (weakSelf.onAction) {
            weakSelf.onAction (0, weakSelf);
        }
    }
    else {
        if (weakSelf.onAction) {
            weakSelf.onAction (1, weakSelf);
        }
    }
}

@end
