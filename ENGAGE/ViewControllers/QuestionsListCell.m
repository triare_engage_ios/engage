//
//  QuestionsListCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/19/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionsListCell.h"

@interface QuestionsListCell ()
@property (weak, nonatomic) IBOutlet UILabel *choiceLabel;
@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;


@end

@implementation QuestionsListCell


-(void) addQuestion:(Question *) question withUsersCount:(NSNumber *) usersCount {
    
    self.contentLabel.text = question.body;
    
    if (question.isMultipleAnswerValue) {
        self.choiceLabel.text = NSLocalizedString(@" MULTIPLY CHOICE ", @"v1.0");
    }
    else {
        self.choiceLabel.text = NSLocalizedString(@" SINGLE CHOICE ", @"v1.0");
    }
    
    self.createDateLabel.text = [question formattedCreateAt];
    
    if (question.answeredUsersCount.integerValue >= usersCount.integerValue) {
        self.progressLabel.text = NSLocalizedString(@" COMPLETED ", @"v1.0");
    }
    else if (question.answeredUsersCount.integerValue < usersCount.integerValue) {
        if (question.startsAt.timeIntervalSince1970 <= [NSDate date].timeIntervalSince1970) {
            self.progressLabel.text = NSLocalizedString(@" IN PROGRESS ", @"v1.0");
        }
        else {
            NSString *labelText = NSLocalizedString(@" STARTS DATE", @"v1.0");
            self.progressLabel.text = [NSString stringWithFormat:@"%@ %@ ", labelText, [question formattedStartsAt]];
        }
    }
    
    if (question.answeredUsersCount.integerValue > 0) {
        self.arrowImageView.hidden = NO;
    }
    else {
        self.arrowImageView.hidden = YES;
    }
    
}

- (void) awakeFromNib {
    [super awakeFromNib];
    
    UIColor *textColor = [UIColor appTextColor];
    
    self.choiceLabel.textColor = textColor;
    self.choiceLabel.font = [UIFont appFontThin:10];
    
    self.progressLabel.textColor = textColor;
    self.progressLabel.font = [UIFont appFontThin:10];
    
    self.createDateLabel.textColor = textColor;
    self.createDateLabel.font = [UIFont appFontThin:14];
    
    self.arrowImageView.image = [UIImage imageNamed:@"icon-rightArrow"];
    self.arrowImageView.hidden = NO;
    
    self.contentLabel.textColor = textColor;
    self.contentLabel.font = [UIFont appFont:17];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.choiceLabel.layer.borderWidth = 1.0;
    self.choiceLabel.layer.borderColor = [UIColor appTextColor].CGColor;
    self.choiceLabel.layer.cornerRadius = 3.0;
    self.choiceLabel.clipsToBounds = YES;
    
    self.progressLabel.layer.borderWidth = 1.0;
    self.progressLabel.layer.borderColor = [UIColor appTextColor].CGColor;
    self.progressLabel.layer.cornerRadius = 3.0;
    self.progressLabel.clipsToBounds = YES;
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
