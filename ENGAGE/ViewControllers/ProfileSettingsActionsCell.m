//
//  ProfileSettingsActionsCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/6/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileSettingsActionsCell.h"

@implementation ProfileSettingsActionsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.rightButton.layer.borderWidth = 1;
    self.rightButton.layer.borderColor = [UIColor appTextColor].CGColor;
    self.rightButton.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) prepareForReuse {
    
}

- (IBAction)rightButtonPressed:(id)sender {
    if (self.onRightAction) {
        self.onRightAction();
    }
}

@end
