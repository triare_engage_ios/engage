//
//  BootingUpVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "BootingUpVC.h"
#import <MBCircularProgressBar/MBCircularProgressBarView.h>
#import "DataManager.h"
#import "RequestManager.h"
#import "AKFileManager.h"


@interface BootingUpVC ()

@property (weak, nonatomic) IBOutlet UIImageView *logoView;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *progressBar;

@end

@implementation BootingUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.progressBar setValue:0 animateWithDuration:0];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self downloadLogo];
}

-(void) downloadLogo {
    __weak typeof(self) weakSelf = self;
    
    NSString *url = [DataManager user].company.logoUrl;
    
    NSString *path = [AKFileManager companyDirectory];
    [AKFileManager createDirectoryIfNeed:path];
    path = [path stringByAppendingPathComponent:@"logo.png"];
    
    [[RequestManager sharedManager] downloadFile:url destination:path progress:^(NSProgress *progress) {
        CGFloat globalProgress = progress.fractionCompleted * .4 * 100;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf setProgressBarProgress:globalProgress];
        });
    } onCompleate:^(NSURL *filePath, NSError *error) {
        if (error == nil) {
            weakSelf.logoView.image = [UIImage imageWithContentsOfFile:filePath.path];
        }
        
        [weakSelf downloadBackground];
    }];
}

-(void) downloadBackground {
    __weak typeof(self) weakSelf = self;
    
    NSString *url = [DataManager user].company.backgroundUrl;
    
    NSString *path = [AKFileManager companyDirectory];
    [AKFileManager createDirectoryIfNeed:path];
    path = [path stringByAppendingPathComponent:@"background.png"];
    
    [[RequestManager sharedManager] downloadFile:url destination:path progress:^(NSProgress *progress) {
        CGFloat globalProgress = progress.fractionCompleted * .6 * 100 + 40;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf setProgressBarProgress:globalProgress];
        });
    } onCompleate:^(NSURL *filePath, NSError *error) {
        [[AppDelegate theApp].mainViewController updateBackground];
        [weakSelf closeVC];
    }];
}

-(void) setProgressBarProgress:(CGFloat) progress {
    [self.progressBar setValue:progress animateWithDuration:0];
}

-(void) closeVC {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, .5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.presentingViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    });
    
}

@end
