//
//  ActivityFeedCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 08.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>

@interface ActivityFeedCell : SWTableViewCell


@property (nonatomic, copy) void (^onRetap)(ActivityFeedCell *cell);
@property (nonatomic, copy) void (^onComments)(ActivityFeedCell *cell);
@property (nonatomic, copy) void (^onShowRetaps)(ActivityFeedCell *cell);

@property (nonatomic, copy) void (^onShowReceiver)(ActivityFeedCell *cell);
@property (nonatomic, copy) void (^onShowSender)(ActivityFeedCell *cell);
@property (nonatomic, copy) void (^onShowBehavior)(ActivityFeedCell *cell);

+(CGFloat) cellHeight:(NSString *) contentText andBehaviorText:(NSString *) behaviorText width:(CGFloat) tableWidth;

-(void) addFeed:(ActivityFeed *) feed;

@end
