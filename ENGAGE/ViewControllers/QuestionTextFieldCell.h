//
//  QuestionTextFieldCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionTextFieldCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic) NSInteger index; //from 0 to 5;

@property (nonatomic, copy) void (^onChangeText)(NSString *text, QuestionTextFieldCell *cell);
@property (nonatomic, copy) void (^onAction)(QuestionTextFieldCell *cell);

- (void) setAttributePlaceholderText:(NSString *) text;

@end
