//
//  ProfileBehavior3Cell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 25.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileBehavior3Cell.h"

@implementation ProfileBehavior3Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeAppRoundedView:self.cellImageView1];
    
    self.cellTitleLabel1.font = [UIFont appFont:17];
    self.cellTitleLabel1.textColor = [UIColor appTextColor];
    
    
    //[UIView makeAppRoundedView:self.cellImageView2];
    
    self.cellTitleLabel2.font = [UIFont appFont:17];
    self.cellTitleLabel2.textColor = [UIColor appTextColor];
    
    
    //[UIView makeAppRoundedView:self.cellImageView3];
    
    self.cellTitleLabel3.font = [UIFont appFont:17];
    self.cellTitleLabel3.textColor = [UIColor appTextColor];
    
    self.badgeLabel1.badgeHeight = 15;
    self.badgeLabel1.isNeedResizeBadge = @YES;
    self.badgeLabel1.font = [UIFont appFont:12];
    self.badgeLabel2.badgeHeight = 15;
    self.badgeLabel2.isNeedResizeBadge = @YES;
    self.badgeLabel2.font = [UIFont appFont:12];
    self.badgeLabel3.badgeHeight = 15;
    self.badgeLabel3.font = [UIFont appFont:12];
    self.badgeLabel3.isNeedResizeBadge = @YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setBehaviors:(NSArray *) items {
    UserBehavior *b1 = items.firstObject;
    
    self.cellTitleLabel1.text = b1.title;
    [self.cellImageView1 sd_setImageWithURL:[AppTemplate bigIconURL:b1.iconUrl] placeholderImage:[Behavior placeholder]];
    
    self.badgeLabel1.text = [NSString stringWithFormat:@"%@", b1.count];
    
    self.cellImageView2.superview.hidden = YES;
    self.cellImageView3.superview.hidden = YES;
    
    if (items.count >= 2) {
        UserBehavior *b2 = [items objectAtIndex:1];
        
        self.cellTitleLabel2.text = b2.title;
        
        [self.cellImageView2 sd_setImageWithURL:[AppTemplate bigIconURL:b2.iconUrl] placeholderImage:[Behavior placeholder]];
        self.badgeLabel2.text = [NSString stringWithFormat:@"%@", b2.count];
        
        self.cellImageView2.superview.hidden = NO;
    }
    
    if (items.count >= 3) {
        UserBehavior *b3 = [items objectAtIndex:2];
        
        self.cellTitleLabel3.text = b3.title;
        [self.cellImageView3 sd_setImageWithURL:[AppTemplate bigIconURL:b3.iconUrl] placeholderImage:[Behavior placeholder]];
        self.badgeLabel3.text = [NSString stringWithFormat:@"%@", b3.count];
        
        self.cellImageView3.superview.hidden = NO;
    }
    
    
}

- (IBAction)behavior1IconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowBehavior) {
        weakSelf.onShowBehavior(0);
    }
}

- (IBAction)behavior2IconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowBehavior) {
        weakSelf.onShowBehavior(1);
    }
}

- (IBAction)behavior3IconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowBehavior) {
        weakSelf.onShowBehavior(2);
    }
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    
    self.badgeLabel1.layer.cornerRadius = 15/2;
    self.badgeLabel2.layer.cornerRadius = 15/2;
    self.badgeLabel3.layer.cornerRadius = 15/2;
    
    self.badgeLabel1.clipsToBounds = YES;
    self.badgeLabel2.clipsToBounds = YES;
    self.badgeLabel3.clipsToBounds = YES;
    
}



@end
