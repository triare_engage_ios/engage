//
//  TeamMessageListCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/13/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>

@interface TeamMessageListCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *showMoreLabel;

-(void) addTeamMessage:(TeamMessage *) message fullInfo:(BOOL) fullInfo;

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth;

@end
