//
//  InnovationsConfirmVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"
#import "InnovationConfirmAnswer.h"

typedef enum : NSUInteger {
    InnovationConfirmScreenStateStep1,
    InnovationConfirmScreenStateStep2,
    InnovationConfirmScreenStateStep3
} InnovationConfirmScreenState;

@interface InnovationsConfirmVC : ContentViewController
@property (nonatomic) InnovationConfirmScreenState screenState;
@property (nonatomic, strong) InnovationConfirmAnswer *answer;

+ (NSString *) storyboardIdentifier;

@end
