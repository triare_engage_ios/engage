//
//  GiveTapStep2_1VC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/22/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface GiveTapStep2_1VC : ContentViewController

@property (nonatomic) BOOL isNeedSkipFirstStepWhenBack; //Need for skip first step if give tap from user profile

@end
