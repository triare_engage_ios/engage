//
//  ProfileUserCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 16.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileUserCell.h"

@interface ProfileUserCell ()

@property (nonatomic, strong) NSNumber *userImageBorderWidth;
@property (nonatomic, strong) UIColor *userImageBorderColor;

@end

@implementation ProfileUserCell


#pragma mark - Public Methods

- (void) hideGiveTapAndEvaluation {
    self.giveTapButton.superview.hidden = YES;
    self.evaluateButton.superview.hidden = YES;
}

-(void) setSuggestOnLeftButton {
    [self.giveTapButton setImage:[UIImage imageNamed:@"icon-sendSuggestion"] forState:UIControlStateNormal];
    self.giveTapLabel.text = NSLocalizedString(@"SUGGESTION", @"v1.0");
}

-(void) isPersonalProfile:(BOOL) isPersonal {
    
    self.giveTapButton.superview.hidden = isPersonal;
    self.evaluateButton.superview.hidden = isPersonal;
}

- (void) showBorderForAvatar {
    self.userImageBorderColor = [UIColor avatarBorderColor];
    self.userImageBorderWidth = [NSNumber numberWithFloat:3.0];
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeRoundedView:self.userImageView];
    //[self.userCenterView makeCircle];
    //[self.userImageView makeCircle];
    
    self.usernameLabel.font = [UIFont appFontThin:22];
    self.usernameLabel.textColor = [UIColor appTextColor];
    
    self.userJobLabel.font = [UIFont appFontThin:17];
    self.userJobLabel.textColor = [UIColor appTextColor];
    
    
    self.evaluateLabel.textColor = [UIColor appTextColor];
    self.evaluateLabel.font = [UIFont appFont:14];
    self.evaluateLabel.text = NSLocalizedString(@"EVALUATE", @"v1.0");
    
    self.giveTapLabel.textColor = [UIColor appTextColor];
    self.giveTapLabel.font = [UIFont appFont:14];
    self.giveTapLabel.text = NSLocalizedString(@"GIVE A TAP", @"v1.0");
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.userImageView.layer.borderWidth = self.userImageBorderWidth.floatValue;
    self.userImageView.layer.borderColor = self.userImageBorderColor.CGColor;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.userImageBorderColor = nil;
    self.userImageBorderWidth = nil;
}

#pragma mark - Action

- (IBAction)giveTapAction:(id)sender {
    if (self.onLeftAction) {
        self.onLeftAction();
    }
}

- (IBAction)evaluateAction:(id)sender {
    if (self.onRigntAction) {
        self.onRigntAction();
    }
}

#pragma mark - Properties

- (NSNumber *) userImageBorderWidth {
    if (_userImageBorderWidth == nil) {
        _userImageBorderWidth = [NSNumber numberWithFloat:0.0];
    }
    
    return _userImageBorderWidth;
}

- (UIColor *) userImageBorderColor {
    if (_userImageBorderColor == nil) {
        _userImageBorderColor = [UIColor clearColor];
    }
    
    return _userImageBorderColor;
}

@end
