//
//  InnovationsActionCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENButton.h"

@interface InnovationsActionCell : UITableViewCell
@property (nonatomic, copy) void (^onAction)(NSInteger actionState, InnovationsActionCell *cell);

+(CGFloat) cellHeight;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

-(void) applyButtonsLeftTitle:(NSString *) leftTitle andRightTitle:(NSString *) rightTitle;

@end
