//
//  CollectionHeaderView.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 05.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "CollectionHeaderView.h"

@implementation CollectionHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];

    self.label.font = [UIFont appFontThin:18];
    self.label.textColor = [UIColor appTextColor];
}

@end
