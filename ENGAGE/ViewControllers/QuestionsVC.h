//
//  QuestionsVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface QuestionsVC : ContentViewController

@property (nonatomic, copy) void (^onBackAction)();

+(NSString *) storyboardIdentifier;

@end
