//
//  CustomAnswerHeaderCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAnswerHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
