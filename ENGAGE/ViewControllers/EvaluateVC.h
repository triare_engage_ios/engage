//
//  EvaluateVC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 30.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EvaluateDragGoalView : UIView

@property BOOL isActive;
@property (nonatomic, strong) NSIndexPath *cellIndexPath;

-(void) changeState:(BOOL) isActive;

@end


@interface EvaluateDragView : UIView

@property CGPoint startCenter;
@property (nonatomic, strong) NSIndexPath *cellIndexPath;

@end



@interface EvaluateVC : ContentViewController

@property (nonatomic, strong) NSNumber *userId;

@end
