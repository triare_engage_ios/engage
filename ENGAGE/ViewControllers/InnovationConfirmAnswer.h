//
//  InnovationConfirmAnswer.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InnovationConfirmAnswer : NSObject

@property (nonatomic) BOOL isStartDoingDone;
@property (nonatomic) BOOL isStopDoingDone;
@property (nonatomic) BOOL isNeedDoingDone;

-(instancetype) init;
-(NSDictionary *) serializeForSend;

@end
