//
//  CommentsVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 11.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "CommentsVC.h"
#import "CommentCell.h"
#import "ENButton.h"
#import "LoadingCell.h"
#import "ProfileVC.h"

@interface CommentsVC () <UITextViewDelegate, SWTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *textForm;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFormHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textFormBottomSpace;

@property (nonatomic, strong) PaginationInfo *pagination;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *textViewBackground;
@property (weak, nonatomic) IBOutlet ENButton *sendButton;

@property (nonatomic, strong) NSMutableArray *fullCommentsIds;

@property (nonatomic, strong) UIRefreshControl *refreshControl;


@property BOOL isKeyboardVisability;


@end

@implementation CommentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    [self.navigationBar setTitle:NSLocalizedString(@"COMMENTS", @"v1.0")];
    [self.navigationBar showLeftButton];
    
    self.registerKeyboardNotifications = YES;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CommentCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.pagination = [[PaginationInfo alloc] init];
    self.pagination.currentPage = 0;
    self.pagination.totalPages = 1;
    [self loadCommentsDataForPage:1];
    
    [self.sendButton setTitle:@"ADD COMMENT" forState:UIControlStateNormal];
    [self.sendButton needHideImage:YES];
    
    self.textView.font = [UIFont appFontThin:17];
    self.textView.textColor = [UIColor blackColor];
    
    self.textViewBackground.image = [[UIImage imageNamed:@"message-textBackground.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    self.textFormHeight.constant = 130;
    
    
    self.fullCommentsIds = [NSMutableArray array];
    
    [self addDownRefresh];
    
    self.textView.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[AppDelegate theApp].mainViewController hideTabBarView:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.feed.commentsCount.integerValue == 0) {
        [self.textView becomeFirstResponder];
    }
    
    [AnalyticsManager trackScreen:ENAnalyticScreenCommentsList];
    [AnalyticsManager trackScreen:ENAnalyticScreenCommentsList withScreenClass:NSStringFromClass([self class])];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[AppDelegate theApp].mainViewController showTabBarView:YES];
}

#pragma mark - Data

-(void) loadNextCommentsData {
    
    if (self.pagination.isLoading) return;
    
    if (self.pagination.totalPages > self.pagination.currentPage) {
        [self loadCommentsDataForPage:self.pagination.currentPage + 1];
    }
    else {
        [self reloadData];
    }
}

-(void) loadCommentsDataForPage:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    weakSelf.pagination.isLoading = YES;
    [[RequestManager sharedManager] getComments:self.feed.feedId page:page onCompleate:^(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage) {
        
        if (success) {
            weakSelf.pagination.currentPage = pagination.currentPage;
            weakSelf.pagination.totalPages = pagination.totalPages;
            
            if (weakSelf.pagination.currentPage == 1) {
                [DataManager saveCommentsList:list feedId:weakSelf.feed.feedId];
            } else {
                [DataManager appendCommentsList:list feedId:weakSelf.feed.feedId];
            }
            
            [weakSelf reloadData]; //need to show alert
        }
        
        weakSelf.pagination.isLoading = NO;
        
        if (!success) {
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
            [weakSelf.refreshControl endRefreshing];
        }
        //[weakSelf reloadData];
    }];
}

- (void)reloadData {
    
    self.fetchedResultsController = nil;
    
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [Comment MR_requestAllSortedBy:@"createDate" ascending:NO inContext:[DataManager context]];
    
    NSMutableArray *predicates = [NSMutableArray array];
    NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"activityFeed.itemType MATCHES[cd] %@ ", @"tap"];
    NSPredicate *idPredicate = [NSPredicate predicateWithFormat:@"activityFeed.feedId == %@", self.feed.feedId];
    [predicates addObject:typePredicate];
    [predicates addObject:idPredicate];
    fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    
    //Deprecated.
    //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"activityFeed.feedId == %@", self.feed.feedId];
    NSFetchedResultsController *fetchController = [Comment MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - Action

-(void) showSenderUserProfileForItemAtIndex:(NSIndexPath *) indexPath {
    Comment *comment = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    vc.userId = comment.senderId;
    vc.profileTitle = comment.senderName;
    
    [self showViewController:vc sender:nil];
}

- (void) deleteCommentAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    Comment *c = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [[RequestManager sharedManager] deleteCommentWithId:c.commentId success:^(BOOL success) {
        [DataManager deleteCommentWithId:c.commentId];
        weakSelf.fetchedResultsController = nil;
        [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        //Refresh comments count, when come back to activity feed.
        if (weakSelf.needRefresh) {
            weakSelf.needRefresh (YES);
        }
    } failure:^(NSString *message) {
        [weakSelf showAlertWithTitle:nil andText:message];
    }];
}

- (IBAction)sendCommentAction:(id)sender {
    
    //Validation for comment length
    if (self.textView.text.length == 0) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add comment", @"v1.0")];
        return;
    }
    
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if ([self.textView.text stringByTrimmingCharactersInSet:set].length == 0) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Comment", @"v1.0") text:NSLocalizedString(@"Comment cannot consist only space", @"v1.0")];
        [self.navigationBar rightButtonEnable:NO];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:nil style:LoadingViewStyleCoverContent];
    
    [[RequestManager sharedManager] addComment:self.textView.text feedId:self.feed.feedId success:^(NSDictionary *info) {
        [weakSelf hideLoadingView];
        
        if (info) {
            weakSelf.textView.text = nil;
            [weakSelf.textView resignFirstResponder];
            
            [DataManager appendCommentAfterCreation:info feedId:self.feed.feedId];
            [weakSelf reloadData];
            
            if (weakSelf.needRefresh) {
                weakSelf.needRefresh (YES);
            }
        }
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [weakSelf.refreshControl endRefreshing];
        [weakSelf showAlertWithTitle:nil andText:message];
    }];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if (self.pagination.currentPage < self.pagination.totalPages) {
        return number + 1;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        
        __weak typeof(self) weakSelf = self;
        
        Comment *c = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        [cell addComment:c fullInfo:[self.fullCommentsIds containsObject:c.commentId]];
        cell.onShowSender = ^ (CommentCell *returnedCommentCell) {
            NSIndexPath *indexPathCommentReturned = [weakSelf.tableView indexPathForCell:returnedCommentCell];
            [weakSelf showSenderUserProfileForItemAtIndex:indexPathCommentReturned];
        };
        
        if ([self canDeleteComment:c]) {
            cell.rightUtilityButtons = [self deleteButton];
        }
        
        cell.delegate = self;
        
        return cell;
    } else {
        return [LoadingCell cell];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        
        CGFloat collapsedHeight = 130;
        
        Comment *c = self.fetchedResultsController.fetchedObjects[indexPath.row];
        CGFloat height = [CommentCell cellHeight:c.body width:tableView.frame.size.width];
        
        if (height < collapsedHeight) {
            [self.fullCommentsIds addObject:c.commentId];
        } else {
            if (![self.fullCommentsIds containsObject:c.commentId]) {
                return collapsedHeight;
            }
        }
        
        return height;
    }
    
    return 80; //loading cell height
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        Comment *c = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if (![self.fullCommentsIds containsObject:c.commentId]) {
            [self.fullCommentsIds addObject:c.commentId];
            
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isLoadingCell]) {
        [self loadNextCommentsData];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.textView resignFirstResponder];
}

#pragma mark - SWTableViewCellDelegate

//Can swipe only one row in time (close if swipe another)
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    [self showDeleteAlertForIndexPath:indexPath];
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    Comment *c = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([self canDeleteComment:c]) {
        return YES;
    }
    
    return NO;
}

- (NSArray *) deleteButton {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor redColor] icon:[UIImage imageNamed:@"icon-delete"]];
    
    return rightUtilityButtons;
}

#pragma mark - Update UI

- (void) hideButtonsForIndexPath: (NSIndexPath *) indexPath {
    
    CommentCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [cell hideUtilityButtonsAnimated:YES];
}

#pragma mark - Alerts

- (void) showDeleteAlertForIndexPath:(NSIndexPath *) indexPath {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to delete tap?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self deleteCommentAtIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString * newString = [[textView text] stringByReplacingCharactersInRange:range withString:text];
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textView.text.length == 0 && [newString stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    return YES;
}


#pragma mark - Keyboard

- (void)keyboardWillShow:(NSNotification *)notification {

    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    CGRect keyboardFrame = [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    self.textFormHeight.constant = 160;
    self.textFormBottomSpace.constant = keyboardFrame.size.height;
    [self.view setNeedsLayout];
    
    
    if (!self.isKeyboardVisability) {
        self.isKeyboardVisability = YES;

        CGFloat offsetY = self.tableView.contentOffset.y;
        CGFloat upHeight = keyboardFrame.size.height;
        CGFloat canScrollArea = self.tableView.contentSize.height - (self.tableView.frame.size.height - self.tableView.contentInset.top - upHeight);
        
        CGFloat newOffsetY = offsetY ;
        if ((canScrollArea - upHeight) > 0) {
            newOffsetY = offsetY + upHeight + 30;
        } else if ((canScrollArea - upHeight) > (upHeight * -1)) {
            newOffsetY = offsetY + canScrollArea + 30;
        }
        
        CGPoint offset = self.tableView.contentOffset;
        offset.y = newOffsetY;
        [self.tableView setContentOffset:offset animated:YES];
    }
    
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    self.isKeyboardVisability = NO;
    
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    self.textFormBottomSpace.constant = 0;
    self.textFormHeight.constant = 130;
    
    [self.view setNeedsLayout];
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                     }];
    
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadCommentsDataForPage:1];
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

- (BOOL) canDeleteComment:(Comment *) comment {
    BOOL canDelete = NO;
    
    //Admin and manager can delete any comments
    if ([DataManager user].isAdmin || [DataManager user].isManager) {
        canDelete = YES;
    }
    
    //User can delete his comments
    if ([DataManager user].userIdValue == comment.senderIdValue) {
        canDelete = YES;
    }
    
    return canDelete;
}


@end
