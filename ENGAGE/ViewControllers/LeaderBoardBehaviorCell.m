//
//  LeaderBoardBehaviorCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 19.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "LeaderBoardBehaviorCell.h"

@implementation LeaderBoardBehaviorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeAppRoundedView:self.cellImageView];
    
    self.cellTitleCell.textColor = [UIColor appTextColor];
    self.cellTitleCell.font = [UIFont appFont:17];
    
    self.pointsLabel.textColor = [UIColor appTextColor];
    self.pointsLabel.font = [UIFont appFont:17];
    
    self.leadLabel.textColor = [UIColor appTextColor];
    self.leadLabel.font = [UIFont appFontBold:22];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
