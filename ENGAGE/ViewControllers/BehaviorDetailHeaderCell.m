//
//  BehaviorDetailHeaderCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorDetailHeaderCell.h"

@implementation BehaviorDetailHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.headerTitleLabel.textColor = [UIColor appTextColor];
    self.headerTitleLabel.font = [UIFont appFontBold:17];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    //20 - top header constraint
    //140 - image size
    //10 - to label
    //17 - minimum size
    //5 - space
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {
    
    /*
     CGRect rect = [contentText boundingRectWithSize:CGSizeMake(tableWidth - 30, CGFLOAT_MAX) options:(NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:@{ NSFontAttributeName : [UIFont appFontThin:17]} context:nil];
     */
    
    CGFloat minimumHeight = 220;
    
    //bad font, use this way for calculate text height
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFontBold:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    height += 20; //top header constraint
    height += 160; //image size
    height += 10; //to label constarint
    height += 10; //bottom space
    
    if (height < minimumHeight) {
        height = minimumHeight;
    }
    
    return height;
}


@end
