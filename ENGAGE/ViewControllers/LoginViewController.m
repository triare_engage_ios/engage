//
//  LoginViewController.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 02.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "LoginViewController.h"
#import "RequestManager.h"
#import "DataManager.h"
#import "UIViewController+Keyboard.h"
#import "UIViewController+LoadingView.h"
#import "SFSafariManager.h"

@interface LoginViewController () <UITextViewDelegate, SFSafariViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

@property (weak, nonatomic) IBOutlet UIView *splashView;

@property CGFloat standartFormCenterY;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *formCenterY;
@property (weak, nonatomic) IBOutlet UIView *formView;

@end

@implementation LoginViewController

#pragma mark - View Controller Life Cycle

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self addKeyboardNotification];
    [self setupTextField];
}


-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeKeyboardNotification];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.standartFormCenterY = self.formCenterY.constant;
    
    self.usernameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Email", @"v1.0") attributes:@{NSForegroundColorAttributeName: [UIColor appGrayColor],NSFontAttributeName: [UIFont appFontThin:50]}];
    self.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Password", @"v1.0") attributes:@{NSForegroundColorAttributeName: [UIColor appGrayColor],NSFontAttributeName: [UIFont appFontThin:50]}];
    
    self.usernameField.font = [UIFont appFontThin:20];
    self.passwordField.font = [UIFont appFontThin:20];
    
    [self.signInButton setTitle:NSLocalizedString(@"SIGN IN", @"v1.0") forState:UIControlStateNormal];
    [self.forgotButton setTitle:NSLocalizedString(@"Forgot password?", @"v1.0") forState:UIControlStateNormal];
    [self.signUpButton setTitle:NSLocalizedString(@"SIGN UP", @"v1.0") forState:UIControlStateNormal];
    
    self.signInButton.titleLabel.font = [UIFont appFontBold:20];
    self.forgotButton.titleLabel.font = [UIFont appFontThin:20];
    self.signUpButton.titleLabel.font = [UIFont appFontBold:20];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenLogin];
    [AnalyticsManager trackScreen:ENAnalyticScreenLogin withScreenClass:NSStringFromClass([self class])];
}

#pragma mark - Setup 

- (void) setupTextField {
    //[[UITextField appearance] setTintColor:[UIColor appOrangeColor]];
}

#pragma mark - Validation

-(BOOL) isFieldsDataValid {
    if (self.usernameField.text.length == 0 && self.passwordField.text.length == 0) {
        return NO;
    }
    
    return YES;
}

-(BOOL) isEmailEmpty {
    BOOL isEmpty = NO;
    
    if (self.usernameField.text.length == 0) {
        isEmpty = YES;
    }
    
    return isEmpty;
}

-(BOOL) isPasswordEmpty {
    BOOL isEmpty = NO;
    
    if (self.passwordField.text.length == 0) {
        isEmpty = YES;
    }
    
    return isEmpty;
}

- (BOOL)isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - Action

- (IBAction)signInAction:(id)sender {
    [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignIn label:ENAnalyticLabelTry];
    
    //Email and pass empty
    if (![self isFieldsDataValid]) {
        [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignInError label:ENAnalyticLabelEmailAndPassEmpty];
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Login", @"v1.0") text:NSLocalizedString(@"Add email and password", @"v1.0")];
        return;
    }
    
    //Email empty
    if ([self isEmailEmpty]) {
        [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignInError label:ENAnalyticLabelEmailEmpty];
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Login", @"v1.0") text:NSLocalizedString(@"Add email", @"v1.0")];
        return;
    }
    
    //Email validation
    if (![self isValidEmail:self.usernameField.text]) {
        [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignInError label:ENAnalyticLabelEmailNotValid];
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Login", @"v1.0") text:NSLocalizedString(@"Email is not valid", @"v1.0")];
        return;
    }
    
    //Password empty
    if ([self isPasswordEmpty]) {
        [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignInError label:ENAnalyticLabelPasswordEmpty];
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Login", @"v1.0") text:NSLocalizedString(@"Add password", @"v1.0")];
        return;
    }
    
    [self doLogin];
}

-(void) doLogin {
    
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:nil style:LoadingViewStyleCoverContent];
    
    [[RequestManager sharedManager] authorizeWithEmail:self.usernameField.text password:self.passwordField.text success:^(NSDictionary *userInfo) {
        [weakSelf hideLoadingView];
        
        [weakSelf userDidLogin:userInfo];
    } failure:^(NSString *message) {
        [weakSelf hideLoadingView];
        [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignInError label:message];
        [weakSelf showAlertWithTitle:NSLocalizedString(@"Login", @"v1.0") andText:message];
    }];
}

-(void) userDidLogin:(NSDictionary *) info {
    
    NSString *email = [info valueForKey:@"email"];
    NSString *token = [info valueForKey:@"authentication_token"];
    
    [DataManager authorizeUser:info];
    [[RequestManager sharedManager] setApiToken:token email:email];
    
    [self registerForPushNotificationIfPosible];
    
    [self showBootingUpController];
    
    [[AppDelegate theApp].mainViewController userDidLogin];
    [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignIn label:ENAnalyticLabelSuccess];
}

- (IBAction)signUpAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionSignUp label:ENAnalyticLabelTry];
    [SFSafariManager openURLWithSafariController:EN_LINK_SIGH_UP forController:weakSelf];
}
- (IBAction)forgotAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    [AnalyticsManager trackEvent:ENAnalyticCategoryLogin action:ENAnalyticActionForgot label:ENAnalyticLabelTry];
    [SFSafariManager openURLWithSafariController:EN_LINK_FORGOT_PASSWORD forController:weakSelf];
}

#pragma mark - Navigation

-(void) showBootingUpController {
    
    [self.view bringSubviewToFront:self.splashView];
    self.splashView.hidden = NO;
    
    [self performSegueWithIdentifier:@"showBooting" sender:nil];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //New text
    NSString * newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textField.text.length == 0 && [newString stringByTrimmingCharactersInSet:set].length == 0 && textField == self.usernameField) {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.usernameField) {
        [self.passwordField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - SFSafariViewControllerDelegate

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboard methods

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    CGFloat move = 60;
    if (IS_IPHONE_5) move = 80;
    
    self.formCenterY.constant = self.standartFormCenterY - move;
    [self.view setNeedsLayout];
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    self.formCenterY.constant = self.standartFormCenterY;
    [self.view setNeedsLayout];
    [UIView animateWithDuration:duration.floatValue
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];

}

#pragma mark - Helper Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

- (void) registerForPushNotificationIfPosible {
    
    if ([AppDelegate theApp].deviceTokenForPush != nil) {
        [[RequestManager sharedManager] registerForPushWithToken:[AppDelegate theApp].deviceTokenForPush success:^(BOOL success) {
            [DataManager updatePushTokenForCurrentUser];
        } failure:^(NSString *message) {
            //
        }];
    }
}


@end
