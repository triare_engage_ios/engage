//
//  QuestionAnswerTitleCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionAnswerTitleCell.h"

@implementation QuestionAnswerTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.titleLabel.font = [UIFont appFont:17];
    self.titleLabel.textColor = [UIColor appTextColor];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth {
    
    CGFloat minHeight = 50;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFont:17];
    l.text = contentText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    height += 30;
    
    if (height < minHeight) {
        height = minHeight;
    }
    
    return height;
}

@end
