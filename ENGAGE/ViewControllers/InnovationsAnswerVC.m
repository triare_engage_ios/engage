//
//  InnovationsAnswerVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "InnovationsAnswerVC.h"

#import "InnovationsAnswerCell.h"
#import "InnovationsActionCell.h"

@interface SendInnovationAnswer : NSObject
@property (nonatomic, strong) NSMutableArray *answersList;

-(instancetype) init;
-(BOOL) isAnswersListValid;
-(NSInteger) notValidAnswer;
-(NSDictionary *) serializeForSend;

@end

@implementation SendInnovationAnswer

- (instancetype) init {
    if (self = [super init]) {
        self.answersList = [NSMutableArray new];
        [self fillEmptyAnswersList];
    }
    
    return self;
}

- (NSDictionary *) serializeForSend {
    return nil;
}

- (void) fillEmptyAnswersList {
    [self.answersList removeAllObjects];
    [self.answersList addObject:@""];
    [self.answersList addObject:@""];
    [self.answersList addObject:@""];
}

- (BOOL) isAnswersListValid {
    
    for (NSString *answerText in self.answersList) {
        if (answerText.length == 0) {
            return NO;
        }
    }
    
    if (self.answersList.count < 3) {
        return NO;
    }
    
    return YES;
}

- (NSInteger) notValidAnswer {
    
    for (NSInteger i = 0; i < self.answersList.count; i++) {
        NSString *answer = self.answersList[i];
        
        if (answer.length == 0) {
            return i;
        }
    }
    
    return -1;
}

@end

typedef enum : NSUInteger {
    CellItemTypeAnswerStartDoing,
    CellItemTypeAnswerStopDoing,
    CellItemTypeAnswerNeedDoing,
    CellItemTypeAction
} CellItemType;


@interface InnovationsAnswerVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;

@property (nonatomic, strong) SendInnovationAnswer *currentAnswer;
@property (nonatomic, strong) NSArray *itemsCell;

@end

@implementation InnovationsAnswerVC

#pragma mark - Public Methods

+(NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self prepareNavigationBar];
    [self prepareHeaderView];
    self.registerKeyboardNotifications = YES;
    [self prepareTableView];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - Prepare Methods

- (void) prepareNavigationBar {
    
    [self.navigationBar setTitle:[NSLocalizedString(@"innovations", @"v1.0") uppercaseString]];
}

- (void) prepareHeaderView {
    self.headerView.backgroundColor = [UIColor clearColor];
    self.headerTitleLabel.font = [UIFont appFont:15];
    self.headerTitleLabel.textColor = [UIColor appTextColor];
    self.headerTitleLabel.text = NSLocalizedString(@"Add your plans for the next month", @"v1.0");
}

- (void) prepareTableView {
    [self.tableView registerNib:[UINib nibWithNibName:[InnovationsAnswerCell cellNimbName] bundle:nil] forCellReuseIdentifier:[InnovationsAnswerCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[InnovationsActionCell cellNimbName] bundle:nil] forCellReuseIdentifier:[InnovationsActionCell cellReuseIdentifier]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets.top = EN_NAVIGATION_BAR_HEIGHT;
    contentInsets.bottom = 0;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Data

-(SendInnovationAnswer *) currentAnswer {
    if (_currentAnswer == nil) {
        SendInnovationAnswer *answer = [[SendInnovationAnswer alloc] init];
        _currentAnswer = answer;
    }
    
    return _currentAnswer;
}

-(NSArray *) itemsCell {
    if (_itemsCell == nil) {
        NSArray *result = @[@(CellItemTypeAnswerStartDoing), @(CellItemTypeAnswerStopDoing), @(CellItemTypeAnswerNeedDoing), @(CellItemTypeAction)];
        _itemsCell = result;
    }
    
    return _itemsCell;
}

-(void) reloadData {
    self.itemsCell = nil;
    [self.tableView reloadData];
}

#pragma mark - Table View Delegate & Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsCell.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *itemNumber =self.itemsCell[indexPath.row];
    CellItemType item = itemNumber.integerValue;
    
    if (item == CellItemTypeAction) {
        return [InnovationsActionCell cellHeight];
    }
    else {
        return [InnovationsAnswerCell cellHeight];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *itemNumber =self.itemsCell[indexPath.row];
    CellItemType item = itemNumber.integerValue;

    if (item == CellItemTypeAction) {
        return [self actionCell:tableView atIndexPath:indexPath];
    }
    else {
        return [self answerCell:tableView atIndexPath:indexPath];
    }
    
    return nil;
}

#pragma mark - Cells

-(InnovationsAnswerCell *) answerCell:(UITableView *) tableView atIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    InnovationsAnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:[InnovationsAnswerCell cellReuseIdentifier]];
    
    switch (indexPath.row) {
        case 0:
            [cell applyCellStyle:InnovationsAnswerCellStyleStart];
            break;
            
        case 1:
            [cell applyCellStyle:InnovationsAnswerCellStyleStop];
            break;
            
        case 2:
            [cell applyCellStyle:InnovationsAnswerCellStyleNeed];
            break;
            
        default:
            [cell applyCellStyle:InnovationsAnswerCellStyleStart];
            break;
    }
    
    cell.onChangeText = ^(NSString *changedText, InnovationsAnswerCell *returnedCell) {
        NSIndexPath *returnedIndexPath = [weakSelf.tableView indexPathForCell:returnedCell];
        [weakSelf changeAnswerText:changedText forIndexPath:returnedIndexPath];
    };
    
    cell.onDidBeginChangingText = ^(InnovationsAnswerCell *returnedEditedCell) {
        NSIndexPath *returnedEditedIndexPath = [weakSelf.tableView indexPathForCell:returnedEditedCell];
        [weakSelf scrollToCellAtIndexPath:returnedEditedIndexPath];
    };
    
    return cell;
}

-(InnovationsActionCell *) actionCell:(UITableView *) tableView atIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    InnovationsActionCell *cell = [tableView dequeueReusableCellWithIdentifier:[InnovationsActionCell cellReuseIdentifier]];
    
    [cell applyButtonsLeftTitle:[NSLocalizedString(@"skip", @"v1.0") uppercaseString] andRightTitle:[NSLocalizedString(@"submit", @"v1.0") uppercaseString]];
    
    cell.onAction = ^(NSInteger actionState, InnovationsActionCell *returnedCell) {
        if (actionState == 0) {
            [weakSelf skipAction];
        }
        else if (actionState == 1) {
            [weakSelf submitAction];
        }
    };
    
    return cell;
}

#pragma mark - Actions

- (void) skipAction {
    [self closeVC];
}

- (void) submitAction {
    
    if (![self.currentAnswer isAnswersListValid]) {
        //Not need in current realisation
        //NSInteger invalidNumber = [self.currentAnswer notValidAnswer];
        [self showAlertWithMessage:NSLocalizedString(@"Please, fill in all fields", @"v1.0")];
        return;
    }
    
    //TODO: Add request method for sending info for API
    [self closeVC];
}

-(void) changeAnswerText:(NSString *) changedText forIndexPath:(NSIndexPath *) indexPath {
    [self.currentAnswer.answersList replaceObjectAtIndex:indexPath.row withObject:changedText];
}

-(void) scrollToCellAtIndexPath:(NSIndexPath *) indexPath {
    
    //Need scrool to next cell
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
    
    NSInteger maxIndex = [self.tableView numberOfRowsInSection:indexPath.section];
    
    if (nextIndexPath.row > maxIndex) {
        return;
    }
    
    [self.tableView scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - Navigation

- (void) closeVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboards Methods

#define kDeltaKeyboardMovement 0

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(EN_NAVIGATION_BAR_HEIGHT, 0.0, kbRect.size.height + kDeltaKeyboardMovement, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets.top = EN_NAVIGATION_BAR_HEIGHT;
    contentInsets.bottom = 0;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Alert

- (void) showAlertWithMessage:(NSString *) text {
    
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}


@end
