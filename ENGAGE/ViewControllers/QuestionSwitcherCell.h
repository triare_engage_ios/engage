//
//  QuestionSwitcherCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionSwitcherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTopSpace;

@property (nonatomic, copy) void (^onAction)(NSInteger index);

- (void) changeSwitcherStateEnable:(BOOL)state;
- (void) changeSwitcherToOn:(BOOL) isOn;

@end
