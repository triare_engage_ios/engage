//
//  CustomAnswerHeaderCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/20/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "CustomAnswerHeaderCell.h"

@implementation CustomAnswerHeaderCell

- (void) awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFontBold:18];
}

@end
