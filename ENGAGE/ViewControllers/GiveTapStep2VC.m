//
//  GiveTapStep2VC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "GiveTapStep2VC.h"
#import "GiveTapStep1VC.h"
#import "GiveTapNavController.h"
#import "NavigationBar.h"
#import "CollectionCell.h"
#import "CollectionHeaderView.h"

//================================
//Deprecated. Use: GiveTapStep2_1VC
//================================

@interface GiveTapStep2VC ()

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation GiveTapStep2VC

-(void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    
    //Need for skip first step if give tap from user profile
    if (self.isNeedSkipFirstStepWhenBack) {
        self.navigationBar.onLeftAction = ^{
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        };
        
    }
    else {
        self.navigationBar.onLeftAction = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:NSLocalizedString(@"STEP 2", @"v1.0")];
    
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT;
    self.collectionView.contentInset = insets;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.isDataLoaded) {
        self.isDataLoaded = YES;
        [self loadBehaviour];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.collectionView reloadData];
}

-(void) loadBehaviour {
    
    __weak typeof(self) weakSelf = self;
    
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getBehaviours:^(BOOL success, NSArray *info) {
        if (success) {
            [DataManager saveBehaviorsList:info];
            [weakSelf reloadData];
        }
        [weakSelf hideLoadingView];
    }];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    GiveTapNavController *navigationController = (GiveTapNavController *) self.navigationController;
    navigationController.behavior = sender;
}


#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [Behavior MR_requestAllSortedBy:@"title" ascending:YES inContext:[DataManager context]];
    fetchRequest.includesSubentities = NO;
    NSFetchedResultsController *fetchController = [Behavior MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}


#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Behavior *behaviour = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.cellLabel.text = behaviour.title;
    [cell.cellImageView sd_setImageWithURL:[AppTemplate mediumIconURL:behaviour.iconUrl] placeholderImage:[Behavior placeholder]];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [CollectionCell sizeForCollectionWidth:collectionView.frame.size.width];
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Behavior *item = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [self performSegueWithIdentifier:@"goToNextStep" sender:item];
}

//header
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    
    CollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                        UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
    headerView.label.text = NSLocalizedString(@"What are you tapping them for?", @"v1.0");
    
    return headerView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    return CGSizeMake(200, 60);
}

@end
