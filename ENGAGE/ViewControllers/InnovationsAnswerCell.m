//
//  InnovationsAnswerCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "InnovationsAnswerCell.h"

#define kDefaultCellHeight 130 //Top space (10) + Title Label (21) + TextView (70) + Bottom space (5)
#define kDefaultCellIdentifier @"innovationsAnswerCell"

@interface InnovationsAnswerCell () <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIImageView *textViewBackground;

@end

@implementation InnovationsAnswerCell

#pragma mark - Public Methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

-(void) applyCellStyle:(InnovationsAnswerCellStyle) style {
    _cellStyle = style;
    [self changeDataForStyle:style];
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _cellStyle = InnovationsAnswerCellStyleStart;
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:16];
    
    self.subtitleLabel.textColor = [UIColor appTextColor];
    self.subtitleLabel.font = [UIFont appFont:13];
    
    self.textView.font = [UIFont appFontThin:17];
    self.textView.textColor = [UIColor blackColor];
    
    self.textView.delegate = self;
    
    self.textViewBackground.image = [[UIImage imageNamed:@"message-textBackground.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    
    [self changeDataForStyle:self.cellStyle];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.titleLabel.text = nil;
    self.subtitleLabel.text = nil;
    self.textView.text = nil;
    self.onChangeText = nil;
    self.onDidBeginChangingText = nil;
    _cellStyle = InnovationsAnswerCellStyleStart;
    
}

- (void) changeDataForStyle:(InnovationsAnswerCellStyle) style {
    switch (style) {
        case InnovationsAnswerCellStyleStart:
            self.titleLabel.text = NSLocalizedString(@"Start doing", @"v1.0");
            self.subtitleLabel.text = NSLocalizedString(@"what you want to start doing?", @"v1.0");
            break;
            
        case InnovationsAnswerCellStyleStop:
            self.titleLabel.text = NSLocalizedString(@"Stop doing", nil);
            self.subtitleLabel.text = NSLocalizedString(@"what you want to stop doing?", @"v1.0");
            break;
            
        case InnovationsAnswerCellStyleNeed:
            self.titleLabel.text = NSLocalizedString(@"Need to improve", nil);
            self.subtitleLabel.text = NSLocalizedString(@"what you want to improve?", @"v1.0");
            break;
    }
}

#pragma mark - UITextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    __weak typeof(self) weakSelf = self;
    
    NSString * newString = [[textView text] stringByReplacingCharactersInRange:range withString:text];
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textView.text.length == 0 && [newString stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    if (weakSelf.onChangeText) {
        weakSelf.onChangeText (newString, weakSelf);
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onDidBeginChangingText) {
        weakSelf.onDidBeginChangingText (weakSelf);
    }
}

@end
