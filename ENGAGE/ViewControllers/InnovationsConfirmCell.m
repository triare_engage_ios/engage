//
//  InnovationsConfirmCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "InnovationsConfirmCell.h"

#define kDefaultCellIdentifier @"innovationsConfirmCell"

@interface InnovationsConfirmCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorLine;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

@end



@implementation InnovationsConfirmCell

#pragma mark - Public Methods

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

+(CGFloat) cellHeightForText:(NSString *) innovationText withTableWidth:(CGFloat) tableWidth {
    
    CGFloat minHeight = 320;
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFont:13];
    l.text = innovationText;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(tableWidth - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    height += 5; //Top space for title label
    height += 21; //Height of title label
    height += 5; //Top space for separator line
    height += 6; //Top space for label
    height += 10; //Bottom space
    
    if (height < minHeight) {
        height = minHeight;
    }
    
    return height;
}

-(void) applyCellStyle:(InnovationsConfirmCellStyle) style {
    _cellStyle = style;
    [self changeTextForStyle:style];
}

-(void) setInnovationText:(NSString *) text {
    self.subtitleLabel.text = text;
}

#pragma mark - Life Cycle

-(void)awakeFromNib {
    [super awakeFromNib];
    
    _cellStyle = InnovationsConfirmCellStyleStart;
    
    self.titleLabel.textColor = [UIColor appTextColor];
    self.titleLabel.font = [UIFont appFont:16];
    
    self.separatorLine.backgroundColor = [UIColor whiteColor];
    
    self.subtitleLabel.textColor = [UIColor appTextColor];
    self.subtitleLabel.font = [UIFont appFont:13];
}

#pragma mark - Private Action Methods

-(void) changeTextForStyle:(InnovationsConfirmCellStyle) style {
    
    switch (style) {
        case InnovationsConfirmCellStyleStart:
            self.titleLabel.text = NSLocalizedString(@"Start doing", @"v1.0");
            break;
            
        case InnovationsConfirmCellStyleStop:
            self.titleLabel.text = NSLocalizedString(@"Stop doing", @"v1.0");
            break;
        
        case InnovationsConfirmCellStyleNeed:
            self.titleLabel.text = NSLocalizedString(@"Need to improve", @"v1.0");
            break;
    }
}

@end
