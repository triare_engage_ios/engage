//
//  ChallengeInfoVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

typedef enum : NSUInteger {
    ScreenStateFromActivityFeed,
    ScreenStateFromNavigationBar,
    ScreenStateFromUserProfile,
    ScreenStateFromActivityFeedCell
} ScreenState;

@interface ChallengeInfoVC : ContentViewController

@property (nonatomic, strong) Challenge *challenge;
@property (nonatomic) ScreenState screenState;

+(NSString *) storyboardIdentifier;

@end
