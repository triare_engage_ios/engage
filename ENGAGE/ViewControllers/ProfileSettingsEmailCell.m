//
//  ProfileSettingsEmailCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileSettingsEmailCell.h"

#define kDefaultCenterY 0
#define kShowUnconfirmedEmailCenterY -10
#define kDefaultUnconfirmedEmailHeight 0
#define kShowUnconfirmedEmailHeight 21


@interface ProfileSettingsEmailCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftTitleCenterY;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightTextFieldCenterY;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftUnconfirmedEmailHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightUnconfirmedEmailHeight;

@end

@implementation ProfileSettingsEmailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.leftTitle.textColor = [UIColor appTextColor];
    self.leftTitle.font = [UIFont appFont:17];
    
    self.rightTextField.textColor = [UIColor appTextFieldColor];
    self.rightTextField.font = [UIFont appFont:17];
    
    self.leftUnconfirmedEmail.textColor = [UIColor appTextColor];
    self.leftUnconfirmedEmail.font = [UIFont appFont:14];
    
    self.rightUnconfirmedEmail.textColor = [UIColor appTextFieldColor];
    self.rightUnconfirmedEmail.font = [UIFont appFont:14];
    
    self.rightUnconfirmedEmail.hidden = YES;
    self.leftUnconfirmedEmail.hidden = YES;
    
    self.rightTextField.keyboardType = UIKeyboardTypeEmailAddress;
}

- (void)prepareForReuse {
    self.rightTextField.text = nil;
    self.rightTextField.delegate = nil;
    self.rightTextField.tag = 0;
    self.rightTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.rightTextField.keyboardType = UIKeyboardTypeEmailAddress;
    [self.rightTextField setSecureTextEntry:NO];
    
    self.leftTitle.text = nil;
    
    self.rightUnconfirmedEmail.text = nil;
    self.leftUnconfirmedEmail.text = nil;
    
    [self needShowUnconfirmedEmail:NO];
}

- (void) needShowUnconfirmedEmail:(BOOL) show {
    self.leftUnconfirmedEmail.hidden = !show;
    self.rightUnconfirmedEmail.hidden = !show;
    
    if (show) {
        self.leftTitleCenterY.constant = kShowUnconfirmedEmailCenterY;
        self.rightTextFieldCenterY.constant = kShowUnconfirmedEmailCenterY;
        self.rightUnconfirmedEmailHeight.constant = kShowUnconfirmedEmailHeight;
        self.leftUnconfirmedEmailHeight.constant = kShowUnconfirmedEmailHeight;
    }
    else {
        self.leftTitleCenterY.constant = kDefaultCenterY;
        self.rightTextFieldCenterY.constant = kDefaultCenterY;
        self.rightUnconfirmedEmailHeight.constant = kDefaultUnconfirmedEmailHeight;
        self.leftUnconfirmedEmailHeight.constant = kDefaultUnconfirmedEmailHeight;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
