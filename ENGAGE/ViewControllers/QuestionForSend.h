//
//  QuestionForSend.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuestionForSend : NSObject

@property (nonatomic, strong) NSString *body;
@property (nonatomic) BOOL haveMultipleChoice;
@property (nonatomic, strong) NSDate *dateTime;
@property (nonatomic) BOOL isSchedule;
@property (nonatomic, strong) NSMutableArray *answers;
@property (nonatomic) BOOL haveCustomAnswer;

-(instancetype) init;
-(NSDictionary *) serializeForSend;
- (BOOL) isAnswersFull;
- (BOOL) isAnswersMinimum;

@end
