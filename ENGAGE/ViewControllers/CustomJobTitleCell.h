//
//  CustomJobTitleCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/7/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomJobTitleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *jobTextField;

@end
