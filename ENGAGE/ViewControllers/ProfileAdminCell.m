//
//  ProfileAdminCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 23.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileAdminCell.h"

@implementation ProfileAdminCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.suggestionButton setTitle:NSLocalizedString(@"VIRTUAL SUGGESTIONS", @"v1.0") forState:UIControlStateNormal];
    [self.questionsButton setTitle:NSLocalizedString(@"PERIODICAL QUESTIONS", @"v1.0") forState:UIControlStateNormal];
    [self.peopleButton setTitle:NSLocalizedString(@"MANAGE PEOPLE", @"v1.0") forState:UIControlStateNormal];
    [self.privatButton setTitle:NSLocalizedString(@"TEAM MESSAGE", @"v1.0") forState:UIControlStateNormal];
    
    self.suggestionBadge.isNeedResizeBadge = @NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)suggestionAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    if (weakSelf.onAction) {
        weakSelf.onAction(0);
    }
}
- (IBAction)questionsAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    if (weakSelf.onAction) {
        weakSelf.onAction(1);
    }
}
- (IBAction)peopleActions:(id)sender {
    __weak typeof(self) weakSelf = self;
    if (weakSelf.onAction) {
        weakSelf.onAction(2);
    }
}
- (IBAction)privatAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    if (weakSelf.onAction) {
        weakSelf.onAction(3);
    }
}

-(void) setNewSuggestionCount:(NSInteger) count {
    if (count > 0) {
        self.suggestionBadge.hidden = NO;
        self.suggestionBadge.text = [NSString stringWithFormat:@"%ld", (long)count];
        //[self.suggestionBadge resizeWidth];
    } else {
        self.suggestionBadge.hidden = YES;
    }
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    if (IS_IPHONE_5) {
        self.suggestionButton.titleLabel.font = [UIFont appFontBold:11];
        self.questionsButton.titleLabel.font = [UIFont appFontBold:11];
        self.peopleButton.titleLabel.font = [UIFont appFontBold:11];
        self.privatButton.titleLabel.font = [UIFont appFontBold:11];
    }
}


@end
