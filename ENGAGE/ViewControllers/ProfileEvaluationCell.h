//
//  ProfileEvaluationCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/1/16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedLabel.h"

@interface ProfileEvaluationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENRoundedLabel *leftNumberLabel;
@property (weak, nonatomic) IBOutlet ENRoundedLabel *rightNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTitleLabel;

@end
