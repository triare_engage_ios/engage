//
//  RetapsListVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 15.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "RetapsListVC.h"
#import "NSDate+TimeAgo.h"
#import "LoadingCell.h"

#import "ProfileVC.h"
#import "NSDictionary+SecureValue.h"

@implementation RetapsListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeRoundedView:self.senderImage];
    //[self.senderImage makeCircle];
    
    self.nameLabel.textColor = [UIColor appTextColor];
    self.nameLabel.font = [UIFont appFont:17];
    
    self.dateLabel.textColor = [UIColor appTextColor];
    self.dateLabel.font = [UIFont appFont:14];
    
    self.jobLabel.textColor = [UIColor appTextColor];
    self.jobLabel.font = [UIFont appFont:17];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    
    self.rightUtilityButtons = nil;
}

@end




@interface RetapsListVC () <SWTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *tableView;

@property (strong, nonatomic) NSMutableArray *items;

@property (nonatomic, strong) PaginationInfo *pagination;

@end

@implementation RetapsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    [self.navigationBar showLeftButton];
    [self.navigationBar setTitle:NSLocalizedString(@"RETAPS", @"v1.0")];
    
    self.pagination = [[PaginationInfo alloc] init];
    self.pagination.currentPage = 0;
    self.pagination.totalPages = 1;
    [self loadDataForPage:1];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenRetapList];
    [AnalyticsManager trackScreen:ENAnalyticScreenRetapList withScreenClass:NSStringFromClass([self class])];
    
}


-(void) loadNextCommentsData {
    
    if (self.pagination.isLoading) return;
    
    if (self.pagination.totalPages > self.pagination.currentPage) {
        [self loadDataForPage:self.pagination.currentPage + 1];
    }
}

-(void) loadDataForPage:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    weakSelf.pagination.isLoading = YES;
    [[RequestManager sharedManager] getRetaps:self.feed.feedId page:page onCompleate:^(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage) {
        
        if (success) {
            weakSelf.pagination.currentPage = pagination.currentPage;
            weakSelf.pagination.totalPages = pagination.totalPages;
            
            if (weakSelf.pagination.currentPage == 1) {
                weakSelf.items = [NSMutableArray arrayWithArray:list];
            } else {
                [weakSelf.items addObjectsFromArray:list];
            }
            
            [weakSelf.tableView reloadData]; //added, because show allert every time
        }
        
        weakSelf.pagination.isLoading = NO;
//
//        [weakSelf.tableView reloadData];
        
        if (!success) {
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
        }
    }];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger number = self.items.count;
    
    if (self.pagination.currentPage < self.pagination.totalPages) {
        return number + 1;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.items.count) {
        
        NSDictionary *item = [self.items objectAtIndex:indexPath.row];
        
        RetapsListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"retapCell"];
        [cell.senderImage sd_setImageWithURL:[AppTemplate bigIconURL:[item secureStringForKey:@"user.avatar_url" andDefault:@""]] placeholderImage:[TeamUser placeholder]];
        cell.nameLabel.text = [item secureStringForKey:@"user.full_name" andDefault:@""];
        cell.jobLabel.text = [item secureStringForKey:@"user.job_title" andDefault:@""];
        
        NSNumber *timestamp = [item secureNumberForKey:@"created_at"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp.integerValue];
        cell.dateLabel.text = [date formattedAsTimeAgo];
        
        cell.delegate = self;
        
        //Not used. For Ivan request
//        if ([self canDeleteCommentForItem:item]) {
//            cell.rightUtilityButtons = [self deleteButton];
//        }
        
        return cell;
    } else {
        return [LoadingCell cell];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *item = [self.items objectAtIndex:indexPath.row];
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    
    vc.userId = [item valueForKeyPath:@"user.id"];
    vc.profileTitle = [item valueForKeyPath:@"user.full_name"];
    
    [self showViewController:vc sender:nil];
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isLoadingCell]) {
        [self loadNextCommentsData];
    }
}

#pragma mark - SWTableViewCellDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSDictionary *item = [self.items objectAtIndex:indexPath.row];
    
    if ([self canDeleteCommentForItem:item]) {
        [self showDeleteAlertForItem:item atIndexPath:indexPath];
    }
    
}

//Can swipe only one row in time (close if swipe another)
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

//Implementattion of showing buttons looks in cellForRowAtIndexPath:
//- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
//    return YES;
//}

- (NSArray *) deleteButton {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor redColor] icon:[UIImage imageNamed:@"icon-delete"]];
    
    return rightUtilityButtons;
}

#pragma mark - Action Methods

- (void) showDeleteAlertForItem:(NSDictionary *)item atIndexPath:(NSIndexPath *) indexPath {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to delete retap?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self deleteRetapForItem:item atIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

//Not used Now
- (void) deleteRetapForItem:(NSDictionary *) item atIndexPath:(NSIndexPath *) indexPath {
    //__weak typeof(self) weakSelf = self;
    
    [[RequestManager sharedManager] deleteRetapWithId:nil success:^(BOOL success) {
        //
    } failure:^(NSString *message) {
        //
    }];
    
}

#pragma mark - Update UI

//Not used Now
- (void) hideButtonsForIndexPath: (NSIndexPath *) indexPath {
    RetapsListCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [cell hideUtilityButtonsAnimated:YES];
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

//Not used Now
- (BOOL) canDeleteCommentForItem:(NSDictionary *) item {
    BOOL canDelete = NO;
    
    //Admin and manager can delete any retap
    if ([DataManager user].isAdmin || [DataManager user].isManager) {
        canDelete = YES;
    }
    
    //User can delete his retaps
    NSNumber *userId = [item secureNumberForKey:@"user.id" andDefault:[NSNumber numberWithInt:-1]];
    
    if ([DataManager user].userIdValue == userId.integerValue) {
        canDelete = YES;
    }
    
    return canDelete;
}

@end
