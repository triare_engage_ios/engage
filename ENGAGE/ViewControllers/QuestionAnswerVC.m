//
//  QuestionAnswerVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionAnswerVC.h"

#import "QuestionAnswerTitleCell.h"
#import "QuestionNativeAnswerCell.h"
#import "QuestionCustomAnswerCell.h"
#import "QuestionAnswerActionCell.h"

#import "UITableViewCell+Separator.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface SendAnswer : NSObject
@property (nonatomic, strong) NSMutableArray *answers;
@property (nonatomic, strong) NSString *customAnswerText;
@property (nonatomic) BOOL haveCustomAnser;

- (instancetype) init;
- (BOOL) isAnswerOptionExist:(ActivityFeedAnswerOption *) option;
- (void) deleteAnswerOption:(ActivityFeedAnswerOption *) option;
- (void) addAnswerOption:(ActivityFeedAnswerOption *) option;
- (void) removeAllAnswers;
- (NSDictionary *) serializeForSend;

@end

@implementation SendAnswer

- (instancetype) init {
    if (self = [super init]) {
        self.answers = [NSMutableArray new];
        self.haveCustomAnser = NO;
    }
    
    return self;
}

- (BOOL) isAnswerOptionExist:(ActivityFeedAnswerOption *) option {
    
    BOOL isExist = NO;
    
    for (ActivityFeedAnswerOption *optionExist in self.answers) {
        if ([optionExist.answerOptionId integerValue] == [option.answerOptionId integerValue]) {
            isExist = YES;
        }
    }
    
    return isExist;
}

- (void) deleteAnswerOption:(ActivityFeedAnswerOption *) option {
    
    for (int i = 0; i < self.answers.count; i++ ) {
        ActivityFeedAnswerOption *optionInArray = self.answers[i];
        
        if ([optionInArray.answerOptionId integerValue] == [option.answerOptionId integerValue]) {
            [self.answers removeObjectAtIndex:i];
        }
    }
}

- (void) addAnswerOption:(ActivityFeedAnswerOption *) option {
    [self.answers addObject:option];
}

- (void) removeAllAnswers {
    [self.answers removeAllObjects];
}

- (NSDictionary *) serializeForSend {
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    if (self.haveCustomAnser && [self isCustomAnswerCheckmarkExist]) {
        [result setValue:self.customAnswerText forKey:@"body"];
    }
    
    NSMutableString *answerOptionIds = [[NSMutableString alloc] init];
    
    for (int i = 0; i < self.answers.count; i ++) {
        ActivityFeedAnswerOption *option = self.answers[i];
        
        if (self.answers.count - 1 == i) {
            [answerOptionIds appendString:[NSString stringWithFormat:@"%@", [option.answerOptionId stringValue]]];
        }
        else {
            [answerOptionIds appendString:[NSString stringWithFormat:@"%@,", [option.answerOptionId stringValue]]];
        }
    }
    
    if (answerOptionIds.length > 0) {
        [result setValue:answerOptionIds forKey:@"answer_option_ids"];
    }
    
    return result;
}

- (BOOL) isCustomAnswerCheckmarkExist {
    BOOL isExist = NO;
    
    for (ActivityFeedAnswerOption *optionExist in self.answers) {
        if (optionExist.isCustomValue) {
            isExist = YES;
        }
    }
    
    return isExist;
}

@end







@interface QuestionAnswerVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;

@property (nonatomic, strong) ActivityFeedQuestion *question;
@property (nonatomic, strong) SendAnswer *questionAnswer;

@end

@implementation QuestionAnswerVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupQuestion];
    [self setupTableView];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self addKeyboardNotification];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeKeyboardNotification];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionSendAnswer];
    [AnalyticsManager trackScreen:ENAnalyticScreenQuestionSendAnswer withScreenClass:NSStringFromClass([self class])];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Setup

- (void) setupNavigationBar {
    [self.navigationBar setTitle:NSLocalizedString(@"QUESTION", @"v1.0")];
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

#pragma mark - Data

- (void) setupQuestion {
    self.question = [DataManager activityFeedQuestion];
    self.questionAnswer = [[SendAnswer alloc] init];
    self.questionAnswer.haveCustomAnser = self.question.haveCustomAnswerOptionValue;
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 1;
    }
    
    if (section == 1) {
        return self.question.answerOptions.array.count;
    }
    
    if (section == 2) {
        return 1;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        return [QuestionAnswerTitleCell cellHeight:self.question.body width:tableView.frame.size.width];
    }
    
    if (indexPath.section == 1) {
        ActivityFeedAnswerOption *answerOption = self.question.answerOptions.array[indexPath.row];
        
        if (answerOption.isCustomValue) {
            return 50;
        }
        else {
            return [QuestionNativeAnswerCell cellHeight:answerOption.title width:tableView.frame.size.width];
        }
    }
    
    if (indexPath.section == 2) {
        return 50;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        QuestionAnswerTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell"];
        cell.titleLabel.text = self.question.body;
        [cell showSeparatorLine];
        
        return cell;
    }
    
    if (indexPath.section == 1) {
        ActivityFeedAnswerOption *answerOption = self.question.answerOptions.array[indexPath.row];
        
        __weak typeof(self) weakSelf = self;
        
        if (answerOption.isCustomValue) {
            QuestionCustomAnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"customAnswerCell"];
            cell.onChangeText = ^(NSString *text, QuestionCustomAnswerCell *cell) {
                weakSelf.questionAnswer.customAnswerText = text;
            };
            
            return cell;
        }
        else {
            QuestionNativeAnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nativeAnswerCell"];
            cell.titleLabel.text = answerOption.title;
            
            return cell;
        }
    }
    
    else {
        __weak typeof(self) weakSelf = self;
        
        QuestionAnswerActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionCell"];
        cell.onAction = ^(NSInteger state) {
            if (state == 0) {
                [weakSelf closeVC];
            }
            
            if (state == 1) {
                [weakSelf sendQuestionAnswer];
            }
        };
        
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        ActivityFeedAnswerOption *answerOption = self.question.answerOptions.array[indexPath.row];
        
        //For custom answer with multiple choise
        if (answerOption.isCustomValue && self.question.isMultipleValue) {
            QuestionCustomAnswerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if ([self.questionAnswer isAnswerOptionExist:answerOption]) {
                [self.questionAnswer deleteAnswerOption:answerOption];
                [cell changeCheckmarkToSelected:NO];
                [cell.textField resignFirstResponder];
            }
            else {
                [self.questionAnswer addAnswerOption:answerOption];
                [cell changeCheckmarkToSelected:YES];
                [cell.textField becomeFirstResponder];
            }
        }
        
        //For Native answer with multiple choise
        else if (self.question.isMultipleValue) {
            QuestionNativeAnswerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            
            if ([self.questionAnswer isAnswerOptionExist:answerOption]) {
                [self.questionAnswer deleteAnswerOption:answerOption];
                [cell changeCheckmarkToSelected:NO];
            }
            else {
                [self.questionAnswer addAnswerOption:answerOption];
                [cell changeCheckmarkToSelected:YES];
            }
        }
        
        //For custom answer with single choice
        else {
            //Custom answer
            if (answerOption.isCustomValue) {
                
                QuestionCustomAnswerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                
                if ([self.questionAnswer isAnswerOptionExist:answerOption]) {
                    [self.questionAnswer deleteAnswerOption:answerOption];
                    [cell changeCheckmarkToSelected:NO];
                    [cell.textField resignFirstResponder];
                }
                else {
                    //Anmark all lines
                    for (int i = 0; i < [self.contentTableView numberOfRowsInSection:1]; i++) {
                        if (i == [self.contentTableView numberOfRowsInSection:1]) {
                            QuestionCustomAnswerCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                            [cell changeCheckmarkToSelected:NO];
                        }
                        else if (indexPath.row != i) {
                            QuestionNativeAnswerCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                            [cell changeCheckmarkToSelected:NO];
                        }
                    }
                    [self.questionAnswer removeAllAnswers];
                    [self.questionAnswer addAnswerOption:answerOption];
                    [cell changeCheckmarkToSelected:YES];
                    [cell.textField becomeFirstResponder];
                }
            }
            
            //Native answer
            else {
                QuestionNativeAnswerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                
                if ([self.questionAnswer isAnswerOptionExist:answerOption]) {
                    [self.questionAnswer deleteAnswerOption:answerOption];
                    [cell changeCheckmarkToSelected:NO];
                }
                else {
                    
                    //anmark all lines
                    for (int i = 0; i < [self.contentTableView numberOfRowsInSection:1]; i++) {
                        if (i == [self.contentTableView numberOfRowsInSection:1]) {
                            QuestionCustomAnswerCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                            [cell changeCheckmarkToSelected:NO];
                        }
                        else if (indexPath.row != i) {
                            QuestionNativeAnswerCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:1]];
                            [cell changeCheckmarkToSelected:NO];
                        }
                    }
                    [self.questionAnswer removeAllAnswers];
                    [self.questionAnswer addAnswerOption:answerOption];
                    [cell changeCheckmarkToSelected:YES];
                }
            }
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}


#pragma mark - Action

- (void) sendQuestionAnswer {
    
    //Validation for checked checkmark and not
    if ([self.questionAnswer isCustomAnswerCheckmarkExist] && self.questionAnswer.customAnswerText.length == 0) {
        [self showAlertWithMessage:NSLocalizedString(@"Please fill in the text field or change the checkbox", @"v1.0")];
        return;
    }
    
    //Validation for answers
    if (self.questionAnswer.answers.count == 0) {
        [self showAlertWithMessage:NSLocalizedString(@"Please make a choice", @"v1.0")];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    
    //Give answer
    [[RequestManager sharedManager] giveAnswerForQuestionId:self.question.questionId withInfo:[self.questionAnswer serializeForSend] success:^(BOOL success) {
        [DataManager deleteActivityFeedQuestion];
        [weakSelf closeVC];
        [weakSelf showSentAlert];
        
    } failure:^(NSString *message) {
        [weakSelf showAlertWithMessage:message];
    }];
    
}

- (void) closeVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Keyboard Movement

#define kDeltaKeyboardMovement 60

- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(EN_NAVIGATION_BAR_HEIGHT, 0.0, kbRect.size.height + kDeltaKeyboardMovement, 0.0);
    self.contentTableView.contentInset = contentInsets;
    self.contentTableView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = self.contentTableView.contentInset;
    contentInsets.top = EN_NAVIGATION_BAR_HEIGHT;
    contentInsets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.contentTableView.contentInset = contentInsets;
    self.contentTableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) text {
    
    if (text.length == 0) {
        return;
    }
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:text preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"v1.0") style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void) showSentAlert {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Thank your for answer", @"v1.0")];
}

@end
