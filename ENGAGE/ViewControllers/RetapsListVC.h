//
//  RetapsListVC.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 15.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENCircleImageView.h"
#import <SWTableViewCell/SWTableViewCell.h>


@interface RetapsListCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet ENCircleImageView *senderImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobLabel;

@end




@interface RetapsListVC : ContentViewController

@property (nonatomic, strong) ActivityFeed *feed;

@end
