//
//  BehaviorsManageVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/23/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorsManageVC.h"
#import "CompanyValuesListVC.h"
#import "BehaviorIconsListVC.h"

#import "NavigationBar.h"
#import "ContentScrollView.h"
#import "TakePictureVC.h"
#import "AKFileManager.h"

#import "MPTextView.h"
#import "ENButton.h"

#import "UIImage+Extends.h"
#import "TakePictureVC.h"

#import <RSKImageCropper/RSKImageCropper.h>
#import <SVProgressHUD/SVProgressHUD.h>


@interface BehaviorsManageVC () <UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RSKImageCropViewControllerDelegate, RSKImageCropViewControllerDataSource>

#define kAddIconImageViewSquareSize 140
#define kAddIconImageViewRectangleWidth 309
#define kValueListViewHeight 50
#define kAnimationDuration 0.5
#define kBehaviorMinimumImageSize 500
#define kValueMinimumImageHeight 689
#define kValueMinimumImageWidth 1224

//Main part
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentScrollView *contentScrollView;

//Add image part
@property (weak, nonatomic) IBOutlet UIImageView *addIconImageView;
@property (weak, nonatomic) IBOutlet UIButton *addIconButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addIconImageViewWidth; //default 140
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addIconImageViewHeight; //default 140
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;

//Choose behavior or value part
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet UILabel *switcherTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *switcherSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIView *switcherSeparatorLine;

//Choose Value from list
@property (weak, nonatomic) IBOutlet UIView *valueListView;
@property (weak, nonatomic) IBOutlet UIView *valueListSeparatorLine;
@property (weak, nonatomic) IBOutlet UILabel *valueListTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueListSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *valueListRightImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *valueListViewHeight; //default 50
@property (weak, nonatomic) IBOutlet UIButton *valueListButton;

//Description part
@property (weak, nonatomic) IBOutlet UILabel *descriptionTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *descriptionBackgroundTextImageView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

//Save button
@property (weak, nonatomic) IBOutlet ENButton *actionButton;

//For keyboard moving
@property (nonatomic) BOOL isTextFieldActive;
@property (nonatomic) BOOL isTextViewActive;

//Data saving
@property (nonatomic, strong) NSNumber *valueId;
@property (nonatomic, strong) UIImage *valueImage;
@property (nonatomic, strong) NSString *valueImagePath;
@property (nonatomic, strong) UIImage *behaviorImage;
@property (nonatomic, strong) NSString *behaviorImagePath;

//Behavior Icon Image
@property (nonatomic, strong) NSString *behaviorIconImageUrl;
@property (nonatomic, strong) NSNumber *behaviorIconId;

//Photo library
@property (nonatomic, strong) UIImagePickerController *imagePicker;

@end

@implementation BehaviorsManageVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationBar];
    [self setupScrollView];
    [self setupAddImagePart];
    [self setupValueAndBehaviorPart];
    [self setupValueFromListPart];
    [self setupDescriptionPart];
    
    //Make default parameters for image state
    if (self.screenState == BehaviorScreenState) {
        self.imageState = ImageStateFromPhone;
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self addKeyboardNotification];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self trackScreen];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeKeyboardNotification];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    
    //Layout imageView
    self.addIconImageView.layer.cornerRadius = 5.0;
    self.addIconImageView.clipsToBounds = YES;
    
    //Layout descroption
    self.descriptionBackgroundTextImageView.layer.borderColor = [UIColor appTextColor].CGColor;
    self.descriptionBackgroundTextImageView.layer.borderWidth = 1.0;
    self.descriptionBackgroundTextImageView.layer.cornerRadius = 10.0;
    self.descriptionBackgroundTextImageView.clipsToBounds = YES;
}

#pragma mark - Setup Methods

- (void) setupNavigationBar {
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    [self.navigationBar showLeftButton];
    
    [self changeNavigatorBarTitleForState:self.screenState];
}

- (void) setupScrollView {
    self.contentScrollView.delegate = self;
}

- (void) setupAddImagePart {
    self.titleTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TITLE", @"v1.0") attributes:@{NSForegroundColorAttributeName: [UIColor appGrayColor],NSFontAttributeName: [UIFont appFont:17]}];
    self.titleTextField.textColor = [UIColor appTextColor];
    self.titleTextField.font = [UIFont appFont:17];
    self.titleTextField.delegate = self;
    self.titleTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    [self changeAddImageForState:self.screenState animated:NO];
}

- (void) setupValueAndBehaviorPart {
    if (self.screenState == ValueScreenState) {
         [self.switcher setOn:YES];
    }
    else {
        [self.switcher setOn:NO];
    }
   
    self.switcher.onTintColor = [UIColor appOrangeColor];
    
    self.switcherTitleLabel.textColor = [UIColor appTextColor];
    self.switcherTitleLabel.font = [UIFont appFont:17];
    
    self.switcherSubtitleLabel.textColor = [UIColor appTextColor];
    self.switcherSubtitleLabel.font = [UIFont appFont:12];
    
    self.switcherSeparatorLine.backgroundColor = [UIColor colorWithWhite:1 alpha:.5];
    
    [self changeSwitcherLabelsForState:self.screenState];
}

- (void) setupValueFromListPart {
    self.valueListTitleLabel.textColor = [UIColor appTextColor];
    self.valueListTitleLabel.font = [UIFont appFont:17];
    self.valueListTitleLabel.text = [NSLocalizedString(@"value", @"v1.0") uppercaseString];
    
    self.valueListSubtitleLabel.textColor = [UIColor appTextColor];
    self.valueListSubtitleLabel.font = [UIFont appFont:17];
    self.valueListSubtitleLabel.text = @"";
    
    self.valueListRightImageView.image = [UIImage imageNamed:@"icon-rightArrow"];
    
    self.valueListSeparatorLine.backgroundColor = [UIColor colorWithWhite:1 alpha:.5];
    
    [self changeValueListVisabilityForState:self.screenState animated:NO];
}

- (void) setupDescriptionPart {
    self.descriptionTitleLabel.textColor = [UIColor appTextColor];
    self.descriptionTitleLabel.font = [UIFont appFont:17];
    self.descriptionTitleLabel.text = [NSLocalizedString(@"description", @"v1.0") uppercaseString];

    self.descriptionTextView.textColor = [UIColor appTextColor];
    self.descriptionTextView.font = [UIFont appFont:17];
    
    self.descriptionBackgroundTextImageView.image = [[UIImage imageNamed:@"message-textBackground.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    self.descriptionBackgroundTextImageView.alpha = 0.5;
    
    self.descriptionTextView.delegate = self;
}

- (void) setupButton {
    [self.actionButton setTitle:[NSLocalizedString(@"save", @"v1.0") uppercaseString] forState:UIControlStateNormal];
}

#pragma mark - UI changes

-(void) changeNavigatorBarTitleForState:(ScreenState) state {
    
    if (state == ValueScreenState) {
        [self.navigationBar setTitle:NSLocalizedString(@"VALUE", @"v1.0")];
    }
    else {
        [self.navigationBar setTitle:NSLocalizedString(@"BEHAVIOR", @"v1.0")];
    }
}

- (void) changeAddImageForState:(ScreenState)state animated:(BOOL)animated {
    
    //Value
    if (state == ValueScreenState) {
        self.addIconImageViewWidth.constant = kAddIconImageViewRectangleWidth;
        [self.addIconImageView setNeedsLayout];
        [self.addIconButton setNeedsLayout];
        self.addIconImageView.image = [UIImage imageNamed:@"addValueRectangle"];
        
        //Animation
        if (animated) {
            [UIView animateWithDuration:kAnimationDuration
                             animations:^{
                                 [self.addIconImageView layoutIfNeeded];
                                 [self.addIconButton layoutIfNeeded];
                             } completion:^(BOOL finished) {
                             }];
        }
        else {
            [self.addIconImageView layoutIfNeeded];
            [self.addIconButton layoutIfNeeded];
            self.addIconImageView.image = [UIImage imageNamed:@"addValueRectangle"];
        }
    }
    
    //Behavior
    else {
        self.addIconImageViewWidth.constant = kAddIconImageViewSquareSize;
        self.addIconImageView.image = [UIImage imageNamed:@"addBehaviorSquare"];
        [self.addIconImageView setNeedsLayout];
        [self.addIconButton setNeedsLayout];
        
        //Animation
        if (animated) {
            [UIView animateWithDuration:kAnimationDuration
                             animations:^{
                                 [self.addIconImageView layoutIfNeeded];
                                 [self.addIconButton layoutIfNeeded];
                             } completion:^(BOOL finished) {
                                
                             }];
        }
        else {
            [self.addIconImageView layoutIfNeeded];
            self.addIconImageView.image = [UIImage imageNamed:@"addBehaviorSquare"];
        }
    }
}

- (void) changeSwitcherLabelsForState:(ScreenState)state {
    
    if (state == ValueScreenState) {
        self.switcherTitleLabel.text = [NSLocalizedString(@"value", @"v1.0") uppercaseString];
        self.switcherSubtitleLabel.text = NSLocalizedString(@"You create «Value»", @"v1.0");
    }
    else {
        self.switcherTitleLabel.text = [NSLocalizedString(@"behavior", @"v1.0") uppercaseString];
        self.switcherSubtitleLabel.text = NSLocalizedString(@"You create «Behavior»", @"v1.0");
    }
}

- (void) changeValueListVisabilityForState:(ScreenState)state animated:(BOOL)animated {
    
    if (state == ValueScreenState) {
        self.valueListViewHeight.constant = 0;
        [self.valueListView setNeedsLayout];
        
        if (animated) {
            [UIView animateWithDuration:kAnimationDuration
                             animations:^{
                                 [self.valueListView layoutIfNeeded];
                                 self.valueListView.alpha = 0;
                             } completion:^(BOOL finished) {
                                 self.valueListView.hidden = YES;
                             }];
        }
        else {
            self.valueListViewHeight.constant = 0;
            self.valueListView.hidden = YES;
            self.valueListView.alpha = 0;
        }
    }
    else {
        self.valueListViewHeight.constant = kValueListViewHeight;
        [self.valueListView setNeedsLayout];
        
        if (animated) {
            [UIView animateWithDuration:kAnimationDuration
                             animations:^{
                                 [self.valueListView layoutIfNeeded];
                                 self.valueListView.alpha = 1;
                             } completion:^(BOOL finished) {
                                 self.valueListView.hidden = NO;
                             }];
        }
        else {
            [self.valueListView layoutIfNeeded];
            self.valueListView.alpha = 1;
            self.valueListView.hidden = NO;
        }
    }
}

- (void) changeAddIconImageToCustomForState:(ScreenState) state {
    if (state == BehaviorScreenState && self.behaviorImage) {
        self.addIconImageView.image = self.behaviorImage;
    }
    else if (self.valueImage) {
        self.addIconImageView.image = self.valueImage;
    }
}

- (void) changeAddIconImageToCustomForImageState:(ImageState) state {
    if (state == ImageStateFromLibrary && self.behaviorIconId && self.screenState == BehaviorScreenState) {
        [self.addIconImageView sd_setImageWithURL:[AppTemplate bigIconURL:self.behaviorIconImageUrl] placeholderImage:[Behavior placeholder]];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - Action

- (IBAction)addIconButtonPressed:(id)sender {
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:nil
                                 message:nil
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* takePhoto = [UIAlertAction
                                actionWithTitle:NSLocalizedString (@"Take a photo", @"v1.0")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self takePhotoAction];
                                    [view dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    UIAlertAction* photoroll = [UIAlertAction
                                actionWithTitle:NSLocalizedString (@"Choose from photoroll", @"v1.0")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self chooseFromPhotoRollAction];
                                    [view dismissViewControllerAnimated:YES completion:nil];
                                    
                                }];
    
    UIAlertAction *library = [UIAlertAction
                              actionWithTitle:NSLocalizedString (@"Choose from library", @"v1.0")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self chooseFromLibraryAction];
                                  [view dismissViewControllerAnimated:YES completion:nil];
                                  
                              }];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:takePhoto];
    [view addAction:photoroll];
    if (self.screenState == BehaviorScreenState) {
        [view addAction:library];
    }
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

- (IBAction)switcherStateChange:(id)sender {
    if (self.switcher.isOn) {
        self.screenState = ValueScreenState;
        self.imageState = ImageStateFromPhone;
        [self changeScreenStateTo:ValueScreenState];
    }
    else {
        self.screenState = BehaviorScreenState;
        self.imageState = ImageStateFromPhone;
        [self changeScreenStateTo:BehaviorScreenState];
    }
}

- (IBAction)valueListButtonPressed:(id)sender {
    [self showValueListOfCompany];
}

- (IBAction)actionButtonPressed:(id)sender {
    [self validateAndSendData];
}

- (void) changeScreenStateTo:(ScreenState)state {
    [self changeAddImageForState:state animated:NO];
    [self changeNavigatorBarTitleForState:state];
    [self changeSwitcherLabelsForState:state];
    [self changeValueListVisabilityForState:state animated:NO];
    [self cleanImageDataAndPath];
}

- (void) validateAndSendData {
    //Validate image
    
    //Behavior state
    if (self.screenState == BehaviorScreenState) {
        //Icon from phone
        if (self.imageState == ImageStateFromPhone) {
            if (self.behaviorImage == nil || self.behaviorImagePath == nil) {
                [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add icon", @"v1.0")];
                return;
            }
        }
        //Icon from library
        else if (self.imageState == ImageStateFromLibrary) {
            if (self.behaviorIconId == nil && self.behaviorIconImageUrl == nil) {
                [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add icon", @"v1.0")];
                return;
            }
        }
        
    }
    
    //Value state (no position from library, because no library list)
    else if (self.screenState == ValueScreenState) {
        if (self.valueImage == nil || self.valueImagePath == nil) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add icon", @"v1.0")];
            return;
        }
    }
    
    //Title validation
    if (self.titleTextField.text.length == 0) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add title", @"v1.0")];
        return;
    }
    
    //Value validation for behavior
    if (self.screenState == BehaviorScreenState) {
        if (self.valueId == nil) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Choose Value", @"v1.0")];
            return;
        }
    }
    
    //Description validation
    if (self.descriptionTextView.text.length == 0) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Add description", @"v1.0")];
        return;
    }
    
    //Description validation for length
    if (self.descriptionTextView.text.length > 150) {
        [[AppDelegate theApp] showAlert:nil text:NSLocalizedString(@"Description is too long. Maximus 300 symbols.", @"v1.0")];
        return;
    }
    
    [self.view endEditing:YES];
    
    [self sendData];
    
}

- (void) sendData {
    __weak typeof(self) weakSelf = self;
    
    if (self.screenState == BehaviorScreenState) {
        if (self.imageState == ImageStateFromLibrary) {
            [self showLoadingViewBellowView:self.navigationBar];
            [[RequestManager sharedManager] createBehaviorWithInfo:[self serializeDataForSend] andIconId:self.behaviorIconId success:^(BOOL success, NSNumber *behaviorId) {
                [weakSelf hideLoadingView];
                [weakSelf closeVC];
                [weakSelf showSentAlertForState:BehaviorScreenState];
                if (weakSelf.onSendAction) {
                    weakSelf.onSendAction();
                }
            } failure:^(NSString *message) {
                [weakSelf hideLoadingView];
                [weakSelf showAlertWithMessage:message];
            }];
        }
        else {
            [self showLoadingViewBellowView:self.navigationBar];
            [[RequestManager sharedManager] createBehaviorWithInfo:[self serializeDataForSend] success:^(BOOL success, NSNumber *behaviorId) {
                [weakSelf updateImageForState:BehaviorScreenState andId:behaviorId];
            } failure:^(NSString *message) {
                [weakSelf hideLoadingView];
                [weakSelf showAlertWithMessage:message];
            }];
        }
    }
    else {
        [self showLoadingViewBellowView:self.navigationBar];
        [[RequestManager sharedManager] createValueWithInfo:[self serializeDataForSend] success:^(BOOL success, NSNumber *valueId) {
            [weakSelf updateImageForState:ValueScreenState andId:valueId];
        } failure:^(NSString *message) {
            [weakSelf hideLoadingView];
            [weakSelf showAlertWithMessage:message];
        }];
    }
}

- (void) updateImageForState:(ScreenState)state andId:(NSNumber *) ID {
    __weak typeof(self) weakSelf = self;
    
    if (state == BehaviorScreenState) {
        [[RequestManager sharedManager] updateBehaviorIconWithImagePath:self.behaviorImagePath andBehaviorId:ID success:^(BOOL success) {
            [weakSelf hideLoadingView];
            [weakSelf closeVC];
            [weakSelf showSentAlertForState:BehaviorScreenState];
            if (weakSelf.onSendAction) {
                weakSelf.onSendAction();
            }
        } failure:^(NSString *message) {
            [weakSelf hideLoadingView];
            [weakSelf showAlertWithMessage:message];
        }];
    }
    else {
        [[RequestManager sharedManager] updateValueIconWithImagePath:self.valueImagePath andValueId:ID success:^(BOOL success) {
            [weakSelf hideLoadingView];
            [weakSelf closeVC];
            [weakSelf showSentAlertForState:ValueScreenState];
        } failure:^(NSString *message) {
            [weakSelf hideLoadingView];
            [weakSelf showAlertWithMessage:message];
        }];
    }
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"showValuesList"]) {
        __weak typeof(self) weakSelf = self;
        
        CompanyValuesListVC *dest = segue.destinationViewController;
        dest.onBackAction = ^(NSString *valueTitle, NSNumber *valueId) {
            weakSelf.valueId = valueId;
            weakSelf.valueListSubtitleLabel.text = valueTitle;
        };
        
    }
    
    if ([segue.identifier isEqualToString:@"showIconListLibrary"]) {
        __weak typeof(self) weakSelf = self;
        
        BehaviorIconsListVC *dest = segue.destinationViewController;
        dest.onAction = ^(BehaviorIcon *iconItem) {
            weakSelf.behaviorIconImageUrl = iconItem.iconImageUrl;
            weakSelf.behaviorIconId = iconItem.iconId;
            [weakSelf changeAddIconImageToCustomForImageState:self.imageState];
        };
    }
}

- (void) closeVC {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) showValueListOfCompany {
    [self performSegueWithIdentifier:@"showValuesList" sender:nil];
}

- (void) takePhotoAction {
    self.imageState = ImageStateFromPhone;
    TakePictureVC *pic = [[TakePictureVC alloc] initWithNibName:@"TakePictureVC" bundle:nil];
    pic.imageWidth = MAXFLOAT;
    pic.onSelectPicture = ^(UIImage *image) {
        RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeCustom];
        imageCropVC.delegate = self;
        imageCropVC.dataSource = self;
        [[[AppDelegate theApp] mainViewController] hideTabBarView:NO];
        [self.navigationController pushViewController:imageCropVC animated:YES];
    };
    [self presentViewController:pic animated:YES completion:nil];
}

- (void) chooseFromPhotoRollAction {
    self.imageState = ImageStateFromPhone;
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (void) chooseFromLibraryAction {
    self.imageState = ImageStateFromLibrary;
    
    [self performSegueWithIdentifier:@"showIconListLibrary" sender:nil];
}

#pragma mark - RSKImageCropViewController Delegate & DataSource methods

- (CGRect)imageCropViewControllerCustomMaskRect:(RSKImageCropViewController *)controller {
    if (self.screenState == BehaviorScreenState) {
        CGFloat cropWidth = SCREEN_WIDTH - 40;
        CGFloat cropHeight = SCREEN_WIDTH - 40;
        
        return CGRectMake(20, 90, cropWidth, cropHeight);
    }
    else {
        CGFloat cropWidth = SCREEN_WIDTH - 40;
        CGFloat cropHeight = (SCREEN_WIDTH - 40) / 16 * 9;
        
        return CGRectMake(20, 90, cropWidth, cropHeight);
    }
}

- (UIBezierPath *)imageCropViewControllerCustomMaskPath:(RSKImageCropViewController *)controller {
    
    if (self.screenState == BehaviorScreenState) {
        CGFloat cropWidth = SCREEN_WIDTH - 40;
        CGFloat cropHeight = SCREEN_WIDTH - 40;
        
        return [UIBezierPath bezierPathWithRect:CGRectMake(20, 90, cropWidth, cropHeight)];
    }
    else {
        CGFloat cropWidth = SCREEN_WIDTH - 40;
        CGFloat cropHeight = (SCREEN_WIDTH - 40) / 16 * 9;
        
        return [UIBezierPath bezierPathWithRect:CGRectMake(20, 90, cropWidth, cropHeight)];
    }
    
}

- (void)imageCropViewControllerDidCancelCrop:(RSKImageCropViewController *)controller {
    [[[AppDelegate theApp] mainViewController] showTabBarView:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)imageCropViewController:(RSKImageCropViewController *)controller didCropImage:(UIImage *)croppedImage usingCropRect:(CGRect)cropRect {
    
    if (self.screenState == BehaviorScreenState && (croppedImage.size.width < kBehaviorMinimumImageSize || croppedImage.size.height < kBehaviorMinimumImageSize)) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Photo", @"v1.0") text:NSLocalizedString(@"Behavior image size must be 500 x 500 px and more", @"v1.0")];
        return;
    }
    
    if (self.screenState == ValueScreenState && (croppedImage.size.width < kValueMinimumImageWidth || croppedImage.size.height < kValueMinimumImageHeight)) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Photo", @"v1.0") text:NSLocalizedString(@"Value image size must be 1224 x 689 px and more", @"v1.0")];
        return;
    }
    
    if (self.screenState == BehaviorScreenState) {
        self.behaviorImage = [UIImage imageWithImage:croppedImage scaledToWidth:kBehaviorMinimumImageSize];
        self.behaviorImagePath = [self saveAvatarImage:self.behaviorImage forState:BehaviorScreenState];
        [self changeAddIconImageToCustomForState:BehaviorScreenState];
    }
    else {
        self.valueImage = [UIImage imageWithImage:croppedImage scaledToWidth:kValueMinimumImageWidth];
        self.valueImagePath = [self saveAvatarImage:self.valueImage forState:ValueScreenState];
        [self changeAddIconImageToCustomForState:ValueScreenState];
    }
    
    [[[AppDelegate theApp] mainViewController] showTabBarView:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIImagePickerController Delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary <NSString *, id> *)info {
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeCustom];
    imageCropVC.delegate = self;
    imageCropVC.dataSource = self;
    [self.navigationController pushViewController:imageCropVC animated:YES];
    [[[AppDelegate theApp] mainViewController] hideTabBarView:YES];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //New text
    NSString * newString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    //Validation for characters, space, and "-"
    if (![self isCharacterText:string]) {
        [[AppDelegate theApp] showAlert:NSLocalizedString(@"Title", @"v1.0") text:NSLocalizedString(@"This field accepts only characters, space and hyphen", @"v1.0")];
        return NO;
    }
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textField.text.length == 0 && [newString stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.isTextFieldActive = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.isTextFieldActive = NO;
}


#pragma mark - Text View Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    //New textview string
    NSString * newString = [[textView text] stringByReplacingCharactersInRange:range withString:text];
    
    //Validation for first symbol space
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if (textView.text.length == 0 && [newString stringByTrimmingCharactersInSet:set].length == 0) {
        return NO;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.isTextViewActive = YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    self.isTextViewActive = NO;
}

#pragma mark - Keyboard Movement

#define kDeltaKeyboardMovement 0

- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(EN_NAVIGATION_BAR_HEIGHT, 0.0, kbRect.size.height + kDeltaKeyboardMovement + EN_NAVIGATION_TAB_BAR_HEIGHT, 0.0);
    self.contentScrollView.contentInset = contentInsets;
    self.contentScrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    
    if (self.isTextFieldActive) {
        if (!CGRectContainsPoint(aRect, self.titleTextField.frame.origin) ) {
            [self.contentScrollView scrollRectToVisible:self.titleTextField.frame animated:YES];
        }
    }
    else if (self.isTextViewActive) {
        CGRect textRect = self.descriptionTextView.frame;
        textRect = CGRectMake(textRect.origin.x, textRect.origin.y, textRect.size.width, textRect.size.height);
        
        [self.contentScrollView scrollRectToVisible:textRect animated:YES];
    }

}

- (void)keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = self.contentScrollView.contentInset;
    contentInsets.top = EN_NAVIGATION_BAR_HEIGHT;
    contentInsets.bottom = EN_NAVIGATION_TAB_BAR_HEIGHT;
    self.contentScrollView.contentInset = contentInsets;
    self.contentScrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Helpers Methods

//Check is text is character, space and "-"
- (BOOL) isCharacterText:(NSString *) text {
    
    BOOL isLetter = YES;
    
    NSMutableCharacterSet *symbols = [NSMutableCharacterSet letterCharacterSet];
    [symbols addCharactersInString:@" -"];
    
    if ([text rangeOfCharacterFromSet:[symbols invertedSet]].location != NSNotFound) {
        isLetter = NO;
    }
    
    return isLetter;
}

-(NSString *) saveAvatarImage:(UIImage *) image forState:(ScreenState) state {
    
    //Current date
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    NSInteger minutes = [components minute];
    NSInteger seconds = [components second];
    
    NSString *currentTime = [NSString stringWithFormat:@"%ld%ld%ld%ld%ld", (long)day, (long)month, (long)year, (long)minutes, (long)seconds];
    
    //Image name for save
    NSString *fileName = [[NSString alloc] init];
    
    if (state == ValueScreenState) {
        fileName = [NSString stringWithFormat:@"ValueImage_%@", currentTime];
    }
    else {
      fileName = [NSString stringWithFormat:@"Behavior_%@", currentTime];
    }
    
    //Image path
    NSString *imageFilePath = [[AKFileManager temporaryDirectory] stringByAppendingPathComponent:fileName];
    
    //Image Data
    NSData *dataImage = [UIImage dataWithImage:image];
    
    //Save file to image
    [dataImage writeToFile:imageFilePath atomically:YES];
    
    return imageFilePath;
}

- (void) cleanImageDataAndPath {
    self.valueImage = nil;
    self.valueImagePath = nil;
    
    self.behaviorImage = nil;
    self.behaviorImagePath = nil;
}

- (NSDictionary *) serializeDataForSend {
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    if (self.screenState == BehaviorScreenState) {
        [result setValue:self.titleTextField.text forKey:@"title"];
        [result setValue:self.descriptionTextView.text forKey:@"description"];
        [result setValue:self.valueId forKey:@"value_id"];
    }
    else {
        [result setValue:self.titleTextField.text forKey:@"title"];
        [result setValue:self.descriptionTextView.text forKey:@"description"];
    }
    
    return [result copy];
}

- (void) showAlertWithMessage:(NSString *) message {
    
    if (message.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:nil text:message];
}

-(void) showSentAlertForState:(ScreenState)state {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    if (state == BehaviorScreenState) {
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"«Behavior» created", @"v1.0")];
    }
    else {
        [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"«Value» has been created", @"v1.0")];
    }
}

- (void) trackScreen {
    
    if (self.screenState == BehaviorScreenState) {
        [AnalyticsManager trackScreen:ENAnalyticScreenBehaviorCreate];
        [AnalyticsManager trackScreen:ENAnalyticScreenBehaviorCreate withScreenClass:NSStringFromClass([self class])];
    }
    else {
        [AnalyticsManager trackScreen:ENAnalyticScreenValueCreate];
        [AnalyticsManager trackScreen:ENAnalyticScreenValueCreate withScreenClass:NSStringFromClass([self class])];
    }
    
}

@end
