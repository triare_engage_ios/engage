//
//  QuestionAnswerActionCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/21/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "QuestionAnswerActionCell.h"

@implementation QuestionAnswerActionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.leftButton setTitle:NSLocalizedString(@"SKIP", @"v1.0") forState:UIControlStateNormal];
    [self.rightButton setTitle:NSLocalizedString(@"SUBMIT", @"v1.0") forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)leftButtonAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onAction) {
        weakSelf.onAction (0);
    }
}

- (IBAction)rightButtonAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onAction) {
        weakSelf.onAction (1);
    }
}


@end
