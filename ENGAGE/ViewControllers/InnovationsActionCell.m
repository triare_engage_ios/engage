//
//  InnovationsActionCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/25/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "InnovationsActionCell.h"

#define kDefaultCellHeight 80 //
#define kDefaultCellIdentifier @"innovationsActionCell"

@interface InnovationsActionCell ()
@property (weak, nonatomic) IBOutlet ENButton *leftButton;
@property (weak, nonatomic) IBOutlet ENButton *rightButton;

@end

@implementation InnovationsActionCell

#pragma mark - Public Methods

+(CGFloat) cellHeight {
    return kDefaultCellHeight;
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

-(void) applyButtonsLeftTitle:(NSString *) leftTitle andRightTitle:(NSString *) rightTitle {
    [self.leftButton setTitle:leftTitle forState:UIControlStateNormal];
    [self.rightButton setTitle:rightTitle forState:UIControlStateNormal];
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.onAction = nil;
}

#pragma mark - Action

- (IBAction)leftButtonAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onAction) {
        weakSelf.onAction (0, weakSelf);
    }
}

- (IBAction)rightButtonAction:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onAction) {
        weakSelf.onAction (1, weakSelf);
    }
}


@end
