//
//  ProfileSettingsJobTitleCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileSettingsJobTitleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
