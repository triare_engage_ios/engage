//
//  BehaviorListCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENCircleImageView.h"

@interface BehaviorListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leadPositionLabel;
@property (weak, nonatomic) IBOutlet ENCircleImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobLabel;

@property (nonatomic, strong) NSNumber *userBorderWidth;
@property (nonatomic, strong) UIColor *userBorderColor;

@end
