//
//  BehaviorTitleCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorTitleCell.h"

@implementation BehaviorTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.valueTitle.textColor = [UIColor appTextColor];
    self.valueTitle.font = [UIFont appFontBold:25];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
