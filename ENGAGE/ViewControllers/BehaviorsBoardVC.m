//
//  BehaviorsBoardVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "BehaviorsBoardVC.h"
#import "AKLineMenuView.h"

#import "ValueHeaderCell.h"
#import "ValueCell.h"
#import "BehaviorTitleCell.h"
#import "ValueDescriptionCell.h"

#import "BehaviorDetailVC.h"
#import "BehaviorsManageVC.h"

#import "ENPlaceholderView.h"

#import "FeaturesAccessManager.h"

typedef enum : NSUInteger {
    ListStateValues,
    ListStateBehaviors
} ListState;

@interface BehaviorsBoardVC () <AKLineMenuViewDelegate, UITableViewDelegate, UITableViewDataSource, NavigationBarSearchDelegate>

@property (weak, nonatomic) IBOutlet ContentTableView *contentTableView;
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet AKLineMenuView *lineMenu;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) NSMutableArray* selectedSection;

@property ListState listState;

@property (nonatomic, strong) NSString *searchText;

@property (nonatomic, strong) ENPlaceholderView *tablePlaceholder;

@end

@implementation BehaviorsBoardVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    [self setupTableView];
    [self setupLineMenu];
    
    self.listState = ListStateValues;
    
    self.selectedSection = [NSMutableArray new];
    
    [self loadData];
    
    [self addDownRefresh];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf createTablePlaceholder];
    });
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self trackScreenIsFirebase:NO];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self trackScreenIsFirebase:YES];
}

#pragma mark - Setup

- (void) setupNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:NSLocalizedString(@"VALUES & BEHAVIORS", @"v1.0")];
    
    self.navigationBar.searchDelegate = self;
    [self.navigationBar showRightButton];
    
    [self.navigationBar changeLeftButtonIcon:[UIImage imageNamed:@"icon-plus-without-circle"]];
    
    if ([[DataManager user] isAdmin] || [[DataManager user] isManager]) {
        
        if ([FeaturesAccessManager isAccessFeature:AppFeatureTypeCreateBehaviorOrValue forScreen:AppScreenTypeValueAndBehavior withUser:[DataManager user]]) {
            self.navigationBar.onLeftAction = ^{
                [weakSelf showBehaviorsManageVC];
            };
            
        }
        else {
            self.navigationBar.onLeftAction = ^{
                [weakSelf showAlertWithTitle:nil andText:NSLocalizedString(@"This functionality is not available in your plan", @"v1.0")];
            };
        }
        
        [self.navigationBar showLeftButton];
        
    }
}

- (void) setupTableView {
    self.contentTableView.delegate = self;
    self.contentTableView.dataSource = self;
}

- (void) setupLineMenu {
    self.lineMenu.delegate = self;
    
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurEffectView.frame = self.lineMenu.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.lineMenu addSubview:blurEffectView];
    [self.lineMenu sendSubviewToBack:blurEffectView];
    
    [self.lineMenu addMenuItems:@[NSLocalizedString(@"VALUES",nil),NSLocalizedString(@"BEHAVIORS",nil)]];
    
    [self addTableTopInset:self.lineMenu.frame.size.height];
}

- (void) createTablePlaceholder {
    
    self.tablePlaceholder = [ENPlaceholderView createPlaceholderWithText:NSLocalizedString(@"No result found", @"v1.0") andFont:[UIFont appFont:17]];
    
    self.tablePlaceholder.hidden = YES;
    [self.contentTableView addSubview:self.tablePlaceholder];
    
}

#pragma mark - Data

- (void) loadData {
    __weak typeof(self) weakSelf = self;
    [self showLoadingViewBellowView:self.navigationBar];
    
    [[RequestManager sharedManager] getValuesListSuccess:^(NSArray *list) {
        [DataManager saveValuesList:list];
        [weakSelf reloadData];
        [weakSelf hideLoadingView];
    } failure:^(NSString *message) {
        [weakSelf.refreshControl endRefreshing];
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithTitle:nil andText:message];
    }];
    
}

-(void) reloadData {
    self.fetchedResultsController = nil;
    [self.refreshControl endRefreshing];
    [self.contentTableView reloadData];
}

#pragma mark - Action

- (void) showBehaviorDetailsById:(NSNumber *) behaviorId {
     __weak typeof(self) weakSelf = self;
    
    Behavior *item = [Behavior MR_findFirstByAttribute:@"behaviorId" withValue:behaviorId];
    
    BehaviorDetailVC * vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorDetailVC"];
    vc.item = item;
    vc.onHideAction = ^{
        [weakSelf loadData];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) showBehaviorsManageVC {
    
    __weak typeof(self) weakSelf = self;
    
    BehaviorsManageVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorsManageVC"];
    vc.onSendAction = ^{
        [weakSelf loadData];
    };
    
    if (self.listState == ListStateValues) {
        vc.screenState = ValueScreenState;
    }
    else {
        vc.screenState = BehaviorScreenState;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
    
    //[self performSegueWithIdentifier:@"showAddBehaviorOrValue" sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    __weak typeof(self) weakSelf = self;
//    
//    if ([segue.identifier isEqualToString:@"showAddBehaviorOrValue"]) {
//        BehaviorsManageVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"BehaviorsManageVC"];
//        vc.onSendAction = ^{
//            [weakSelf loadData];
//        };
//        vc.test = [NSNumber numberWithInt:1];
//        
//    }
}


#pragma mark - fetched Results Controller

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    if (self.listState == ListStateValues) {
        _fetchedResultsController = [self valuesFetchedResultsController];
    } else {
        _fetchedResultsController = [self behaviorsFetchedResultsController];
    }
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

- (NSFetchedResultsController *) valuesFetchedResultsController {
    
    NSMutableArray *predicates = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [Value MR_requestAllSortedBy:@"title" ascending:YES inContext:[DataManager context]];
    
    if (self.searchText.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@ ", self.searchText];
        [predicates addObject:searchPredicate];
    }
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [Value MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    return _fetchedResultsController;
}

- (NSFetchedResultsController *) behaviorsFetchedResultsController {
    
    NSMutableArray *predicates = [NSMutableArray array];
    
    NSFetchRequest *fetchRequest = [Value MR_requestAllSortedBy:@"title" ascending:YES inContext:[DataManager context]];
    
    if (self.searchText.length) {
        NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"ANY behaviors.title CONTAINS[cd] %@ ", self.searchText];
        [predicates addObject:searchPredicate];
    }
    
    if (predicates.count) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [Value MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    return _fetchedResultsController;
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if (number == 0 && self.navigationBar.barView.searchBar.text.length > 0) {
        self.tablePlaceholder.hidden = NO;
    }
    else {
        self.tablePlaceholder.hidden = YES;
    }
    
    return number;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Values
    if (self.listState == ListStateValues) {
        //If section is open
        if ([self isSectionSelected:section]) {
            Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:section];
            
            //If description exist add row in open section
            NSInteger valueDescriptionRow = 0;
            if (item.valueDescription.length > 0) {
                valueDescriptionRow = 1;
            }
            
            //Number of behaviors row
            NSInteger numberOfRows = (int) ceilf((float)item.behaviorsSet.array.count / (float) 3);
            return numberOfRows + 1 + valueDescriptionRow; //Number of behaviors row + value row + value description row
        }
        else {
            return 1;
        }
    }
    //Behaviors
    else {
        Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:section];
        NSInteger numberOfRows = (int) ceilf((float)item.behaviorsSet.array.count / (float) 3);
        return numberOfRows + 1;
    }
}

- (BOOL) isSectionSelected:(NSInteger) section {
    BOOL isSelected = NO;

    for (int i = 0; i < self.selectedSection.count; i++ ) {
        NSNumber *item = self.selectedSection[i];
        if ([item integerValue] == section) {
            isSelected = YES;
        }
    }
    
    return isSelected;
}

- (void) deleteSectionFromSelected:(NSInteger) section {
    for (int i = 0; i < self.selectedSection.count; i++ ) {
        NSNumber *item = self.selectedSection[i];
        if ([item integerValue] == section) {
            [self.selectedSection removeObjectAtIndex:i];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.listState == ListStateValues) {
        
        Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.section];
        
        //Value header cell
        if (indexPath.row == 0) {
            return SCREEN_WIDTH / 16 * 9;
        }
        //Value Description cell
        else if (indexPath.row == 1 && [self isSectionSelected:indexPath.section] && item.valueDescription.length > 0) {
            return [ValueDescriptionCell cellHeight:item.valueDescription width:tableView.frame.size.width];
            //return 80;
        }
        //Value cell
        else {
            return SCREEN_WIDTH / 16 * 9 / 1.5;
        }
    } else {
        if (indexPath.row == 0) {
            return 40;
        }
        else {
            return SCREEN_WIDTH / 16 * 9 / 1.5;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    //Values
    if (self.listState == ListStateValues) {
        
        Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.section];
        
        //Header cell
        if (indexPath.row == 0) {
            
            ValueHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"valueHeaderCell"];
            
            cell.titleLabel.text = [item.title uppercaseString];
            //Not used, because we show description after tap in description cell.
//            if (item.valueDescription.length > 0) {
//                cell.descriptionLabel.text = item.valueDescription;
//            }
//            else {
//                cell.descriptionLabel.hidden = YES;
//            }
            cell.descriptionLabel.hidden = YES;
            
            [cell.backgroundImageView sd_setImageWithURL:[AppTemplate bigIconURL:item.iconUrl] placeholderImage:[Value placeholder]];
            
            return cell;
        }
        //Description cell
        else if (indexPath.row == 1 && item.valueDescription.length > 0 && [self isSectionSelected:indexPath.section]) {
            ValueDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"valueDescriptionCell"];
            
            cell.descriptionLabel.text = item.valueDescription;
            
            return cell;
        }
        //Value cell (with Behavior)
        else {
            
            ValueCell *cell = [tableView dequeueReusableCellWithIdentifier:@"valueCell"];
            
            //Used if we have description
            NSInteger numberOfRowBeforValueCell = 1;
            
            if (item.valueDescription.length > 0) {
                numberOfRowBeforValueCell = 2;
            }
            
            NSInteger itemNumber = (indexPath.row - numberOfRowBeforValueCell) * 3;
            
            NSMutableArray *arrayOfBehaviors = [[NSMutableArray alloc] init];
            
            if (item.behaviorsSet.array.count > itemNumber) {
                
                Behavior *itemBehavior = item.behaviorsSet.array[itemNumber];
                [arrayOfBehaviors addObject:itemBehavior];
            }
            
            if (item.behaviorsSet.array.count > itemNumber + 1) {
                Behavior *itemBehavior = item.behaviorsSet.array[itemNumber + 1];
                [arrayOfBehaviors addObject:itemBehavior];
            }
            
            if (item.behaviorsSet.array.count > itemNumber + 2) {
                Behavior *itemBehavior = item.behaviorsSet.array[itemNumber + 2];
                [arrayOfBehaviors addObject:itemBehavior];
            }
            
            [cell addBehaviors:arrayOfBehaviors];
            
            cell.onAction = ^(NSNumber *behaviorId) {
                [weakSelf showBehaviorDetailsById:behaviorId];
            };
            
            return cell;
        }
    }
    
    //Behaviours
    else {
        
        Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:indexPath.section];
        
        if (indexPath.row == 0) {
            BehaviorTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"behaviorTitleCell"];
            
            cell.valueTitle.text = item.title;
            
            return cell;
        }
        else {
            ValueCell *cell = [tableView dequeueReusableCellWithIdentifier:@"valueCell"];
            
            NSInteger itemNumber = (indexPath.row - 1) * 3;
            
            NSMutableArray *arrayOfBehaviors = [[NSMutableArray alloc] init];
            
            if (item.behaviorsSet.array.count > itemNumber) {
                
                Behavior *itemBehavior = item.behaviorsSet.array[itemNumber];
                [arrayOfBehaviors addObject:itemBehavior];
            }
            
            if (item.behaviorsSet.array.count > itemNumber + 1) {
                Behavior *itemBehavior = item.behaviorsSet.array[itemNumber + 1];
                [arrayOfBehaviors addObject:itemBehavior];
            }
            
            if (item.behaviorsSet.array.count > itemNumber + 2) {
                Behavior *itemBehavior = item.behaviorsSet.array[itemNumber + 2];
                [arrayOfBehaviors addObject:itemBehavior];
            }
            
            [cell addBehaviors:arrayOfBehaviors];
            
            cell.onAction = ^(NSNumber *behaviorId) {
                [weakSelf showBehaviorDetailsById:behaviorId];
            };
            
            return cell;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.listState == ListStateValues) {
        //hide section
        if ([self isSectionSelected:indexPath.section] && indexPath.row == 0) {
            [self deleteSectionFromSelected:indexPath.section];
            NSArray *rowsForDelete = [self arrayOfRowsInSection:indexPath.section];
            [tableView deleteRowsAtIndexPaths:rowsForDelete withRowAnimation:UITableViewRowAnimationMiddle];
        }
        //show section
        else if (![self isSectionSelected:indexPath.section] && indexPath.row == 0) {
            [self.selectedSection addObject:[NSNumber numberWithInteger:indexPath.section]];
            NSArray *rowsForAdd = [self arrayOfRowsInSection:indexPath.section];
            [tableView insertRowsAtIndexPaths:rowsForAdd withRowAnimation:UITableViewRowAnimationMiddle];
            [tableView scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    } else {
        //
    }
}

- (NSArray *) arrayOfRowsInSection:(NSInteger)section {
    
    Value *item = [self.fetchedResultsController.fetchedObjects objectAtIndex:section];
    
    NSInteger numberOfRows = (int) ceilf((float)item.behaviorsSet.array.count / (float) 3);
    
    if (item.valueDescription.length > 0) {
        numberOfRows += 1;
    }
    
    NSMutableArray *rowsArray = [NSMutableArray new];
    
    for (int i = 0; i < numberOfRows; i++ ) {
        NSIndexPath *addedIndexPath = [NSIndexPath indexPathForItem:i+1 inSection:section];
        [rowsArray addObject:addedIndexPath];
    }
    
    return [rowsArray copy];
}

#pragma mark - AKLineMenuView

- (void)lineMenu:(AKLineMenuView *) menu didSelectItem:(NSInteger)itemIndex {
    
    self.listState = itemIndex;
    [self trackScreen];
    
    [self reloadData];
    
    [self.contentTableView scrollTableToTop:YES];
    
    [self checkSearchPlaceholder];
}

#pragma mark NavigationBarSearchDelegate

-(void) checkSearchPlaceholder {
    if (self.listState == ListStateValues) {
        self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by Values", @"v1.0");
    } else {
        self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by behavior title", @"v1.0");
    }
}

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    self.searchText = nil;
    [self reloadData];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [self checkSearchPlaceholder];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    self.searchText = searchText;
    
    [self reloadData];
}


#pragma mark - Helpers Methods

-(void) addTableTopInset:(CGFloat) topInset {
    UIEdgeInsets insets = self.contentTableView.contentInset;
    insets.top += topInset;
    self.contentTableView.contentInset = insets;
}

#pragma mark - Pull down to refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.contentTableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadData];
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}

- (void) trackScreenIsFirebase:(BOOL) firebase {
    
    if (firebase) {
        switch (self.listState) {
            case ListStateValues:
                [AnalyticsManager trackScreen:ENAnalyticScreenMainValuesList withScreenClass:NSStringFromClass([self class])];
                break;
                
            case ListStateBehaviors:
                [AnalyticsManager trackScreen:ENAnalyticScreenMainBehaviorsList withScreenClass:NSStringFromClass([self class])];
                break;
        }
    }
    else {
        switch (self.listState) {
            case ListStateValues:
                [AnalyticsManager trackScreen:ENAnalyticScreenMainValuesList];
                break;
                
            case ListStateBehaviors:
                [AnalyticsManager trackScreen:ENAnalyticScreenMainBehaviorsList];
                break;
        }
    }
    
}

- (void) trackScreen {
    [self trackScreenIsFirebase:YES];
    [self trackScreenIsFirebase:NO];
}

@end
