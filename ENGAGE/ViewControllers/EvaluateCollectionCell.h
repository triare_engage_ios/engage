//
//  EvaluateCollectionCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"
#import "ENRoundedView.h"

@interface EvaluateCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet ENRoundedImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;

@property (weak, nonatomic) IBOutlet ENRoundedView *evaluateView;
@property (weak, nonatomic) IBOutlet UILabel *evaluateLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelEvaluateButton;

@property (nonatomic, copy) void (^onCancelEvaluate)();

+ (CGSize) sizeForCollectionWidth:(CGFloat) width;

-(void) evaluateState:(NSInteger) state;

@end
