//
//  SuggestionsVC.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 23.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "SuggestionsVC.h"
#import "CommentCell.h"
#import "LoadingCell.h"

@interface SuggestionsVC ()

@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (nonatomic, strong) PaginationInfo *pagination;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSMutableArray *fullCellsIds;

@end

@implementation SuggestionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    self.navigationBar.onLeftAction = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    [self.navigationBar setTitle:NSLocalizedString(@"VIRTUAL SUGGESTIONS", @"v1.0")];
    [self.navigationBar showLeftButton];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CommentCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.pagination = [[PaginationInfo alloc] init];
    self.pagination.currentPage = 0;
    self.pagination.totalPages = 1;
    [self loadDataForPage:1];
    
    self.fullCellsIds = [NSMutableArray array];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenSuggestionList];
    [AnalyticsManager trackScreen:ENAnalyticScreenSuggestionList withScreenClass:NSStringFromClass([self class])];
}

-(void) loadNextData {
    
    if (self.pagination.isLoading) return;
    
    if (self.pagination.totalPages > self.pagination.currentPage) {
        [self loadDataForPage:self.pagination.currentPage + 1];
    }
}

-(void) loadDataForPage:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    weakSelf.pagination.isLoading = YES;
    [[RequestManager sharedManager] getSuggestions:page onCompleate:^(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage) {
        
        if (success) {
            weakSelf.pagination.currentPage = pagination.currentPage;
            weakSelf.pagination.totalPages = pagination.totalPages;
            
            if (weakSelf.pagination.currentPage == 1 || weakSelf.pagination.currentPage == 0) {
                [DataManager saveSuggestionsList:list];
            } else {
                [DataManager appendSuggestionsList:list];
            }
            
            [weakSelf reloadData];
        }
        
        weakSelf.pagination.isLoading = NO;
        
        //[weakSelf reloadData];
        
        if (!success) {
            [weakSelf showAlertWithTitle:nil andText:errorMessage];
        }
    }];
}

- (void)reloadData {
    
    self.fetchedResultsController = nil;
    
    [self.tableView reloadData];
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [Suggestion MR_requestAllSortedBy:@"createDate" ascending:NO inContext:[DataManager context]];
    NSFetchedResultsController *fetchController = [Suggestion MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if (self.pagination.currentPage < self.pagination.totalPages) {
        return number + 1;
    }
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        Suggestion *item = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.contentLabel.text = item.body;
        [cell addSuggestion:item fullInfo:[self.fullCellsIds containsObject:item.suggestionId]];
        
        if (item.isReadValue) {
            cell.backgroundColor = [UIColor clearColor];
        } else {
            cell.backgroundColor = [UIColor colorWithWhite:1 alpha:.1];
        }
        
        return cell;
    } else {
        return [LoadingCell cell];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        
        CGFloat collapsedHeight = 130;
        
        Suggestion *item = self.fetchedResultsController.fetchedObjects[indexPath.row];
        CGFloat height = [CommentCell cellHeight:item.body width:tableView.frame.size.width];
        
        
        if (height < collapsedHeight) {
            [self.fullCellsIds addObject:item.suggestionId];
        } else {
            if (![self.fullCellsIds containsObject:item.suggestionId]) {
                return collapsedHeight;
            }
        }
        
        return height;
    }
    
    return 80; //loading cell height
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        Suggestion *item = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
        if (![self.fullCellsIds containsObject:item.suggestionId]) {
            [self.fullCellsIds addObject:item.suggestionId];
            
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isLoadingCell]) {
        [self loadNextData];
    }
}

#pragma mark - Helpers Methods

- (void) showAlertWithTitle:(NSString *) title andText:(NSString*)messageText {
    
    if (messageText.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:title text:messageText];
}


@end
