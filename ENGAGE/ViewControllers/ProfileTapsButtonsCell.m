//
//  ProfileTapsButtonsCell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 17.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileTapsButtonsCell.h"

@implementation ProfileTapsButtonsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.recievedButton setTitle:NSLocalizedString(@"TAPS RECEIVED", @"v1.0") forState:UIControlStateNormal];
    [self.givenButton setTitle:NSLocalizedString(@"TAPS GIVEN", @"v1.0") forState:UIControlStateNormal];
//    [self.recievedButton setTitle:NSLocalizedString(@"TAPS GIVEN", nil) forState:UIControlStateNormal];
//    [self.givenButton setTitle:NSLocalizedString(@"TAPS RECEIVED", nil) forState:UIControlStateNormal];
    
    [self changeButtonState:self.recievedButton isSelected:YES];
    [self changeButtonState:self.givenButton isSelected:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) changeButtonState:(UIButton *) button isSelected:(BOOL) selected {
    
    button.layer.cornerRadius = 7;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor whiteColor].CGColor;
    
    if (selected) {
        button.backgroundColor = [UIColor appOrangeColor];
        button.layer.borderColor = [UIColor appOrangeColor].CGColor;
    } else {
        button.backgroundColor = [UIColor clearColor];
    }
}
- (IBAction)givenAction:(id)sender {
    [self changeButtonState:self.recievedButton isSelected:NO];
    [self changeButtonState:self.givenButton isSelected:YES];
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onChangeSort) {
        weakSelf.onChangeSort(NO);
    }
}
- (IBAction)receivedAction:(id)sender {
    [self changeButtonState:self.recievedButton isSelected:YES];
    [self changeButtonState:self.givenButton isSelected:NO];
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onChangeSort) {
        weakSelf.onChangeSort(YES);
    }
}

-(void) isReceived:(BOOL) isReseived {
    if (isReseived) {
        [self changeButtonState:self.recievedButton isSelected:YES];
        [self changeButtonState:self.givenButton isSelected:NO];
    } else {
        [self changeButtonState:self.recievedButton isSelected:NO];
        [self changeButtonState:self.givenButton isSelected:YES];
    }
}

@end
