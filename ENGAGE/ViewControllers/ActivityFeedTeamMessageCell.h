//
//  ActivityFeedTeamMessageCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/13/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENCircleImageView.h"
#import "ENCircleView.h"
#import "ENRoundedView.h"

@interface ActivityFeedTeamMessageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet ENRoundedView *backgroundRoundedView;
@property (weak, nonatomic) IBOutlet ENCircleImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *senderIconButton;

@property (nonatomic, copy) void (^onShowSender)(ActivityFeedTeamMessageCell *cell);

- (void) addFeed:(ActivityFeed *) feed;
+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth;


@end
