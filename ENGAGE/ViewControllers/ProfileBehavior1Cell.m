//
//  ProfileBehavior1Cell.m
//  ENGAGE
//
//  Created by Alex Kravchenko on 25.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "ProfileBehavior1Cell.h"

@implementation ProfileBehavior1Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[UIView makeAppRoundedView:self.cellImageView];
    
    self.cellTitleLabel.font = [UIFont appFont:17];
    self.cellTitleLabel.textColor = [UIColor appTextColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setBehavior:(UserBehavior *) behavior {
    self.cellTitleLabel.text = behavior.title;
    [self.cellImageView sd_setImageWithURL:[AppTemplate bigIconURL:behavior.iconUrl] placeholderImage:[Behavior placeholder]];
    //[self.cellImageView sd_setImageWithURL:[AppTemplate originalIconURL:behavior.iconUrl] placeholderImage:[Behavior placeholder]];
    
    self.badgeLabel.text = [NSString stringWithFormat:@"%@", behavior.count];
}

- (IBAction)behaviorIconButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowBehavior) {
        weakSelf.onShowBehavior();
    }
}


@end
