//
//  PredefinedJobTitleCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 9/7/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PredefinedJobTitleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *centerLabel;

@end
