//
//  ProfileSettingsUserImageCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 9/5/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ProfileSettingsUserImageCell.h"
#import "ENCircleView.h"

@interface ProfileSettingsUserImageCell ()
@property (weak, nonatomic) IBOutlet ENRoundedView *backgroundUserImage;
@property (weak, nonatomic) IBOutlet UILabel *editLabel;


@end

@implementation ProfileSettingsUserImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.editLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.editLabel.text = NSLocalizedString(@"edit", @"v1.0");
    self.editLabel.textColor = [UIColor appTextColor];
    self.editLabel.font = [UIFont appFont:14];
    
    //[self.backgroundUserImage makeCircle];
    
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.userImage = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
