//
//  BadgeListVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/15/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface BadgeListVC : ContentViewController
@property (nonatomic, strong) NSNumber *userId;

+ (NSString *) storyboardIdentifier;

@end
