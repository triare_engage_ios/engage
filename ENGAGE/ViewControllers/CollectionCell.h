//
//  CollectionCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 04.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENCircleImageView.h"

typedef enum : NSUInteger {
    CollectionCellDefault,
    CollectionCellBadge,
} CollectionCellType;

@interface CollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet ENCircleImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (nonatomic) CollectionCellType cellType;

+ (CGSize) sizeForCollectionWidth:(CGFloat) width;

- (void) fillCustomData; //Used for fiiling default data, for testing
- (void) addChallenge:(Challenge *) challenge;

@end
