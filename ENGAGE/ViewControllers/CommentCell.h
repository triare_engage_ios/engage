//
//  CommentCell.h
//  ENGAGE
//
//  Created by Alex Kravchenko on 11.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ENRoundedImageView.h"
#import "ENCircleImageView.h"
#import <SWTableViewCell/SWTableViewCell.h>

@interface CommentCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet ENCircleImageView *senderAvatar;
@property (weak, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *showMoreLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (nonatomic, copy) void (^onShowSender)(CommentCell *cell);

-(void) addComment:(Comment *) comment fullInfo:(BOOL) fullInfo;

-(void) addSuggestion:(Suggestion *) suggestion fullInfo:(BOOL) fullInfo;

+(CGFloat) cellHeight:(NSString *) contentText width:(CGFloat) tableWidth;

@end
