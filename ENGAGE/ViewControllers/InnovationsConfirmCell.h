//
//  InnovationsConfirmCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/28/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    InnovationsConfirmCellStyleStart,
    InnovationsConfirmCellStyleStop,
    InnovationsConfirmCellStyleNeed
} InnovationsConfirmCellStyle;

@interface InnovationsConfirmCell : UITableViewCell
@property (nonatomic, readonly) InnovationsConfirmCellStyle cellStyle;

+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;
+(CGFloat) cellHeightForText:(NSString *) innovationText withTableWidth:(CGFloat) tableWidth;

-(void) applyCellStyle:(InnovationsConfirmCellStyle) style;
-(void) setInnovationText:(NSString *) text;

@end
