//
//  RemotePushManager.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/22/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "RemotePushManager.h"

@interface RemotePushItem ()
@end

@implementation RemotePushItem

- (instancetype) initWithInfo:(NSDictionary *) info andAppState:(UIApplicationState) appState {
    if (self = [super init]) {
        [self updateWithInfo:info forAppState:appState];
    }
    
    return self;
}

- (void) updateWithInfo:(NSDictionary *) info forAppState:(UIApplicationState) appState {
    
    NSString *event = [info valueForKey:@"event"];
    
    //Parse type
    if ([event isEqualToString:@"team_message"]) {
        _type = RemotePushItemTypeTeamMessage;
    }
    else if ([event isEqualToString:@"question"]) {
        _type = RemotePushItemTypeQuestion;
    }
    else if ([event isEqualToString:@"tap"]) {
        _type = RemotePushItemTypeTap;
    }
    else if ([event isEqualToString:@"top_points"]) {
        _type = RemotePushItemTypeTopPointWinner;
    }
    else if ([event isEqualToString:@"challenge_started"]) {
        _type = RemotePushItemTypeChallengeStarted;
    }
    else if ([event isEqualToString:@"challenge_ended"]) {
        _type = RemotePushItemTypeChallengeEnded;
    }
    else if ([event isEqualToString:@"challenge_winner"]) {
        _type = RemotePushItemTypeChallengeWinner;
    }
    
    _state = appState;
    
    //Prepare Action list
    _actionList = [self actionListForItemType:_type andState:appState];
}

-(NSArray *) actionListForItemType:(RemotePushItemType) itemType andState:(UIApplicationState) appState {
    
    //Team message, Question, Tap, Point Winner, Challenges
    if (itemType == RemotePushItemTypeTeamMessage || itemType == RemotePushItemTypeQuestion || itemType == RemotePushItemTypeTap || itemType == RemotePushItemTypeTopPointWinner || itemType == RemotePushItemTypeChallengeStarted || itemType == RemotePushItemTypeChallengeEnded) {
        switch (appState) {
            case UIApplicationStateBackground:
                return @[@(RemotePushItemActionUpdateActivityFeed)];
                break;
            
            case UIApplicationStateActive:
                return @[@(RemotePushItemActionNeedRefreshActivityFeed), @(RemotePushItemActionCreateDotOnActivityFeed)];
                break;
                
            case UIApplicationStateInactive:
                return @[@(RemotePushItemActionShowActivityFeed)];
                break;
        }
    }
    //Challenge winner
    else if (itemType == RemotePushItemTypeChallengeWinner) {
        switch (appState) {
            case UIApplicationStateBackground:
                return @[@(RemotePushItemActionUpdateActivityFeedAndCreateDotOnProfile)];
                break;
                
            case UIApplicationStateActive:
                return @[@(RemotePushItemActionCreateDotOnProfile),@(RemotePushItemActionNeedRefreshActivityFeed)];
            
            case UIApplicationStateInactive:
                return @[@(RemotePushItemActionNeedRefreshActivityFeed), @(RemotePushItemActionShowChallengeBadgeList)];
                
            default:
                break;
        }
    }

    //Default item
    return @[@(RemotePushItemActionUpdateActivityFeed)];
    
}

- (void) launchAction {
    for (NSNumber *actionNumber in self.actionList) {
        RemotePushItemAction action = actionNumber.integerValue;
        
        switch (action) {
            case RemotePushItemActionUpdateActivityFeed:
                [[AppDelegate theApp].mainViewController updateActivityFeed];
                break;
                
            case RemotePushItemActionCreateDotOnActivityFeed:
                [[AppDelegate theApp].mainViewController newEventForActivityFeed];
                break;
                
            case RemotePushItemActionShowActivityFeed:
                [[AppDelegate theApp].mainViewController showActivityFeed];
                break;
                
            case RemotePushItemActionNeedRefreshActivityFeed:
                [AppDelegate theApp].mainViewController.isAddedNewTap = YES;
                break;
                
            case RemotePushItemActionUpdateActivityFeedAndCreateDotOnProfile:
                [[AppDelegate theApp].mainViewController updateActivityFeedAndSetDotOnProfile:YES];
                break;
                
            case RemotePushItemActionCreateDotOnProfile:
                [[AppDelegate theApp].mainViewController newEventForProfile];
                break;
                
            case RemotePushItemActionShowChallengeBadgeList:
                [[AppDelegate theApp].mainViewController challengeWinnerBadgeList];
                break;
        }
    }
}

@end







@interface RemotePushManager ()
@end


@implementation RemotePushManager

+(void) parsePushAndActionWithInfo:(NSDictionary *) info forAppState:(UIApplicationState) appState {
    
    RemotePushItem *item = [[RemotePushItem alloc] initWithInfo:info andAppState:appState];
    [item launchAction];
}

@end
