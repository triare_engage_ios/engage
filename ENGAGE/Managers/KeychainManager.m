//
//  KeychainManager.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/30/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "KeychainManager.h"

#define kApiTokenKey @"api_token"
#define kApiUserEmail @"api_email"

@interface KeychainManager ()
@end

@implementation KeychainManager

#pragma mark - Public Methods

+ (UICKeyChainStore *) appGroupKeychain
{
    return [UICKeyChainStore keyChainStoreWithService:EN_KEYCHAIN_SERVICE_NAME accessGroup:EN_KEYCHAIN_APPGROUP_ID];
}

+ (void) saveApiToken:(NSString *) token {
    [self saveStringValue:token forKey:kApiTokenKey toKeychain:[self appGroupKeychain]];
}

+ (NSString *) apiToken {
    return [self stringForKey:kApiTokenKey inKeychain:[self appGroupKeychain]];
}

+ (void) saveApiEmail:(NSString *) email {
    [self saveStringValue:email forKey:kApiUserEmail toKeychain:[self appGroupKeychain]];
}

+ (NSString *) userEmail {
    return [self stringForKey:kApiUserEmail inKeychain:[self appGroupKeychain]];
}

#pragma mark - Private Methods

+ (void) saveStringValue:(NSString *) value forKey:(NSString *) key toKeychain:(UICKeyChainStore *) keychain {
    
    if (key == nil) {
        return;
    }
    
    if (keychain == nil) {
        return;
    }
    
    keychain[key] = value;
}

+ (NSString *) stringForKey:(NSString *) key inKeychain:(UICKeyChainStore *) keychain {
    
    if (key == nil) {
        return nil;
    }
    
    if (keychain == nil) {
        return nil;
    }
    
    return [keychain stringForKey:key];
}

@end
