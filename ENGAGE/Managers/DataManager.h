//
//  DataManager.h
//  ENGAGE..
//
//  Created by Alex Kravchenko on 03.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MagicalRecord/NSManagedObject+MagicalRequests.h>
#import <MagicalRecord/NSManagedObject+MagicalFinders.h>

#import "User.h"
#import "Company.h"
#import "TeamUser.h"
#import "Behavior.h"
#import "ActivityFeed.h"
#import "Comment.h"
#import "LeaderUser.h"
#import "LeaderBehavior.h"
#import "Suggestion.h"
#import "UserBehavior.h"
#import "EvaluateBehavior.h"
#import "BehaviorTeamUserEvaluations.h"
#import "BehaviorUser.h"
#import "TeamMessage.h"
#import "Value.h"
#import "Question.h"
#import "QuestionAnswer.h"
#import "QuestionCustomAnswer.h"
#import "ActivityFeedQuestion.h"
#import "ActivityFeedAnswerOption.h"
#import "CompanyValue.h"
#import "BehaviorIcon.h"
#import "Challenge.h"
#import "CDLearning.h"

@interface DataManager : NSObject

#pragma mark - Init

+ (void)setupCoreDataStack;
+ (NSManagedObjectContext *) context;

#pragma mark - Current User

+(User *) user;
+(Company *) userCompany;
+(void) updateUser:(NSDictionary *) info;
+(void) updateSomeUserInfo:(NSDictionary *) info;

#pragma mark - Authorization

+(void) authorizeUser:(NSDictionary *) info;
+(NSString *)apiToken;
+(void)saveApiToken:(NSString *)token;
+(NSString *) userEmail;
+(void) saveUserEmail:(NSString *) email;
+(void) updatePushTokenForCurrentUser;
+(void) logoutUser;

#pragma mark - Team User

+(void) saveTeamUsersList:(NSArray *) usersInfo;
+(void) saveTeamUser:(NSDictionary *) info;
+(TeamUser *) teamUserWithId:(NSNumber *) userId;
+(void) updateTeamUserList:(NSArray *) list;
+(void) deleteFromCompanyTeamUser:(NSNumber *) userId;
+(void) updateTeamUser:(NSNumber *) userId withInfo:(NSDictionary *)info;

#pragma mark - Behaviors

+(void) saveBehaviorsList:(NSArray *) list; //Not used
+(void) saveValuesList:(NSArray *) list;
+(void) saveCurrentUserEvaluationList:(NSArray *) list;
+(Behavior *) behaviorByID:(NSNumber *) behaviorId;
+(void) saveValueCompanyList:(NSArray *) list;
+(void) updateBehaviorID:(NSNumber *) behaviorId withInfo:(NSDictionary*) info;
+(void) saveBehaviorsIconList:(NSArray *) iconList;
+(void) appendBehaviorsIconList:(NSArray *) iconList;

#pragma mark - ActivityFeed

+(void) saveActivityFeedWithInfo:(NSDictionary *) info;
+(void) saveTopPointWithInfo:(NSDictionary *) info;
+(void) appendTopPointWithInfo:(NSDictionary *) info;
+(void) saveActivityFeedList:(NSArray *) list;
+(void) appendActivityFeedList:(NSArray *) list;
+(void) appendCommentAfterCreation:(NSDictionary *) info feedId:(NSNumber *) feedId;
+(void) deleteFeedWithId:(NSNumber *) feedId forType:(ActivityFeedType) feedType;
+(void) saveActivityFeedQuestionList:(NSArray *) list;
+(ActivityFeedQuestion *) activityFeedQuestion;
+(void) deleteActivityFeedQuestion;

#pragma mark - Challenge

+(Challenge *) activityFeedChallenge;
+(Challenge *) currentChallenge;
+(void) saveActivityFeedChallenge:(NSDictionary *) info;
+(void) saveCurrentChallenge:(NSDictionary *) info;
+(void) deteteActivityFeedChallenge;
+(void) deleteCurrentChallenge;
+(void) saveChallengeList:(NSArray *) list;

#pragma mark - Learnings
+(void) appendLearningList:(NSArray *) list fromLearningList:(BOOL) isList;
+(void) saveLearningsList:(NSArray *) list fromLearningList:(BOOL) isList;
+(void) updateLearningWithInfo:(NSDictionary *) info fromLearningList:(BOOL) isList;

#pragma mark - Comment

+(void) saveCommentsList:(NSArray *) list feedId:(NSNumber *) feedId;
+(void) appendCommentsList:(NSArray *) list feedId:(NSNumber *) feedId;
+(void) deleteCommentWithId:(NSNumber *) commentId;

#pragma mark - Retap 

+(void) saveRetapForFeed:(NSNumber *) feedId isRetaped:(BOOL) retaped;

+(void) saveLeaderUserList:(NSArray *) list;
+(void) saveLeaderBehaviorList:(NSArray *) list;

+(void) saveSuggestionsList:(NSArray *) list;
+(void) appendSuggestionsList:(NSArray *) list;

+(void) saveUserBehaviors:(NSArray *) list userId:(NSNumber *) userId;

+(void) saveEvaluations:(NSArray *) list userId:(NSNumber *) userId;
+(void) saveState:(NSInteger) state forEvaluate:(EvaluateBehavior *) evaluate;

+(void) saveBehaviorUsersList:(NSArray *) list;

+(void) saveTeamMessageList:(NSArray *) list;
+(void) deleteTeamMessageWithId:(NSNumber *) messageId;

+(void) saveQuestionsList:(NSArray *) list;
+(void) deleteQuestionById:(NSNumber *) questionId;

@end
