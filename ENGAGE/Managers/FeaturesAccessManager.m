//
//  FeaturesAccessManager.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "FeaturesAccessManager.h"

@implementation FeaturesAccessManager

+(BOOL) isAccessDropMenuStyle:(DropMenuStyle) style forScreen:(AppScreenType) screen withUser:(User *) user {
    
    if ([user.company.plan isEqualToString:@"premium"]) {
        
        if (screen == AppScreenTypeActivityFeed || screen == AppScreenTypeLearningList) {
            
            if (style == DropMenuStyleTop) {
                return YES;
            }
            else {
                return YES;
            }
            
        }
        
    }
    
    if ([user.company.plan isEqualToString:@"standart"] || [user.company.plan isEqualToString:@"basic"]) {
        
        if (screen == AppScreenTypeActivityFeed || screen == AppScreenTypeLearningList) {
            
            if (style == DropMenuStyleTop) {
                return NO;
            }
            else {
                return YES;
            }
            
        }
        
    }
    
    return NO;
}

+(BOOL) isAccessFeature:(AppFeatureType) featureType forScreen:(AppScreenType) screen withUser:(User *) user {
    
    if ([user.company.plan isEqualToString:@"basic"]) {
        
        if (screen == AppScreenTypeValueAndBehavior) {
            
            if (featureType == AppFeatureTypeCreateBehaviorOrValue) {
                return NO;
            }
            
        }
        
        if (screen == AppScreenTypeUserProfile) {
            if (featureType == AppFeatureTypeGiveEvaluation || featureType == AppFeatureTypeVirtualSuggestion) {
                return NO;
            }
        }
        
    }
    
    if ([user.company.plan isEqualToString:@"basic"] || [user.company.plan isEqualToString:@"standart"]) {
        
        if (screen == AppScreenTypeUserProfile) {
            
            if (featureType == AppFeatureTypeQuestion || featureType == AppFeatureTypeChallenge) {
                return NO;
            }
            
        }
        
        if (screen == AppScreenTypeActivityFeed) {
            if (featureType == AppFeatureTypeChallenge) {
                return NO;
            }
        }
        
    }
    
    
    return YES;
}

@end
