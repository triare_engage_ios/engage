//
//  DataManager.m
//  ENGAGE..
//
//  Created by Alex Kravchenko on 03.08.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import "DataManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "AKFileManager.h"
#import "NSDictionary+SecureValue.h"
#import "AppDelegate.h"
#import "KeychainManager.h"
#import "UserDefaultsManager.h"

@implementation DataManager

#pragma mark Initializations

+ (instancetype)sharedManager
{
    static DataManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DataManager alloc] init];
    });
    
    return instance;
}

+ (void)setupCoreDataStack
{
    [MagicalRecord setLoggingLevel:MagicalRecordLoggingLevelOff];
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"Engage.sqlite"];
}

+ (NSManagedObjectContext *) context {
    return [NSManagedObjectContext MR_defaultContext];
}

//methods for authorized user
#pragma mark Current User

+(User *) user {
    return [User MR_findFirst];
}

+(Company *) userCompany {
    return [DataManager user].company;
}

+ (NSString *)apiToken
{
    return [KeychainManager apiToken];
}

+ (void) saveApiToken:(NSString *)token
{
    [KeychainManager saveApiToken:token];
}

+ (NSString *) userEmail {
    return [KeychainManager userEmail];
}

+ (void) saveUserEmail:(NSString *) email {
    [KeychainManager saveApiEmail:email];
}

+(void) authorizeUser:(NSDictionary *) info {
    
    NSNumber *userId = [info valueForKey:@"id"];
    NSString *token = [info valueForKey:@"authentication_token"];
    NSString *email = [info valueForKey:@"email"];
    NSString *plane = [info valueForKey:@"company_plan"];
    
    if (userId == nil) return;

    [self saveApiToken:token];
    [self saveUserEmail:email];
    [UserDefaultsManager saveApiTokenState:YES];
    [UserDefaultsManager saveCompanyPlane:plane];
    
    [self clearPreviosUserData];
    
    [self createUser:info];
    
    [self updateUser:info];
}

+(void) updatePushTokenForCurrentUser {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        User *user = [User MR_findFirstInContext:localContext];
        [user updatePushTokenWithData:AppDelegate.theApp.deviceTokenForPush];
    }];
}

+(void) logoutUser {
    
    if ([DataManager user].pushDeviceToken.length > 0) {
        [[RequestManager sharedManager] unsubscribeFromPushWithToken:[DataManager user].pushDeviceToken success:^(BOOL success) {
            //
        } failure:^(NSString *message) {
            //
        }];
    }
    
    [self saveApiToken:nil];
    [self saveUserEmail:nil];
    [UserDefaultsManager saveApiTokenState:NO];
    [UserDefaultsManager saveCompanyPlane:nil];
    [self clearPreviosUserData];
    [[RequestManager sharedManager] setApiToken:nil email:nil];
}

+(void) clearPreviosUserData {
    
    [[NSFileManager defaultManager] removeItemAtPath:[AKFileManager companyDirectory] error:nil];
    
    
    NSArray *entityNames = [[NSPersistentStoreCoordinator MR_defaultStoreCoordinator].managedObjectModel.entities valueForKey:@"name"];
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    for (NSString *name in entityNames) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:name];
        NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
        delete.resultType = NSBatchDeleteResultTypeCount;
        
        NSError *error = nil;
        NSBatchDeleteResult *result = [localContext executeRequest:delete error:&error];
        if (result) {
            //MLog(@"deleted %@ objects of '%@'", result.result, name);
        }
    }
    
    [localContext MR_saveToPersistentStoreAndWait];
}

+(void) clearEntities:(NSString *) entityName {

    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [request setIncludesSubentities:NO];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    delete.resultType = NSBatchDeleteResultTypeCount;
    
    NSError *error = nil;
    NSBatchDeleteResult *result = [localContext executeRequest:delete error:&error];
    if (result) {
        //MLog(@"deleted %@ objects of '%@'", result.result, entityName);
    }
    
    [localContext MR_saveToPersistentStoreAndWait];
}

+(void) clearEntities:(NSString *) entityName withPredicate:(NSPredicate *) predicate {
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
    [request setIncludesSubentities:NO];
    
    if (predicate) {
        [request setPredicate:predicate];
    }
    
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    delete.resultType = NSBatchDeleteResultTypeCount;
    
    NSError *error = nil;
    NSBatchDeleteResult *result = [localContext executeRequest:delete error:&error];
    if (result) {
        //MLog(@"deleted %@ objects of '%@'", result.result, entityName);
    }
    
    [localContext MR_saveToPersistentStoreAndWait];
}

+(void) createUser:(NSDictionary *) info {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        User *user = [User MR_createEntityInContext:localContext];
        user.userId = [info secureNumberForKey:@"id"];
        
        user.company = [Company MR_createEntityInContext:localContext];
    }];
}

+(void) updateUser:(NSDictionary *) info {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        User *user = [User MR_findFirstInContext:localContext];
        [user updateUserInfo:info];
    }];
}

+(void) updateSomeUserInfo:(NSDictionary *) info {
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        User *user = [User MR_findFirstInContext:localContext];
        [user updateSomeUserInfo:info];
    }];
}



+(void) saveTeamUsersList:(NSArray *) list {
    
    [self clearEntities:@"TeamUser"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        for (NSDictionary *userInfo in list) {
            TeamUser *user = [TeamUser MR_createEntityInContext:localContext];
            [user updateUserWithInfo:userInfo];
        }
    }];
    
}

+(void) saveTeamUser:(NSDictionary *) info {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        NSNumber *uID = [info secureNumberForKey:@"id"];
        
        TeamUser *user = [TeamUser MR_findFirstByAttribute:@"userId" withValue:uID inContext:localContext];
        if (user == nil) {
            user = [TeamUser MR_createEntityInContext:localContext];
        }
        
        [user updateUserWithInfo:info];
    }];
}

+(TeamUser *) teamUserWithId:(NSNumber *) userId {
    return [TeamUser MR_findFirstByAttribute:@"userId" withValue:userId];
}

+(void) updateTeamUserList:(NSArray *) list {
    
    [self clearEntities:@"TeamUser"];
    [self clearEntities:@"LeaderUser"];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSDictionary *userInfo in list) {
            NSNumber *uId = [userInfo secureNumberForKey:@"id"];
            
            TeamUser *user = [TeamUser MR_findFirstByAttribute:@"userId" withValue:uId inContext:localContext];
            
            if (user == nil) {
                user = [TeamUser MR_createEntityInContext:localContext];
                [user updateUserWithInfo:userInfo];
            }
            else {
                [user updateSomeUserInfo:userInfo];
            }
        }
    }];
}

+(void) deleteFromCompanyTeamUser:(NSNumber *) userId {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        TeamUser *user = [TeamUser MR_findFirstByAttribute:@"userId" withValue:userId inContext:localContext];
        
        [user MR_deleteEntityInContext:localContext];
    }];
    
}

+(void) updateTeamUser:(NSNumber *) userId withInfo:(NSDictionary *)info {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        TeamUser *user = [TeamUser MR_findFirstByAttribute:@"userId" withValue:userId inContext:localContext];
        
        if (user == nil) {
            user = [TeamUser MR_createEntityInContext:localContext];
            [user updateUserWithInfo:info];
        }
        else {
            [user updateSomeUserInfo:info];
        }
        
    }];
}

/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Values and Behaviors

//Not used
+(void) saveBehaviorsList:(NSArray *) list {
    
    [self clearEntities:@"Behavior"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            Behavior *item = [Behavior MR_createEntityInContext:localContext];
            [item createWithInfo:info];
        }
    }];
}

+(void) saveValuesList:(NSArray *) list {
    
    [self clearEntities:@"Value"];
    [self clearEntities:@"Behavior"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            Value *item = [Value MR_createEntityInContext:localContext];
            [item createValueWithInfo:info inContext:localContext];
        }
    }];
    
}

+(Behavior *) behaviorByID:(NSNumber *) behaviorId {
    
    Behavior *item = [Behavior MR_findFirstByAttribute:@"behaviorId" withValue:behaviorId];
    
    return item;
}

//Used for saving short list of values, that have company (also empty).
+ (void) saveValueCompanyList:(NSArray *) list {
    [self clearEntities:@"CompanyValue"];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            CompanyValue *item = [CompanyValue MR_createEntityInContext:localContext];
            [item createWithInfo:info];
        }
    }];
}

+(void) updateBehaviorID:(NSNumber *) behaviorId withInfo:(NSDictionary*) info {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        //
        Behavior *item = [Behavior MR_findFirstByAttribute:@"behaviorId" withValue:behaviorId inContext:localContext];
        
        if (item != nil) {
            [item updateWithInfo:info];
        }
        
    }];
}

//Clean and save icons list of behaviors. Used when admin or manager create behavior, and choose "from library"
+(void) saveBehaviorsIconList:(NSArray *) iconList {
    [self clearEntities:@"BehaviorIcon"];
    
    [self appendBehaviorsIconList:iconList];
}

//Add icons list of behaviors
+(void) appendBehaviorsIconList:(NSArray *) iconList {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        for (NSDictionary *info in iconList) {
            BehaviorIcon *icon = [BehaviorIcon MR_createEntityInContext:localContext];
            [icon createWithInfo:info];
        }
    }];
    
}

+(void) saveCurrentUserEvaluationList:(NSArray *) list {
    
    [self clearEntities:@"BehaviorTeamUserEvaluations"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            BehaviorTeamUserEvaluations *item = [BehaviorTeamUserEvaluations MR_createEntityInContext:localContext];
            [item createWithInfo:info];
        }
    }];
}

/////////////////////////////////////////////////////////////////////////////////
#pragma mark - Activity Feed

+(void) saveActivityFeedWithInfo:(NSDictionary *) info {
    NSArray *feeds = [info valueForKey:@"feeds"];
    NSArray *questions = [info valueForKey:@"questions"];
    NSDictionary *challenge = [info valueForKey:@"challenge"];
    NSDictionary *topPoint = [info valueForKey:@"top_point"];
    
    if (feeds.count > 0) {
        [self saveActivityFeedList:feeds];
    }    
    
    if (questions.count > 0) {
        [self saveActivityFeedQuestionList:questions];
    }
    
    if ([challenge valueForKey:@"title"]) {
        [self saveActivityFeedChallenge:challenge];
    }

    if ([topPoint valueForKey:@"id"]) {
        [self saveTopPointWithInfo:topPoint];
    }
    
}

+(void) saveTopPointWithInfo:(NSDictionary *) info {
    
    NSNumber *topPointId = [info valueForKey:@"id"];
    NSPredicate *typePredicate = [ActivityFeed predicateForType:ActivityFeedTypeTopPoint andId:topPointId];
    [self clearEntities:@"ActivityFeed" withPredicate:typePredicate];
    [self appendTopPointWithInfo:info];
}

+(void) appendTopPointWithInfo:(NSDictionary *) info {
    
    NSNumber *topPointId = [info valueForKey:@"id"];
    
    if (topPointId == nil) {
        return;
    }
    
    NSPredicate *typePredicate = [ActivityFeed predicateForType:ActivityFeedTypeTopPoint andId:topPointId];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        ActivityFeed *item = [ActivityFeed MR_findFirstWithPredicate:typePredicate inContext:localContext];
        
        if (item == nil) {
            item = [ActivityFeed MR_createEntityInContext:localContext];
        }
        
        [item createWithInfo:info inContext:localContext];
    }];
}

+(void) saveActivityFeedList:(NSArray *) list {
    
    [self clearEntities:@"ActivityFeed"];
    
    [self appendActivityFeedList:list];
}

+(void) appendActivityFeedList:(NSArray *) list {

    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            ActivityFeed *item = [ActivityFeed MR_createEntityInContext:localContext];
             
            [item createWithInfo:info inContext:localContext];
            
            if ([item feedType] == ActivityFeedTypeTap && [info valueForKey:@"behaviour"]) {
                Behavior *behavior = [Behavior MR_findFirstByAttribute:@"behaviorId" withValue:item.behaviourId inContext:localContext];
                
                if (behavior == nil) {
                    behavior = [Behavior MR_createEntityInContext:localContext];
                    [behavior createWithInfo:[info valueForKey:@"behaviour"]];
                }
                else {
                    [behavior updateWithInfo:[info valueForKey:@"behaviour"]];
                }
            }
        }
    }];
}

+(void) saveActivityFeedQuestionList:(NSArray *) list {
    [self clearEntities:@"ActivityFeedQuestion"];
    [self clearEntities:@"ActivityFeedAnswerOption"];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            ActivityFeedQuestion *item = [ActivityFeedQuestion MR_createEntityInContext:localContext];
            [item createWithInfo:info inContext:localContext];
        }
    }];
}

+(ActivityFeedQuestion *) activityFeedQuestion {
    return [ActivityFeedQuestion MR_findFirstInContext:[DataManager context]];
}

+(void) deleteActivityFeedQuestion {
    [self clearEntities:@"ActivityFeedQuestion"];
}

#pragma mark - Challenge

+(Challenge *) activityFeedChallenge {
    NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"isActivityFeedChallenge == %@", [NSNumber numberWithBool:YES]];
    
    Challenge *item = [Challenge MR_findFirstWithPredicate:typePredicate inContext:[DataManager context]];
    
    return item;
}

+(Challenge *) currentChallenge {
    
    NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"isCurrentChallenge == %@", [NSNumber numberWithBool:YES]];
    
    Challenge *item = [Challenge MR_findFirstWithPredicate:typePredicate inContext:[DataManager context]];
    
    return item;
}

+(void) saveActivityFeedChallenge:(NSDictionary *) info {
    [self deteteActivityFeedChallenge];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        //
        Challenge *item = [Challenge MR_createEntityInContext:localContext];
        [item createWithInfo:info activityFeed:YES];
    }];
}

+(void) saveCurrentChallenge:(NSDictionary *) info {
    [self deleteCurrentChallenge];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        //
        Challenge *item = [Challenge MR_createEntityInContext:localContext];
        [item createWithInfo:info current:YES];
    }];
}

+(void) deteteActivityFeedChallenge {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"isActivityFeedChallenge == %@", [NSNumber numberWithBool:YES]];
        
        NSArray *list = [Challenge MR_findAllWithPredicate:typePredicate inContext:localContext];
        
        if (list.count > 0) {
            for (Challenge *c in list) {
                if (c != nil) {
                    [c MR_deleteEntityInContext:localContext];
                }
            }
        }
    }];
}

+(void) deleteCurrentChallenge {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"isCurrentChallenge == %@", [NSNumber numberWithBool:YES]];
        
        NSArray *list = [Challenge MR_findAllWithPredicate:typePredicate inContext:localContext];
        
        if (list.count > 0) {
            for (Challenge *c in list) {
                if (c != nil) {
                    [c MR_deleteEntityInContext:localContext];
                }
            }
        }
    }];
}

+(void) saveChallengeList:(NSArray *) list {
    
    NSMutableArray *predicates = [NSMutableArray array];
    NSPredicate *currentChallengePredicate = [NSPredicate predicateWithFormat:@"isCurrentChallenge == %@", [NSNumber numberWithBool:NO]];
    NSPredicate *activityFeedChallengePredicate = [NSPredicate predicateWithFormat:@"isActivityFeedChallenge == %@", [NSNumber numberWithBool:NO]];
    [predicates addObject:currentChallengePredicate];
    [predicates addObject:activityFeedChallengePredicate];
    
    [self clearEntities:@"Challenge" withPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicates]];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        for (NSDictionary *info in list) {
            
            NSMutableArray *predicates = [NSMutableArray array];
            NSPredicate *currentChallengePredicate = [NSPredicate predicateWithFormat:@"isCurrentChallenge == %@", [NSNumber numberWithBool:NO]];
            NSPredicate *activityFeedChallengePredicate = [NSPredicate predicateWithFormat:@"isActivityFeedChallenge == %@", [NSNumber numberWithBool:NO]];
            
            NSPredicate *idPredicate = [NSPredicate predicateWithFormat:@"feedId == %@", [info valueForKey:@"id"]];
            
            [predicates addObject:currentChallengePredicate];
            [predicates addObject:activityFeedChallengePredicate];
            [predicates addObject:idPredicate];
            
            Challenge *item = [Challenge MR_findFirstWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicates] inContext:localContext];
            
            if (item == nil) {
                item = [Challenge MR_createEntityInContext:localContext];
                [item createWithInfo:info current:NO];
            }
            else {
                [item updateWithInfo:info];
            }
        }
    }];
    
}

#pragma mark - Learnings

+(void) appendLearningList:(NSArray *) list fromLearningList:(BOOL) isList {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            NSNumber *learningId = [info valueForKey:@"id"];
            CDLearning *learning = [CDLearning MR_findFirstByAttribute:@"feedId" withValue:learningId inContext:localContext];
            
            if (learning == nil) {
                learning = [CDLearning MR_createEntityInContext:localContext];
            }
            
            [learning createWithInfo:info learningList:isList];
            
        }
    }];
}

+(void) saveLearningsList:(NSArray *) list fromLearningList:(BOOL) isList {
    
    [self deleteLearningList:isList];
    
    [self appendLearningList:list fromLearningList:isList];

}

+(void) deleteLearningList:(BOOL) isFromLearningList {
    
    NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"isFromLearningList == %@", [NSNumber numberWithBool:isFromLearningList]];
    
    [self clearEntities:@"CDLearning" withPredicate:typePredicate];
}

+(void) updateLearningWithInfo:(NSDictionary *) info fromLearningList:(BOOL) isList {
    
    NSNumber *learningId = [info valueForKey:@"id"];
    
    if (learningId == nil) {
        return;
    }
    
    NSMutableArray *predicates = [NSMutableArray array];
    NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"isFromLearningList == %@", [NSNumber numberWithBool:isList]];
    NSPredicate *idPredicate = [NSPredicate predicateWithFormat:@"feedId == %@", learningId];
    [predicates addObject:typePredicate];
    [predicates addObject:idPredicate];
    
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        CDLearning *learning = [CDLearning MR_findFirstWithPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:predicates] inContext:localContext];
        
        if (learning == nil) {
            learning = [CDLearning MR_createEntityInContext:localContext];
        }

        [learning createWithInfo:info learningList:isList];
        
    }];
    
}

#pragma mark - Comments
     
+(void) saveCommentsList:(NSArray *) list feedId:(NSNumber *) feedId {
    
    NSPredicate *feedPredicate = [ActivityFeed predicateForType:ActivityFeedTypeTap andId:feedId];
    
    ActivityFeed *feed = [ActivityFeed MR_findFirstWithPredicate:feedPredicate];
    
    //Deprecated.
    //ActivityFeed *feed = [ActivityFeed MR_findFirstByAttribute:@"feedId" withValue:feedId];
    
    if (feed.commentsSet.count) {
        
        [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
            
            //Deprecated.
            //ActivityFeed *f = [ActivityFeed MR_findFirstByAttribute:@"feedId" withValue:feedId inContext:localContext];
            
            ActivityFeed *f = [ActivityFeed MR_findFirstWithPredicate:feedPredicate inContext:localContext];
            
            for (Comment *c in f.comments) {
                [c MR_deleteEntityInContext:localContext];
            }
        }];
    }
    
    [self appendCommentsList:list feedId:feedId];
}


+(void) appendCommentsList:(NSArray *) list feedId:(NSNumber *) feedId {
    
    NSPredicate *feedPredicate = [ActivityFeed predicateForType:ActivityFeedTypeTap andId:feedId];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        ActivityFeed *feed = [ActivityFeed MR_findFirstWithPredicate:feedPredicate inContext:localContext];
        
        //Deprecated.
        //ActivityFeed *feed = [ActivityFeed MR_findFirstByAttribute:@"feedId" withValue:feedId inContext:localContext];
        
        for (NSDictionary *info in list) {
            Comment *item = [Comment MR_createEntityInContext:localContext];
            [item createWithInfo:info];
            
            [feed addCommentsObject:item];
        }
    }];
}

+(void) deleteCommentWithId:(NSNumber *) commentId {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        Comment *c = [Comment MR_findFirstByAttribute:@"commentId" withValue:commentId inContext:localContext];
        
        if (c != nil) {
            [c MR_deleteEntityInContext:localContext];
        }
        
    }];
    
}

+(void) appendCommentAfterCreation:(NSDictionary *) info feedId:(NSNumber *) feedId {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        NSPredicate *feedPredicate = [ActivityFeed predicateForType:ActivityFeedTypeTap andId:feedId];
        ActivityFeed *feed = [ActivityFeed MR_findFirstWithPredicate:feedPredicate inContext:localContext];
        
        //Deprecated.
        //ActivityFeed *feed = [ActivityFeed MR_findFirstByAttribute:@"feedId" withValue:feedId inContext:localContext];
        feed.commentsCount = [NSNumber numberWithInteger:feed.commentsCount.integerValue+1];
        
        Comment *item = [Comment MR_createEntityInContext:localContext];
        [item createWithInfo:info];
        
        [feed addCommentsObject:item];
    }];
}

+(void) deleteFeedWithId:(NSNumber *) feedId forType:(ActivityFeedType) feedType {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        NSPredicate *feedPredicate = [ActivityFeed predicateForType:feedType andId:feedId];
        
        ActivityFeed *feed = [ActivityFeed MR_findFirstWithPredicate:feedPredicate inContext:localContext];
        
        if (feed != nil) {
            
            //Delete all comments of Feed
            if (feed.commentsSet.count) {
                for (Comment *c in feed.comments) {
                    [c MR_deleteEntityInContext:localContext];
                }
            }
            
            if (feed.learning != nil) {
                [feed.learning MR_deleteEntityInContext:localContext];
            }
            
            [feed MR_deleteEntityInContext:localContext];
    
        }
        
    }];
}

+(void) saveRetapForFeed:(NSNumber *) feedId isRetaped:(BOOL) retaped {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        //Deprecated.
        //ActivityFeed *feed = [ActivityFeed MR_findFirstByAttribute:@"feedId" withValue:feedId inContext:localContext];
        
        NSPredicate *feedPredicate = [ActivityFeed predicateForType:ActivityFeedTypeTap andId:feedId];
        ActivityFeed *feed = [ActivityFeed MR_findFirstWithPredicate:feedPredicate inContext:localContext];
        
        feed.isRetaped = [NSNumber numberWithBool:retaped];
        
        NSInteger count = feed.retapsCount.integerValue;
        if (retaped) count++;
        else count--;
        
        feed.retapsCount = [NSNumber numberWithInteger:count];
    }];
}

+(void) saveLeaderUserList:(NSArray *) list {
    [self clearEntities:@"LeaderUser"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        for (NSDictionary *userInfo in list) {
            LeaderUser *user = [LeaderUser MR_createEntityInContext:localContext];
            [user updateUserWithInfo:userInfo];
        }
    }];
    
}

+(void) saveLeaderBehaviorList:(NSArray *) list {
    [self clearEntities:@"LeaderBehavior"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        for (NSDictionary *info in list) {
            LeaderBehavior *item = [LeaderBehavior MR_createEntityInContext:localContext];
            [item createWithInfo:info];
        }
    }];
    
}

+(void) saveSuggestionsList:(NSArray *) list {
    
    [self clearEntities:@"Suggestion"];
    
    NSNumber *userId = [self user].userId;
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        TeamUser *user = [TeamUser MR_findFirstByAttribute:@"userId" withValue:userId inContext:localContext];
        user.unreadSuggestionsCount = @0;
    }];
    
    [self appendSuggestionsList:list];
}


+(void) appendSuggestionsList:(NSArray *) list {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        for (NSDictionary *info in list) {
            Suggestion *item = [Suggestion MR_createEntityInContext:localContext];
            [item createWithInfo:info];
        }
    }];
}


+(void) saveUserBehaviors:(NSArray *) list userId:(NSNumber *) userId {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        TeamUser *user = [TeamUser MR_findFirstByAttribute:@"userId" withValue:userId inContext:localContext];
        
        for (UserBehavior *b in user.behaviors) {
            [b MR_deleteEntityInContext:localContext];
        }
        
        for (NSDictionary *info in list) {
            UserBehavior *item = [UserBehavior MR_createEntityInContext:localContext];
            [item createWithInfo:info];
            
            [user addBehaviorsObject:item];
        }
    }];
}

+(void) saveEvaluations:(NSArray *) list userId:(NSNumber *) userId {
    
    [self clearEntities:@"EvaluateBehavior"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        TeamUser *user = [TeamUser MR_findFirstByAttribute:@"userId" withValue:userId inContext:localContext];
        
        for (EvaluateBehavior *b in user.evaluations) {
            [b MR_deleteEntityInContext:localContext];
        }
        
        for (NSDictionary *info in list) {
            EvaluateBehavior *item = [EvaluateBehavior MR_createEntityInContext:localContext];
            [item createWithInfo:info];
            
            [user addEvaluationsObject:item];
        }
    }];
}

+(void) saveState:(NSInteger) state forEvaluate:(EvaluateBehavior *) evaluate {
        
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"behaviorId == %@ && teamUser.userId == %@", evaluate.behaviorId, evaluate.teamUser.userId];
        
        EvaluateBehavior *item = [EvaluateBehavior MR_findFirstWithPredicate:predicate inContext:localContext];
        item.evaluateState = [NSNumber numberWithInteger:state];
        
    }];
}

+(void) saveBehaviorUsersList:(NSArray *) list {
    
    [self clearEntities:@"BehaviorUser"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        for (NSDictionary *info in list) {
            BehaviorUser *item = [BehaviorUser MR_createEntityInContext:localContext];
            [item createWithInfo:info];
        }
    }];
}

+(void) saveTeamMessageList:(NSArray *) list {
    
    [self clearEntities:@"TeamMessage"];
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        for (NSDictionary *info in list) {
            TeamMessage *message = [TeamMessage MR_createEntityInContext:localContext];
            [message createWithInfo:info];
        }
    }];
    
}

+(void) deleteTeamMessageWithId:(NSNumber *) messageId {
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        TeamMessage *message = [TeamMessage MR_findFirstByAttribute:@"messageId" withValue:messageId inContext:localContext];
        
        if (message != nil) {
            [message MR_deleteEntityInContext:localContext];
        }
    }];
    
}

+(void) saveQuestionsList:(NSArray *) list {
    [self clearEntities:@"Question"];
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        //
        for (NSDictionary *info in list) {
            Question *question = [Question MR_createEntityInContext:localContext];
            [question createQuestionWithInfo:info inContext:localContext];
        }
    }];
}

+(void) deleteQuestionById:(NSNumber *) questionId {
    
    [MagicalRecord saveWithBlockAndWait: ^(NSManagedObjectContext *localContext) {
        
        Question *question = [Question MR_findFirstByAttribute:@"questionId" withValue:questionId inContext:localContext];
        
        for (QuestionAnswer *answer in question.answers) {
            [answer MR_deleteEntityInContext:localContext];
        }
        
        for (QuestionCustomAnswer *customAnswer in question.customAnswers) {
            [customAnswer MR_deleteEntityInContext:localContext];
        }
        
        ActivityFeedQuestion *activityFeedQuesttion = [ActivityFeedQuestion MR_findFirstByAttribute:@"questionId" withValue:questionId inContext:localContext];
        
        if (activityFeedQuesttion != nil) {
            [activityFeedQuesttion MR_deleteEntityInContext:localContext];
        }
        
        [question MR_deleteEntityInContext:localContext];
    }];
}

@end
