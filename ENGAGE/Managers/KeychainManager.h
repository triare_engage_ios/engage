//
//  KeychainManager.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/30/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UICKeyChainStore/UICKeyChainStore.h"

@interface KeychainManager : NSObject

/**
 Use for Store Sensetive Data and share it with Extension and other Group App.

 @return Return App Group Keychain Store.
 */
+ (UICKeyChainStore *) appGroupKeychain;

/**
 Saved Request Manager API Token in Group App Keychain.

 @param token Request Manager API Token, could be nil
 */
+ (void) saveApiToken:(NSString *) token;


/**
 Uses for access to Request Manager API token with Extension and other Group App.

 @return Request Manager API Token, could be nil
 */
+ (NSString *) apiToken;


/**
 Saved Request Manager API Email in Group App Keychain for current login user.

 @param email Requset Manager API Email, could be nil
 */
+ (void) saveApiEmail:(NSString *) email;


/**
 Uses for access to Request Manager API email with Extension and other Group App.

 @return Request Manager API Email, could be nil
 */
+ (NSString *) userEmail;

@end
