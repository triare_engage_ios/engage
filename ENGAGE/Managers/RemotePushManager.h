//
//  RemotePushManager.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/22/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    RemotePushItemActionUpdateActivityFeed,
    RemotePushItemActionUpdateActivityFeedAndCreateDotOnProfile,
    RemotePushItemActionCreateDotOnActivityFeed,
    RemotePushItemActionShowActivityFeed,
    RemotePushItemActionNeedRefreshActivityFeed,
    RemotePushItemActionCreateDotOnProfile,
    RemotePushItemActionShowChallengeBadgeList
} RemotePushItemAction;

typedef enum : NSUInteger {
    RemotePushItemTypeTap,
    RemotePushItemTypeQuestion,
    RemotePushItemTypeTeamMessage,
    RemotePushItemTypeTopPointWinner,
    RemotePushItemTypeChallengeStarted,
    RemotePushItemTypeChallengeEnded,
    RemotePushItemTypeChallengeWinner
} RemotePushItemType;


@interface RemotePushItem : NSObject
@property (nonatomic, readonly) RemotePushItemType type;
@property (nonatomic, readonly) UIApplicationState state;
@property (nonatomic, readonly) NSArray *actionList;

- (instancetype) initWithInfo:(NSDictionary *) info andAppState:(UIApplicationState) appState;

- (void) launchAction;

@end






@interface RemotePushManager : NSObject

+(void) parsePushAndActionWithInfo:(NSDictionary *) info forAppState:(UIApplicationState) appState;

@end
