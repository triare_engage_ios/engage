//
//  FeaturesAccessManager.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/16/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ENDropMenuHelper.h"

/**
 Type for AppScreen
 */
typedef enum : NSUInteger {
    AppScreenTypeActivityFeed,
    AppScreenTypeLearningList,
    AppScreenTypeValueAndBehavior,
    AppScreenTypeUserProfile
} AppScreenType;


typedef enum : NSUInteger {
    AppFeatureTypeCreateBehaviorOrValue,
    AppFeatureTypeGiveEvaluation,
    AppFeatureTypeVirtualSuggestion,
    AppFeatureTypeQuestion,
    AppFeatureTypeChallenge
} AppFeatureType;


@interface FeaturesAccessManager : NSObject

+(BOOL) isAccessDropMenuStyle:(DropMenuStyle) style forScreen:(AppScreenType) screen withUser:(User *) user;

+(BOOL) isAccessFeature:(AppFeatureType) featureType forScreen:(AppScreenType) screen withUser:(User *) user;

@end
