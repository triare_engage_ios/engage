//
//  UserDefaultsManager.h
//  ENGAGE
//
//  Created by Maksym Savisko on 11/30/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultsManager : NSObject

+ (NSUserDefaults *) appGroupUserDefaults;
+ (void) saveApiTokenState:(BOOL) state;
+ (BOOL) isApiTokenExist;

+ (void) saveCompanyPlane:(NSString *) plane;
+ (NSString *) companyPlane;

@end
