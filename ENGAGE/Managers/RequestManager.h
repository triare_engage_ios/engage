//
//  RequestManager.h
//  
//
//  Created by Alex Kravchenko on 15.06.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaginationInfo.h"

typedef NS_ENUM(NSInteger, ENUpdateCurrentUserError) {
    ENUpdateCurrentUserErrorAuthentication = 0,
    ENUpdateCurrentUserErrorCurrentPassword = 1,
    ENUpdateCurrentUserErrorNotMatchedPassword = 2,
    ENUpdateCurrentUserErrorShortNewPassword = 3,
};

@interface RequestManager : NSObject

+ (instancetype)sharedManager;

- (void) setApiToken:(NSString *) token email:(NSString *) email;
- (BOOL) isAuthorize;
- (void) clearApiToken;
- (NSString *) headerToken;



-(void) downloadFile:(NSString *) url destination:(NSString *) destination progress:(void (^)(NSProgress *progress)) progress onCompleate:(void (^)(NSURL *filePath, NSError *error)) onCompleate;

-(void) authorizeWithEmail:(NSString *) email password:(NSString *) password success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getTeam:(void (^)(BOOL success, NSArray *list)) onCompleate;

-(void) getBehaviours:(void (^)(BOOL success, NSArray *list)) onCompleate;

-(void) getValuesListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getEvaluationsForUser:(NSNumber *) userId complete:(void (^)(BOOL success, NSArray *list, NSString *errorMessage)) onCompleate;

-(void) giveTap:(NSDictionary *) parameters onCompleate:(void (^)(BOOL success, NSString *errorMessage)) onCompleate;

-(void) getActivityFeedForPage:(NSInteger) page onCompleate:(void (^)(BOOL success, PaginationInfo *pagination, NSDictionary *info, NSString *errorMessage)) onCompleate;

-(void) retapFeed:(NSNumber *) feedId onCompleate:(void (^)(BOOL success, NSString *errorMessage)) onCompleate;

-(void) getRetaps:(NSNumber *) feedId page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage)) onCompleate;

-(void) searchInActivityFeed:(NSString *) query page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination)) onCompleate;

-(void) getUserProfile:(NSNumber *) userId success:(void (^)(NSDictionary *userInfo, NSArray *behaviors)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) updateUserWithInfo:(NSDictionary *) info success:(void (^)(BOOL success, NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) checkPassword:(NSString *)password success:(void (^)(BOOL success, BOOL isCorrect, NSString *password)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) updateUserAvatarWithImagePath:(NSString *) imagePath success:(void (^)(BOOL success, NSString *avatarUrl)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getUserBehaviors:(NSNumber *) userId sort:(NSDictionary *) parameters success:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getCompanyJobsOnSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getUserLeaderboard:(NSDictionary *)parameters onCompleate:(void (^)(BOOL success, NSArray *list, NSString *errorMessage)) onCompleate;

-(void) getBehaviorLeaderboard:(NSDictionary *)parameters onCompleate:(void (^)(BOOL success, NSArray *list, NSString *errorMessage)) onCompleate;

-(void) sendSuggestion:(NSDictionary *) parameters onCompleate:(void (^)(BOOL success, NSString *errorMessage)) onCompleate;

-(void) getSuggestions:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage)) onCompleate;

-(void) getEvaluations:(NSNumber *) userId onCompleate:(void (^)(BOOL success, NSArray *list)) onCompleate;

-(void) changeEvaluations:(NSDictionary *) parameters onCompleate:(void (^)(BOOL success)) onCompleate;

-(void) getBehaviorUsersListsByBehaviorId:(NSNumber *) behaviorId success:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getTeamMessageListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getSelfUserSuccess:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Questions
//////////////////////////////////////////////////////////////////////

-(void) sendQuestionWithInfo:(NSDictionary *) info success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getQuestionsListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) deleteQuestionById:(NSNumber *) questionId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) giveAnswerForQuestionId:(NSNumber *) questionId withInfo:(NSDictionary *)info success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Learnings
//////////////////////////////////////////////////////////////////////

-(void) createLearningWithInfo:(NSDictionary *) info success:(void (^)(NSDictionary *result)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getLearningsForPage:(NSInteger) page success:(void (^)(PaginationInfo *pagination, NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) evaluateLearningWithId:(NSNumber *) learningId andVote:(NSNumber *) vote success:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) searchInLearningFeed:(NSString *) query page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination)) onCompleate;

-(void) deleteLearningWithId:(NSNumber *) learningId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Challenges
//////////////////////////////////////////////////////////////////////

-(void) getChallengeWithId:(NSNumber *) challengeId success:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure;

- (void) challengeWasShown:(NSNumber *) challengeId success:(void (^)(BOOL isSuccess)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getCurrentChallengeSuccess:(void (^)(BOOL isNotEmpty, NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getChallengeThatWonUser:(NSNumber *) userId success:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Retaps
//////////////////////////////////////////////////////////////////////

-(void) deleteRetapWithId:(NSNumber *) retapId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Comments
//////////////////////////////////////////////////////////////////////

-(void) getComments:(NSNumber *) feedId page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage)) onCompleate;

-(void) addComment:(NSString *) comment feedId:(NSNumber *) feedId success:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) deleteCommentWithId:(NSNumber *) commentId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Taps
//////////////////////////////////////////////////////////////////////

-(void) deleteTapWithId:(NSNumber *) tapId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Team Message
//////////////////////////////////////////////////////////////////////

-(void) sendTeamMessage:(NSString *) message withEmail:(BOOL) emailState success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) deleteTeamMessageWithId:(NSNumber *) messageId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - User
//////////////////////////////////////////////////////////////////////

-(void) registerForPushWithToken:(NSData *) token success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) unsubscribeFromPushWithToken:(NSString *) token success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Behaviours
//////////////////////////////////////////////////////////////////////

-(void) createBehaviorWithInfo:(NSDictionary *) info success:(void (^)(BOOL success, NSNumber *behaviorId)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) createBehaviorWithInfo:(NSDictionary *) info andIconId:(NSNumber *) iconId success:(void (^)(BOOL success, NSNumber *behaviorId)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) updateBehaviorIconWithImagePath:(NSString *) imagePath andBehaviorId:(NSNumber *) behaviorId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) deactivateBehaviorWithId:(NSNumber *) behaviorId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getBehaviorIconsForPage:(NSInteger) page iconsPerPage:(NSInteger) iconsPerPage success:(void (^)(NSArray *list, PaginationInfo *pagination)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Values
//////////////////////////////////////////////////////////////////////

-(void) createValueWithInfo:(NSDictionary *) info success:(void (^)(BOOL success, NSNumber *valueId)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) updateValueIconWithImagePath:(NSString *) imagePath andValueId:(NSNumber *) valueId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) getValuesTitlesAndIdsSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

//////////////////////////////////////////////////////////////////////
#pragma mark - Team User
//////////////////////////////////////////////////////////////////////

-(void) getTeamUsersListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) deleteFromCompanyTeamUser:(NSNumber *) userId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) inviteTeamUserToCompanyWithEmail:(NSString *) email success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) giveManagerRightsToTeamUser:(NSNumber *) userId success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure;

-(void) removeManagerRightsFromTeamUser:(NSNumber *) userId success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure;


@end
