//
//  UserDefaultsManager.m
//  ENGAGE
//
//  Created by Maksym Savisko on 11/30/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "UserDefaultsManager.h"

#define kApiTokenSaved @"apiTokenSaved"
#define kCompanyPlaneKey @"companyPlaneSaved"

@implementation UserDefaultsManager

#pragma mark - Public Methods

+ (NSUserDefaults *) appGroupUserDefaults {
    return [[NSUserDefaults alloc] initWithSuiteName:EN_APPGROUP_ID];
    
}

+ (void) saveApiTokenState:(BOOL) state {
    [self saveBoolValue:state forKey:kApiTokenSaved inUserDefaults:[self appGroupUserDefaults]];
}

+ (BOOL) isApiTokenExist {
    return [self boolValueForKey:kApiTokenSaved inUserDefaults:[self appGroupUserDefaults]];
}

+ (void) saveCompanyPlane:(NSString *) plane {
    [self saveValue:plane forKey:kCompanyPlaneKey inUserDefaults:[self appGroupUserDefaults]];
}

+ (NSString *) companyPlane {
    return [self valueForKey:kCompanyPlaneKey inUserDefaults:[self appGroupUserDefaults]];
}

#pragma mark - Private Methods

+ (void) saveBoolValue:(BOOL) value forKey:(NSString *) key inUserDefaults:(NSUserDefaults *) userDefaults {
    
    if (userDefaults == nil) {
        return;
    }
    
    if (key == nil) {
        return;
    }
    
    [userDefaults setBool:value forKey:key];
    [userDefaults synchronize];
}

+ (void) saveValue:(id) value forKey:(NSString *) key inUserDefaults:(NSUserDefaults *) userDefaults {
    
    if (userDefaults == nil) {
        return;
    }
    
    if (key == nil) {
        return;
    }
    
    [userDefaults setObject:value forKey:key];
}

+ (BOOL) boolValueForKey:(NSString *) key inUserDefaults:(NSUserDefaults *) userDefaults {
    
    if (userDefaults == nil) {
        return NO;
    }
    
    if (key == nil) {
        return NO;
    }
    
    BOOL returnValue = [userDefaults boolForKey:key];
    
    return returnValue;
}

+ (id) valueForKey:(NSString *) key inUserDefaults:(NSUserDefaults *) userDefaults {
    
    if (userDefaults == nil) {
        return nil;
    }
    
    if (key == nil) {
        return nil;
    }
    
    return [userDefaults valueForKey:key];
}

@end
