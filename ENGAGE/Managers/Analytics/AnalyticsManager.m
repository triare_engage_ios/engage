//
//  AnalyticsManager.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/17/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "AnalyticsManager.h"
#import <Google/Analytics.h>
@import Firebase;

@implementation AnalyticsManager

#pragma mark - Public Methods

+ (void)configure {
    
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    //NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
#if TARGET_IPHONE_SIMULATOR
    //Do not need to send information, when run from simulator
    [[GAI sharedInstance] setDryRun:YES];
#endif
    
    [FIRApp configure];
}

+ (void)trackScreen:(NSString *)screenName {
    if (screenName == nil) {
        return;
    }
    
    if (![self isNeedTrack]) {
        return;
    }
    
    if ([DataManager user] == nil && [screenName isEqualToString:ENAnalyticScreenMainActivityFeed]) {
        return;
    }
    
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //Firebase Analytic Methods
    [self trackEventScreen:screenName];
}

+ (void)trackScreen:(NSString *)screenName withScreenClass:(NSString *)screenClass {
    
    if (screenName == nil) {
        return;
    }
    
    if (screenClass == nil) {
        return;
    }
    
    if (![self isNeedTrack]) {
        return;
    }
    
    [FIRAnalytics setScreenName:screenName screenClass:screenClass];
}

+ (void)trackEvent:(NSString *)category action:(NSString *)action label:(NSString *)label {
    
    if (category == nil || action == nil) {
        return;
    }
    
    if (action == nil || [action isEqualToString:@""]) {
        return;
    }
    
    if (![self isNeedTrack]) {
        return;
    }
    
    //Google Analytics Method
    [self trackEvent:category action:action label:label value:nil];
    
    //Firebase Analytics Method
    [self trackEventUserActionWith:category action:action andLabel:label];
}

+ (void)trackEvent:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value
{
    id <GAITracker> tracker = [GAI sharedInstance].defaultTracker;
    
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category   // ui_action
                                                          action:action                                           // button_press
                                                           label:label                                             // play
                                                           value:value] build]];                                     // Event value
}

+ (void)setUserProperty:(NSString *)value forPropertyType:(FireBasePropertyType) type {
    
    if (value == nil) {
        return;
    }
    
    if (![self isNeedTrack]) {
        return;
    }
    
    if (type == FireBasePropertyTypeCompanyPlan) {
        [FIRAnalytics setUserPropertyString:value forName:@"company_plan"];
        return;
    }
    
    if (type == FireBasePropertyTypeUserRole) {
        [FIRAnalytics setUserPropertyString:value forName:@"user_role"];
        return;
    }
    
}

+ (void)setUserId:(NSString *)value {
    
    if (value == nil) {
        return;
    }
    
    if (![self isNeedTrack]) {
        return;
    }
    
    [FIRAnalytics setUserID:value];
}

+ (void) userDidLogout {
    
    [FIRAnalytics setUserID:nil];
    [FIRAnalytics setUserPropertyString:nil forName:@"company_plan"];
    [FIRAnalytics setUserPropertyString:nil forName:@"user_role"];

}

#pragma mark - Private Methods

+ (NSArray *)testCompanies {
    return @[@(1), @(4), @(37), @(48), @(50), @(51), @(58), @(61), @(62), @(69), @(72), @(73)];
}

+ (BOOL)isNeedTrack {
    
    if ([DataManager user].company.companyId == nil) {
        return YES;
    }
    
    if ([[self testCompanies] containsObject:[DataManager user].company.companyId]) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Firebase Private Methods

+(void) trackEventUserActionWith:(NSString *) category action:(NSString *) actionName andLabel:(NSString *) label {
    
    [FIRAnalytics logEventWithName:@"user_action"
                        parameters:@{
                                     kFIRParameterItemCategory:category,
                                     @"action_name": actionName,
                                     @"action_label": label
                                     }];
}

+(void) trackEventScreen:(NSString *) screenName {
    
    NSString *role = [DataManager user].role;
    NSString *plan = [DataManager user].company.plan;
    
    if (role == nil) {
        role = @"not_authorized";
    }
    
    if (plan == nil) {
        plan = @"not_authorized";
    }
    
    [FIRAnalytics logEventWithName:@"screen_open"
                        parameters:@{
                                     @"screen_name":screenName,
                                     @"user_role":role,
                                     @"company_plan":plan
                                     }];
    
}

@end
