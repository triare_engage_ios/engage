//
//  AnalyticsConstants.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/17/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "AnalyticsConstants.h"

#pragma mark - Screens

NSString *ENAnalyticScreenMainActivityFeed = @"Activity Feed";
NSString *ENAnalyticScreenLearningList = @"Learning List";
NSString *ENAnalyticScreenLeaderBoardTapsReceived = @"Leaderboard Taps Received";
NSString *ENAnalyticScreenLeaderBoardTapsGiven = @"Leaderboard Taps Given";
NSString *ENAnalyticScreenLeaderBoardBehavior = @"Leaderboard Behavior";
NSString *ENAnalyticScreenMainValuesList = @"Main Values List";
NSString *ENAnalyticScreenMainBehaviorsList = @"Main Behaviors List";
NSString *ENAnalyticScreenProfile = @"Profile";
NSString *ENAnalyticScreenProfileBadgeList = @"Profile Badge List";
NSString *ENAnalyticScreenProfileEvaluateList = @"Profile Evaluate List";
NSString *ENAnalyticScreenProfileCurrentUser = @"Profile Current User";
NSString *ENAnalyticScreenProfileCurrentUserEvaluateList = @"Profile Current User Evaluate List";
NSString *ENAnalyticScreenProfileCurrentUserSettings = @"Profile Current User Settings";
NSString *ENAnalyticScreenProfileCurrentUserJobtitleList = @"Profile Current User Jobtitle List";
NSString *ENAnalyticScreenProfileCurrentUserTeamMessageList = @"Profile Current User Team Message List";
NSString *ENAnalyticScreenLeaderBoardBehaviorTapsReceivedList = @"Leaderboard Behavior Taps Received List";
NSString *ENAnalyticScreenLeaderBoardBehaviorTapsGivenList = @"Leaderboard Behavior Taps Given List";
NSString *ENAnalyticScreenBehaviorDetail = @"Behavior Detail";
NSString *ENAnalyticScreenChallengeCurrent = @"Challenge Current";
NSString *ENAnalyticScreenChallengeCell = @"Challenge Cell";
NSString *ENAnalyticScreenChallengeProfile = @"Challenge Profile";
NSString *ENAnalyticScreenQuestionSendAnswer = @"Question Send Answer";
NSString *ENAnalyticScreenLearningCreate = @"Learning Create";
NSString *ENAnalyticScreenLearningRateVote = @"Learning Rate Vote";
NSString *ENAnalyticScreenEvaluateUserList = @"Evaluate User List";
NSString *ENAnalyticScreenEvaluate = @"Evaluate";
NSString *ENAnalyticScreenGiveATapUserList = @"Give A Tap User List";
NSString *ENAnalyticScreenGiveATapBehaviorList = @"Give A Tap Behavior List";
NSString *ENAnalyticScreenGiveATapCreate = @"Give A Tap Create";
NSString *ENAnalyticScreenTeamMessageCreate = @"Team message Create";
NSString *ENAnalyticScreenSuggestionCreate = @"Suggestion Create";
NSString *ENAnalyticScreenSuggestionList = @"Suggestion List";
NSString *ENAnalyticScreenRetapList = @"Retap List";
NSString *ENAnalyticScreenCommentsList = @"Comments List";
NSString *ENAnalyticScreenQuestionAnswerDetail = @"Question Answer Detail";
NSString *ENAnalyticScreenQuestionList = @"Question List";
NSString *ENAnalyticScreenQuestionCreate = @"Question Create";
NSString *ENAnalyticScreenManagePeople = @"Manage People";
NSString *ENAnalyticScreenBehaviorCreate = @"Behavior Create";
NSString *ENAnalyticScreenValueCreate = @"Value Create";
NSString *ENAnalyticScreenValueCompanyList = @"Value Company List";
NSString *ENAnalyticScreenBehaviorIconsList = @"Behavior Icons List";
NSString *ENAnalyticScreenLogin = @"Login";

#pragma mark - Category

NSString *ENAnalyticCategoryActionMenu = @"Action Menu";
NSString *ENAnalyticCategoryTabNavigationBar = @"Tab Navigation Bar";
NSString *ENAnalyticCategoryLogin = @"Login";
NSString *ENAnalyticCategoryLeaderboard = @"Leaderboard";

#pragma mark - Actions

NSString *ENAnalyticActionChallenge = @"Challenge";
NSString *ENAnalyticActionMenuTop = @"Action Menu Top";
NSString *ENAnalyticActionSearch = @"Search";
NSString *ENAnalyticActionLearnings = @"Learnings";
NSString *ENAnalyticActionActivityFeed = @"Activity Feed";
NSString *ENAnalyticActionNewLearning = @"New Learning";
NSString *ENAnalyticActionSuggestionToAdmin = @"Suggestion to Admin";
NSString *ENAnalyticActionEvaluate = @"Evaluate";
NSString *ENAnalyticActionGiveATap = @"Give a Tap";
NSString *ENAnalyticActionNewTeamMessage = @"New Team Message";
NSString *ENAnalyticActionNewQuestion = @"New Question";
NSString *ENAnalyticActionLeaderboard = @"Leaderboard";
NSString *ENAnalyticActionValuesAndBehaviors = @"Values and Behaviors";
NSString *ENAnalyticActionCurrentProfile = @"Current Profile";
NSString *ENAnalyticActionActionMenuBottom = @"Action Menu Bottom";
NSString *ENAnalyticActionReceiverProfile = @"Receiver Profile";
NSString *ENAnalyticActionSenderProfile = @"Sender Profile";
NSString *ENAnalyticActionBehavior = @"Behavior";
NSString *ENAnalyticActionComments = @"Comments";
NSString *ENAnalyticActionRetapConfirm = @"Retap Confirm";
NSString *ENAnalyticActionRetapList = @"Retap List";
NSString *ENAnalyticActionDeleteSwipe = @"Delete Swipe";
NSString *ENAnalyticActionDeleteSwipeConfirm = @"Delete Swipe Confirm";
NSString *ENAnalyticActionDeleteAlertCancel = @"Delete Alert Cancel";
NSString *ENAnalyticActionDeleteAlertConfirm = @"Delete Alert Confirm";
NSString *ENAnalyticActionTapTry = @"Tap Try";
NSString *ENAnalyticActionTapTryPointsIcon = @"Tap Try Points Icon";
NSString *ENAnalyticActionRateVoteConfirm = @"Rate Vote Confirm";
NSString *ENAnalyticActionRateVoteTry = @"Rate Vote Try";
NSString *ENAnalyticActionLink = @"Link";
NSString *ENAnalyticActionShowMore = @"Show more";
NSString *ENAnalyticActionLogout = @"Logout";
NSString *ENAnalyticActionSignInError = @"Sign In Error";
NSString *ENAnalyticActionForgot = @"Forgot";
NSString *ENAnalyticActionSignUp = @"Sign Up";
NSString *ENAnalyticActionSignIn = @"Sign In";
NSString *ENAnalyticActionFilterData = @"Filter Data";

#pragma mark - Labels

NSString *ENAnalyticLabelNavigationBar = @"From Navigation Bar";
NSString *ENAnalyticLabelActionMenu = @"From Action Menu";
NSString *ENAnalyticLabelFeedTypeTap = @"From Feed Type Tap";
NSString *ENAnalyticLabelFeedTypeTeamMessage = @"From Feed Type Team Message";
NSString *ENAnalyticLabelFeedTypeTopPoints = @"From Feed Type Top Points";
NSString *ENAnalyticLabelFeedTypeChallenge = @"From Feed Type Challenge";
NSString *ENAnalyticLabelFeedTypeLearningLink = @"From Feed Type Learning Link";
NSString *ENAnalyticLabelFeedTypePaginationSpinner = @"From Feed Type Pagination Spinner";
NSString *ENAnalyticLabelFeedTypeLearningComment = @"From Feed Type Learning Comment";
NSString *ENAnalyticLabelOpen = @"Open";
NSString *ENAnalyticLabelClose = @"Close";
NSString *ENAnalyticLabelSuccess = @"Success";
NSString *ENAnalyticLabelEmailAndPassEmpty = @"Email and pass empty";
NSString *ENAnalyticLabelEmailEmpty = @"Email empty";
NSString *ENAnalyticLabelEmailNotValid = @"Email not valid";
NSString *ENAnalyticLabelPasswordEmpty = @"Password empty";
NSString *ENAnalyticLabelTry = @"Try";
