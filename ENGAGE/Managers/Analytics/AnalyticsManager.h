//
//  AnalyticsManager.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/17/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnalyticsConstants.h"

typedef enum : NSUInteger {
    FireBasePropertyTypeCompanyPlan,
    FireBasePropertyTypeUserRole
} FireBasePropertyType;

typedef enum : NSUInteger {
    AnalyticsTypeGoogleAnalytics,
    AnalyticsTypeFirebase,
    AnalyticsTypeAll
} AnalyticsType;

@interface AnalyticsManager : NSObject

//All in One Methods
+ (void)configure;
+ (void)trackEvent:(NSString *)category action:(NSString *)action label:(NSString *)label;
+ (void)userDidLogout;

//Google Analytics Methods
+ (void)trackScreen:(NSString *)screenName;
+ (void)trackEvent:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value;

//Firebase Analytics Methods
+ (void)trackScreen:(NSString *)screenName withScreenClass:(NSString *)screenClass;
+ (void)setUserProperty:(NSString *)value forPropertyType:(FireBasePropertyType) type;
+ (void)setUserId:(NSString *)value;

@end
