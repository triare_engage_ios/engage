//
//  RequestManager.m
//  
//
//  Created by Alex Kravchenko on 15.06.16.
//  Copyright © 2016 Alex Kravchenko. All rights reserved.
///

#import "RequestManager.h"
#import <AFNetworking/AFNetworking.h>
#import "AKApplication.h"

#if SHAREEXT
#else
#import "AppDelegate.h"
#endif

#define LOG_ENABLED 0

#define kNoInternetConnectionText NSLocalizedString(@"No internet connection", @"v1.0")
#define kNoConnectionToServerText NSLocalizedString(@"Could not update page, try again later", @"v1.0")
#define kNoErrorMessage @""
#define kEmptyMessage @""

@interface RequestManager ()

@property (nonatomic, strong) AFHTTPSessionManager *manager;

@end


@implementation RequestManager

#pragma mark Initialization

-(NSString*) apiCall:(NSString *) call {
    return [NSString stringWithFormat:@"%@/api/v1/%@", EN_API_SERVER_ADDRESS, call];
}

+ (instancetype)sharedManager {
    static RequestManager * instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[RequestManager alloc] init];
        
        instance.manager = [AFHTTPSessionManager manager];
        instance.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [instance.manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];

        [instance.manager.requestSerializer setValue:[AKApplication appVersion] forHTTPHeaderField:@"app_version"];
        [instance.manager.requestSerializer setValue:[AKApplication bundleId] forHTTPHeaderField:@"app_id"];
        //instance.manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        
        //[instance startReachabilityMonitoring];
        
    });
    
    return instance;
}
/*
-(void) startReachabilityMonitoring {
    AFNetworkReachabilityManager *reachabilityManager = [AFNetworkReachabilityManager managerForDomain:@"www.google.com"];
    [reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        if (status == AFNetworkReachabilityStatusReachableViaWWAN ||
            status == AFNetworkReachabilityStatusReachableViaWiFi ) {
            [self networkBecomeReachable];
        }
    }];
    
    self.manager.reachabilityManager = reachabilityManager;
    [self.manager.reachabilityManager startMonitoring];
}

-(void) networkBecomeReachable {
    if ([self isAuthorize]) {
        [DataManager sendLocalChangesToServer];
        [[PurchaseController sharedInstance] sendSavedReceipts];
        [[PurchaseController sharedInstance] requestProductsListIfEmpty];
    }
}
*/

-(NSString *) headerToken {
    return [self.manager.requestSerializer valueForHTTPHeaderField:@"X-API-AUTH-TOKEN"];
}



#pragma mark Parent Requests

- (void) setApiToken:(NSString *) token email:(NSString *) email {
    [self.manager.requestSerializer setValue:token forHTTPHeaderField:@"X-API-AUTH-TOKEN"];
    [self.manager.requestSerializer setValue:email forHTTPHeaderField:@"X-API-EMAIL"];
}

- (BOOL) isAuthorize {
    return ([self.manager.requestSerializer valueForHTTPHeaderField:@"X-API-AUTH-TOKEN"].length);
}

- (void)  clearApiToken {
    [self.manager.requestSerializer setValue:nil forHTTPHeaderField:@"X-API-AUTH-TOKEN"];
    [self.manager.requestSerializer setValue:nil forHTTPHeaderField:@"X-API-EMAIL"];
}

- (void) checkIsAuthorize:(NSError *) error {
    
#if SHAREEXT
#else
    if (error.code == -1011 && [error.userInfo valueForKey:NSLocalizedDescriptionKey]) {
        NSString *description = [[[error.userInfo valueForKey:NSLocalizedDescriptionKey] componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        

        
        if ([description isEqualToString:@"401"]) {
            [[[AppDelegate theApp] mainViewController] logoutUser];
            return;
        }
    }
#endif
}

- (void)POST:(NSString *)URLString
  parameters:(id)parameters
     success:(void (^)(NSURLSessionTask *task, id responseObject))success
     failure:(void (^)(NSURLSessionTask *task, NSError *error))failure {
    
    [self log:[NSString stringWithFormat:@"POST: %@", URLString]];
    
    if (![self isAuthorize]) {
        [self logNoAuthorizeError:URLString];
        if (failure) failure (nil, nil);
        return;
    }
    
    [self.manager POST:URLString parameters:parameters progress:nil success:^(NSURLSessionDataTask * task, id responseObject) {
        [self logRequestAnswer:responseObject url:URLString];
        
        if (success) success(task, responseObject);
    } failure:^(NSURLSessionDataTask * task, NSError * error) {
        [self logRequestError:error url:URLString];
        
        [self checkIsAuthorize:error];
        if (failure) failure (task, error);
    }];
}

- (void) GET:(NSString *)URLString
  parameters:(id)parameters
     success:(void (^)(NSURLSessionTask *task, id responseObject))success
     failure:(void (^)(NSURLSessionTask *task, NSError *error))failure {
    
    [self log:[NSString stringWithFormat:@"GET: %@", URLString]];

    if (![self isAuthorize]) {
        [self logNoAuthorizeError:URLString];
        if (failure) failure (nil, nil);
        return;
    }
    
    [self.manager GET:URLString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [self logRequestAnswer:responseObject url:URLString];
        
        if (success) success(task, responseObject);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        [self logRequestError:error url:URLString];
        
        [self checkIsAuthorize:error];
        if (failure) failure (task, error);
    }];
}

- (void) PATCH:(NSString *)URLString
  parameters:(id)parameters
     success:(void (^)(NSURLSessionTask *task, id responseObject))success
     failure:(void (^)(NSURLSessionTask *task, NSError *error))failure {
    
    [self log:[NSString stringWithFormat:@"PATCH: %@", URLString]];
    
    if (![self isAuthorize]) {
        [self logNoAuthorizeError:URLString];
        if (failure) failure (nil, nil);
        return;
    }
    
    [self.manager PATCH:URLString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self logRequestAnswer:responseObject url:URLString];
        
        if (success) success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self logRequestError:error url:URLString];
        
        if (failure) failure (task, error);
    }];
}

- (void) PUT:(NSString *)URLString
    parameters:(id)parameters
       success:(void (^)(NSURLSessionTask *task, id responseObject))success
       failure:(void (^)(NSURLSessionTask *task, NSError *error))failure {
    
    [self log:[NSString stringWithFormat:@"PUT: %@", URLString]];
    
    if (![self isAuthorize]) {
        [self logNoAuthorizeError:URLString];
        if (failure) failure (nil, nil);
        return;
    }
    
    [self.manager PUT:URLString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self logRequestAnswer:responseObject url:URLString];
        
        if (success) success(task, responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self logRequestError:error url:URLString];
        
        if (failure) failure (task, error);
    }];
}

- (void)DELETE:(NSString *)URLString
  parameters:(id)parameters
     success:(void (^)(NSURLSessionTask *task, id responseObject))success
       failure:(void (^)(NSURLSessionTask *task, NSError *error))failure {
    
    [self log:[NSString stringWithFormat:@"DELETE: %@", URLString]];
    
    if (![self isAuthorize]) {
        [self logNoAuthorizeError:URLString];
        if (failure) failure (nil, nil);
        return;
    }
    
    [self.manager DELETE:URLString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) success(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) failure (task, error);
    }];
}











#pragma mark Request helpers

-(NSString *) errorTextFromResponse:(NSError *) error {
    
    if (error.code == kCFURLErrorNotConnectedToInternet) {
        return kNoInternetConnectionText;
    }
    
    if (error.code == kCFURLErrorTimedOut) {
        return kEmptyMessage;
    }
    
    if (error.code == 9) {
        return kEmptyMessage;
    }
    
//    if (error.code == kCFURLErrorBadServerResponse) {
//        return kNoConnectionToServerText;
//    }
    
    if (error == nil) {
        return kNoErrorMessage;
    }
    
    id responseData = [error.userInfo valueForKey:@"com.alamofire.serialization.response.error.data"];
    
    if (responseData == nil && [error.userInfo valueForKey:NSLocalizedDescriptionKey]) {
        return [error.userInfo valueForKey:NSLocalizedDescriptionKey];
    }
    
    //NSString *jsonString = [NSString stringWithUTF8String:[responseData bytes]];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    
    NSString *text = [json valueForKey:@"error"];
    if (text == nil) {
        text = [json valueForKey:@"errors"];
    }
    
    if (text == nil) {
        text = kNoConnectionToServerText;
    }
    
    return text;
}


-(void) log:(NSString *) log {
    if (LOG_ENABLED) MLog(log);
}

-(void) logRequestAnswer:(id) responseObject url:(NSString *) url {
    if (LOG_ENABLED) MLog(@"Request %@ answer: %@", url, responseObject);
}

-(void) logRequestError:(NSError *) error url:(NSString *) url {
    if (LOG_ENABLED) MLog(@"Request %@ error: %@", url, error);
}

-(void) logNoAuthorizeError:(NSString *) url {
    if (LOG_ENABLED) MLog(@"Request %@ canceled: not authorize", url);
}

-(void) logHeaders:(NSURLSessionTask *) task {
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
    if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
        NSDictionary *dictionary = [httpResponse allHeaderFields];
        MLog([dictionary description]);
    }
}






#pragma mark Requests


-(void) downloadFile:(NSString *) url destination:(NSString *) destination progress:(void (^)(NSProgress *progress)) progress onCompleate:(void (^)(NSURL *filePath, NSError *error)) onCompleate {
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];

    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:progress destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        //NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        //return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        
        return [NSURL fileURLWithPath:destination];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (onCompleate) onCompleate(filePath, error);
    }];
    [downloadTask resume];
}



-(void) authorizeWithEmail:(NSString *) email password:(NSString *) password success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSDictionary *parameters = @{@"session":@{@"email":email, @"password":password}};
    
    [self.manager POST:[self apiCall:@"sessions"] parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *info = [responseObject objectForKey:@"user"];
        if (onSuccess) onSuccess(info);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure([self errorTextFromResponse:error]);
    }];
}


-(void) getTeam:(void (^)(BOOL success, NSArray *list)) onCompleate {
    
    [self GET:[self apiCall:@"teams"] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES, [responseObject valueForKeyPath:@"team.users"]);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil);
    }];
}

-(void) getBehaviours:(void (^)(BOOL success, NSArray *list)) onCompleate {
    
    [self GET:[self apiCall:@"behaviours"] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES, [responseObject valueForKeyPath:@"behaviours"]);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil);
    }];
}

-(void) getValuesListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    [self GET:[self apiCall:@"behaviours"] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) onSuccess ([responseObject valueForKeyPath:@"values"]);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure ([self errorTextFromResponse:error]);
    }];
}

-(void) getEvaluationsForUser:(NSNumber *) userId complete:(void (^)(BOOL success, NSArray *list, NSString *errorMessage)) onCompleate {
    
    NSString *call = [NSString stringWithFormat:@"users/%@/evaluations", userId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES, [responseObject valueForKeyPath:@"behaviours"], nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil, [self errorTextFromResponse:error]);
    }];
}

-(void) giveTap:(NSDictionary *) parameters onCompleate:(void (^)(BOOL success, NSString *errorMessage)) onCompleate {
    
    NSDictionary *info = [NSDictionary dictionaryWithObject:parameters forKey:@"tap"];
    
    [self POST:[self apiCall:@"taps"] parameters:info success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES, nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, [self errorTextFromResponse:error]);
    }];
}

-(void) getActivityFeedForPage:(NSInteger) page onCompleate:(void (^)(BOOL success, PaginationInfo *pagination, NSDictionary *info, NSString *errorMessage)) onCompleate {
    
    NSString *url = [NSString stringWithFormat:@"%@?page=%ld", [self apiCall:@"feeds"], (long)page];
    
    [self GET:url parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        
        if (onCompleate) onCompleate(YES, pInfo, responseObject, nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil, nil, [self errorTextFromResponse:error]);
    }];
}

-(void) searchInActivityFeed:(NSString *) query page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination)) onCompleate {
    
    NSDictionary *parameters = @{@"page":[NSNumber numberWithInteger:page], @"query":query};
    
    [self GET:[self apiCall:@"feeds/search"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *feeds = [responseObject valueForKeyPath:@"feeds"];
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        
        if (onCompleate) onCompleate(YES, feeds, pInfo);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        [self errorTextFromResponse:error];
        if (onCompleate) onCompleate(NO, nil, nil);
    }];
}

-(void) retapFeed:(NSNumber *) feedId onCompleate:(void (^)(BOOL success, NSString *errorMessage)) onCompleate {
    
    NSDictionary *parameters = @{@"retap":@{@"achievement_id":feedId}};
    
    [self POST:[self apiCall:@"retaps"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        
        if (onCompleate) onCompleate(YES, nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, [self errorTextFromResponse:error]);
    }];
}

-(void) getRetaps:(NSNumber *) feedId page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage)) onCompleate {
    
    NSDictionary *parameters = @{@"page":[NSNumber numberWithInteger:page], @"tap_id":feedId};
    
    [self GET:[self apiCall:@"retaps"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *feeds = [responseObject valueForKeyPath:@"retaps"];
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        
        if (onCompleate) onCompleate(YES, feeds, pInfo, nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil, nil, [self errorTextFromResponse:error]);
    }];
}

-(void) getUserProfile:(NSNumber *) userId success:(void (^)(NSDictionary *userInfo, NSArray *behaviors)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/%@", userId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *user = [responseObject valueForKey:@"user"];
        NSArray *behaviors = [responseObject valueForKey:@"received_behaviours"];
        if (onSuccess) onSuccess(user, behaviors);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure([self errorTextFromResponse:error]);
    }];
}

-(void) updateUserWithInfo:(NSDictionary *) info success:(void (^)(BOOL success, NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users"];
    
    NSDictionary *parameters = @{@"user":info};
    
    [self PATCH:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *user = [responseObject valueForKey:@"user"];
        
        if (onSuccess) {
            onSuccess (YES, user);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure([self errorTextFromResponse:error]);
    }];
}

-(void) updateUserAvatarWithImagePath:(NSString *) imagePath success:(void (^)(BOOL success, NSString *avatarUrl)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/update_avatar"];
    
    NSURL *filePath = [NSURL fileURLWithPath:imagePath];
    
    NSMutableURLRequest *request = [self.manager.requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:[self apiCall:call] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"avatar" fileName:@"image.jpeg" mimeType:@"image/jpeg" error:nil];
    } error:nil];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [self.manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          [self checkIsAuthorize:error];
                          if (onFailure) onFailure([self errorTextFromResponse:error]);
                      } else {
                          NSString *newAvatarUrl = [responseObject valueForKey:@"avatar_url"];
                          if (onSuccess) onSuccess(YES, newAvatarUrl);
                      }
                  }];
    
    [uploadTask resume];
}

-(void) checkPassword:(NSString *)password success:(void (^)(BOOL success, BOOL isCorrect, NSString *password)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/check_password"];
    
    NSDictionary *parameters = @{@"current_password":password};
    
    [self POST:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        
        NSNumber *correctNumber = [responseObject valueForKey:@"correct"];
        
        BOOL correct = [correctNumber boolValue];
        
        if (onSuccess) {
            onSuccess (YES, correct, password);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure([self errorTextFromResponse:error]);
    }];
}

-(void) getUserBehaviors:(NSNumber *) userId sort:(NSDictionary *) parameters success:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/%@/behaviours", userId];
    
    [self GET:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) onSuccess([responseObject valueForKey:@"behaviours"]);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure([self errorTextFromResponse:error]);
    }];
}

-(void) getCompanyJobsOnSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"jobs"];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess ([responseObject valueForKey:@"jobs"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) getUserLeaderboard:(NSDictionary *)parameters onCompleate:(void (^)(BOOL success, NSArray *list, NSString *errorMessage)) onCompleate {
    NSDictionary *p;
    
    NSString *from = [parameters valueForKey:@"from"];
    NSString *to = [parameters valueForKey:@"to"];
    
    if (from && to) {
        p = @{@"from_date": from, @"to_date": to};
    }
    
    [self GET:[self apiCall:@"leaderboards/users"] parameters:p success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES, [responseObject valueForKeyPath:@"users"], nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil, [self errorTextFromResponse:error]);
    }];
}

-(void) getBehaviorLeaderboard:(NSDictionary *)parameters onCompleate:(void (^)(BOOL success, NSArray *list, NSString *errorMessage)) onCompleate {
    
    NSDictionary *p;
    
    NSString *from = [parameters valueForKey:@"from"];
    NSString *to = [parameters valueForKey:@"to"];
    
    if (from && to) {
        p = @{@"from_date": from, @"to_date": to};
    }
    
    [self GET:[self apiCall:@"leaderboards/behaviours"] parameters:p success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES, [responseObject valueForKeyPath:@"behaviours"], nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil, [self errorTextFromResponse:error]);
    }];
}

-(NSString *) stringDate:(NSDate *) date {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    
    return [dateFormat stringFromDate:date];
}

-(void) sendSuggestion:(NSDictionary *) parameters onCompleate:(void (^)(BOOL success, NSString *errorMessage)) onCompleate {
    
    NSDictionary *info = [NSDictionary dictionaryWithObject:parameters forKey:@"suggestion"];
    
    [self POST:[self apiCall:@"suggestions"] parameters:info success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES, nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, [self errorTextFromResponse:error]);
    }];
}

-(void) getSuggestions:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage)) onCompleate {
    
    NSDictionary *parameters = @{@"page":[NSNumber numberWithInteger:page]};
    
    [self GET:[self apiCall:@"suggestions"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *feeds = [responseObject valueForKeyPath:@"suggestions"];
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        
        if (onCompleate) onCompleate(YES, feeds, pInfo, nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil, nil, [self errorTextFromResponse:error]);
    }];
}

-(void) getEvaluations:(NSNumber *) userId onCompleate:(void (^)(BOOL success, NSArray *list)) onCompleate {
    
    NSString *call = [NSString stringWithFormat:@"evaluations/%@", userId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *list = [responseObject valueForKeyPath:@"behaviours"];

        if (onCompleate) onCompleate(YES, list);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil);
    }];
}

-(void) changeEvaluations:(NSDictionary *) parameters onCompleate:(void (^)(BOOL success)) onCompleate {
    
    [self POST:[self apiCall:@"evaluations"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onCompleate) onCompleate(YES);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO);
    }];
}

-(void) getBehaviorUsersListsByBehaviorId:(NSNumber *) behaviorId success:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"behaviours/%@", behaviorId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *list = [[responseObject valueForKey:@"behaviour"] valueForKey:@"users"];
        if (onSuccess) {
            onSuccess (list);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

-(void) getTeamMessageListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"team_messages"];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *list = [responseObject valueForKey:@"team_messages"];
        if (onSuccess) {
            onSuccess (list);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) getSelfUserSuccess:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users"];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess ([responseObject valueForKeyPath:@"user"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Questions
////////////////////////////////////////////////////////////////////

-(void) sendQuestionWithInfo:(NSDictionary *) info success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSDictionary *parameters = @{@"question":info};
    
    [self POST:[self apiCall:@"questions"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) getQuestionsListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"questions"];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *list = [responseObject valueForKeyPath:@"questions"];
        if (onSuccess) {
            onSuccess (list);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

-(void) deleteQuestionById:(NSNumber *) questionId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"questions/%@", questionId];
    
    [self DELETE:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess(YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) giveAnswerForQuestionId:(NSNumber *) questionId withInfo:(NSDictionary *)info success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"questions/%@/answer", questionId];
    
    NSDictionary *parameters = @{@"answer":info};
    
    [self POST:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Learnings
//////////////////////////////////////////////////////////////////////

-(void) createLearningWithInfo:(NSDictionary *) info success:(void (^)(NSDictionary *result)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"learnings"];
    
    [self POST:[self apiCall:call] parameters:info success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess([responseObject valueForKey:@"learning"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) getLearningsForPage:(NSInteger) page success:(void (^)(PaginationInfo *pagination, NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *url = [NSString stringWithFormat:@"%@?page=%ld", [self apiCall:@"learnings"], (long)page];
    
    [self GET:url parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        if (onSuccess) {
            onSuccess (pInfo, [responseObject valueForKey:@"learnings"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) evaluateLearningWithId:(NSNumber *) learningId andVote:(NSNumber *) vote success:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"learnings/%@/evaluate", learningId];
    NSDictionary *parameters = @{@"vote":vote};
    
    [self PUT:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess([responseObject valueForKey:@"learning"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) searchInLearningFeed:(NSString *) query page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination)) onCompleate {
    
    NSDictionary *parameters = @{@"page":[NSNumber numberWithInteger:page], @"query":query};
    
    [self GET:[self apiCall:@"learnings/search"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *learnings = [responseObject valueForKeyPath:@"learnings"];
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        
        if (onCompleate) onCompleate(YES, learnings, pInfo);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        [self errorTextFromResponse:error];
        if (onCompleate) onCompleate(NO, nil, nil);
    }];
    
}

-(void) deleteLearningWithId:(NSNumber *) learningId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"learnings/%@", learningId];
    
    [self DELETE:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Challenges
//////////////////////////////////////////////////////////////////////

-(void) getChallengeWithId:(NSNumber *) challengeId success:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"challenges/%@", challengeId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess(responseObject);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

- (void) challengeWasShown:(NSNumber *) challengeId success:(void (^)(BOOL isSuccess)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"challenges/%@/mark_viewed", challengeId];
    
    [self PUT:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess(YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

- (void) getCurrentChallengeSuccess:(void (^)(BOOL isNotEmpty, NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"challenges/current"];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSDictionary *challenge = [responseObject valueForKey:@"challenge"];
        
        BOOL isNotEmpty = NO;
        
        if ([challenge valueForKey:@"title"]) {
            isNotEmpty = YES;
        }
        
        if (onSuccess) {
            onSuccess (isNotEmpty, challenge);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

- (void) getChallengeThatWonUser:(NSNumber *) userId success:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/%@/challenge_victories", userId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *list = [responseObject valueForKey:@"challenges"];
        
        if (onSuccess) {
            onSuccess (list);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Retaps
//////////////////////////////////////////////////////////////////////

-(void) deleteRetapWithId:(NSNumber *) retapId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"retaps/%@", retapId];
    
    [self DELETE:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Comments
//////////////////////////////////////////////////////////////////////

-(void) getComments:(NSNumber *) feedId page:(NSInteger) page onCompleate:(void (^)(BOOL success, NSArray *list, PaginationInfo *pagination, NSString *errorMessage)) onCompleate {
    
    NSDictionary *parameters = @{@"page":[NSNumber numberWithInteger:page], @"tap_id":feedId};
    
    [self GET:[self apiCall:@"comments"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *feeds = [responseObject valueForKeyPath:@"comments"];
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        
        if (onCompleate) onCompleate(YES, feeds, pInfo, nil);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onCompleate) onCompleate(NO, nil, nil, [self errorTextFromResponse:error]);
    }];
}

-(void) addComment:(NSString *) comment feedId:(NSNumber *) feedId success:(void (^)(NSDictionary *info)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSDictionary *parameters = @{@"comment":@{@"body":comment, @"achievement_id":feedId}};
    
    [self POST:[self apiCall:@"comments"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) onSuccess([responseObject valueForKey:@"comment"]);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure([self errorTextFromResponse:error]);
    }];
}

-(void) deleteCommentWithId:(NSNumber *) commentId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"comments/%@", commentId];
    
    [self DELETE:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Taps
//////////////////////////////////////////////////////////////////////

-(void) deleteTapWithId:(NSNumber *) tapId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    
    NSString *call = [NSString stringWithFormat:@"taps/%@", tapId];
    
    [self DELETE:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Team Message
//////////////////////////////////////////////////////////////////////

-(void) sendTeamMessage:(NSString *) message withEmail:(BOOL) emailState success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    [body setValue:message forKey:@"body"];
    
    if (emailState) {
        [body setValue:@YES forKey:@"send_to_emails"];
    }
    
    NSDictionary *parameters = @{@"team_message":body};
    
    [self POST:[self apiCall:@"team_messages"] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

- (void) deleteTeamMessageWithId:(NSNumber *) messageId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    [self deleteTapWithId:messageId success:^(BOOL success) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSString *message) {
        if (onFailure) {
            onFailure (message);
        }
    }];
}

//////////////////////////////////////////////////////////////////////
#pragma mark - User
//////////////////////////////////////////////////////////////////////

-(void) registerForPushWithToken:(NSData *) token success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"devices"];
    
    NSString *deviceToken = [[token description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    if (deviceToken.length < 1) {
        deviceToken = @"";
    }
    
    NSDictionary *parameters = @{@"device":@{@"key":deviceToken, @"system":@1}};
    
    [self POST:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess(YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) unsubscribeFromPushWithToken:(NSString *) token success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    //delete space in token
    NSString *tokenWithoutSpace = [token stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSString *call = [NSString stringWithFormat:@"devices/%@", tokenWithoutSpace];
    
    [self DELETE:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Behaviours
//////////////////////////////////////////////////////////////////////

-(void) createBehaviorWithInfo:(NSDictionary *) info success:(void (^)(BOOL success, NSNumber *behaviorId)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"behaviours"];
    
    NSDictionary *parameters = @{@"behaviour":info};
    
    [self POST:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES, [[responseObject valueForKey:@"behaviour"] valueForKey:@"id"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

- (void) createBehaviorWithInfo:(NSDictionary *) info andIconId:(NSNumber *) iconId success:(void (^)(BOOL success, NSNumber *behaviorId)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"behaviours"];
    
    NSDictionary *parameters = @{@"icon_id":iconId, @"behaviour":info};
    
    [self POST:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES, [[responseObject valueForKey:@"behaviour"] valueForKey:@"id"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

-(void) updateBehaviorIconWithImagePath:(NSString *) imagePath andBehaviorId:(NSNumber *) behaviorId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"behaviours/%@/update_icon", behaviorId];
    
    NSURL *filePath = [NSURL fileURLWithPath:imagePath];
    
    NSMutableURLRequest *request = [self.manager.requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:[self apiCall:call] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"icon" fileName:@"image.jpeg" mimeType:@"image/jpeg" error:nil];
    } error:nil];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [self.manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          [self checkIsAuthorize:error];
                          if (onFailure) onFailure([self errorTextFromResponse:error]);
                      } else {
                          if (onSuccess) onSuccess(YES);
                      }
                  }];
    
    [uploadTask resume];
}

-(void) deactivateBehaviorWithId:(NSNumber *) behaviorId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"behaviours/%@/deactivate", behaviorId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) getBehaviorIconsForPage:(NSInteger) page iconsPerPage:(NSInteger) iconsPerPage success:(void (^)(NSArray *list, PaginationInfo *pagination)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *url = [NSString stringWithFormat:@"%@?page=%ld&per_page=%ld", [self apiCall:@"icons"], (long)page, (long)iconsPerPage];
    
    [self GET:url parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *iconsList = [responseObject valueForKey:@"icons"];
        
        PaginationInfo *pInfo = [[PaginationInfo alloc] init];
        pInfo.currentPage = ((NSNumber *)[responseObject valueForKeyPath:@"current_page"]).integerValue;
        pInfo.totalPages = ((NSNumber *)[responseObject valueForKeyPath:@"total_pages"]).integerValue;
        pInfo.elementsPerPage = ((NSNumber *)[responseObject valueForKeyPath:@"per_page"]).integerValue;
        
        if (onSuccess) {
            onSuccess (iconsList, pInfo);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

//////////////////////////////////////////////////////////////////////
#pragma mark - Values
//////////////////////////////////////////////////////////////////////

-(void) createValueWithInfo:(NSDictionary *) info success:(void (^)(BOOL success, NSNumber *valueId)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"values"];
    
    NSDictionary *parameters = @{@"value":info};
    
    [self POST:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES, [[responseObject valueForKey:@"value"] valueForKey:@"id"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
    
}

-(void) updateValueIconWithImagePath:(NSString *) imagePath andValueId:(NSNumber *) valueId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"values/%@/update_icon", valueId];
    
    NSURL *filePath = [NSURL fileURLWithPath:imagePath];
    
    NSMutableURLRequest *request = [self.manager.requestSerializer multipartFormRequestWithMethod:@"PUT" URLString:[self apiCall:call] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:filePath name:@"icon" fileName:@"image.jpeg" mimeType:@"image/jpeg" error:nil];
    } error:nil];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [self.manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          [self checkIsAuthorize:error];
                          if (onFailure) onFailure([self errorTextFromResponse:error]);
                      } else {
                          if (onSuccess) onSuccess(YES);
                      }
                  }];
    
    [uploadTask resume];
}

-(void) getValuesTitlesAndIdsSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"values"];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *list = [responseObject valueForKeyPath:@"values"];
        
        if (onSuccess) {
            onSuccess (list);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}


//////////////////////////////////////////////////////////////////////
#pragma mark - Team User
//////////////////////////////////////////////////////////////////////

-(void) getTeamUsersListSuccess:(void (^)(NSArray *list)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users"];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess ([responseObject valueForKey:@"users"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) deleteFromCompanyTeamUser:(NSNumber *) userId success:(void (^)(BOOL success)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/%@", userId];
    
    [self DELETE:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess (YES);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) inviteTeamUserToCompanyWithEmail:(NSString *) email success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"invitations"];
    
    NSDictionary *parameters = @{@"invitation":@{@"email":email}};
    
    [self POST:[self apiCall:call] parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess ([responseObject valueForKey:@"user"]);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) giveManagerRightsToTeamUser:(NSNumber *) userId success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/%@/add_manager", userId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) {
            onSuccess(responseObject);
        }
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) {
            onFailure ([self errorTextFromResponse:error]);
        }
    }];
}

-(void) removeManagerRightsFromTeamUser:(NSNumber *) userId success:(void (^)(NSDictionary *userInfo)) onSuccess failure:(void (^)(NSString *message)) onFailure {
    
    NSString *call = [NSString stringWithFormat:@"users/%@/remove_manager", userId];
    
    [self GET:[self apiCall:call] parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (onSuccess) onSuccess (responseObject);
    } failure:^(NSURLSessionTask *task, NSError *error) {
        if (onFailure) onFailure ([self errorTextFromResponse:error]);
    }];
}




@end
