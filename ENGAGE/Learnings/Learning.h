//
//  Learning.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    LearningValidationErrorNone,
    LearningValidationErrorNotValidLink,
    LearningValidationErrorEmptyFields
} LearningValidationError;

@interface Learning : NSObject

@property (nonatomic, strong) NSString *linkURL;
@property (nonatomic, strong) NSString *thoughts;

- (NSDictionary *) serialize;
- (BOOL) isValid;
- (LearningValidationError) validationInfo;

@end
