//
//  LearningCreateVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface LearningCreateVC : ContentViewController

+(NSString *) storyboardIdentifier;

@end
