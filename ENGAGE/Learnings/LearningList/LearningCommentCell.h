//
//  LearningCommentCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>

@interface LearningCommentCell : SWTableViewCell
@property (nonatomic, copy) void (^onShowMore)(LearningCommentCell *cell);
@property (nonatomic, copy) void (^onShowSenderInfo)(LearningCommentCell *cell);
@property (nonatomic, copy) void (^onShowVote)(LearningCommentCell *cell);

+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;
+(CGFloat) cellHeightForLearning:(CDLearning *) learning fullInfo:(BOOL) fullInfo;
-(void) addLearning:(CDLearning *) learning fullInfo:(BOOL) fullInfo;

@end
