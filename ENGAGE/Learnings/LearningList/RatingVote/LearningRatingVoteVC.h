//
//  LearningRatingVoteVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface LearningRatingVoteVC : ContentViewController

/**
 Need to set methods, that must be invoke after successfully voting of learning.
 */
@property (nonatomic, copy) void (^onBackAction)();

/**
 Need to set learning, that must be opened in screen.
 */
@property (nonatomic, strong) CDLearning *learning;

/**
 Need to set YES - if show screen from LearningListVC
 Need to set NO - if show screen from ActivityFeedVC
 */
@property (nonatomic) BOOL isFromLearningList;

/**
 Used when init VC and open it.

 @return storyboard identifier
 */
+(NSString *) storyboardIdentifier;

@end
