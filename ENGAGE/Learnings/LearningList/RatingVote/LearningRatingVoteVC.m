//
//  LearningRatingVoteVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/14/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "LearningRatingVoteVC.h"
#import "ContentScrollView.h"
#import "ENButton.h"

#import <SVProgressHUD/SVProgressHUD.h>

@interface LearningRatingVoteVC ()
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

#pragma mark - Header View
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *circleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

#pragma mark - Stars View
@property (weak, nonatomic) IBOutlet UIView *starView;
@property (weak, nonatomic) IBOutlet UIButton *firstStarButton;
@property (weak, nonatomic) IBOutlet UIButton *secondStarButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdStarButton;
@property (weak, nonatomic) IBOutlet UIButton *fourthStarButton;
@property (weak, nonatomic) IBOutlet UIButton *fifthStarButton;
@property (nonatomic, strong) NSArray *buttonList;

#pragma mark - Action View
@property (weak, nonatomic) IBOutlet ENButton *actionButton;

#pragma mark - Data
@property (nonatomic) NSInteger rating;

@end

@implementation LearningRatingVoteVC

#pragma mark - Public Methods

+(NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareHeaderView];
    [self prepareNavigationBar];
    [self prepareActionButton];
    
    [self updateScreenForStarNumber:[CDLearning starsFromRating:self.learning.userRating.integerValue]];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenLearningRateVote];
    [AnalyticsManager trackScreen:ENAnalyticScreenLearningRateVote withScreenClass:NSStringFromClass([self class])];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Prepare Methods

- (void)prepareHeaderView {
    
    self.headerView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    
    self.circleLabel.layer.cornerRadius = self.circleLabel.frame.size.width / 2;
    self.circleLabel.clipsToBounds = YES;
    
    self.circleLabel.layer.borderColor = [UIColor whiteColor].CGColor;
    self.circleLabel.layer.borderWidth = 1.0;

    self.circleLabel.textColor = [UIColor whiteColor];
    
    self.ratingLabel.textColor = [UIColor whiteColor];
    self.ratingLabel.font = [UIFont appFont:13];
}

- (void) prepareNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:[NSLocalizedString(@"rating", @"v1.0") uppercaseString]];
    [self.navigationBar showLeftButton];
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
}

-(void) prepareActionButton {
    
    [self.actionButton setTitle:[NSLocalizedString(@"rate", @"v1.0") uppercaseString] forState:UIControlStateNormal];
}

#pragma mark - Action

- (IBAction)starButtonPressed:(UIButton *)sender {
    
    NSInteger senderTag = sender.tag;
    senderTag = senderTag - 1110;
    
    if (senderTag < 1 || senderTag > 5) {
        return;
    }
    
    [self updateScreenForStarNumber:senderTag];
}

- (IBAction)actionButtonPressed:(UIButton *)sender {

    __weak typeof(self) weakSelf = self;
    
    if (self.rating == 0) {
        [self showAlertWithMessage:NSLocalizedString(@"Please rate", @"v1.0")];
        return;
    }
    
    self.actionButton.userInteractionEnabled = NO;
    [[RequestManager sharedManager] evaluateLearningWithId:self.learning.feedId andVote:[NSNumber numberWithInteger:self.rating] success:^(NSDictionary *info) {
        [DataManager updateLearningWithInfo:info fromLearningList:weakSelf.isFromLearningList];
        [weakSelf hideLoadingView];
        [weakSelf closeVC];
        weakSelf.actionButton.userInteractionEnabled = YES;
        [weakSelf showAlertLearningVoted];
        if (weakSelf.onBackAction) {
            weakSelf.onBackAction ();
        }
    } failure:^(NSString *message) {
        weakSelf.actionButton.userInteractionEnabled = YES;
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithMessage:message];
    }];
     
    
}

- (void) updateScreenForStarNumber:(NSInteger) starNumber {
    [self updateCircleForStarNumber:starNumber];
    [self updateRatingLabelForStarNumber:starNumber];
    [self updateStarsButtonsForStarNumber:starNumber];
    [self updateCurrentRatingToStarNumber:starNumber];
}

- (void) updateCurrentRatingToStarNumber:(NSInteger) starNumber {
    
    if (starNumber < 0 || starNumber > 5) {
        return;
    }
    
    self.rating = [CDLearning ratingFromStars:starNumber];
}

- (void) closeVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Properties

-(NSArray *) buttonList {
    if (_buttonList == nil) {
        NSMutableArray *buttons = [NSMutableArray new];
        [buttons addObject:self.firstStarButton];
        [buttons addObject:self.secondStarButton];
        [buttons addObject:self.thirdStarButton];
        [buttons addObject:self.fourthStarButton];
        [buttons addObject:self.fifthStarButton];
        _buttonList = buttons;
    }
    
    return _buttonList;
}

#pragma mark - Change UI

- (void) updateCircleForStarNumber:(NSInteger) starNumber {
    
    if (starNumber < 0 || starNumber > 5) {
        return;
    }
    
    self.circleLabel.text = [NSString stringWithFormat:@"%ld", (long)starNumber];
    
}

- (void) updateRatingLabelForStarNumber:(NSInteger) starNumber {
    
    switch (starNumber) {
        case 0:
            self.ratingLabel.text = NSLocalizedString(@"Rate the article", @"v1.0");
            break;
            
        case 1:
            self.ratingLabel.text = NSLocalizedString(@"Highly not recommended", @"v1.0");
            break;
            
        case 2:
            self.ratingLabel.text = NSLocalizedString(@"Not recommended", @"v1.0");
            break;
            
        case 3:
            self.ratingLabel.text = NSLocalizedString(@"Good", @"v1.0");
            break;
            
        case 4:
            self.ratingLabel.text = NSLocalizedString(@"Recommended", @"v1.0");
            break;
            
        case 5:
            self.ratingLabel.text = NSLocalizedString(@"Highly recommended", @"v1.0");
            break;
            
        default:
            break;
    }
    
}

- (void) updateStarsButtonsForStarNumber:(NSInteger) starNumber {
    
    if (starNumber == 0) {
        [self deselectAllStars];
        return;
    }
    
    if (starNumber > 0 && starNumber <= 5) {
        [self deselectAllStars];
        for (int i = 0; i < starNumber; i++) {
            [self updateButtonNumber:i toFillState:YES];
        }
        return;
    }
    
}

- (void) updateButtonNumber:(NSInteger) number toFillState:(BOOL)state {
    
    NSString *imageName = @"learning-star33";
    
    if (state) {
        imageName = @"learning-filled-star33";
    }
    
    UIButton *button = [self.buttonList objectAtIndex:number];
    [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}

- (void) deselectAllStars {
    
    for (int i = 0; i < self.buttonList.count; i++ ) {
        [self updateButtonNumber:i toFillState:NO];
    }
    
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) text {
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}

-(void) showAlertLearningVoted {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Successfully vote", @"v1.0")];
}


@end
