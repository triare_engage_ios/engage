//
//  LearningLinkCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "LearningLinkCell.h"
#import "ENCircleImageView.h"
#import "NSString+URLValidation.h"

#define kDefaultBorderWidth 1.0
#define kDefaultCellIdentifier @"learningLinkCell"

@interface LearningLinkCell ()

#pragma mark - User View
@property (weak, nonatomic) IBOutlet ENCircleImageView *senderAvatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *senderAvatarButton;
@property (weak, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderJobLabel;
@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *createDateWidth;
//@property (weak, nonatomic) IBOutlet UILabel *totalRatingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *totalRatingImageView;
@property (weak, nonatomic) IBOutlet UIButton *totalRatingButton;
@property (nonatomic, strong) NSNumber *borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

#pragma mark - Site Image View
@property (weak, nonatomic) IBOutlet UIView *siteView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *siteViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *siteImageView;
@property (weak, nonatomic) IBOutlet UIButton *siteImageButton;

#pragma mark - Site Url View
@property (weak, nonatomic) IBOutlet UIView *siteUrlView;
@property (weak, nonatomic) IBOutlet UILabel *siteTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *urlLinkLabel;
@property (weak, nonatomic) IBOutlet UIButton *siteUrlButton;

#pragma mark - Comment View
@property (weak, nonatomic) IBOutlet UIView *commentSeparatorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorViewTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorViewBottomSpace;

@property (weak, nonatomic) IBOutlet UILabel *userCommentLabel;
@property (weak, nonatomic) IBOutlet UIButton *showMoreButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showMoreButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showMoreButtonTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showMoreButtonBottomSpace;



@end

@implementation LearningLinkCell

#pragma mark - Public Methods

+(CGFloat) cellHeightForLearning:(CDLearning *) learning withFullInfo:(BOOL) fullInfo {

    //User comment exist
    if (learning.userComment.length > 0) {
        
        //Site image exist
        if (learning.siteImageUrl.length > 0 && [learning.siteImageUrl isValidURL]) {
            
            //Need show as full info
            if (fullInfo) {
                return [self cellHeightWithFullInfoForLearning:learning withSite:YES];
            }
            
            //Nee show as short info
            else {
                return 365;
            }
        }
        //Site image NOT exist
        else {
            
            //Full info exist
            if (fullInfo) {
                return [self cellHeightWithFullInfoForLearning:learning withSite:NO];
            }
            
            //Short info
            else {
                return 230;
            }
        }
    }
    //User comment is not exist
    else {
        if (learning.siteImageUrl.length > 0 && [learning.siteImageUrl isValidURL]) {
            return 295;
        }
        else {
            return 155;
        }
    }
}

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

-(void) addLearning:(CDLearning *) learning fullInfo:(BOOL) fullInfo {
    
    //Need to change frame size if site image not exist
    if (learning.siteImageUrl.length > 0 && [learning.siteImageUrl isValidURL]) {
        [self.siteImageView sd_setImageWithURL:[NSURL URLWithString:learning.siteImageUrl]];
        self.siteViewHeight.constant = 140;
    }
    else {
        self.siteViewHeight.constant = 5;
    }
    
    [self.senderAvatarImageView sd_setImageWithURL:[AppTemplate mediumIconURL:learning.senderAvatarUrl] placeholderImage:[TeamUser placeholder]];
    
    self.senderNameLabel.text = learning.senderFullName;
    self.senderJobLabel.text = learning.senderJobTitle;
    
    self.createDateLabel.text = [learning formattedCreateDate];
    CGSize dateSize = [self.createDateLabel sizeThatFits:CGSizeZero];
    self.createDateWidth.constant = dateSize.width;
    
    //self.totalRatingLabel.text = [NSString stringWithFormat:@"%ld%@", (long) learning.rating.integerValue, @"%"];
    self.totalRatingImageView.image = [self starImageForRatingNumber:learning.rating.integerValue];
    
    if (learning.isSenderTopPointWinnerValue) {
        self.borderWidth = [NSNumber numberWithFloat:3.0];
        self.borderColor = [UIColor avatarBorderColor];
    }
    
    if (learning.siteTitle.length > 0) {
        self.siteTitleLabel.text = learning.siteTitle;
    }
    else if (learning.siteDescription.length > 0) {
        self.siteTitleLabel.text = learning.siteDescription;
    }
    else if (learning.siteUrl.length > 0) {
        self.siteTitleLabel.text = learning.siteUrl;
    }
    else {
        self.siteTitleLabel.text = @"";
    }
    
    self.siteTitleLabel.text = learning.siteTitle;
    
    if (learning.userComment.length > 0) {
        self.userCommentLabel.text = learning.userComment;
    }
    else {
        self.commentSeparatorView.hidden = YES;
    }
    
    self.urlLinkLabel.text = [[NSURL URLWithString:learning.siteUrl] host];
    
    if ([self numberOfCommentLines] > 1.1) {
        if (fullInfo) {
            self.showMoreButton.hidden = fullInfo;
        }
        else {
            self.showMoreButton.hidden = fullInfo;
        }
    }
    
    //Need to change frame size of button
    if (learning.userComment.length > 0) {
        [self changeConstraintToSmallSize:NO];
    }
    else {
        [self changeConstraintToSmallSize:YES];
    }
    
    //Current user cannot vote learning
    //In current realization - show alert
    /*
    if (learning.senderIdValue == [DataManager user].userIdValue) {
        self.totalRatingButton.userInteractionEnabled = NO;
    }
    else {
        self.totalRatingButton.userInteractionEnabled = YES;
    }
    */
    
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.senderNameLabel.textColor = [UIColor blackColor];
    self.senderNameLabel.font = [UIFont appFont:17];
    
    self.senderJobLabel.textColor = [UIColor appGrayColor];
    self.senderJobLabel.font = [UIFont appFontThin:13];
    
    self.createDateLabel.textColor = [UIColor appGrayColor];
    self.createDateLabel.font = [UIFont appFontBold:14];
    
    //self.totalRatingLabel.textColor = [UIColor blackColor];
    //self.totalRatingLabel.font = [UIFont appFont:17];
    
    self.siteTitleLabel.textColor = [UIColor blackColor];
    self.siteTitleLabel.font = [UIFont appFontBold:17];
    
    self.userCommentLabel.textColor = [UIColor appGrayColor];
    self.userCommentLabel.font = [UIFont appFontThin:17];
    
    self.urlLinkLabel.textColor = [UIColor appGrayColor];
    self.urlLinkLabel.font = [UIFont appFont:13];
    
    self.commentSeparatorView.backgroundColor = [UIColor appGrayColor];
    self.siteUrlView.backgroundColor = [UIColor colorFromHexString:@"#F8F8F8"];
    
    [self.showMoreButton setTitle:NSLocalizedString(@"show more", @"v1.0") forState:UIControlStateNormal];
    [self.showMoreButton setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    self.showMoreButton.titleLabel.font = [UIFont appFont:14];
    
    self.showMoreButton.hidden = YES;
    self.totalRatingButton.userInteractionEnabled = YES;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.senderNameLabel.text = nil;
    self.senderJobLabel.text = nil;
    
    self.borderColor = nil;
    self.borderWidth = nil;
    
    self.createDateLabel.text = nil;
    self.userCommentLabel.text = nil;
    
    //self.totalRatingLabel.text = nil;
    //self.totalRatingImageView.image = nil;
    
    self.siteTitleLabel.text = nil;
    self.urlLinkLabel.text = nil;
    
    self.commentSeparatorView.hidden = NO;
    self.showMoreButton.hidden = YES;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.siteImageView.clipsToBounds = YES;
    
    self.siteUrlView.layer.borderWidth = 0.5;
    self.siteUrlView.layer.borderColor = [UIColor appGrayColor].CGColor;
    
    self.senderAvatarImageView.layer.borderWidth = self.borderWidth.floatValue;
    self.senderAvatarImageView.layer.borderColor = self.borderColor.CGColor;
}

#pragma mark - Private Methods

- (IBAction)showMoreAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowMore) {
        weakSelf.onShowMore(weakSelf);
    }
    
}

- (IBAction)siteUrlButtonPressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowUrlLink) {
        weakSelf.onShowUrlLink (weakSelf);
    }
    
}

- (IBAction)senderAvatarPressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowSenderInfo) {
        weakSelf.onShowSenderInfo (weakSelf);
    }
    
}

- (IBAction)totalRatingPressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowVote) {
        weakSelf.onShowVote (weakSelf);
    }
    
}


- (UIImage *) starImageForRatingNumber:(NSInteger) rating {
    
    if (rating <= 10) {
        return [UIImage imageNamed:@"icon-rating-star-0"];
    }
    
    if (rating > 10 && rating <= 30) {
        return [UIImage imageNamed:@"icon-rating-star-1"];
    }
    
    if (rating > 30 && rating <= 50 ) {
        return [UIImage imageNamed:@"icon-rating-star-2"];
    }
    
    if (rating > 50 && rating <= 70) {
        return [UIImage imageNamed:@"icon-rating-star-3"];
    }
    
    if (rating > 70 && rating <= 90) {
        return [UIImage imageNamed:@"icon-rating-star-4"];
    }
    
    if (rating > 90) {
        return [UIImage imageNamed:@"icon-rating-star-5"];
    }
    
    return [UIImage imageNamed:@"icon-rating-star-0"];
    
}

- (void) changeConstraintToSmallSize:(BOOL) needChange {
    
    if (needChange) {
        self.showMoreButtonHeight.constant = 0;
        self.showMoreButtonTopSpace.constant = 0;
        self.showMoreButtonBottomSpace.constant = 0;
        self.separatorViewHeight.constant = 0;
        self.separatorViewTopSpace.constant = 0;
        self.separatorViewBottomSpace.constant = 0;
        [self layoutIfNeeded];
    }
    else {
        self.showMoreButtonHeight.constant = 21;
        self.showMoreButtonTopSpace.constant = 10;
        self.showMoreButtonBottomSpace.constant = 5;
        self.separatorViewHeight.constant = 0.5;
        self.separatorViewTopSpace.constant = 15;
        self.separatorViewBottomSpace.constant = 15;
        [self layoutIfNeeded];
    }
    
}

#pragma mark - Properties

-(NSNumber *) borderWidth {
    if (_borderWidth == nil) {
        _borderWidth = [NSNumber numberWithFloat:kDefaultBorderWidth];
    }
    
    return _borderWidth;
}

- (UIColor *) borderColor {
    if (_borderColor == nil) {
        _borderColor = [UIColor appLightGrayColor];
    }
    
    return _borderColor;
}

#pragma mark - Private Methods

+(CGFloat) cellHeightWithFullInfoForLearning:(CDLearning *) learning withSite:(BOOL) site {
    
    //Result
    CGFloat totalHeight = 0;
    
    //Calcualte height
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFontThin:17];
    l.text = learning.userComment;
    l.numberOfLines = 0;
    CGSize size = [l sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    //Line height
    //CGFloat lineHeight = l.font.lineHeight;
    
    //Site height
    CGFloat siteHeight = 5;
    
    if (site) {
        siteHeight = 140;
    }
    
    totalHeight = height + siteHeight + 210;
    
    return totalHeight;
    
}

#pragma mark - Helpers

-(CGFloat) numberOfCommentLines {
    
    CGFloat fullHeightForCommentLabel = [self heightOfText:self.userCommentLabel.text withFont:self.userCommentLabel.font andWidth:SCREEN_WIDTH - 30];
    CGFloat numberOfFullLines = [self numberOfLinesForTextHeight:fullHeightForCommentLabel andFont:self.userCommentLabel.font];
    
    return numberOfFullLines;
}

#pragma mark - Text Size

- (CGFloat) heightOfText:(NSString *) text withFont:(UIFont *) font andWidth:(CGFloat) width {
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = font;
    l.text = text;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    return height;
}

- (CGFloat) numberOfLinesForTextHeight:(CGFloat) height andFont:(UIFont *) font {
    
    return height / font.lineHeight;
    
}

@end
