//
//  LearningListVC.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "ContentViewController.h"

@interface LearningListVC : ContentViewController

+(NSString *) storyboardIdentifier;

@end
