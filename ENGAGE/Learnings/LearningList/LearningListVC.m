//
//  LearningListVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "LearningListVC.h"
#import "ProfileVC.h"
#import "LearningRatingVoteVC.h"
#import "ChallengeInfoVC.h"

#import "ENPlaceholderView.h"
#import "NSString+URLValidation.h"

#import "LoadingCell.h"
#import "LearningLinkCell.h"
#import "LearningCommentCell.h"

#import "FeaturesAccessManager.h"
#import "AnalyticsManager.h"
#import "SFSafariManager.h"

@interface LearningListVC () <NavigationBarSearchDelegate, UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate, SFSafariViewControllerDelegate>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet ContentTableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;


@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property BOOL isSearchState;
@property (nonatomic, strong) PaginationInfo *pagination;
@property (nonatomic, strong) PaginationInfo *searchPagination;
@property (nonatomic, strong) NSMutableArray *fullLearningCommentsIds;

@property (nonatomic, strong) ENPlaceholderView *searchTablePlaceholder;
@property (nonatomic, strong) ENPlaceholderView *emptyFeedTablePlaceholder;

@end

@implementation LearningListVC

#pragma mark - Public Methods

+(NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareNavigationBar];
    [self prepareTableView];
    
    self.pagination = [[PaginationInfo alloc] init];
    [self addDownRefresh];
    [self loadFeedData];
    [self registerForNotifications];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenLearningList];
}

-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (self.fetchedResultsController.fetchedObjects.count == 0 && !self.isSearchState) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.fetchedResultsController.fetchedObjects.count == 0 && !self.isSearchState) {
                self.emptyFeedTablePlaceholder.hidden = NO;
            }
            else {
                self.emptyFeedTablePlaceholder.hidden = YES;
            }
        });
    }
    
    [AnalyticsManager trackScreen:ENAnalyticScreenLearningList withScreenClass:NSStringFromClass([self class])];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:EN_NOTIFICATION_LEARNING_DID_CREATE object:nil];
}

#pragma mark - Prepare Methods

- (void) prepareNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    [self.navigationBar setTitle:[NSLocalizedString(@"learnings", @"v1.0") uppercaseString]];
    
    [self.navigationBar showRightButton];
    
    if ([self isNeedAccessChallenge]) {
        [self.navigationBar showLeftButton];
        [self.navigationBar changeLeftButtonIcon:[UIImage imageNamed:@"icon-challenge"]];
        self.navigationBar.onLeftAction = ^{
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionChallenge label:ENAnalyticLabelNavigationBar];
            [weakSelf showCurrentChallenge];
        };
    };
    
    if ([self isNeedAccessCenterNavigationBar]) {
        self.navigationBar.onCenterAction = ^{
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionChallenge label:ENAnalyticLabelNavigationBar];
            [[[AppDelegate theApp] mainViewController] showLearningListDropMenu];
        };
        [self.navigationBar centerInteractionEnable:YES];
        [self.navigationBar addCenterTitleIcon:[UIImage imageNamed:@"icon-arrowDown"]];
    }
    
    self.navigationBar.searchDelegate = self;
    self.navigationBar.barView.searchBar.placeholder = NSLocalizedString(@"Search by learnings", @"v1.0");
    
}

- (void) prepareTableView {
    [self.tableView registerNib:[UINib nibWithNibName:[LearningLinkCell cellNimbName] bundle:nil]forCellReuseIdentifier:[LearningLinkCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[LearningCommentCell cellNimbName] bundle:nil]forCellReuseIdentifier:[LearningCommentCell cellReuseIdentifier]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

-(void) registerForNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadFeedData) name:EN_NOTIFICATION_LEARNING_DID_CREATE object:nil];
}

#pragma mark - Properties

-(ENPlaceholderView *) searchTablePlaceholder {
    
    if (_searchTablePlaceholder == nil) {
        _searchTablePlaceholder = [ENPlaceholderView createPlaceholderWithText:NSLocalizedString(@"No result found", @"v1.0") andFont:[UIFont appFont:17]];
        _searchTablePlaceholder.hidden = YES;
        [self.tableView addSubview:_searchTablePlaceholder];
    }
    
    return _searchTablePlaceholder;
}

-(ENPlaceholderView *) emptyFeedTablePlaceholder {
 
    if (_emptyFeedTablePlaceholder == nil) {
        _emptyFeedTablePlaceholder = [ENPlaceholderView createCenterPlaceholderWithText:NSLocalizedString(@"No feeds so far, stay tuned", @"v1.0") andFond:[UIFont appFontBold:22]];
        _emptyFeedTablePlaceholder.hidden = YES;
        [self.tableView addSubview:_emptyFeedTablePlaceholder];
    }
    
    return _emptyFeedTablePlaceholder;
}

#pragma mark - Data

- (void)reloadData {
    
    self.fullLearningCommentsIds = nil;
    self.fetchedResultsController = nil;
    
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}

-(void) loadFeedData {
    
    self.pagination.currentPage = 0;
    self.pagination.totalPages = 1;
    
    [self showLoadingViewBellowView:self.navigationBar];
    [self loadFeedDataForPage:self.pagination.currentPage + 1];
}

-(void) loadNextFeedData {
    
    if (self.pagination.isLoading) return;
    
    if (self.pagination.totalPages > self.pagination.currentPage) {
        [self loadFeedDataForPage:self.pagination.currentPage + 1];
    }
}

-(void) loadFeedDataForPage:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    weakSelf.pagination.isLoading = YES;
    
    [[RequestManager sharedManager] getLearningsForPage:page success:^(PaginationInfo *pagination, NSArray *list) {
        weakSelf.pagination.currentPage = pagination.currentPage;
        weakSelf.pagination.totalPages = pagination.totalPages;
        
        if (weakSelf.pagination.currentPage == 1) {
            [DataManager saveLearningsList:list fromLearningList:YES];
        }
        else {
            [DataManager appendLearningList:list fromLearningList:YES];
        }
        
        [weakSelf reloadData];
        
        weakSelf.pagination.isLoading = NO;
        [weakSelf hideLoadingView];
        
    } failure:^(NSString *message) {
        weakSelf.pagination.isLoading = NO;
        [weakSelf hideLoadingView];
        [weakSelf.refreshControl endRefreshing];
        [weakSelf showAlertWithMessage:message];
    }];
}

-(PaginationInfo *) currentPagination {
    if (self.isSearchState) {
        return self.searchPagination;
    } else {
        return self.pagination;
    }
}

- (NSMutableArray *) fullLearningCommentsIds {
    if (_fullLearningCommentsIds == nil) {
        _fullLearningCommentsIds = [NSMutableArray array];
    }
    
    return _fullLearningCommentsIds;
}

#pragma mark - Fetch Result Controller

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [CDLearning MR_requestAllSortedBy:@"createDate" ascending:NO inContext:[DataManager context]];
    
    NSMutableArray *predicates = [NSMutableArray array];
    NSPredicate *typePredicate = [NSPredicate predicateWithFormat:@"isFromLearningList == %@", [NSNumber numberWithBool:YES]];
    [predicates addObject:typePredicate];
    
    if (predicates.count > 0) {
        fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    NSFetchedResultsController *fetchController = [CDLearning MR_fetchController:fetchRequest delegate:nil useFileCache:NO groupedBy:nil inContext:[DataManager context]];
    
    _fetchedResultsController = fetchController;
    
    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error])
    {
        MLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    return _fetchedResultsController;
    
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger number = self.fetchedResultsController.fetchedObjects.count;
    
    if ([self currentPagination].currentPage < [self currentPagination].totalPages) {
        self.emptyFeedTablePlaceholder.hidden = YES;
        self.searchTablePlaceholder.hidden = YES;
        return number + 1;
    }
    
    if (number == 0 && self.navigationBar.barView.searchBar.text.length > 0) {
        self.searchTablePlaceholder.hidden = NO;
    }
    else if (number == 0 && !self.isSearchState) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([self.tableView numberOfRowsInSection:0] == 0 && !self.isSearchState) {
                self.emptyFeedTablePlaceholder.hidden = NO;
            }
            else {
                self.emptyFeedTablePlaceholder.hidden = NO;
            }
        });
    }
    else {
        self.emptyFeedTablePlaceholder.hidden = YES;
        self.searchTablePlaceholder.hidden = YES;
    }
 
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if (learning.siteUrl.length > 0) {
            return [self linkCell:tableView indexPath:indexPath];
        }
        else {
            return [self commentCell:tableView indexPath:indexPath];
        }
        
    } else {
        return [LoadingCell cell];
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if (learning.siteUrl.length > 0) {
            return [self heightForCellWithIdentifier:[LearningLinkCell cellReuseIdentifier] atIndexPath:indexPath];
        }
        else {
            return [self heightForCellWithIdentifier:[LearningCommentCell cellReuseIdentifier] atIndexPath:indexPath];
        }
        
    }
    
    return 80; //loading cell height
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if (learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
    }
    else {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionTapTry label:ENAnalyticLabelFeedTypePaginationSpinner];
        return;
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([cell isLoadingCell]) {
        if (self.isSearchState) {
            [self loadNextSearchData];
        }
        else {
            [self loadNextFeedData];
        }
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.navigationBar.barView.searchBar resignFirstResponder];
}

#pragma mark - Cells

-(LearningLinkCell *) linkCell: (UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    LearningLinkCell *cell = [tableView dequeueReusableCellWithIdentifier:[LearningLinkCell cellReuseIdentifier]];
    
    if ([self.fullLearningCommentsIds containsObject:learning.feedId]) {
        [cell addLearning:learning fullInfo:YES];
    }
    else {
        [cell addLearning:learning fullInfo:NO];
    }
    
    cell.onShowMore = ^(LearningLinkCell *showMoreReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionShowMore label:ENAnalyticLabelFeedTypeLearningLink];
        NSIndexPath *indexPathShowMore = [weakSelf.tableView indexPathForCell:showMoreReturnedCell];
        [weakSelf showFullInfoAtIndexPath:indexPathShowMore];
    };
    
    cell.onShowUrlLink = ^(LearningLinkCell *urlLinkReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionLink label:ENAnalyticLabelFeedTypeLearningLink];
        NSIndexPath *indexPathUrlLink = [weakSelf.tableView indexPathForCell:urlLinkReturnedCell];
        [weakSelf showUrlLinkAtIndexPath:indexPathUrlLink];
    };
    
    cell.onShowSenderInfo = ^(LearningLinkCell *senderInfoReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionSenderProfile label:ENAnalyticLabelFeedTypeLearningLink];
        NSIndexPath *senderInfoIndexPath = [weakSelf.tableView indexPathForCell:senderInfoReturnedCell];
        [weakSelf showSenderInfoDetailAtIndexPath:senderInfoIndexPath];
    };
    
    cell.onShowVote = ^(LearningLinkCell *voteReturnedCell) {
        NSIndexPath *voteIndexPath = [weakSelf.tableView indexPathForCell:voteReturnedCell];
        
        if (learning.senderIdValue == [DataManager user].userIdValue) {
            [weakSelf showAlertWithMessage:NSLocalizedString(@"You can't rate your article", @"v1.0")];
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionRateVoteTry label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [weakSelf showVoteRateAtIndexPath:voteIndexPath];
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionRateVoteConfirm label:ENAnalyticLabelFeedTypeLearningLink];
        }
    };
    
    if ([self canDeleteLearning:learning]) {
        cell.rightUtilityButtons = [self deleteButton];
    }
    
    cell.delegate = self;
    
    return cell;
}

-(LearningCommentCell *) commentCell: (UITableView *) tableView indexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    LearningCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:[LearningCommentCell cellReuseIdentifier]];
    
    if ([self.fullLearningCommentsIds containsObject:learning.feedId]) {
        [cell addLearning:learning fullInfo:YES];
    }
    else {
        [cell addLearning:learning fullInfo:NO];
    }
    
    cell.onShowMore = ^(LearningCommentCell *showMoreReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionShowMore label:ENAnalyticLabelFeedTypeLearningComment];
        NSIndexPath *indexPathShowMore = [weakSelf.tableView indexPathForCell:showMoreReturnedCell];
        [weakSelf showFullInfoAtIndexPath:indexPathShowMore];
    };
    
    cell.onShowVote = ^(LearningCommentCell *voteReturnedCell) {
        NSIndexPath *voteIndexPath = [weakSelf.tableView indexPathForCell:voteReturnedCell];
        
        if (learning.senderIdValue == [DataManager user].userIdValue) {
            [weakSelf showAlertWithMessage:NSLocalizedString(@"You can't rate your article", @"v1.0")];
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionRateVoteTry label:ENAnalyticLabelFeedTypeLearningComment];
        }
        else {
            [weakSelf showVoteRateAtIndexPath:voteIndexPath];
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionRateVoteConfirm label:ENAnalyticLabelFeedTypeLearningComment];
        }
    };
    
    cell.onShowSenderInfo = ^(LearningCommentCell *senderReturnedCell) {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionSenderProfile label:ENAnalyticLabelFeedTypeLearningComment];
        NSIndexPath *senderIndexPath = [weakSelf.tableView indexPathForCell:senderReturnedCell];
        [weakSelf showSenderInfoDetailAtIndexPath:senderIndexPath];
    };
    
    if ([self canDeleteLearning:learning]) {
        cell.rightUtilityButtons = [self deleteButton];
    }
    
    cell.delegate = self;
    
    return cell;
}

#pragma mark - Cells Height

- (CGFloat) heightForCellWithIdentifier:(NSString *) identifier atIndexPath:(NSIndexPath *) indexPath {
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([identifier isEqualToString:[LearningLinkCell cellReuseIdentifier]]) {
        
        CGFloat height = [LearningLinkCell cellHeightForLearning:learning withFullInfo:YES];
        CGFloat collapsedHeight = [LearningLinkCell cellHeightForLearning:learning withFullInfo:NO];
        
        if (height < collapsedHeight) {
            [self.fullLearningCommentsIds addObject:learning.feedId];
        }
        else {
            if (![self.fullLearningCommentsIds containsObject:learning.feedId]) {
                return collapsedHeight;
            }
        }
        return height;
    }
    
    if ([identifier isEqualToString:[LearningCommentCell cellReuseIdentifier]]) {
        
        CGFloat height = [LearningCommentCell cellHeightForLearning:learning fullInfo:YES];
        CGFloat collapsedHeight = [LearningCommentCell cellHeightForLearning:learning fullInfo:NO];
        
        if (height < collapsedHeight) {
            [self.fullLearningCommentsIds addObject:learning.feedId];
        }
        else {
            if (![self.fullLearningCommentsIds containsObject:learning.feedId]) {
                return collapsedHeight;
            }
        }
        
        return height;
    }
    
    else {
        return 80; //default height
    }
    
}

#pragma mark - SWTableViewCellDelegate

- (NSArray *) deleteButton {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor clearColor] icon:[UIImage imageNamed:@"icon-delete2"]];
    
    return rightUtilityButtons;
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (learning.siteUrl.length > 0) {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteSwipeConfirm label:ENAnalyticLabelFeedTypeLearningLink];
    }
    else {
        [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteSwipeConfirm label:ENAnalyticLabelFeedTypeLearningComment];
    }
    
    [self showDeleteAlertForIndexPath:indexPath];
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if ([self canDeleteLearning:learning]) {
        
        if (learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteSwipe label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteSwipe label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
        return YES;
    }
    
    return NO;
    
}

#pragma mark - Action

- (void) showFullInfoAtIndexPath:(NSIndexPath *) indexPath {
    
    if (indexPath.row < self.fetchedResultsController.fetchedObjects.count) {
        CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
        
        if (![self.fullLearningCommentsIds containsObject:learning.feedId]) {
            [self.fullLearningCommentsIds addObject:learning.feedId];
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (void) showUrlLinkAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    if (learning.siteUrl > 0 && [learning.siteUrl isValidURL] ) {
        [SFSafariManager openURLWithSafariController:learning.siteUrl forController:weakSelf];
    }
    
}

- (void) showSenderInfoDetailAtIndexPath:(NSIndexPath *) indexPath {
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    ProfileVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ProfileVC"];
    vc.userId = learning.senderId;
    vc.profileTitle = learning.senderFullName;
    
    [self showViewController:vc sender:nil];
    
}

- (void) deleteFeedAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    [[RequestManager sharedManager] deleteLearningWithId:learning.feedId success:^(BOOL success) {
        [DataManager deleteFeedWithId:learning.feedId forType:ActivityFeedTypeLearning];
        weakSelf.fetchedResultsController = nil;
        [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    } failure:^(NSString *message) {
        [weakSelf showAlertWithMessage:message];
    }];
}

- (void) showVoteRateAtIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    LearningRatingVoteVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[LearningRatingVoteVC storyboardIdentifier]];
    vc.learning = learning;
    vc.isFromLearningList = YES;
    
    vc.onBackAction = ^() {
        weakSelf.fetchedResultsController = nil;
        if ([weakSelf.tableView numberOfRowsInSection:0] > indexPath.row && indexPath != nil) {
            [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    };
    
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}

- (void) showCurrentChallenge {
    ChallengeInfoVC *vc = [[UIStoryboard activityFeedStoryboard] instantiateViewControllerWithIdentifier:[ChallengeInfoVC storyboardIdentifier]];
    vc.screenState = ScreenStateFromNavigationBar;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Alerts

- (void) showDeleteAlertForIndexPath:(NSIndexPath *) indexPath {
    
    CDLearning *learning = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Do you want to delete learning?", @"v1.0") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", @"v1.0") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if (learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteAlertCancel label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteAlertCancel label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
        [self hideButtonsForIndexPath:indexPath];
    }];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", @"v1.0") style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
         if (learning.siteUrl.length > 0) {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteAlertConfirm label:ENAnalyticLabelFeedTypeLearningLink];
        }
        else {
            [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionDeleteAlertConfirm label:ENAnalyticLabelFeedTypeLearningComment];
        }
        
        [self deleteFeedAtIndexPath:indexPath];
    }];
    
    [alertController addAction:noAction];
    [alertController addAction:yesAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Update UI

- (void) hideButtonsForIndexPath: (NSIndexPath *) indexPath {
    
    SWTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    [cell hideUtilityButtonsAnimated:YES];
    
}

#pragma mark - Refresh

-(void) addDownRefresh {
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor appTextColor];
    [self.refreshControl addTarget:self action:@selector(pullDownRefresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

-(void) removeDownRefresh {
    [self.refreshControl removeFromSuperview];
    self.refreshControl = nil;
}

- (void)pullDownRefresh
{
    [self loadFeedData];
}

#pragma mark - NavigationBarSearchDelegate

- (void)navigationBarDidCancelSearch:(NavigationBar *)barView {
    [self stopSearch];
}

- (void)navigationBarDidStartSearch:(NavigationBar *)barView {
    [AnalyticsManager trackEvent:ENAnalyticScreenLearningList action:ENAnalyticActionSearch label:ENAnalyticLabelNavigationBar];
    [self startSearch];
}

- (void)searchView:(UISearchBar *)view textDidChange:(NSString *)searchText {
    [self searchText:searchText page:self.searchPagination.currentPage];
}

#pragma mark - Search

-(void) startSearch {
    self.searchPagination = [[PaginationInfo alloc] init];
    self.searchPagination.currentPage = 1;
    self.searchPagination.totalPages = 1;
    
    self.isSearchState = YES;
    
    [self removeDownRefresh];
}

-(void) stopSearch {
    self.searchTablePlaceholder.hidden = YES;
    self.navigationBar.barView.searchBar.text = nil;
    self.searchPagination = nil;
    
    self.isSearchState = NO;
    
    
    [self addDownRefresh];
    [self loadFeedData];
}

-(void) searchText:(NSString *) text page:(NSInteger) page {
    
    __weak typeof(self) weakSelf = self;
    
    weakSelf.searchPagination.isLoading = YES;
    
    [[RequestManager sharedManager] searchInLearningFeed:text page:page onCompleate:^(BOOL success, NSArray *list, PaginationInfo *pagination) {
        if (success) {
            weakSelf.searchPagination.currentPage = pagination.currentPage;
            weakSelf.searchPagination.totalPages = pagination.totalPages;
            
            if (weakSelf.searchPagination.currentPage == 1) {
                [DataManager saveLearningsList:list fromLearningList:YES];
            }
            else {
                [DataManager appendLearningList:list fromLearningList:YES];
            }
            
        }
        
        weakSelf.searchPagination.isLoading = NO;
        
        [weakSelf reloadData];
        
    }];
    
}

-(void) loadNextSearchData {
    
    if (self.searchPagination.isLoading) return;
    
    NSString *text = self.navigationBar.barView.searchBar.text;
    NSInteger page = self.searchPagination.currentPage + 1;
    
    [self searchText:text page:page];
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) text {
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}

- (BOOL) canDeleteLearning:(CDLearning *) learning {
    BOOL canDelete = NO;
    
    //Admin and manager can delete any comments
    if ([DataManager user].isAdmin || [DataManager user].isManager) {
        canDelete = YES;
    }
    
    //User can delete his comments
    if ([DataManager user].userIdValue == learning.senderIdValue) {
        canDelete = YES;
    }
    
    return canDelete;
}

- (BOOL) isNeedAccessCenterNavigationBar {
    
    if ([FeaturesAccessManager isAccessDropMenuStyle:DropMenuStyleTop forScreen:AppScreenTypeLearningList withUser:[DataManager user]]) {
        return YES;
    }
    
    return NO;
}

- (BOOL) isNeedAccessChallenge {
    
    if ([FeaturesAccessManager isAccessFeature:AppFeatureTypeChallenge forScreen:AppScreenTypeActivityFeed withUser:[DataManager user]]) {
        return YES;
    }
    
    return NO;
}

#pragma mark - SFSafariViewControllerDelegate

- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
