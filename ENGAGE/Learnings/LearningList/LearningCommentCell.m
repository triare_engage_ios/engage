//
//  LearningCommentCell.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "LearningCommentCell.h"
#import "ENCircleImageView.h"

#define kDefaultBorderWidth 1.0
#define kDefaultCellHeight 182
#define kDefaultCellIdentifier @"learningCommentCell"

@interface LearningCommentCell ()

@property (weak, nonatomic) IBOutlet ENCircleImageView *senderImageView;
@property (weak, nonatomic) IBOutlet UIButton *senderAvatarButton;
@property (weak, nonatomic) IBOutlet UILabel *senderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderJobLabel;
@property (nonatomic, strong) NSNumber *borderWidth;
@property (nonatomic, strong) UIColor *borderColor;

@property (weak, nonatomic) IBOutlet UILabel *createDateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *createDateWidth;
//@property (weak, nonatomic) IBOutlet UILabel *totalRatingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *totalRatingImageView;
@property (weak, nonatomic) IBOutlet UIButton *totalRatingButton;

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIButton *showMoreButton;

@end

@implementation LearningCommentCell

#pragma mark - Public Methods

+(NSString *) cellNimbName {
    return NSStringFromClass([self class]);
}

+(NSString *) cellReuseIdentifier {
    return kDefaultCellIdentifier;
}

+(CGFloat) cellHeightForLearning:(CDLearning *) learning fullInfo:(BOOL) fullInfo {
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = [UIFont appFontThin:17];
    l.text = learning.userComment;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(SCREEN_WIDTH - 30, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    height += 20; //Top space for content view
    height += 50; //Sender info view height
    height += 5; //Sender comment top space
    height += 30; //Show more button name
    height += 41; //More space
    
    if (fullInfo) {
        return height;
    }
    
    return kDefaultCellHeight;
}

-(void) addLearning:(CDLearning *) learning fullInfo:(BOOL) fullInfo {
    
    [self.senderImageView sd_setImageWithURL:[AppTemplate mediumIconURL:learning.senderAvatarUrl] placeholderImage:[TeamUser placeholder]];
    self.senderNameLabel.text = learning.senderFullName;
    self.senderJobLabel.text = learning.senderJobTitle;
    
    if (learning.isSenderTopPointWinnerValue) {
        self.borderWidth = [NSNumber numberWithFloat:3.0];
        self.borderColor = [UIColor avatarBorderColor];
    }
    
    self.createDateLabel.text = [learning formattedCreateDate];
    
    CGSize dateSize = [self.createDateLabel sizeThatFits:CGSizeZero];
    self.createDateWidth.constant = dateSize.width;
    
    //self.totalRatingLabel.text = [NSString stringWithFormat:@"%ld%@", (long)learning.rating.integerValue, @"%"];
    
    self.totalRatingImageView.image = [self starImageForRatingNumber:learning.rating.integerValue];
    
    self.commentLabel.text = learning.userComment;
    
    if ([self numberOfCommentLines] > 1) {
        if (fullInfo) {
            self.showMoreButton.hidden = fullInfo;
        }
        else {
            self.showMoreButton.hidden = fullInfo;
        }
    }
    
    //Current user cannot vote learning
    //In current realization - show alert
    /*
    if (learning.senderIdValue == [DataManager user].userIdValue) {
        self.totalRatingButton.userInteractionEnabled = NO;
    }
    else {
        self.totalRatingButton.userInteractionEnabled = YES;
    }
     */
}

#pragma mark - Life Cycle

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.senderNameLabel.textColor = [UIColor blackColor];
    self.senderNameLabel.font = [UIFont appFont:17];
    
    self.senderJobLabel.textColor = [UIColor appGrayColor];
    self.senderJobLabel.font = [UIFont appFontThin:13];
    
    self.createDateLabel.textColor = [UIColor appGrayColor];
    self.createDateLabel.font = [UIFont appFontBold:14];
    
    self.commentLabel.textColor = [UIColor appGrayColor];
    self.commentLabel.font = [UIFont appFontThin:17];
    
//    self.totalRatingLabel.textColor = [UIColor blackColor];
//    self.totalRatingLabel.font = [UIFont appFont:17];
    
    [self.showMoreButton setTitle:NSLocalizedString(@"show more", @"v1.0") forState:UIControlStateNormal];
    [self.showMoreButton setTitleColor:[UIColor appOrangeColor] forState:UIControlStateNormal];
    self.showMoreButton.titleLabel.font = [UIFont appFont:14];
    
    self.showMoreButton.hidden = YES;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    
    self.senderNameLabel.text = nil;
    self.senderJobLabel.text = nil;
    
    self.borderColor = nil;
    self.borderWidth = nil;
    
    self.createDateLabel.text = nil;
    self.commentLabel.text = nil;
    
//    self.totalRatingLabel.text = nil;
//    self.totalRatingImageView.image = nil;
    
    self.showMoreButton.hidden = YES;
    
    self.onShowMore = nil;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.senderImageView.layer.borderWidth = self.borderWidth.floatValue;
    self.senderImageView.layer.borderColor = self.borderColor.CGColor;
}

#pragma mark - Private Methods

- (IBAction)showMoreButtonAction:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowMore) {
        weakSelf.onShowMore(weakSelf);
    }
    
}

- (IBAction)senderAvatarPressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowSenderInfo) {
        weakSelf.onShowSenderInfo (weakSelf);
    }
    
}

- (IBAction)totalRatingPressed:(id)sender {
    
    __weak typeof(self) weakSelf = self;
    
    if (weakSelf.onShowVote) {
        weakSelf.onShowVote (weakSelf);
    }
    
}

- (UIImage *) starImageForRatingNumber:(NSInteger) rating {
    
    if (rating <= 10) {
        return [UIImage imageNamed:@"icon-rating-star-0"];
    }
    
    if (rating > 10 && rating <= 30) {
        return [UIImage imageNamed:@"icon-rating-star-1"];
    }
    
    if (rating > 30 && rating <= 50 ) {
        return [UIImage imageNamed:@"icon-rating-star-2"];
    }
    
    if (rating > 50 && rating <= 70) {
        return [UIImage imageNamed:@"icon-rating-star-3"];
    }
    
    if (rating > 70 && rating <= 90) {
        return [UIImage imageNamed:@"icon-rating-star-4"];
    }
    
    if (rating > 90) {
        return [UIImage imageNamed:@"icon-rating-star-5"];
    }
    
    return [UIImage imageNamed:@"icon-rating-star-0"];
    
    
}

#pragma mark - Properties

-(NSNumber *) borderWidth {
    if (_borderWidth == nil) {
        _borderWidth = [NSNumber numberWithFloat:kDefaultBorderWidth];
    }
    
    return _borderWidth;
}

- (UIColor *) borderColor {
    if (_borderColor == nil) {
        _borderColor = [UIColor appLightGrayColor];
    }
    
    return _borderColor;
}

#pragma mark - Helpers

-(CGFloat) numberOfCommentLines {
    
    CGFloat fullHeightForCommentLabel = [self heightOfText:self.commentLabel.text withFont:self.commentLabel.font andWidth:SCREEN_WIDTH - 30];
    CGFloat numberOfFullLines = [self numberOfLinesForTextHeight:fullHeightForCommentLabel andFont:self.commentLabel.font];
    
    return numberOfFullLines;
}

#pragma mark - Text Size

- (CGFloat) heightOfText:(NSString *) text withFont:(UIFont *) font andWidth:(CGFloat) width {
    
    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    l.font = font;
    l.text = text;
    l.numberOfLines = 0;
    
    CGSize size = [l sizeThatFits:CGSizeMake(width, CGFLOAT_MAX)];
    CGFloat height = ceilf(size.height);
    
    return height;
}

- (CGFloat) numberOfLinesForTextHeight:(CGFloat) height andFont:(UIFont *) font {
    
    return height / font.lineHeight;

}

@end
