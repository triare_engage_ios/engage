//
//  LearningLinkCell.h
//  ENGAGE
//
//  Created by Maksym Savisko on 12/12/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell/SWTableViewCell.h>

@interface LearningLinkCell : SWTableViewCell

@property (nonatomic, copy) void (^onShowMore)(LearningLinkCell *cell);
@property (nonatomic, copy) void (^onShowUrlLink)(LearningLinkCell *cell);
@property (nonatomic, copy) void (^onShowSenderInfo)(LearningLinkCell *cell);
@property (nonatomic, copy) void (^onShowVote)(LearningLinkCell *cell);

+(CGFloat) cellHeightForLearning:(CDLearning *) learning withFullInfo:(BOOL) fullInfo;
+(NSString *) cellNimbName;
+(NSString *) cellReuseIdentifier;

-(void) addLearning:(CDLearning *) learning  fullInfo:(BOOL) fullInfo;


@end
