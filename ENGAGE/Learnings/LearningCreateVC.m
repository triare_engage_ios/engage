//
//  LearningCreateVC.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "LearningCreateVC.h"
#import "ENActionButtonCell.h"
#import "ENTextViewCell.h"

#import "Learning.h"

#import "RequestManager.h"
#import <SVProgressHUD/SVProgressHUD.h>

typedef enum : NSUInteger {
    CellItemTypeLink,
    CellItemTypeDescription,
    CellItemTypeAction
} CellItemType;

@interface LearningCreateVC () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet NavigationBar *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;

@property (nonatomic, strong) NSArray *itemsCell;
@property (nonatomic, strong) Learning *learningForSend;

@end

@implementation LearningCreateVC

#pragma mark - Public Methods

+(NSString *) storyboardIdentifier {
    return NSStringFromClass([self class]);
}

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prepareNavigationBar];
    [self prepareTabelView];
    self.registerKeyboardNotifications = YES;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AnalyticsManager trackScreen:ENAnalyticScreenLearningCreate];
    [AnalyticsManager trackScreen:ENAnalyticScreenLearningCreate withScreenClass:NSStringFromClass([self class])];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Prepare Methods

- (void) prepareNavigationBar {
    
    __weak typeof(self) weakSelf = self;
    
    self.navigationBar.onLeftAction = ^{
        [weakSelf closeVC];
    };
    
    [self.navigationBar showLeftButton];
    
    [self.navigationBar setTitle:[NSLocalizedString(@"new learning", @"v1.0") uppercaseString]];
}

- (void) prepareTabelView {
    
    [self.tableView registerNib:[UINib nibWithNibName:[ENTextViewCell cellNimbName] bundle:nil] forCellReuseIdentifier:[ENTextViewCell cellReuseIdentifier]];
    [self.tableView registerNib:[UINib nibWithNibName:[ENActionButtonCell cellNimbName] bundle:nil] forCellReuseIdentifier:[ENActionButtonCell cellReuseIdentifier]];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    UIEdgeInsets insets = self.tableView.contentInset;
    insets.top = EN_NAVIGATION_BAR_HEIGHT;
    insets.bottom = 0;
    self.tableView.contentInset = insets;
}

#pragma mark - Data

-(NSArray *) itemsCell {
    if (_itemsCell == nil) {
        NSArray *result = @[@(CellItemTypeLink), @(CellItemTypeDescription), @(CellItemTypeAction)];
        _itemsCell = result;
    }
    
    return _itemsCell;
}

-(Learning *) learningForSend {
    if (_learningForSend == nil) {
        Learning *learning = [[Learning alloc] init];
        _learningForSend = learning;
    }
    
    return _learningForSend;
}

- (void) reloadData {
    self.itemsCell = nil;
    [self.tableView reloadData];
}

#pragma mark - Table View Delegate & Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsCell.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *itemNumber =self.itemsCell[indexPath.row];
    CellItemType item = itemNumber.integerValue;
    
    if (item == CellItemTypeLink) {
        return [ENTextViewCell cellHeight];
    }
    else if (item == CellItemTypeDescription) {
        return 150;
    }
    else {
        return 80;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *itemNumber =self.itemsCell[indexPath.row];
    CellItemType item = itemNumber.integerValue;
    
    if (item == CellItemTypeLink || item == CellItemTypeDescription) {
        return [self textViewCellWithType:item forTableView:tableView atIndexPath:indexPath];
    }
    else {
        return [self actionButtonCellForTableView:tableView atIndexPath:indexPath];
    }
    
    return nil;
}

#pragma mark - Cells

- (ENTextViewCell *) textViewCellWithType:(CellItemType) type forTableView:(UITableView *) tableView atIndexPath:(NSIndexPath *) indexPath {
    __weak typeof(self) weakSelf = self;
    
    ENTextViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[ENTextViewCell cellReuseIdentifier]];
    
    cell.onChangeText = ^(NSString *text, ENTextViewCell *changeTextCell) {
        NSIndexPath *changeTextIndexPath = [weakSelf.tableView indexPathForCell:changeTextCell];
        [weakSelf changeTextViewText:text forIndexPath:changeTextIndexPath];
    };
    
    cell.onDidBeginChangingText = ^(ENTextViewCell *didBeginChangingCell) {
        NSIndexPath *didBeginChangingIndexPath = [weakSelf.tableView indexPathForCell:didBeginChangingCell];
        [weakSelf scrollToCellAtIndexPath:didBeginChangingIndexPath];
    };
    
    [cell setIsNeedValidateFirstSpace:YES];
    
    if (type == CellItemTypeLink) {
        [cell setTextViewPlaceholderText:NSLocalizedString(@"Paste link here or live it blank", @"v1.0")];
        [self inputTraitsForTextViewCell:cell forItemType:type];
    }
    else if (type == CellItemTypeDescription) {
        [cell setTextViewPlaceholderText:NSLocalizedString(@"Add your thoughts", @"v1.0")];
        [self inputTraitsForTextViewCell:cell forItemType:type];
    }
    
    return cell;
}

- (ENActionButtonCell *) actionButtonCellForTableView:(UITableView *) tableView atIndexPath:(NSIndexPath *) indexPath {
    
    __weak typeof(self) weakSelf = self;
    
    ENActionButtonCell *cell = [tableView dequeueReusableCellWithIdentifier:[ENActionButtonCell cellReuseIdentifier]];
    
    cell.onAction = ^(ENActionButtonCell *actionReturnedCell) {
        [weakSelf sendLearning];
    };
    
    [cell setActionButtonTitle:[NSLocalizedString(@"send", @"v1.0") uppercaseString]];
 
    return cell;
}

- (void) inputTraitsForTextViewCell:(ENTextViewCell *) cell forItemType:(CellItemType) type {
    switch (type) {
        case CellItemTypeLink:
            cell.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
            cell.textView.dataDetectorTypes = UIDataDetectorTypeLink;
            cell.textView.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.textView.spellCheckingType = UITextSpellCheckingTypeNo;
            cell.textView.keyboardType = UIKeyboardTypeURL;
            break;
            
        case CellItemTypeDescription:
            cell.textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
            cell.textView.autocorrectionType = UITextAutocorrectionTypeYes;
            cell.textView.spellCheckingType = UITextSpellCheckingTypeYes;
            break;
        
        case CellItemTypeAction:
            break;
    }
}


#pragma mark - Action

- (void) closeVC {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) changeTextViewText:(NSString *) text forIndexPath:(NSIndexPath *) indexPath {
    
    NSNumber *itemNumber = self.itemsCell[indexPath.row];
    CellItemType item = itemNumber.integerValue;
    
    if (item == CellItemTypeLink) {
        self.learningForSend.linkURL = text;
    }
    else if (item == CellItemTypeDescription) {
        self.learningForSend.thoughts = text;
    }
}

- (void) scrollToCellAtIndexPath:(NSIndexPath *) indexPath {
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
    
    NSInteger maxIndex = [self.tableView numberOfRowsInSection:indexPath.section];
    
    if (nextIndexPath.row > maxIndex) {
        return;
    }
    
    [self.tableView scrollToRowAtIndexPath:nextIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void) sendLearning {
    
    //Validation
    if (![self.learningForSend isValid]) {
        switch ([self.learningForSend validationInfo]) {
            case LearningValidationErrorEmptyFields:
                [self showAlertWithMessage:NSLocalizedString(@"Add text or link", @"v1.0")];
                break;
                
            case LearningValidationErrorNotValidLink:
                [self showAlertWithMessage:NSLocalizedString(@"Link is not correct", @"v1.0")];
                break;
                
            case LearningValidationErrorNone:
                break;
        }
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    
    [self actionButtonIteractionEnable:NO];
    [self showLoadingViewBellowView:self.navigationBar];
    [[RequestManager sharedManager] createLearningWithInfo:[self.learningForSend serialize] success:^(NSDictionary *result) {
        [AppDelegate theApp].mainViewController.isAddedNewTap = YES;
        [weakSelf.view endEditing:YES];
        [weakSelf hideLoadingView];
        [weakSelf showAlertLearningDidShow];
        [[NSNotificationCenter defaultCenter] postNotificationName:EN_NOTIFICATION_LEARNING_DID_CREATE object:nil];
        [weakSelf closeVC];
    } failure:^(NSString *message) {
        [weakSelf.view endEditing:YES];
        [weakSelf actionButtonIteractionEnable:YES];
        [weakSelf hideLoadingView];
        [weakSelf showAlertWithMessage:message];
    }];
}

- (void) actionButtonIteractionEnable:(BOOL) enable {
    for (NSNumber *index in self.itemsCell) {
        if (index.integerValue == CellItemTypeAction) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index.integerValue inSection:0];
            ENActionButtonCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            
            if (cell != nil) {
                cell.actionButton.userInteractionEnabled = enable;
            }
            
            return;
        }
    }
}

#pragma mark - Keyboards Methods

#define kDeltaKeyboardMovement 0

- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(EN_NAVIGATION_BAR_HEIGHT, 0.0, kbRect.size.height + kDeltaKeyboardMovement, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets.top = EN_NAVIGATION_BAR_HEIGHT;
    contentInsets.bottom = 0;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Helpers Methods

- (void) showAlertWithMessage:(NSString *) text {
    if (text.length == 0) {
        return;
    }
    
    [[AppDelegate theApp] showAlert:text text:nil];
}

-(void) showAlertLearningDidShow {
    [SVProgressHUD setMinimumDismissTimeInterval:1];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"Learning has been sent", @"v1.0")];
}


@end
