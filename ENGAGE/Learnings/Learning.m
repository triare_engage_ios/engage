//
//  Learning.m
//  ENGAGE
//
//  Created by Maksym Savisko on 12/9/16.
//  Copyright © 2016 Maksym Savisko. All rights reserved.
//

#import "Learning.h"
#import "NSDictionary+SecureValue.h"
#import "NSString+URLValidation.h"

@implementation Learning

#pragma mark - Public Methods

- (NSDictionary *) serialize {
    NSMutableDictionary *learning = [NSMutableDictionary new];
    
    if (self.linkURL.length > 0) {
        [learning setValue:self.linkURL forKey:@"site_url"];
    }
    
    if (self.thoughts.length > 0) {
        [learning setValue:self.thoughts forKey:@"comment"];
    }
    
    //Link and comment exist
    if (self.linkURL.length > 0 && self.thoughts.length > 0) {
        [learning setValue:@"real_life" forKey:@"ltype"];
    }
    //Only link exist
    else if ((self.linkURL.length > 0 && self.thoughts.length == 0) || (self.linkURL.length > 0 && self.thoughts == nil)) {
        [learning setValue:@"reading" forKey:@"ltype"];
    }
    //Only comment without link
    else if ((self.thoughts.length > 0 && self.linkURL.length == 0) || (self.thoughts.length > 0 && self.linkURL == nil)) {
        [learning setValue:@"real_life" forKey:@"ltype"];
    }
    
    return learning;
}

- (BOOL) isValid {
    
    if (self.linkURL.length > 0 && ![self.linkURL isValidURL]) {
        return NO;
    }
    
    if (self.linkURL.length > 0 && [self.linkURL isValidURL]) {
        return YES;
    }
    
    if (self.thoughts.length > 0) {
        return YES;
    }
    
    return NO;
}

- (LearningValidationError) validationInfo {
    
    if (self.linkURL.length == 0 && self.thoughts.length == 0) {
        return LearningValidationErrorEmptyFields;
    }
    
    if (![self.linkURL isValidURL]) {
        return LearningValidationErrorNotValidLink;
    }
    
    return LearningValidationErrorNone;
}

@end
